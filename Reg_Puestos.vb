﻿Public Class Reg_Puestos
    Dim val As New Funciones
    Dim conexion As New EnlaceBD
    Dim tabla As New DataTable

    Dim obj As New EnlaceBD
    Dim rfc As String
    Dim modificar As Boolean = False
    Dim result As Boolean = False
    Dim i As Integer
    Dim id As Integer

    Private Sub NAME_PUESTO_KeyPress(sender As Object, e As KeyPressEventArgs) Handles NAME_PUESTO.KeyPress
        'Codigo letras y espacios
        val.EvaluaSoloLetras(e)
    End Sub

    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles NIVEL_SALARIAL.KeyPress
        'Codigo con numeros decimales
        Dim Cadena As String = NIVEL_SALARIAL.Text
        Dim v As Boolean
        Dim Filtro As String = "1234567890"
        If Len(Cadena) > 0 Then
            Filtro += "."
        End If

        For Each caracter In Filtro
            If e.KeyChar = caracter Then
                e.Handled = False
                v = True
                Exit For
            Else
                e.Handled = True
                v = False
            End If
        Next

        If Char.IsControl(e.KeyChar) Then
            e.Handled = False
            v = True
        End If

        If e.KeyChar = "." And Not Cadena.IndexOf(".") Then
            e.Handled = True
            v = False
        End If

        If v = False Then
            MessageBox.Show("Ingrese solo  Numeros con un numero decimal" & vbNewLine & " No Ingrese espacios", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub Reg_Puestos_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        tabla = conexion.Get_Empresas()
        Me.EMPRESA_PUESTO.DataSource = tabla
        Me.EMPRESA_PUESTO.DisplayMember = "Nombre"
        Me.EMPRESA_PUESTO.ValueMember = "RFC"


        tabla = obj.Get_Puesto()
        DG_PUESTOS.DataSource = tabla

    End Sub


    Private Sub EMPRESA_PUESTO_SelectedIndexChanged(sender As Object, e As EventArgs) Handles EMPRESA_PUESTO.SelectedIndexChanged

        rfc = EMPRESA_PUESTO.Text
        tabla = conexion.Get_Empresas()         'obtengo el rfc de la empresa 
        i = tabla.Rows.Count
        For fila As Integer = 0 To i - 1 Step 1
            If tabla.Rows(fila).Item(1) = rfc Then
                rfc = tabla.Rows(fila).Item(0)
            End If
        Next

        tabla = conexion.Get_NameDepartamento(rfc)
        i = tabla.Rows.Count
        If i = 0 Then
            Me.DEPA_PUESTO.DataSource = tabla
            Me.DEPA_PUESTO.DisplayMember = "Nombre"
            Me.DEPA_PUESTO.ValueMember = "No_Departamento"
        Else
            For fila As Integer = 0 To i - 1 Step 1
                If tabla.Rows(fila).Item(2) = rfc Then
                    Me.DEPA_PUESTO.DataSource = tabla
                    Me.DEPA_PUESTO.DisplayMember = "Nombre"
                    Me.DEPA_PUESTO.ValueMember = "No_Departamento"
                End If
            Next
        End If

    End Sub

    Private Sub ACTUALIZAR_PUESTO_Click(sender As Object, e As EventArgs) Handles ACTUALIZAR_PUESTO.Click
        Dim name As String
        Dim nivel_S As Decimal
        Dim depa As Integer
        Dim opc As String
        Dim v As Boolean = False



        If Len(NAME_PUESTO.Text) = 0 Or Len(NIVEL_SALARIAL.Text) = 0 Then
            MessageBox.Show("Ingrese todos los campos", "Puestos", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        Else
            name = NAME_PUESTO.Text
            nivel_S = NIVEL_SALARIAL.Text

            tabla = conexion.Get_Empresas()         'obtengo el rfc de la empresa 
            i = tabla.Rows.Count
            For fila As Integer = 0 To i - 1 Step 1
                If tabla.Rows(fila).Item(1) = rfc Then
                    rfc = tabla.Rows(fila).Item(0)
                End If
            Next

            tabla = conexion.Get_Departamento()         'obtengo el num de departamento 
            i = tabla.Rows.Count
            For fila As Integer = 0 To i - 1 Step 1
                If tabla.Rows(fila).Item(1) = DEPA_PUESTO.Text And tabla.Rows(fila).Item(3) = rfc Then
                    depa = tabla.Rows(fila).Item(0)
                End If
            Next

            If modificar Then
                opc = "M"

                result = obj.Edit_Puesto(opc, id, nivel_S)

                If result Then
                    MessageBox.Show("El Puesto se modifico exitosamente", "Puestos", MessageBoxButtons.OK, MessageBoxIcon.None)
                    NAME_PUESTO.Clear()
                    NIVEL_SALARIAL.Clear()
                    NAME_PUESTO.Enabled = True
                    DEPA_PUESTO.Enabled = True
                    EMPRESA_PUESTO.Enabled = True
                    modificar = False
                End If


            Else
                opc = "I"
                tabla = conexion.Get_NamePuesto(depa, rfc)  'saber si ya existe el puesto en la empresa
                i = tabla.Rows.Count
                For fila As Integer = 0 To i - 1 Step 1
                    If tabla.Rows(fila).Item(1) = name Then
                        v = True
                    End If
                Next                                        'saber si ya existe el puesto en la empresa

                If v Then
                    MessageBox.Show("El Puesto ya existe en el Departamento", "Puestos", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Else

                    result = obj.Add_Puesto(opc, name, nivel_S, rfc, depa)

                    If result Then
                        MessageBox.Show("El Puesto se añadio exitosamente", "Puestos", MessageBoxButtons.OK, MessageBoxIcon.None)
                        NAME_PUESTO.Clear()
                        NIVEL_SALARIAL.Clear()
                    Else
                        MessageBox.Show("No fue posible añadir su Puesto" & vbCrLf & "Verifique que su Puesto no exista en el Departamento", "Puestos", MessageBoxButtons.OK, MessageBoxIcon.None)
                    End If

                End If

            End If

            tabla = obj.Get_Puesto()
            DG_PUESTOS.DataSource = tabla

        End If

    End Sub

    Private Sub DG_PUESTOS_Click(sender As Object, e As EventArgs) Handles DG_PUESTOS.Click


        If (Len(Trim(DG_PUESTOS.CurrentRow.Cells.Item(0).Value.ToString())) <> 0) Then

            modificar = True
            NAME_PUESTO.Enabled = False
            DEPA_PUESTO.Enabled = False
            EMPRESA_PUESTO.Enabled = False

            id = DG_PUESTOS.CurrentRow.Cells.Item(0).Value.ToString()
            NAME_PUESTO.Text = DG_PUESTOS.CurrentRow.Cells.Item(1).Value.ToString()
            NIVEL_SALARIAL.Text = DG_PUESTOS.CurrentRow.Cells.Item(2).Value.ToString()
            DEPA_PUESTO.Text = DG_PUESTOS.CurrentRow.Cells.Item(4).Value.ToString()
            EMPRESA_PUESTO.Text = DG_PUESTOS.CurrentRow.Cells.Item(6).Value.ToString()

        Else

            NAME_PUESTO.Clear()
            NIVEL_SALARIAL.Clear()
            NAME_PUESTO.Enabled = True
            DEPA_PUESTO.Enabled = True
            EMPRESA_PUESTO.Enabled = True

        End If


    End Sub

    Private Sub DELETE_PUESTO_Click(sender As Object, e As EventArgs) Handles DELETE_PUESTO.Click
        If (modificar = True) Then


            If MessageBox.Show("Seguro que Desea Eliminar", "Puestos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then


                result = obj.Delete_Puesto(DG_PUESTOS.CurrentRow.Cells.Item(0).Value.ToString())

                If result Then
                    MessageBox.Show("El Puesto ha sido eliminado exitosamente", "Puestos", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    NAME_PUESTO.Clear()
                    NIVEL_SALARIAL.Clear()
                    NAME_PUESTO.Enabled = True
                    DEPA_PUESTO.Enabled = True
                    EMPRESA_PUESTO.Enabled = True

                    tabla = obj.Get_Puesto()
                    DG_PUESTOS.DataSource = tabla

                    modificar = False
                Else
                    MessageBox.Show("Hubo un error, su Puesto no se elimino", "Puestos", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                End If

            Else
                NAME_PUESTO.Clear()
                NIVEL_SALARIAL.Clear()
                NAME_PUESTO.Enabled = True
                DEPA_PUESTO.Enabled = True
                EMPRESA_PUESTO.Enabled = True

                tabla = obj.Get_Puesto()
                DG_PUESTOS.DataSource = tabla

                modificar = False
            End If

        End If

    End Sub
End Class