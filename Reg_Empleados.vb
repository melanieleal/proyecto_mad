﻿Public Class Reg_Empleados
    Dim conexion As New EnlaceBD
    Dim tabla As New DataTable
    Dim tablaD As New DataTable
    Dim tablaP As New DataTable
    Dim tablaF As New DataTable
    Dim tablaE As New DataTable
    Dim tablaN As New DataTable
    Dim obj As New EnlaceBD

    Dim val As New Funciones
    Dim validarLongitud As Boolean = False
    Dim validarUnico As Boolean = False
    Dim modificarEmpleado As Boolean = False
    Dim result As Boolean = False

    Dim rfc_ As String
    Dim depa As Integer
    Dim i As Integer
    Dim d As Integer

    Private Sub LetrasYEspacios_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MUNICIPIO.KeyPress, APE_PATERNO.KeyPress, APE_MATERNO.KeyPress
        'Codigo letras y espacios
        val.EvaluaSoloLetras(e)
    End Sub

    Private Sub LetrasYNumeros_KeyPress(sender As Object, e As KeyPressEventArgs) Handles PASSWORD.KeyPress, EMAIL.KeyPress
        'Codigo letras, numeros
        val.EvaluaSoloLetrasYNumerosSinEspacio(e)
    End Sub

    Private Sub Numeros_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TELEFONO.KeyPress, NUMERO_CASA.KeyPress, NSS.KeyPress, NO_CUENTA_BANCO.KeyPress, CP.KeyPress
        'Codigo para solo numeros
        val.EvaluaSoloNumeros(e)
    End Sub

    Private Sub LetNumYEsp_KeyPress(sender As Object, e As KeyPressEventArgs) Handles NAME_E.KeyPress, COLONIA.KeyPress, CALLE.KeyPress
        'Codigo letras, numeros y espacios
        val.EvaluaSoloLetrasYNumerosConEspacios(e)
    End Sub

    Private Sub LetMayYNum_KeyPress(sender As Object, e As KeyPressEventArgs) Handles RFC.KeyPress, CURP.KeyPress
        'Codigo para letras mayusculas y numeros
        val.EvaluaSoloLetrasMayusculasYNumeros(e)

    End Sub

    Private Sub Reg_Empleados_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ESTADO2.Items.Add("Aguascalientes")
        ESTADO2.Items.Add("Baja California")
        ESTADO2.Items.Add("Baja California Sur")
        ESTADO2.Items.Add("Campeche")
        ESTADO2.Items.Add("Chiapas")
        ESTADO2.Items.Add("Chihuahua")
        ESTADO2.Items.Add("Ciudad de Mexico")
        ESTADO2.Items.Add("Coahuila")
        ESTADO2.Items.Add("Colima")
        ESTADO2.Items.Add("Durango")
        ESTADO2.Items.Add("Guanajuato")
        ESTADO2.Items.Add("Guerrero")
        ESTADO2.Items.Add("Hidalgo")
        ESTADO2.Items.Add("Jalisco")
        ESTADO2.Items.Add("Michoacan")
        ESTADO2.Items.Add("Morelos")
        ESTADO2.Items.Add("Nayarit")
        ESTADO2.Items.Add("Nuevo Leon")
        ESTADO2.Items.Add("Oaxaca")
        ESTADO2.Items.Add("Puebla")
        ESTADO2.Items.Add("Queretaro")
        ESTADO2.Items.Add("Quintana Roo")
        ESTADO2.Items.Add("San Luis Potosi")
        ESTADO2.Items.Add("Sonora")
        ESTADO2.Items.Add("Sinaloa")
        ESTADO2.Items.Add("Tabasco")
        ESTADO2.Items.Add("Tlaxcala")
        ESTADO2.Items.Add("Veracruz")
        ESTADO2.Items.Add("Yucatan")
        ESTADO2.Items.Add("Zacatecas")

        tabla = conexion.Get_Empresas()
        Me.EMPRESA.DataSource = tabla
        Me.EMPRESA.DisplayMember = "Nombre"
        Me.EMPRESA.ValueMember = "RFC"

        Me.EMPRESA_E.DataSource = tabla
        Me.EMPRESA_E.DisplayMember = "Nombre"
        Me.EMPRESA_E.ValueMember = "RFC"

        tabla = conexion.Get_Bancos()
        Me.BANCO.DataSource = tabla
        Me.BANCO.DisplayMember = "Banco"
        Me.BANCO.ValueMember = "ID_Banco"

        tabla = conexion.Get_Terminacion_Email()
        Me.EMAIL_2.DataSource = tabla
        Me.EMAIL_2.DisplayMember = "Nombre"
        Me.EMAIL_2.ValueMember = "ID_Terminacion_Email"


        tabla = conexion.Get_Empleados()
        DG_EMPLEADOS.DataSource = tabla

        tabla = conexion.Get_Gerentes("G", "", "")
        GERENTE_GENERAL.Text = tabla.Rows(0).Item(1)
    End Sub

    Private Sub ACTUALIZAR_E_Click(sender As Object, e As EventArgs) Handles ACTUALIZAR_E.Click
        'VARIABLES AUXILIARES
        Dim OpcAUX As String
        Dim No_EmpleadoAUX As Integer
        Dim NombreAUX As String
        Dim Ape_PaternoAUX As String
        Dim Ape_MaternoAUX As String
        Dim Fecha_NacimientoAUX As String
        Dim EmailAUX As String
        Dim ID_Terminacion_EmailAUX As Integer
        Dim ContraseñaAUX As String
        Dim Fecha_PuestoAUX As String
        Dim CalleAUX As String
        Dim No_CasaAUX As Integer
        Dim ColoniaAUX As String
        Dim EstadoAUX As String
        Dim CP_AUX As Integer
        Dim MunicipioAUX As String
        Dim TelefonoAUX As String
        Dim Fecha_ContratoAUX As String
        Dim RFC_EMPLEADO_AUX As String
        Dim CURP_AUX As String
        Dim NSS_AUX As String
        Dim No_Cuenta_Banco_AUX As String
        Dim ID_Banco_AUX As Integer
        Dim ID_Puesto_AUX As Integer
        Dim ID_RFC_AUX As String
        Dim ID_Frec_PagoAUX As Integer
        Dim No_Departamento_AUX As Integer
        Dim GERENTE_GENERAL_AUX As Boolean
        Dim GERENTE_EMPRESA_AUX As Boolean
        Dim GERENTE_DEPTO_AUX As Boolean '28 variables
        Dim OneCheck As Integer = 0 'contador de checks

        'If GERENTE_GENERAL_CHECK.CheckState = 1 Then
        '    OneCheck = OneCheck + 1
        'End If

        If GERENTE_EMPRESA_CHECK.CheckState = 1 Then
            OneCheck = OneCheck + 1
        End If

        If GERENTE_DEPTO_CHECK.CheckState = 1 Then
            OneCheck = OneCheck + 1
        End If

        If Trim(NAME_E.Text) = "" Or
           Trim(APE_PATERNO.Text) = "" Or
           Trim(PASSWORD.Text) = "" Or
           Format(FECHA_NACIMIENTO.Value, "yyyy") > Date.Now.Date.Year - 18 Or
           Trim(CURP.Text) = "" Or
           Trim(EMAIL.Text) = "" Or
           Trim(TELEFONO.Text) = "" Or
           Trim(RFC.Text) = "" Or
           Trim(NSS.Text) = "" Or
           BANCO.SelectedIndex = -1 Or
           Trim(NO_CUENTA_BANCO.Text) = "" Or
           Trim(CALLE.Text) = "" Or
           Trim(NUMERO_CASA.Text) = "" Or
           Trim(CP.Text) = "" Or
           Trim(COLONIA.Text) = "" Or
           Trim(MUNICIPIO.Text) = "" Or
           ESTADO2.SelectedIndex = -1 Or
           EMPRESA.SelectedIndex = -1 Or
           FRECUENCIA_PAGO_EMPLEADO.SelectedIndex = -1 Or
           DEPARTAMENTO2.SelectedIndex = -1 Or
           PUESTO_EMPLEADO.SelectedIndex = -1 Or
           OneCheck > 1 Then

            MessageBox.Show("-Llene todos los campos" & vbCrLf & "Seleccione solo un tipo de gerente" & vbCrLf & " -El empleado debe ser mayor de edad", "Empleados", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else


            If Len(Trim(CURP.Text)) <> 18 Or Len(Trim(TELEFONO.Text)) <> 10 Or Len(Trim(RFC.Text)) <> 13 Or Len(Trim(NSS.Text)) <> 11 Or Len(Trim(NO_CUENTA_BANCO.Text)) <> 24 Or Len(Trim(CP.Text)) <> 5 Then
                validarLongitud = True
            End If

            If (modificarEmpleado = True) Then
                tablaE = conexion.Get_EmpleadosValidacionModificar("C", ID_EMPLEADO.Text, CURP.Text, RFC.Text, NSS.Text, NO_CUENTA_BANCO.Text, EMAIL.Text)
                If (tablaE.Rows.Count <> 0) Then
                    validarUnico = True
                End If
                tablaE = conexion.Get_EmpleadosValidacionModificar("R", ID_EMPLEADO.Text, CURP.Text, RFC.Text, NSS.Text, NO_CUENTA_BANCO.Text, EMAIL.Text)
                If (tablaE.Rows.Count <> 0) Then
                    validarUnico = True
                End If
                tablaE = conexion.Get_EmpleadosValidacionModificar("N", ID_EMPLEADO.Text, CURP.Text, RFC.Text, NSS.Text, NO_CUENTA_BANCO.Text, EMAIL.Text)
                If (tablaE.Rows.Count <> 0) Then
                    validarUnico = True
                End If
                tablaE = conexion.Get_EmpleadosValidacionModificar("B", ID_EMPLEADO.Text, CURP.Text, RFC.Text, NSS.Text, NO_CUENTA_BANCO.Text, EMAIL.Text)
                If (tablaE.Rows.Count <> 0) Then
                    validarUnico = True
                End If
                tablaE = conexion.Get_EmpleadosValidacionModificar("E", ID_EMPLEADO.Text, CURP.Text, RFC.Text, NSS.Text, NO_CUENTA_BANCO.Text, EMAIL.Text)
                If (tablaE.Rows.Count <> 0) Then
                    validarUnico = True
                End If
            Else
                tablaE = conexion.Get_EmpleadosValidacion("C", CURP.Text, RFC.Text, NSS.Text, NO_CUENTA_BANCO.Text, EMAIL.Text, 0, 0, 0)
                If (tablaE.Rows.Count <> 0) Then
                    validarUnico = True
                End If
                tablaE = conexion.Get_EmpleadosValidacion("R", CURP.Text, RFC.Text, NSS.Text, NO_CUENTA_BANCO.Text, EMAIL.Text, 0, 0, 0)
                If (tablaE.Rows.Count <> 0) Then
                    validarUnico = True
                End If
                tablaE = conexion.Get_EmpleadosValidacion("N", CURP.Text, RFC.Text, NSS.Text, NO_CUENTA_BANCO.Text, EMAIL.Text, 0, 0, 0)
                If (tablaE.Rows.Count <> 0) Then
                    validarUnico = True
                End If
                tablaE = conexion.Get_EmpleadosValidacion("B", CURP.Text, RFC.Text, NSS.Text, NO_CUENTA_BANCO.Text, EMAIL.Text, 0, 0, 0)
                If (tablaE.Rows.Count <> 0) Then
                    validarUnico = True
                End If
                tablaE = conexion.Get_EmpleadosValidacion("E", CURP.Text, RFC.Text, NSS.Text, NO_CUENTA_BANCO.Text, EMAIL.Text, 0, 0, 0)
                If (tablaE.Rows.Count <> 0) Then
                    validarUnico = True
                End If
            End If



            If validarLongitud Or validarUnico Then

                MessageBox.Show("Tome en cuenta" & vbCrLf &
                                "  *CURP es solo de 18 digitos " & vbCrLf &
                                "  *Telefono es de 10 digitos" & vbCrLf &
                                "  *El RFC es de 13 digitos" & vbCrLf &
                                "  *NSS es de 11 digitos" & vbCrLf &
                                "  *No. de Cuenta es de 24 digitos" & vbCrLf &
                                "  *CP es de 5 digitos" & vbCrLf & vbCrLf &
                                "  *Verifique que su CURP no ha sido registrado antes" & vbCrLf &
                                "  *Verifique que su RFC no ha sido registrado antes" & vbCrLf &
                                "  *Verifique que su NSS no ha sido registrado antes" & vbCrLf &
                                "  *Verifique que su No. de cuenta de Banco no ha sido registrado antes" & vbCrLf &
                                "  *Verifique que su Email no ha sido registrado antes", "Empleados", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                validarLongitud = False
                validarUnico = False
            Else


                NombreAUX = NAME_E.Text
                Ape_PaternoAUX = APE_PATERNO.Text
                Ape_MaternoAUX = APE_MATERNO.Text
                Fecha_NacimientoAUX = FECHA_NACIMIENTO.Text
                EmailAUX = EMAIL.Text

                tabla = conexion.Get_Terminacion_Email()         'obtengo el ID_Terminacion_Email 
                i = tabla.Rows.Count
                For fila As Integer = 0 To i - 1 Step 1
                    If tabla.Rows(fila).Item(1) = EMAIL_2.Text Then
                        ID_Terminacion_EmailAUX = tabla.Rows(fila).Item(0)
                    End If
                Next

                ContraseñaAUX = PASSWORD.Text

                CalleAUX = CALLE.Text
                No_CasaAUX = NUMERO_CASA.Text
                ColoniaAUX = COLONIA.Text
                EstadoAUX = ESTADO2.Text
                CP_AUX = CP.Text
                MunicipioAUX = MUNICIPIO.Text
                TelefonoAUX = TELEFONO.Text
                Fecha_ContratoAUX = DateTime.Now.ToString("dd/MM/yyyy")
                Fecha_PuestoAUX = DateTime.Now.ToString("dd/MM/yyyy")
                RFC_EMPLEADO_AUX = RFC.Text
                CURP_AUX = CURP.Text
                NSS_AUX = NSS.Text
                No_Cuenta_Banco_AUX = NO_CUENTA_BANCO.Text

                tabla = conexion.Get_Bancos()                     'obtengo el ID_Banco_AUX
                i = tabla.Rows.Count
                For fila As Integer = 0 To i - 1 Step 1
                    If tabla.Rows(fila).Item(1) = BANCO.Text Then
                        ID_Banco_AUX = tabla.Rows(fila).Item(0)
                    End If
                Next

                tabla = conexion.Get_Frecuencia("", "Y")              'obtengo el  ID_Frec_PagoAUX
                i = tabla.Rows.Count
                For fila As Integer = 0 To i - 1 Step 1
                    If tabla.Rows(fila).Item(1) = FRECUENCIA_PAGO_EMPLEADO.Text Then
                        ID_Frec_PagoAUX = tabla.Rows(fila).Item(0)
                    End If
                Next


                tabla = conexion.Get_Empresas()                        'obtengo el  ID_RFC_AUX
                i = tabla.Rows.Count
                For fila As Integer = 0 To i - 1 Step 1
                    If tabla.Rows(fila).Item(1) = EMPRESA.Text Then
                        ID_RFC_AUX = tabla.Rows(fila).Item(0)
                    End If
                Next

                tabla = conexion.Get_Departamento()                   'obtengo el  No_Departamento_AUX
                i = tabla.Rows.Count
                For fila As Integer = 0 To i - 1 Step 1
                    If tabla.Rows(fila).Item(1) = DEPARTAMENTO2.Text And tabla.Rows(fila).Item(3) = ID_RFC_AUX Then
                        No_Departamento_AUX = tabla.Rows(fila).Item(0)
                    End If
                Next

                tabla = conexion.Get_Puesto()                     'obtengo el  ID_Puesto_AUX
                i = tabla.Rows.Count
                For fila As Integer = 0 To i - 1 Step 1
                    If tabla.Rows(fila).Item(1) = PUESTO_EMPLEADO.Text And tabla.Rows(fila).Item(3) = No_Departamento_AUX Then
                        ID_Puesto_AUX = tabla.Rows(fila).Item(0)
                    End If
                Next

                'If GERENTE_GENERAL_CHECK.CheckState = 1 Then
                '    GERENTE_GENERAL_AUX = 1
                'End If
                If GERENTE_EMPRESA_CHECK.CheckState = 1 Then
                    GERENTE_EMPRESA_AUX = 1
                End If
                If GERENTE_DEPTO_CHECK.CheckState = 1 Then
                    GERENTE_DEPTO_AUX = 1
                End If

                If modificarEmpleado = False Then
                    OpcAUX = "I"



                    result = obj.Add_Empleado(OpcAUX, No_EmpleadoAUX, NombreAUX, Ape_PaternoAUX, Ape_MaternoAUX, Fecha_NacimientoAUX,
                    EmailAUX, ID_Terminacion_EmailAUX, ContraseñaAUX, Fecha_PuestoAUX, CalleAUX, No_CasaAUX,
                    ColoniaAUX, EstadoAUX, CP_AUX, MunicipioAUX, TelefonoAUX, Fecha_ContratoAUX, RFC_EMPLEADO_AUX,
                    CURP_AUX, NSS_AUX, No_Cuenta_Banco_AUX, ID_Banco_AUX, ID_Puesto_AUX, ID_RFC_AUX, ID_Frec_PagoAUX,
                    No_Departamento_AUX, GERENTE_EMPRESA_AUX, GERENTE_DEPTO_AUX)




                    If result Then
                        MessageBox.Show("El Empleado ha sido añadido exitosamente", "Empleados", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        'If GERENTE_GENERAL_CHECK.CheckState = 1 Then

                        '    tabla = conexion.Get_EmpleadosValidacion("J", "", "", "", "", "", 0, 0, 0)
                        '    If (tabla.Rows.Count <> 0) Then
                        '        Dim No_Empleado_GG As String = tabla.Rows(0).Item(0)
                        '        conexion.modificarGerentes("G", No_Empleado_GG)
                        '        MessageBox.Show("El Gerente General ha sido actualizado", "Empleados", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        '    End If
                        'End If

                        If GERENTE_EMPRESA_CHECK.CheckState = 1 Then
                            rfc_ = EMPRESA.Text
                            tabla = conexion.Get_Empresas()         'obtengo el rfc de la empresa 
                            i = tabla.Rows.Count
                            For fila As Integer = 0 To i - 1 Step 1
                                If tabla.Rows(fila).Item(1) = rfc_ Then
                                    rfc_ = tabla.Rows(fila).Item(0)
                                End If
                            Next


                            tabla = conexion.Get_EmpleadosValidacion("K", "", "", "", "", "", 0, rfc_, 0)
                            If (tabla.Rows.Count <> 0) Then
                                Dim No_Empleado_GE As String = tabla.Rows(0).Item(0)
                                conexion.modificarGerentes("E", No_Empleado_GE)
                                MessageBox.Show("El Gerente de la empresa " & EMPRESA.Text & " ha sido actualizado", "Empleados", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End If

                            tabla = conexion.Get_EmpleadosValidacion("W", "", "", "", "", "", 0, 0, 0) 'Buscar el empleado recien registrado
                            If (tabla.Rows.Count <> 0) Then
                                conexion.add_EmpresaGerente(rfc_, tabla.Rows(0).Item(0)) 'Hacerlo gerente en la TABLA EMPRESAS
                            End If

                        End If

                        If GERENTE_DEPTO_CHECK.CheckState = 1 Then
                            rfc_ = EMPRESA.Text
                            tabla = conexion.Get_Empresas()         'obtengo el rfc de la empresa 
                            i = tabla.Rows.Count
                            For fila As Integer = 0 To i - 1 Step 1
                                If tabla.Rows(fila).Item(1) = rfc_ Then
                                    rfc_ = tabla.Rows(fila).Item(0)
                                End If
                            Next

                            Dim no_depa As Integer
                            tablaD = conexion.Get_NameDepartamento(rfc_)
                            i = tablaD.Rows.Count
                            For fila As Integer = 0 To i - 1 Step 1
                                If tablaD.Rows(fila).Item(2) = rfc_ And tablaD.Rows(fila).Item(1) = DEPARTAMENTO2.Text Then
                                    no_depa = tablaD.Rows(fila).Item(0)
                                End If
                            Next


                            tabla = conexion.Get_EmpleadosValidacion("L", "", "", "", "", "", 0, 0, no_depa)
                            If (tabla.Rows.Count <> 0) Then
                                Dim No_Empleado_GD As String = tabla.Rows(0).Item(0)
                                conexion.modificarGerentes("D", No_Empleado_GD)
                                MessageBox.Show("El Gerente del departamento " & DEPARTAMENTO2.Text & " ha sido actualizado", "Empleados", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End If

                            tabla = conexion.Get_EmpleadosValidacion("W", "", "", "", "", "", 0, 0, 0) 'Buscar el empleado recien registrado
                            If (tabla.Rows.Count <> 0) Then
                                conexion.add_DeptoGerente(no_depa, tabla.Rows(0).Item(0)) 'Hacerlo gerente en la TABLA DEPTOS
                            End If

                        End If



                        'tabla = conexion.Get_Gerentes("G", "", "")
                        'GERENTE_GENERAL.Text = tabla.Rows(0).Item(1)

                        NAME_E.Clear()
                        APE_PATERNO.Clear()
                        APE_MATERNO.Clear()
                        FECHA_NACIMIENTO.Value = Date.Today
                        EMAIL.Clear()
                        PASSWORD.Clear()
                        CALLE.Clear()
                        NUMERO_CASA.Clear()
                        COLONIA.Clear()
                        CP.Clear()
                        MUNICIPIO.Clear()
                        TELEFONO.Clear()
                        RFC.Clear()
                        CURP.Clear()
                        NSS.Clear()
                        NO_CUENTA_BANCO.Clear()


                        'GERENTE_GENERAL_CHECK.Enabled() = True
                        GERENTE_EMPRESA_CHECK.Enabled() = True
                        GERENTE_DEPTO_CHECK.Enabled() = True

                        'GERENTE_GENERAL_CHECK.Checked() = False
                        GERENTE_EMPRESA_CHECK.Checked() = False
                        GERENTE_DEPTO_CHECK.Checked() = False


                    Else
                        MessageBox.Show("Hubo un error, su Empleado no se añadio", "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)


                    End If

                Else
                    OpcAUX = "M"

                    No_EmpleadoAUX = ID_EMPLEADO.Text 'asignarle el valor solo cuando modificara (porque solo ahi es cuando habra un ID)

                    'If (DG_EMPLEADOS.CurrentRow.Cells.Item(31).Value = True And GERENTE_GENERAL_CHECK.CheckState = 0) Then 'Si es gerente GENERAL pero eligio ya no serlo
                    '    GERENTE_GENERAL_AUX = True
                    '    GERENTE_GENERAL_CHECK.Checked() = True
                    '    GERENTE_EMPRESA_AUX = False
                    '    GERENTE_EMPRESA_CHECK.Checked() = False
                    '    GERENTE_DEPTO_AUX = False
                    '    GERENTE_DEPTO_CHECK.Checked() = False
                    '    MessageBox.Show("Modificacion de check de Gerente General denegada. No se puede eliminar un Gerente General, solo actualizarlo.", "Empleado", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'End If

                    If (DG_EMPLEADOS.CurrentRow.Cells.Item(32).Value = True And GERENTE_EMPRESA_CHECK.CheckState = 0) Then 'Si es gerente EMPRESA pero eligio ya no serlo
                        GERENTE_GENERAL_AUX = False
                        'GERENTE_GENERAL_CHECK.Checked() = False
                        GERENTE_EMPRESA_AUX = True
                        GERENTE_EMPRESA_CHECK.Checked() = True
                        GERENTE_DEPTO_AUX = False
                        GERENTE_DEPTO_CHECK.Checked() = False
                        MessageBox.Show("Modificacion de check de Gerente Empresa denegada. No se puede eliminar un Gerente Empresa, solo actualizarlo.", "Empleado", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If

                    If (DG_EMPLEADOS.CurrentRow.Cells.Item(33).Value = True And GERENTE_DEPTO_CHECK.CheckState = 0) Then 'Si es gerente DEPTO pero eligio ya no serlo
                        'GERENTE_GENERAL_AUX = False
                        'GERENTE_GENERAL_CHECK.Checked() = False
                        GERENTE_EMPRESA_AUX = False
                        GERENTE_EMPRESA_CHECK.Checked() = False
                        GERENTE_DEPTO_AUX = True
                        GERENTE_DEPTO_CHECK.Checked() = True
                        MessageBox.Show("Modificacion de check de Gerente Departamento denegada. No se puede eliminar un Gerente Departamento, solo actualizarlo.", "Empleado", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If

                    If (ID_RFC_AUX <> DG_EMPLEADOS.CurrentRow.Cells.Item(25).Value And 'si cambia de empresa y es gerente de empresa o depto
                        (DG_EMPLEADOS.CurrentRow.Cells.Item(33).Value = True Or DG_EMPLEADOS.CurrentRow.Cells.Item(32).Value = True)) Then
                        MessageBox.Show("Modificacion de empresa denegada. El empleado se es gerente de la empresa o de un Departamento perteneciente", "Empleado", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        ID_RFC_AUX = DG_EMPLEADOS.CurrentRow.Cells.Item(25).Value
                        No_Departamento_AUX = DG_EMPLEADOS.CurrentRow.Cells.Item(29).Value
                        ID_Puesto_AUX = DG_EMPLEADOS.CurrentRow.Cells.Item(23).Value

                        EMPRESA.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(26).Value
                        DEPARTAMENTO2.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(30).Value
                        PUESTO_EMPLEADO.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(24).Value
                    End If

                    If (No_Departamento_AUX <> DG_EMPLEADOS.CurrentRow.Cells.Item(29).Value And
                        DG_EMPLEADOS.CurrentRow.Cells.Item(33).Value = True) Then ''' = true
                        MessageBox.Show("Modificacion de departamento denegada. El empleado es gerente del departamento", "Empleado", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        ID_RFC_AUX = DG_EMPLEADOS.CurrentRow.Cells.Item(25).Value
                        No_Departamento_AUX = DG_EMPLEADOS.CurrentRow.Cells.Item(29).Value
                        ID_Puesto_AUX = DG_EMPLEADOS.CurrentRow.Cells.Item(23).Value

                        EMPRESA.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(26).Value
                        DEPARTAMENTO2.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(30).Value
                        PUESTO_EMPLEADO.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(24).Value
                    End If

                    If (ID_RFC_AUX <> DG_EMPLEADOS.CurrentRow.Cells.Item(25).Value Or 'Si no se ha efectuado el Proceso de Nomina que no pueda cambiar de Emp, Depa o Puesto
                        ID_Puesto_AUX <> DG_EMPLEADOS.CurrentRow.Cells.Item(23).Value Or
                        No_Departamento_AUX <> DG_EMPLEADOS.CurrentRow.Cells.Item(29).Value) Then

                        Dim nominaHecha As Boolean = False
                        tablaN = conexion.Get_Nomina2(ID_EMPLEADO.Text)
                        Dim n As Integer = tablaN.Rows.Count
                        For filaN As Integer = 0 To n - 1 Step 1
                            If (tablaN.Rows(filaN).Item(7) = DateTime.Now.ToString("dd/MM/yyyy") Or tablaN.Rows(filaN).Item(7) = DateAdd("d", -1, DateTime.Now.ToString("dd/MM/yyyy"))) Then
                                nominaHecha = True
                            End If
                        Next

                        If (nominaHecha = False) Then
                            MessageBox.Show("Modificacion de zona de trabajo denegada. Se debe efectuar antes el proceso de nomina al empleado", "Empleado", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            ID_RFC_AUX = DG_EMPLEADOS.CurrentRow.Cells.Item(25).Value
                            ID_Puesto_AUX = DG_EMPLEADOS.CurrentRow.Cells.Item(23).Value
                            No_Departamento_AUX = DG_EMPLEADOS.CurrentRow.Cells.Item(29).Value
                        End If

                    End If

                    If (ID_Puesto_AUX <> DG_EMPLEADOS.CurrentRow.Cells.Item(23).Value) Then 'Si cambio de puesto
                        Fecha_PuestoAUX = DateTime.Now.ToString("dd/MM/yyyy") 'actualizar fecha
                        Fecha_ContratoAUX = DG_EMPLEADOS.CurrentRow.Cells.Item(16).Value
                    Else
                        Fecha_ContratoAUX = DG_EMPLEADOS.CurrentRow.Cells.Item(16).Value ' si no actualizo puesto
                        Fecha_PuestoAUX = DG_EMPLEADOS.CurrentRow.Cells.Item(8).Value 'mantener fechas anteriores
                    End If

                    result = obj.Add_Empleado(OpcAUX, No_EmpleadoAUX, NombreAUX, Ape_PaternoAUX, Ape_MaternoAUX, Fecha_NacimientoAUX,
                    EmailAUX, ID_Terminacion_EmailAUX, ContraseñaAUX, Fecha_PuestoAUX, CalleAUX, No_CasaAUX,
                    ColoniaAUX, EstadoAUX, CP_AUX, MunicipioAUX, TelefonoAUX, Fecha_ContratoAUX, RFC_EMPLEADO_AUX,
                    CURP_AUX, NSS_AUX, No_Cuenta_Banco_AUX, ID_Banco_AUX, ID_Puesto_AUX, ID_RFC_AUX, ID_Frec_PagoAUX,
                    No_Departamento_AUX, GERENTE_EMPRESA_AUX, GERENTE_DEPTO_AUX)

                    If result Then
                        MessageBox.Show("El Empleado ha sido modificado exitosamente", "Empelados", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        'If GERENTE_GENERAL_CHECK.CheckState = 1 Then
                        '    Dim No_Empleado_NewGG As Integer = ID_EMPLEADO.Text
                        '    tabla = conexion.Get_EmpleadosValidacion("G", "", "", "", "", "", No_Empleado_NewGG, 0, 0)
                        '    If (tabla.Rows.Count <> 0) Then
                        '        Dim No_Empleado_GG As String = tabla.Rows(0).Item(0)
                        '        conexion.modificarGerentes("G", No_Empleado_GG)
                        '        MessageBox.Show("El Gerente General ha sido actualizado", "Empleados", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        '    End If
                        'End If

                        If GERENTE_EMPRESA_CHECK.CheckState = 1 Then
                            rfc_ = EMPRESA.Text
                            tabla = conexion.Get_Empresas()         'obtengo el rfc de la empresa 
                            i = tabla.Rows.Count
                            For fila As Integer = 0 To i - 1 Step 1
                                If tabla.Rows(fila).Item(1) = rfc_ Then
                                    rfc_ = tabla.Rows(fila).Item(0)
                                End If
                            Next

                            Dim No_Empleado_NewGE As Integer = ID_EMPLEADO.Text
                            tabla = conexion.Get_EmpleadosValidacion("M", "", "", "", "", "", No_Empleado_NewGE, rfc_, 0)
                            If (tabla.Rows.Count <> 0) Then
                                Dim No_Empleado_GE As String = tabla.Rows(0).Item(0)
                                conexion.modificarGerentes("E", No_Empleado_GE)
                                MessageBox.Show("El Gerente de la empresa " & EMPRESA.Text & " ha sido actualizado", "Empleados", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End If

                            conexion.add_EmpresaGerente(rfc_, ID_EMPLEADO.Text) 'Agregarlo a la tabla empresas
                        End If

                        If GERENTE_DEPTO_CHECK.CheckState = 1 Then
                            rfc_ = EMPRESA.Text
                            tabla = conexion.Get_Empresas()         'obtengo el rfc de la empresa 
                            i = tabla.Rows.Count
                            For fila As Integer = 0 To i - 1 Step 1
                                If tabla.Rows(fila).Item(1) = rfc_ Then
                                    rfc_ = tabla.Rows(fila).Item(0)
                                End If
                            Next

                            Dim no_depa As Integer
                            tablaD = conexion.Get_NameDepartamento(rfc_)
                            i = tablaD.Rows.Count
                            For fila As Integer = 0 To i - 1 Step 1
                                If tablaD.Rows(fila).Item(2) = rfc_ And tablaD.Rows(fila).Item(1) = DEPARTAMENTO2.Text Then
                                     no_depa = tablaD.Rows(fila).Item(0)

                                End If
                            Next


                            Dim No_Empleado_NewGD As Integer = ID_EMPLEADO.Text
                            tabla = conexion.Get_EmpleadosValidacion("D", "", "", "", "", "", No_Empleado_NewGD, 0, no_depa)
                            If (tabla.Rows.Count <> 0) Then
                                Dim No_Empleado_GD As String = tabla.Rows(0).Item(0)
                                conexion.modificarGerentes("D", No_Empleado_GD)
                                MessageBox.Show("El Gerente del departamento " & DEPARTAMENTO2.Text & " ha sido actualizado", "Empleados", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End If



                            conexion.add_DeptoGerente(no_depa, ID_EMPLEADO.Text) 'Hacerlo gerente en la TABLA DEPTOS

                        End If


                        'tabla = conexion.Get_Gerentes("G", "", "")               'ACTUALIZAR EL GG
                        'GERENTE_GENERAL.Text = tabla.Rows(0).Item(1)

                        tabla = conexion.Get_Empresas()                      'ACTUALIZAR EL GE
                        i = tabla.Rows.Count
                        For fila As Integer = 0 To i - 1 Step 1
                            If tabla.Rows(fila).Item(1) = EMPRESA_E.Text Then
                                rfc_ = tabla.Rows(fila).Item(0)
                            End If
                        Next

                        tabla = conexion.Get_Gerentes("E", rfc_, "")
                        If (tabla.Rows.Count <> 0) Then
                            GERENTE_EMPRESA.Text = tabla.Rows(0).Item(1)
                        Else
                            GERENTE_EMPRESA.Text = "Sin gerente registrado"
                        End If

                        ID_EMPLEADO.Text = ""
                        NAME_E.Clear()
                        APE_PATERNO.Clear()
                        APE_MATERNO.Clear()
                        FECHA_NACIMIENTO.Value = Date.Today
                        EMAIL.Clear()
                        PASSWORD.Clear()
                        CALLE.Clear()
                        NUMERO_CASA.Clear()
                        COLONIA.Clear()
                        CP.Clear()
                        MUNICIPIO.Clear()
                        TELEFONO.Clear()
                        RFC.Clear()
                        CURP.Clear()
                        NSS.Clear()
                        NO_CUENTA_BANCO.Clear()
                        RFC.Clear()

                        'GERENTE_GENERAL_CHECK.Enabled() = True
                        GERENTE_EMPRESA_CHECK.Enabled() = True
                        GERENTE_DEPTO_CHECK.Enabled() = True

                        'GERENTE_GENERAL_CHECK.Checked() = False
                        GERENTE_EMPRESA_CHECK.Checked() = False
                        GERENTE_DEPTO_CHECK.Checked() = False
                        modificarEmpleado = False
                    Else
                        MessageBox.Show("Ocurrio un error, su Empleado no se ha modificado", "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                        End If
                    End If

                tabla = conexion.Get_Empleados()
                DG_EMPLEADOS.DataSource = tabla


            End If
        End If

    End Sub


    Private Sub DG_EMPLEADOS_Click(sender As Object, e As EventArgs) Handles DG_EMPLEADOS.Click
        'GERENTE_GENERAL_CHECK.Checked() = False
        GERENTE_EMPRESA_CHECK.Checked() = False
        GERENTE_DEPTO_CHECK.Checked() = False



        If (Len(Trim(DG_EMPLEADOS.CurrentRow.Cells.Item(0).Value.ToString())) <> 0) Then

            modificarEmpleado = True
            ID_EMPLEADO.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(0).Value.ToString()
            NAME_E.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(1).Value.ToString()
            APE_PATERNO.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(2).Value.ToString()
            APE_MATERNO.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(3).Value.ToString()
            FECHA_NACIMIENTO.Value = DG_EMPLEADOS.CurrentRow.Cells.Item(4).Value.ToString()
            EMAIL.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(5).Value.ToString()
            EMAIL_2.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(6).Value.ToString()
            PASSWORD.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(7).Value.ToString()

            CALLE.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(9).Value.ToString()
            NUMERO_CASA.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(10).Value.ToString()
            COLONIA.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(11).Value.ToString()

            ESTADO2.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(12).Value.ToString()



            CP.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(13).Value.ToString()
            MUNICIPIO.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(14).Value.ToString()
            TELEFONO.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(15).Value.ToString()

            RFC.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(17).Value.ToString()
            CURP.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(18).Value.ToString()
            NSS.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(19).Value.ToString()

            BANCO.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(21).Value.ToString()

            NO_CUENTA_BANCO.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(22).Value.ToString()



            EMPRESA.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(26).Value.ToString()


            DEPARTAMENTO2.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(30).Value.ToString()
            PUESTO_EMPLEADO.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(24).Value.ToString()
            FRECUENCIA_PAGO_EMPLEADO.Text = DG_EMPLEADOS.CurrentRow.Cells.Item(28).Value.ToString()



            'If (DG_EMPLEADOS.CurrentRow.Cells.Item(31).Value = True) Then
            '    'GERENTE_GENERAL_CHECK.Checked() = True
            'End If

            If (DG_EMPLEADOS.CurrentRow.Cells.Item(32).Value = True) Then
                GERENTE_EMPRESA_CHECK.Checked() = True
            End If

            If (DG_EMPLEADOS.CurrentRow.Cells.Item(33).Value = True) Then
                GERENTE_DEPTO_CHECK.Checked() = True
            End If



        Else

            NAME_E.Clear()
            APE_PATERNO.Clear()
            APE_MATERNO.Clear()
            FECHA_NACIMIENTO.Value = Date.Today
            EMAIL.Clear()
            PASSWORD.Clear()

            CALLE.Clear()
            NUMERO_CASA.Clear()
            COLONIA.Clear()
            CP.Clear()
            MUNICIPIO.Clear()
            TELEFONO.Clear()

            RFC.Clear()
            CURP.Clear()
            NSS.Clear()
            NO_CUENTA_BANCO.Clear()
            RFC.Clear()

            'GERENTE_GENERAL_CHECK.Enabled() = True
            GERENTE_EMPRESA_CHECK.Enabled() = True
            GERENTE_DEPTO_CHECK.Enabled() = True

            'GERENTE_GENERAL_CHECK.Checked() = False
            GERENTE_EMPRESA_CHECK.Checked() = False
            GERENTE_DEPTO_CHECK.Checked() = False


        End If
    End Sub


    Private Sub EMPRESA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles EMPRESA.SelectedIndexChanged
        FRECUENCIA_PAGO_EMPLEADO.Items.Clear()


        tabla = conexion.Get_Empresas()         'obtengo el rfc de la empresa 
        i = tabla.Rows.Count
        For fila As Integer = 0 To i - 1 Step 1
            If tabla.Rows(fila).Item(1) = EMPRESA.Text Then
                rfc_ = tabla.Rows(fila).Item(0)

                Dim tablaF As New DataTable
                tablaF = conexion.Get_Frecuencia(rfc_, "X")
                Dim j As Integer = tablaF.Rows.Count
                For filaF As Integer = 0 To j - 1 Step 1
                    If tablaF.Rows(filaF).Item(1) = 1 Then
                        Me.FRECUENCIA_PAGO_EMPLEADO.Items.Add("Semanal")
                        If filaF = 0 Then
                            FRECUENCIA_PAGO_EMPLEADO.Text = "Semanal"
                        End If
                    End If
                    If tablaF.Rows(filaF).Item(1) = 2 Then
                        Me.FRECUENCIA_PAGO_EMPLEADO.Items.Add("Quincenal")
                        If filaF = 0 Then
                            FRECUENCIA_PAGO_EMPLEADO.Text = "Quincenal"
                        End If
                    End If
                    If tablaF.Rows(filaF).Item(1) = 3 Then
                        Me.FRECUENCIA_PAGO_EMPLEADO.Items.Add("Catorcenal")
                        If filaF = 0 Then
                            FRECUENCIA_PAGO_EMPLEADO.Text = "Catorcenal"
                        End If
                    End If
                    If tablaF.Rows(filaF).Item(1) = 4 Then
                        Me.FRECUENCIA_PAGO_EMPLEADO.Items.Add("Mensual")
                        If filaF = 0 Then
                            FRECUENCIA_PAGO_EMPLEADO.Text = "Mensual"
                        End If
                    End If
                Next



            End If
        Next


        tablaD = conexion.Get_NameDepartamento(rfc_)
        i = tablaD.Rows.Count
        If i = 0 Then
            Me.DEPARTAMENTO2.DataSource = tablaD
            Me.DEPARTAMENTO2.DisplayMember = "Nombre"
            Me.DEPARTAMENTO2.ValueMember = "No_Departamento"

            tablaP = conexion.Get_NamePuesto(0, "")
            Me.PUESTO_EMPLEADO.DataSource = tablaP
            Me.PUESTO_EMPLEADO.DisplayMember = "Nombre"
            Me.PUESTO_EMPLEADO.ValueMember = "Id_Puesto"

        Else
            For fila As Integer = 0 To i - 1 Step 1
                If tablaD.Rows(fila).Item(2) = rfc_ Then
                    depa = tablaD.Rows(fila).Item(0)
                    Me.DEPARTAMENTO2.DataSource = tablaD
                    Me.DEPARTAMENTO2.DisplayMember = "Nombre"
                    Me.DEPARTAMENTO2.ValueMember = "No_Departamento"
                End If
            Next
        End If

    End Sub



    Private Sub DEPARTAMENTO2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DEPARTAMENTO2.SelectedIndexChanged

        tablaE = conexion.Get_Empresas()
        Dim j As Integer = tablaE.Rows().Count
        If j = 0 Then
            Me.EMPRESA.DataSource = tablaE
            Me.EMPRESA.DisplayMember = "Nombre"
            Me.EMPRESA.ValueMember = "RFC"
        Else
            For fila As Integer = 0 To j - 1 Step 1
                If tablaE.Rows(fila).Item(1) = EMPRESA.Text Then
                    rfc_ = tablaE.Rows(fila).Item(0)

                    tablaD = conexion.Get_NameDepartamento(rfc_)
                    Dim k As Integer = tablaD.Rows().Count
                    If k = 0 Then
                        Me.DEPARTAMENTO2.DataSource = tablaD
                        Me.DEPARTAMENTO2.DisplayMember = "Nombre"
                        Me.DEPARTAMENTO2.ValueMember = "No_Departamento"
                    Else
                        For filaD As Integer = 0 To k - 1 Step 1
                            If tablaD.Rows(filaD).Item(1) = DEPARTAMENTO2.Text Then
                                depa = tablaD.Rows(filaD).Item(0)
                            End If
                        Next
                    End If

                End If
            Next

        End If


        tablaP = conexion.Get_NamePuesto(depa, rfc_)
        Dim p As Integer = tablaP.Rows.Count
        If p = 0 Then
            Me.PUESTO_EMPLEADO.DataSource = tablaP
            Me.PUESTO_EMPLEADO.DisplayMember = "Nombre"
            Me.PUESTO_EMPLEADO.ValueMember = "Id_Puesto"
        Else
            For fila As Integer = 0 To p - 1 Step 1
                If tablaP.Rows(fila).Item(2) = depa Then
                    Me.PUESTO_EMPLEADO.DataSource = tablaP
                    Me.PUESTO_EMPLEADO.DisplayMember = "Nombre"
                    Me.PUESTO_EMPLEADO.ValueMember = "Id_Puesto"
                End If
            Next
        End If
    End Sub


    Private Sub LIMPIAR_CAMPOS_MouseClick(sender As Object, e As MouseEventArgs) Handles LIMPIAR_CAMPOS.MouseClick

        modificarEmpleado = False
        ID_EMPLEADO.Text = ""
        NAME_E.Clear()
        APE_PATERNO.Clear()
        APE_MATERNO.Clear()
        FECHA_NACIMIENTO.Value = Date.Today
        EMAIL.Clear()
        PASSWORD.Clear()

        CALLE.Clear()
        NUMERO_CASA.Clear()
        COLONIA.Clear()
        CP.Clear()
        MUNICIPIO.Clear()
        TELEFONO.Clear()

        RFC.Clear()
        CURP.Clear()
        NSS.Clear()
        NO_CUENTA_BANCO.Clear()
        RFC.Clear()

        'GERENTE_GENERAL_CHECK.Enabled() = True
        GERENTE_EMPRESA_CHECK.Enabled() = True
        GERENTE_DEPTO_CHECK.Enabled() = True

        'GERENTE_GENERAL_CHECK.Checked() = False
        GERENTE_EMPRESA_CHECK.Checked() = False
        GERENTE_DEPTO_CHECK.Checked() = False
    End Sub

    Private Sub EMPRESA_E_SelectedIndexChanged(sender As Object, e As EventArgs) Handles EMPRESA_E.SelectedIndexChanged


        tabla = conexion.Get_Empresas()         'obtengo el rfc de la empresa 
        i = tabla.Rows.Count
        For fila As Integer = 0 To i - 1 Step 1
            If tabla.Rows(fila).Item(1) = EMPRESA_E.Text Then
                rfc_ = tabla.Rows(fila).Item(0)

            End If
        Next

        tabla = conexion.Get_Gerentes("E", rfc_, "")
        If (tabla.Rows.Count <> 0) Then
            GERENTE_EMPRESA.Text = tabla.Rows(0).Item(1)
        Else
            GERENTE_EMPRESA.Text = "Sin gerente registrado"
        End If

        tablaD = conexion.Get_NameDepartamento(rfc_)
        d = tablaD.Rows.Count
        If d = 0 Then
            Me.DEPARTAMENTO1.DataSource = tablaD
            Me.DEPARTAMENTO1.DisplayMember = "Nombre"
            Me.DEPARTAMENTO1.ValueMember = "No_Departamento"
        Else
            For fila As Integer = 0 To d - 1 Step 1
                If tablaD.Rows(fila).Item(2) = rfc_ Then
                    depa = tablaD.Rows(fila).Item(0)
                    Me.DEPARTAMENTO1.DataSource = tablaD
                    Me.DEPARTAMENTO1.DisplayMember = "Nombre"
                    Me.DEPARTAMENTO1.ValueMember = "No_Departamento"
                End If
            Next
        End If

    End Sub

    Private Sub DEPARTAMENTO1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DEPARTAMENTO1.SelectedIndexChanged

        tabla = conexion.Get_Empresas()         'obtengo el rfc de la empresa 
        Dim n As Integer = tabla.Rows.Count
        For fila As Integer = 0 To n - 1 Step 1
            If tabla.Rows(fila).Item(1) = EMPRESA_E.Text Then
                rfc_ = tabla.Rows(fila).Item(0)
            End If
        Next

        tablaD = conexion.Get_NameDepartamento(rfc_)
        Dim d1 = tablaD.Rows.Count

        For fila As Integer = 0 To d1 - 1 Step 1
            If tablaD.Rows(fila).Item(2) = rfc_ And tablaD.Rows(fila).Item(1) = DEPARTAMENTO1.Text Then
                depa = tablaD.Rows(fila).Item(0)
            End If
        Next

        tabla = conexion.Get_Gerentes("D", "", depa)
        If (tabla.Rows.Count <> 0) Then
            GERENTE_DEPTO.Text = tabla.Rows(0).Item(1)
        Else
            GERENTE_DEPTO.Text = "Sin gerente registrado"

        End If
    End Sub

    Private Sub DELETE_E_Click(sender As Object, e As EventArgs) Handles DELETE_E.Click
        If (modificarEmpleado = True) Then

            If MessageBox.Show("Seguro que Desea Eliminar", "Empleados", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then


                result = obj.Delete_Empleado(ID_EMPLEADO.Text)

                If result Then
                    MessageBox.Show("El Empleado ha sido eliminado exitosamente", "Empleados", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    ID_EMPLEADO.Text = ""
                    NAME_E.Clear()
                    APE_PATERNO.Clear()
                    APE_MATERNO.Clear()
                    FECHA_NACIMIENTO.Value = Date.Today
                    EMAIL.Clear()
                    PASSWORD.Clear()

                    CALLE.Clear()
                    NUMERO_CASA.Clear()
                    COLONIA.Clear()
                    CP.Clear()
                    MUNICIPIO.Clear()
                    TELEFONO.Clear()

                    RFC.Clear()
                    CURP.Clear()
                    NSS.Clear()
                    NO_CUENTA_BANCO.Clear()
                    RFC.Clear()

                    'GERENTE_GENERAL_CHECK.Enabled() = True
                    GERENTE_EMPRESA_CHECK.Enabled() = True
                    GERENTE_DEPTO_CHECK.Enabled() = True

                    'GERENTE_GENERAL_CHECK.Checked() = False
                    GERENTE_EMPRESA_CHECK.Checked() = False
                    GERENTE_DEPTO_CHECK.Checked() = False

                    tabla = conexion.Get_Empleados()
                    DG_EMPLEADOS.DataSource = tabla

                    modificarEmpleado = False
                Else
                    MessageBox.Show("Hubo un error, su Empleado no se elimino", "Empleados", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                End If

            Else
                ID_EMPLEADO.Text = ""
                NAME_E.Clear()
                APE_PATERNO.Clear()
                APE_MATERNO.Clear()
                FECHA_NACIMIENTO.Value = Date.Today
                EMAIL.Clear()
                PASSWORD.Clear()

                CALLE.Clear()
                NUMERO_CASA.Clear()
                COLONIA.Clear()
                CP.Clear()
                MUNICIPIO.Clear()
                TELEFONO.Clear()

                RFC.Clear()
                CURP.Clear()
                NSS.Clear()
                NO_CUENTA_BANCO.Clear()
                RFC.Clear()

                'GERENTE_GENERAL_CHECK.Enabled() = True
                GERENTE_EMPRESA_CHECK.Enabled() = True
                GERENTE_DEPTO_CHECK.Enabled() = True

                'GERENTE_GENERAL_CHECK.Checked() = False
                GERENTE_EMPRESA_CHECK.Checked() = False
                GERENTE_DEPTO_CHECK.Checked() = False

                tabla = conexion.Get_Empleados()
                DG_EMPLEADOS.DataSource = tabla

                modificarEmpleado = False
            End If



        End If


    End Sub
End Class