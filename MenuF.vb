﻿Imports System.Runtime.InteropServices

Public Class MenuF

    Private Sub BN_MINIMIZAR_Click(sender As Object, e As EventArgs) Handles BN_MINIMIZAR.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    'Desplazar la ventana
    <DllImport("user32.DLL", EntryPoint:="ReleaseCapture")>
    Private Shared Sub ReleaseCapture()
    End Sub

    <DllImport("user32.DLL", EntryPoint:="SendMessage")>
    Private Shared Sub SendMessage(ByVal hWnd As System.IntPtr, ByVal wMsg As Integer, ByVal wParam As Integer, ByVal lParam As Integer)
    End Sub

    Private Sub Panel_Menu_MouseMove(sender As Object, e As MouseEventArgs) Handles PANEL_DESPLAZAR.MouseMove
        ReleaseCapture()
        SendMessage(Me.Handle, &H112&, &HF012&, 0)
    End Sub

    'Abrir/cerrar menu
    Private Sub TIME_MENU_O_Tick(sender As Object, e As EventArgs) Handles TIME_MENU_O.Tick
        If PANEL_MENU.Width <= 60 Then
            Me.TIME_MENU_O.Enabled = False
        Else
            Me.PANEL_MENU.Width = PANEL_MENU.Width - 20
        End If
    End Sub

    Private Sub TIMER_MENU_M_Tick(sender As Object, e As EventArgs) Handles TIMER_MENU_M.Tick
        If PANEL_MENU.Width >= 200 Then
            Me.TIMER_MENU_M.Enabled = False
        Else
            Me.PANEL_MENU.Width = PANEL_MENU.Width + 20
        End If
    End Sub

    Private Sub BN_MENU_Click(sender As Object, e As EventArgs) Handles BN_MENU.Click
        If PANEL_MENU.Width = 200 Then
            TIME_MENU_O.Enabled = True
        Else
            If PANEL_MENU.Width = 60 Then
                TIMER_MENU_M.Enabled = True
            End If
        End If
    End Sub

    'mostrar submenu en un panel
    Private Sub AbrirForm(ByVal formHijo As Object)
        If Me.PANEL_VENTANAS.Controls.Count > 0 Then
            Me.PANEL_VENTANAS.Controls.RemoveAt(0)         'remueve lo que haya en el panel
        End If
        Dim formH As Form = TryCast(formHijo, Form)
        formH.TopLevel = False
        formH.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        formH.Dock = DockStyle.Fill
        Me.PANEL_VENTANAS.Controls.Add(formH)              'añade la nueva ventana al panel
        Me.PANEL_VENTANAS.Tag = formH
        formH.Show()
    End Sub

    'mostrar submenu en un panel
    Private Sub AbrirFormSub(ByVal formHijoSub As Object)
        If Me.PANEL_SUBMENU.Controls.Count > 0 Then
            Me.PANEL_SUBMENU.Controls.RemoveAt(0)         'remueve lo que haya en el panel
        End If
        Dim formHSub As Form = TryCast(formHijoSub, Form)
        formHSub.TopLevel = False
        formHSub.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        formHSub.Dock = DockStyle.Fill
        Me.PANEL_SUBMENU.Controls.Add(formHSub)              'añade la nueva ventana al panel
        Me.PANEL_SUBMENU.Tag = formHSub
        formHSub.Show()
    End Sub

    Private Sub BN_REGISTRO_Click(sender As Object, e As EventArgs) Handles BN_REGISTRO.Click
        AbrirFormSub(New SubMenu_Registros)
        If Me.PANEL_VENTANAS.Controls.Count > 0 Then
            Me.PANEL_VENTANAS.Controls.RemoveAt(0)
        End If
        AbrirForm(New Reg_Empleados)
    End Sub

    Private Sub BN_REPORTES_Click(sender As Object, e As EventArgs) Handles BN_REPORTES.Click
        AbrirFormSub(New SubMenu_Reportes)
        If Me.PANEL_VENTANAS.Controls.Count > 0 Then
            Me.PANEL_VENTANAS.Controls.RemoveAt(0)
        End If
        AbrirForm(New Rep_Nomina)
    End Sub

    Private Sub BN_EMPLEADOS_Click(sender As Object, e As EventArgs) Handles BN_EMPLEADOS.Click
        AbrirFormSub(New SubMenu_Empleados)
        If Me.PANEL_VENTANAS.Controls.Count > 0 Then
            Me.PANEL_VENTANAS.Controls.RemoveAt(0)
        End If
        AbrirForm(New Resumen_Pagos)
    End Sub

    Private Sub BTN_PROC_NOMINA_Click(sender As Object, e As EventArgs) Handles BTN_PROC_NOMINA.Click
        AbrirForm(New Proceso_Nomina)
        If Me.PANEL_SUBMENU.Controls.Count > 0 Then
            Me.PANEL_SUBMENU.Controls.RemoveAt(0)
        End If
    End Sub

    Private Sub BN_CLOSE_Click(sender As Object, e As EventArgs) Handles BN_CLOSE.Click

        If Me.PANEL_VENTANAS.Controls.Count > 0 And Me.PANEL_SUBMENU.Controls.Count > 0 Then
            Me.PANEL_VENTANAS.Controls.RemoveAt(0)
            Me.PANEL_SUBMENU.Controls.RemoveAt(0)
        ElseIf Me.PANEL_SUBMENU.Controls.Count > 0 Then
            Me.PANEL_SUBMENU.Controls.RemoveAt(0)
        End If

        BN_REGISTRO.Enabled = True
        BN_REPORTES.Enabled = True
        BTN_PROC_NOMINA.Enabled = True


        Login.USERNAME.Clear()
        Login.PASSWORD.Clear()
        Me.Hide()
        Login.Show()
    End Sub

End Class

