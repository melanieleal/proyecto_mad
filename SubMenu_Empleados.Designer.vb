﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SubMenu_Empleados
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BN_NOMINA = New System.Windows.Forms.Button()
        Me.BN_NOMINAG = New System.Windows.Forms.Button()
        Me.BN_INCIDENCIAS = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'BN_NOMINA
        '
        Me.BN_NOMINA.FlatAppearance.BorderSize = 0
        Me.BN_NOMINA.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Goldenrod
        Me.BN_NOMINA.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.BN_NOMINA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BN_NOMINA.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BN_NOMINA.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BN_NOMINA.Location = New System.Drawing.Point(152, 0)
        Me.BN_NOMINA.Name = "BN_NOMINA"
        Me.BN_NOMINA.Size = New System.Drawing.Size(149, 34)
        Me.BN_NOMINA.TabIndex = 4
        Me.BN_NOMINA.Text = "Recibo de Nomina "
        Me.BN_NOMINA.UseVisualStyleBackColor = True
        '
        'BN_NOMINAG
        '
        Me.BN_NOMINAG.FlatAppearance.BorderSize = 0
        Me.BN_NOMINAG.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Goldenrod
        Me.BN_NOMINAG.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.BN_NOMINAG.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BN_NOMINAG.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BN_NOMINAG.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BN_NOMINAG.Location = New System.Drawing.Point(-1, 0)
        Me.BN_NOMINAG.Name = "BN_NOMINAG"
        Me.BN_NOMINAG.Size = New System.Drawing.Size(151, 34)
        Me.BN_NOMINAG.TabIndex = 3
        Me.BN_NOMINAG.Text = " Resumen de Pagos"
        Me.BN_NOMINAG.UseVisualStyleBackColor = True
        '
        'BN_INCIDENCIAS
        '
        Me.BN_INCIDENCIAS.FlatAppearance.BorderSize = 0
        Me.BN_INCIDENCIAS.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Goldenrod
        Me.BN_INCIDENCIAS.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.BN_INCIDENCIAS.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BN_INCIDENCIAS.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BN_INCIDENCIAS.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BN_INCIDENCIAS.Location = New System.Drawing.Point(307, 0)
        Me.BN_INCIDENCIAS.Name = "BN_INCIDENCIAS"
        Me.BN_INCIDENCIAS.Size = New System.Drawing.Size(149, 34)
        Me.BN_INCIDENCIAS.TabIndex = 5
        Me.BN_INCIDENCIAS.Text = "Solicitudes"
        Me.BN_INCIDENCIAS.UseVisualStyleBackColor = True
        '
        'SubMenu_Empleados
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(900, 34)
        Me.Controls.Add(Me.BN_INCIDENCIAS)
        Me.Controls.Add(Me.BN_NOMINA)
        Me.Controls.Add(Me.BN_NOMINAG)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "SubMenu_Empleados"
        Me.Text = "SubMenu_Empleados"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents BN_NOMINA As Button
    Friend WithEvents BN_NOMINAG As Button
    Friend WithEvents BN_INCIDENCIAS As Button
End Class
