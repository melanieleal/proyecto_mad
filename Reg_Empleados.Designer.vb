﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Reg_Empleados
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.NAME_E = New System.Windows.Forms.TextBox()
        Me.APE_PATERNO = New System.Windows.Forms.TextBox()
        Me.APE_MATERNO = New System.Windows.Forms.TextBox()
        Me.PASSWORD = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ID_EMPLEADO = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.EMPRESA = New System.Windows.Forms.ComboBox()
        Me.PUESTO_EMPLEADO = New System.Windows.Forms.ComboBox()
        Me.DEPARTAMENTO2 = New System.Windows.Forms.ComboBox()
        Me.FECHA_NACIMIENTO = New System.Windows.Forms.DateTimePicker()
        Me.NIVEL_SALARIAL = New System.Windows.Forms.Label()
        Me.CALLE = New System.Windows.Forms.TextBox()
        Me.NUMERO_CASA = New System.Windows.Forms.TextBox()
        Me.COLONIA = New System.Windows.Forms.TextBox()
        Me.MUNICIPIO = New System.Windows.Forms.TextBox()
        Me.CP = New System.Windows.Forms.TextBox()
        Me.EMAIL = New System.Windows.Forms.TextBox()
        Me.NO_CUENTA_BANCO = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.TELEFONO = New System.Windows.Forms.TextBox()
        Me.DELETE_E = New System.Windows.Forms.Button()
        Me.ACTUALIZAR_E = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.EMPRESA_E = New System.Windows.Forms.ComboBox()
        Me.EMAIL_2 = New System.Windows.Forms.ComboBox()
        Me.BANCO = New System.Windows.Forms.ComboBox()
        Me.ESTADO2 = New System.Windows.Forms.ComboBox()
        Me.RFC = New System.Windows.Forms.TextBox()
        Me.NSS = New System.Windows.Forms.TextBox()
        Me.CURP = New System.Windows.Forms.TextBox()
        Me.DG_EMPLEADOS = New System.Windows.Forms.DataGridView()
        Me.GERENTE_EMPRESA_CHECK = New System.Windows.Forms.CheckBox()
        Me.GERENTE_DEPTO_CHECK = New System.Windows.Forms.CheckBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.GERENTE_GENERAL = New System.Windows.Forms.Label()
        Me.GERENTE_EMPRESA = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.DEPARTAMENTO1 = New System.Windows.Forms.ComboBox()
        Me.GERENTE_DEPTO = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.LIMPIAR_CAMPOS = New System.Windows.Forms.Button()
        Me.FRECUENCIA_PAGO_EMPLEADO = New System.Windows.Forms.ComboBox()
        CType(Me.DG_EMPLEADOS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NAME_E
        '
        Me.NAME_E.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.NAME_E.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NAME_E.Location = New System.Drawing.Point(109, 244)
        Me.NAME_E.Name = "NAME_E"
        Me.NAME_E.Size = New System.Drawing.Size(145, 13)
        Me.NAME_E.TabIndex = 0
        '
        'APE_PATERNO
        '
        Me.APE_PATERNO.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.APE_PATERNO.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.APE_PATERNO.Location = New System.Drawing.Point(409, 244)
        Me.APE_PATERNO.Name = "APE_PATERNO"
        Me.APE_PATERNO.Size = New System.Drawing.Size(145, 13)
        Me.APE_PATERNO.TabIndex = 1
        '
        'APE_MATERNO
        '
        Me.APE_MATERNO.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.APE_MATERNO.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.APE_MATERNO.Location = New System.Drawing.Point(718, 244)
        Me.APE_MATERNO.Name = "APE_MATERNO"
        Me.APE_MATERNO.Size = New System.Drawing.Size(145, 13)
        Me.APE_MATERNO.TabIndex = 2
        '
        'PASSWORD
        '
        Me.PASSWORD.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.PASSWORD.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.PASSWORD.Location = New System.Drawing.Point(718, 209)
        Me.PASSWORD.Name = "PASSWORD"
        Me.PASSWORD.Size = New System.Drawing.Size(145, 13)
        Me.PASSWORD.TabIndex = 3
        Me.PASSWORD.UseSystemPasswordChar = True
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(398, 207)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(106, 17)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "No. Empleado."
        '
        'ID_EMPLEADO
        '
        Me.ID_EMPLEADO.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.ID_EMPLEADO.AutoSize = True
        Me.ID_EMPLEADO.BackColor = System.Drawing.SystemColors.Window
        Me.ID_EMPLEADO.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ID_EMPLEADO.Location = New System.Drawing.Point(516, 207)
        Me.ID_EMPLEADO.Name = "ID_EMPLEADO"
        Me.ID_EMPLEADO.Size = New System.Drawing.Size(12, 17)
        Me.ID_EMPLEADO.TabIndex = 5
        Me.ID_EMPLEADO.Text = " "
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(26, 244)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 17)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Nombre."
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(283, 244)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(120, 17)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Apellido Paterno."
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(573, 244)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(123, 17)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Apellido Materno."
        '
        'EMPRESA
        '
        Me.EMPRESA.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.EMPRESA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.EMPRESA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EMPRESA.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EMPRESA.FormattingEnabled = True
        Me.EMPRESA.Location = New System.Drawing.Point(92, 507)
        Me.EMPRESA.Name = "EMPRESA"
        Me.EMPRESA.Size = New System.Drawing.Size(215, 25)
        Me.EMPRESA.TabIndex = 9
        '
        'PUESTO_EMPLEADO
        '
        Me.PUESTO_EMPLEADO.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.PUESTO_EMPLEADO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.PUESTO_EMPLEADO.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.PUESTO_EMPLEADO.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PUESTO_EMPLEADO.FormattingEnabled = True
        Me.PUESTO_EMPLEADO.Location = New System.Drawing.Point(576, 535)
        Me.PUESTO_EMPLEADO.Name = "PUESTO_EMPLEADO"
        Me.PUESTO_EMPLEADO.Size = New System.Drawing.Size(215, 25)
        Me.PUESTO_EMPLEADO.TabIndex = 10
        '
        'DEPARTAMENTO2
        '
        Me.DEPARTAMENTO2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.DEPARTAMENTO2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DEPARTAMENTO2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.DEPARTAMENTO2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DEPARTAMENTO2.FormattingEnabled = True
        Me.DEPARTAMENTO2.Location = New System.Drawing.Point(576, 500)
        Me.DEPARTAMENTO2.Name = "DEPARTAMENTO2"
        Me.DEPARTAMENTO2.Size = New System.Drawing.Size(215, 25)
        Me.DEPARTAMENTO2.TabIndex = 11
        '
        'FECHA_NACIMIENTO
        '
        Me.FECHA_NACIMIENTO.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.FECHA_NACIMIENTO.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FECHA_NACIMIENTO.Location = New System.Drawing.Point(193, 270)
        Me.FECHA_NACIMIENTO.Name = "FECHA_NACIMIENTO"
        Me.FECHA_NACIMIENTO.Size = New System.Drawing.Size(94, 20)
        Me.FECHA_NACIMIENTO.TabIndex = 12
        '
        'NIVEL_SALARIAL
        '
        Me.NIVEL_SALARIAL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.NIVEL_SALARIAL.AutoSize = True
        Me.NIVEL_SALARIAL.BackColor = System.Drawing.SystemColors.Window
        Me.NIVEL_SALARIAL.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NIVEL_SALARIAL.Location = New System.Drawing.Point(570, 581)
        Me.NIVEL_SALARIAL.Name = "NIVEL_SALARIAL"
        Me.NIVEL_SALARIAL.Size = New System.Drawing.Size(0, 17)
        Me.NIVEL_SALARIAL.TabIndex = 13
        '
        'CALLE
        '
        Me.CALLE.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.CALLE.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CALLE.Location = New System.Drawing.Point(69, 393)
        Me.CALLE.Name = "CALLE"
        Me.CALLE.Size = New System.Drawing.Size(235, 13)
        Me.CALLE.TabIndex = 17
        '
        'NUMERO_CASA
        '
        Me.NUMERO_CASA.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.NUMERO_CASA.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NUMERO_CASA.Location = New System.Drawing.Point(462, 392)
        Me.NUMERO_CASA.Name = "NUMERO_CASA"
        Me.NUMERO_CASA.Size = New System.Drawing.Size(51, 13)
        Me.NUMERO_CASA.TabIndex = 18
        '
        'COLONIA
        '
        Me.COLONIA.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.COLONIA.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.COLONIA.Location = New System.Drawing.Point(86, 427)
        Me.COLONIA.Name = "COLONIA"
        Me.COLONIA.Size = New System.Drawing.Size(277, 13)
        Me.COLONIA.TabIndex = 19
        '
        'MUNICIPIO
        '
        Me.MUNICIPIO.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.MUNICIPIO.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MUNICIPIO.Location = New System.Drawing.Point(462, 427)
        Me.MUNICIPIO.Name = "MUNICIPIO"
        Me.MUNICIPIO.Size = New System.Drawing.Size(164, 13)
        Me.MUNICIPIO.TabIndex = 20
        '
        'CP
        '
        Me.CP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.CP.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CP.Location = New System.Drawing.Point(706, 392)
        Me.CP.Name = "CP"
        Me.CP.Size = New System.Drawing.Size(96, 13)
        Me.CP.TabIndex = 22
        '
        'EMAIL
        '
        Me.EMAIL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.EMAIL.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.EMAIL.Location = New System.Drawing.Point(94, 298)
        Me.EMAIL.Multiline = True
        Me.EMAIL.Name = "EMAIL"
        Me.EMAIL.Size = New System.Drawing.Size(128, 20)
        Me.EMAIL.TabIndex = 23
        '
        'NO_CUENTA_BANCO
        '
        Me.NO_CUENTA_BANCO.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.NO_CUENTA_BANCO.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NO_CUENTA_BANCO.Location = New System.Drawing.Point(665, 346)
        Me.NO_CUENTA_BANCO.Name = "NO_CUENTA_BANCO"
        Me.NO_CUENTA_BANCO.Size = New System.Drawing.Size(177, 13)
        Me.NO_CUENTA_BANCO.TabIndex = 25
        '
        'Label5
        '
        Me.Label5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(23, 363)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(73, 16)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Domicilio."
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(16, 393)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 17)
        Me.Label6.TabIndex = 27
        Me.Label6.Text = "Calle."
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(16, 427)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 17)
        Me.Label7.TabIndex = 28
        Me.Label7.Text = "Colonia."
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(365, 394)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(94, 17)
        Me.Label8.TabIndex = 29
        Me.Label8.Text = "No. de Casa."
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(382, 430)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(74, 17)
        Me.Label9.TabIndex = 30
        Me.Label9.Text = "Municipio."
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(644, 427)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(56, 17)
        Me.Label10.TabIndex = 31
        Me.Label10.Text = "Estado."
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(461, 508)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(109, 17)
        Me.Label11.TabIndex = 32
        Me.Label11.Text = "Departamento."
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(521, 541)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(55, 17)
        Me.Label12.TabIndex = 33
        Me.Label12.Text = "Puesto."
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(26, 273)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(152, 17)
        Me.Label13.TabIndex = 34
        Me.Label13.Text = "Fecha de Nacimiento."
        '
        'Label14
        '
        Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(382, 268)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(47, 17)
        Me.Label14.TabIndex = 35
        Me.Label14.Text = "CURP."
        '
        'Label16
        '
        Me.Label16.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(26, 333)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(37, 17)
        Me.Label16.TabIndex = 37
        Me.Label16.Text = "RFC."
        '
        'Label17
        '
        Me.Label17.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(272, 333)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(34, 17)
        Me.Label17.TabIndex = 38
        Me.Label17.Text = "NSS."
        '
        'Label18
        '
        Me.Label18.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(612, 207)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(88, 17)
        Me.Label18.TabIndex = 39
        Me.Label18.Text = "Contraseña."
        '
        'Label19
        '
        Me.Label19.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(606, 314)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(53, 17)
        Me.Label19.TabIndex = 40
        Me.Label19.Text = "Banco."
        '
        'Label20
        '
        Me.Label20.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(550, 346)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(109, 17)
        Me.Label20.TabIndex = 41
        Me.Label20.Text = "No. de Cuenta."
        '
        'Label21
        '
        Me.Label21.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(26, 299)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(47, 17)
        Me.Label21.TabIndex = 42
        Me.Label21.Text = "Email."
        '
        'Label22
        '
        Me.Label22.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(669, 391)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(31, 17)
        Me.Label22.TabIndex = 43
        Me.Label22.Text = "CP."
        '
        'Label23
        '
        Me.Label23.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(16, 189)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(117, 16)
        Me.Label23.TabIndex = 44
        Me.Label23.Text = "Datos Personales"
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Panel1.Location = New System.Drawing.Point(145, 198)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(742, 1)
        Me.Panel1.TabIndex = 45
        '
        'Label24
        '
        Me.Label24.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(23, 476)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(62, 16)
        Me.Label24.TabIndex = 47
        Me.Label24.Text = "Trabajo."
        '
        'Label25
        '
        Me.Label25.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(16, 508)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(67, 17)
        Me.Label25.TabIndex = 49
        Me.Label25.Text = "Empresa."
        '
        'Label28
        '
        Me.Label28.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(382, 298)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(66, 17)
        Me.Label28.TabIndex = 55
        Me.Label28.Text = "Telefono."
        '
        'TELEFONO
        '
        Me.TELEFONO.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.TELEFONO.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TELEFONO.Location = New System.Drawing.Point(451, 298)
        Me.TELEFONO.Name = "TELEFONO"
        Me.TELEFONO.Size = New System.Drawing.Size(145, 13)
        Me.TELEFONO.TabIndex = 54
        '
        'DELETE_E
        '
        Me.DELETE_E.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.DELETE_E.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DELETE_E.Location = New System.Drawing.Point(831, 614)
        Me.DELETE_E.Name = "DELETE_E"
        Me.DELETE_E.Size = New System.Drawing.Size(69, 35)
        Me.DELETE_E.TabIndex = 81
        Me.DELETE_E.Text = "Eliminar"
        Me.DELETE_E.UseVisualStyleBackColor = True
        '
        'ACTUALIZAR_E
        '
        Me.ACTUALIZAR_E.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.ACTUALIZAR_E.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ACTUALIZAR_E.Location = New System.Drawing.Point(755, 614)
        Me.ACTUALIZAR_E.Name = "ACTUALIZAR_E"
        Me.ACTUALIZAR_E.Size = New System.Drawing.Size(69, 35)
        Me.ACTUALIZAR_E.TabIndex = 80
        Me.ACTUALIZAR_E.Text = "Actualizar"
        Me.ACTUALIZAR_E.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Panel2.Location = New System.Drawing.Point(146, 372)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(742, 1)
        Me.Panel2.TabIndex = 46
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Panel3.Location = New System.Drawing.Point(146, 487)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(742, 1)
        Me.Panel3.TabIndex = 46
        '
        'Label30
        '
        Me.Label30.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(12, 17)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(84, 16)
        Me.Label30.TabIndex = 82
        Me.Label30.Text = "Empleados."
        '
        'Label29
        '
        Me.Label29.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(376, 56)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(67, 17)
        Me.Label29.TabIndex = 84
        Me.Label29.Text = "Empresa."
        '
        'EMPRESA_E
        '
        Me.EMPRESA_E.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.EMPRESA_E.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.EMPRESA_E.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EMPRESA_E.FormattingEnabled = True
        Me.EMPRESA_E.Location = New System.Drawing.Point(379, 83)
        Me.EMPRESA_E.Name = "EMPRESA_E"
        Me.EMPRESA_E.Size = New System.Drawing.Size(132, 21)
        Me.EMPRESA_E.TabIndex = 83
        '
        'EMAIL_2
        '
        Me.EMAIL_2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.EMAIL_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.EMAIL_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EMAIL_2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EMAIL_2.FormattingEnabled = True
        Me.EMAIL_2.Location = New System.Drawing.Point(226, 296)
        Me.EMAIL_2.Name = "EMAIL_2"
        Me.EMAIL_2.Size = New System.Drawing.Size(133, 25)
        Me.EMAIL_2.TabIndex = 86
        '
        'BANCO
        '
        Me.BANCO.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.BANCO.DisplayMember = "Afirme"
        Me.BANCO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.BANCO.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BANCO.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BANCO.FormattingEnabled = True
        Me.BANCO.Location = New System.Drawing.Point(665, 313)
        Me.BANCO.Name = "BANCO"
        Me.BANCO.Size = New System.Drawing.Size(177, 25)
        Me.BANCO.TabIndex = 87
        '
        'ESTADO2
        '
        Me.ESTADO2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.ESTADO2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ESTADO2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ESTADO2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ESTADO2.FormattingEnabled = True
        Me.ESTADO2.Location = New System.Drawing.Point(697, 423)
        Me.ESTADO2.Name = "ESTADO2"
        Me.ESTADO2.Size = New System.Drawing.Size(177, 25)
        Me.ESTADO2.TabIndex = 88
        '
        'RFC
        '
        Me.RFC.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.RFC.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RFC.Location = New System.Drawing.Point(79, 337)
        Me.RFC.Name = "RFC"
        Me.RFC.Size = New System.Drawing.Size(145, 13)
        Me.RFC.TabIndex = 89
        '
        'NSS
        '
        Me.NSS.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.NSS.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NSS.Location = New System.Drawing.Point(320, 337)
        Me.NSS.Name = "NSS"
        Me.NSS.Size = New System.Drawing.Size(145, 13)
        Me.NSS.TabIndex = 90
        '
        'CURP
        '
        Me.CURP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.CURP.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CURP.Location = New System.Drawing.Point(454, 270)
        Me.CURP.Name = "CURP"
        Me.CURP.Size = New System.Drawing.Size(145, 13)
        Me.CURP.TabIndex = 91
        '
        'DG_EMPLEADOS
        '
        Me.DG_EMPLEADOS.AllowUserToDeleteRows = False
        Me.DG_EMPLEADOS.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DG_EMPLEADOS.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DG_EMPLEADOS.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DG_EMPLEADOS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG_EMPLEADOS.Location = New System.Drawing.Point(17, 43)
        Me.DG_EMPLEADOS.Name = "DG_EMPLEADOS"
        Me.DG_EMPLEADOS.ReadOnly = True
        Me.DG_EMPLEADOS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DG_EMPLEADOS.Size = New System.Drawing.Size(348, 117)
        Me.DG_EMPLEADOS.TabIndex = 92
        '
        'GERENTE_EMPRESA_CHECK
        '
        Me.GERENTE_EMPRESA_CHECK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GERENTE_EMPRESA_CHECK.AutoSize = True
        Me.GERENTE_EMPRESA_CHECK.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GERENTE_EMPRESA_CHECK.Location = New System.Drawing.Point(355, 621)
        Me.GERENTE_EMPRESA_CHECK.Name = "GERENTE_EMPRESA_CHECK"
        Me.GERENTE_EMPRESA_CHECK.Size = New System.Drawing.Size(149, 21)
        Me.GERENTE_EMPRESA_CHECK.TabIndex = 94
        Me.GERENTE_EMPRESA_CHECK.Text = "Gerente de Empresa"
        Me.GERENTE_EMPRESA_CHECK.UseVisualStyleBackColor = True
        '
        'GERENTE_DEPTO_CHECK
        '
        Me.GERENTE_DEPTO_CHECK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GERENTE_DEPTO_CHECK.AutoSize = True
        Me.GERENTE_DEPTO_CHECK.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GERENTE_DEPTO_CHECK.Location = New System.Drawing.Point(519, 621)
        Me.GERENTE_DEPTO_CHECK.Name = "GERENTE_DEPTO_CHECK"
        Me.GERENTE_DEPTO_CHECK.Size = New System.Drawing.Size(188, 21)
        Me.GERENTE_DEPTO_CHECK.TabIndex = 95
        Me.GERENTE_DEPTO_CHECK.Text = "Gerente de Departamento"
        Me.GERENTE_DEPTO_CHECK.UseVisualStyleBackColor = True
        '
        'Label32
        '
        Me.Label32.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(536, 17)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(117, 16)
        Me.Label32.TabIndex = 96
        Me.Label32.Text = "Gerente General"
        '
        'Label33
        '
        Me.Label33.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(536, 100)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(120, 16)
        Me.Label33.TabIndex = 98
        Me.Label33.Text = "Gerente Empresa"
        '
        'Label34
        '
        Me.Label34.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(740, 14)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(157, 16)
        Me.Label34.TabIndex = 99
        Me.Label34.Text = "Gerente Departamento"
        '
        'GERENTE_GENERAL
        '
        Me.GERENTE_GENERAL.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.GERENTE_GENERAL.AutoSize = True
        Me.GERENTE_GENERAL.BackColor = System.Drawing.SystemColors.Window
        Me.GERENTE_GENERAL.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GERENTE_GENERAL.Location = New System.Drawing.Point(521, 44)
        Me.GERENTE_GENERAL.Name = "GERENTE_GENERAL"
        Me.GERENTE_GENERAL.Size = New System.Drawing.Size(148, 17)
        Me.GERENTE_GENERAL.TabIndex = 100
        Me.GERENTE_GENERAL.Text = "Sin gerente registrado"
        '
        'GERENTE_EMPRESA
        '
        Me.GERENTE_EMPRESA.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.GERENTE_EMPRESA.AutoSize = True
        Me.GERENTE_EMPRESA.BackColor = System.Drawing.SystemColors.Window
        Me.GERENTE_EMPRESA.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GERENTE_EMPRESA.Location = New System.Drawing.Point(521, 130)
        Me.GERENTE_EMPRESA.Name = "GERENTE_EMPRESA"
        Me.GERENTE_EMPRESA.Size = New System.Drawing.Size(148, 17)
        Me.GERENTE_EMPRESA.TabIndex = 104
        Me.GERENTE_EMPRESA.Text = "Sin gerente registrado"
        '
        'Label40
        '
        Me.Label40.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(797, 44)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(109, 17)
        Me.Label40.TabIndex = 108
        Me.Label40.Text = "Departamento:"
        '
        'DEPARTAMENTO1
        '
        Me.DEPARTAMENTO1.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.DEPARTAMENTO1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DEPARTAMENTO1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.DEPARTAMENTO1.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DEPARTAMENTO1.FormattingEnabled = True
        Me.DEPARTAMENTO1.Location = New System.Drawing.Point(728, 69)
        Me.DEPARTAMENTO1.Name = "DEPARTAMENTO1"
        Me.DEPARTAMENTO1.Size = New System.Drawing.Size(174, 25)
        Me.DEPARTAMENTO1.TabIndex = 107
        '
        'GERENTE_DEPTO
        '
        Me.GERENTE_DEPTO.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.GERENTE_DEPTO.AutoSize = True
        Me.GERENTE_DEPTO.BackColor = System.Drawing.SystemColors.Window
        Me.GERENTE_DEPTO.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GERENTE_DEPTO.Location = New System.Drawing.Point(731, 130)
        Me.GERENTE_DEPTO.Name = "GERENTE_DEPTO"
        Me.GERENTE_DEPTO.Size = New System.Drawing.Size(148, 17)
        Me.GERENTE_DEPTO.TabIndex = 110
        Me.GERENTE_DEPTO.Text = "Sin gerente registrado"
        '
        'Label42
        '
        Me.Label42.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(838, 100)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(64, 17)
        Me.Label42.TabIndex = 109
        Me.Label42.Text = "Gerente:"
        '
        'Label15
        '
        Me.Label15.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(16, 557)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(143, 17)
        Me.Label15.TabIndex = 115
        Me.Label15.Text = "Frecuencia de pago."
        '
        'LIMPIAR_CAMPOS
        '
        Me.LIMPIAR_CAMPOS.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.LIMPIAR_CAMPOS.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LIMPIAR_CAMPOS.Location = New System.Drawing.Point(146, 171)
        Me.LIMPIAR_CAMPOS.Name = "LIMPIAR_CAMPOS"
        Me.LIMPIAR_CAMPOS.Size = New System.Drawing.Size(101, 25)
        Me.LIMPIAR_CAMPOS.TabIndex = 116
        Me.LIMPIAR_CAMPOS.Text = "Limpiar campos"
        Me.LIMPIAR_CAMPOS.UseVisualStyleBackColor = True
        '
        'FRECUENCIA_PAGO_EMPLEADO
        '
        Me.FRECUENCIA_PAGO_EMPLEADO.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.FRECUENCIA_PAGO_EMPLEADO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.FRECUENCIA_PAGO_EMPLEADO.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.FRECUENCIA_PAGO_EMPLEADO.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FRECUENCIA_PAGO_EMPLEADO.FormattingEnabled = True
        Me.FRECUENCIA_PAGO_EMPLEADO.Location = New System.Drawing.Point(165, 554)
        Me.FRECUENCIA_PAGO_EMPLEADO.Name = "FRECUENCIA_PAGO_EMPLEADO"
        Me.FRECUENCIA_PAGO_EMPLEADO.Size = New System.Drawing.Size(177, 25)
        Me.FRECUENCIA_PAGO_EMPLEADO.TabIndex = 117
        '
        'Reg_Empleados
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(912, 666)
        Me.Controls.Add(Me.FRECUENCIA_PAGO_EMPLEADO)
        Me.Controls.Add(Me.LIMPIAR_CAMPOS)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.GERENTE_DEPTO)
        Me.Controls.Add(Me.Label42)
        Me.Controls.Add(Me.Label40)
        Me.Controls.Add(Me.DEPARTAMENTO1)
        Me.Controls.Add(Me.GERENTE_EMPRESA)
        Me.Controls.Add(Me.GERENTE_GENERAL)
        Me.Controls.Add(Me.Label34)
        Me.Controls.Add(Me.Label33)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.GERENTE_DEPTO_CHECK)
        Me.Controls.Add(Me.GERENTE_EMPRESA_CHECK)
        Me.Controls.Add(Me.DG_EMPLEADOS)
        Me.Controls.Add(Me.CURP)
        Me.Controls.Add(Me.NSS)
        Me.Controls.Add(Me.RFC)
        Me.Controls.Add(Me.ESTADO2)
        Me.Controls.Add(Me.BANCO)
        Me.Controls.Add(Me.EMAIL_2)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.EMPRESA_E)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.DELETE_E)
        Me.Controls.Add(Me.ACTUALIZAR_E)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.TELEFONO)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.NO_CUENTA_BANCO)
        Me.Controls.Add(Me.EMAIL)
        Me.Controls.Add(Me.CP)
        Me.Controls.Add(Me.MUNICIPIO)
        Me.Controls.Add(Me.COLONIA)
        Me.Controls.Add(Me.NUMERO_CASA)
        Me.Controls.Add(Me.CALLE)
        Me.Controls.Add(Me.NIVEL_SALARIAL)
        Me.Controls.Add(Me.FECHA_NACIMIENTO)
        Me.Controls.Add(Me.DEPARTAMENTO2)
        Me.Controls.Add(Me.PUESTO_EMPLEADO)
        Me.Controls.Add(Me.EMPRESA)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ID_EMPLEADO)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PASSWORD)
        Me.Controls.Add(Me.APE_MATERNO)
        Me.Controls.Add(Me.APE_PATERNO)
        Me.Controls.Add(Me.NAME_E)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Reg_Empleados"
        Me.Text = "Empleados"
        CType(Me.DG_EMPLEADOS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents NAME_E As TextBox
    Friend WithEvents APE_PATERNO As TextBox
    Friend WithEvents APE_MATERNO As TextBox
    Friend WithEvents PASSWORD As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents ID_EMPLEADO As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents EMPRESA As ComboBox
    Friend WithEvents PUESTO_EMPLEADO As ComboBox
    Friend WithEvents DEPARTAMENTO2 As ComboBox
    Friend WithEvents FECHA_NACIMIENTO As DateTimePicker
    Friend WithEvents NIVEL_SALARIAL As Label
    Friend WithEvents CALLE As TextBox
    Friend WithEvents NUMERO_CASA As TextBox
    Friend WithEvents COLONIA As TextBox
    Friend WithEvents MUNICIPIO As TextBox
    Friend WithEvents CP As TextBox
    Friend WithEvents EMAIL As TextBox
    Friend WithEvents NO_CUENTA_BANCO As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label24 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents TELEFONO As TextBox
    Friend WithEvents DELETE_E As Button
    Friend WithEvents ACTUALIZAR_E As Button
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label30 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents EMPRESA_E As ComboBox
    Friend WithEvents EMAIL_2 As ComboBox
    Friend WithEvents BANCO As ComboBox
    Friend WithEvents ESTADO2 As ComboBox
    Friend WithEvents RFC As TextBox
    Friend WithEvents NSS As TextBox
    Friend WithEvents CURP As TextBox
    Friend WithEvents DG_EMPLEADOS As DataGridView
    Friend WithEvents GERENTE_EMPRESA_CHECK As CheckBox
    Friend WithEvents GERENTE_DEPTO_CHECK As CheckBox
    Friend WithEvents Label32 As Label
    Friend WithEvents Label33 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents GERENTE_GENERAL As Label
    Friend WithEvents GERENTE_EMPRESA As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents DEPARTAMENTO1 As ComboBox
    Friend WithEvents GERENTE_DEPTO As Label
    Friend WithEvents Label42 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents LIMPIAR_CAMPOS As Button
    Friend WithEvents FRECUENCIA_PAGO_EMPLEADO As ComboBox
End Class
