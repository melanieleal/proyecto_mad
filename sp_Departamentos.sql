USE Proyect_MAD 

GO
IF EXISTS (SELECT * FROM sysobjects WHERE name = 'SP_NameDepartamento' AND type = 'P' )
BEGIN
	DROP PROCEDURE SP_NameDepartamento
END
GO 
CREATE PROCEDURE SP_NameDepartamento(
    @Opc	              char(1),
	@ID_RFC     	      varchar(13) = null, --FK
	@No_Departamento      int = null, 
	@Nombre               varchar(30) = null
)
AS 
BEGIN
 	IF @Opc = 'S'
	BEGIN
		SELECT No_Departamento,
			   Nombre,
		       ID_RFC 			 		   
			 FROM Departamentos 
			 WHERE ID_RFC = @ID_RFC
			 AND Activo = 1
			 
			   
	END 
END

GO
IF EXISTS (SELECT * FROM sysobjects WHERE name = 'sp_Departamentos' AND type = 'P' )
BEGIN
	DROP PROCEDURE sp_Departamentos
END
GO 
CREATE PROCEDURE sp_Departamentos(
	@Opc	              char(1),	
	@No_Departamento      int = null, 
	@Nombre               varchar(30) = null, 
	@Sueldo_Base          float = null,
	@ID_RFC     	      varchar(13) = null, --FK
	@No_Empleado          int = null --FK
)
AS
		DECLARE     @Activo         bit
			SET     @Activo = 1 
	    
BEGIN

	IF @Opc = 'I'
	BEGIN
	   
		INSERT INTO  Departamentos (Nombre, Sueldo_Base, ID_RFC, Activo)
		VALUES (@Nombre,@Sueldo_Base, @ID_RFC, @Activo)
	END

	IF @Opc = 'X'
	BEGIN
		SELECT D.No_Departamento 'Clave',
			   D.Nombre,
		       D.Sueldo_Base 'Sueldo Base',
			   E.ID_RFC 'RFC Empresa',
			   E.Razon_Social 'Empresa',
			   D.No_Empleado 'Gerente',
			   D.Activo 
			   FROM Departamentos D 
				  INNER JOIN Empresas E
				  ON E.ID_RFC = D.ID_RFC 
			  WHERE D.Activo = @Activo	   
			   
	END 

	If @Opc = 'M'
	BEGIN

		UPDATE Departamentos  
				SET	 Sueldo_Base  = @Sueldo_Base
			 WHERE No_Departamento  = @No_Departamento 
			AND Activo = @Activo

	END

	IF  @Opc = 'G'---------------------------------------------------
	BEGIN
	           UPDATE Departamentos 
				SET	 
					No_Empleado = null
				WHERE No_Empleado = @No_Empleado
	         


			UPDATE Departamentos 
				SET	 
					No_Empleado = ISNULL(@No_Empleado, No_Empleado) 
				WHERE No_Departamento = @No_Departamento
	END


	IF @Opc = 'D' --////////////////////////////////////
	
			BEGIN
		
					DELETE 
					FROM Departamentos
					WHERE No_Departamento = @No_Departamento
					AND Activo = @Activo
		
			END

END
