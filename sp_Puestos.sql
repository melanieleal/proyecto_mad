USE Proyect_MAD 

GO
IF EXISTS (SELECT * FROM sysobjects WHERE name = 'sp_GetNamePuesto' AND type = 'P' )
BEGIN
	DROP PROCEDURE sp_GetNamePuesto
END
GO 
CREATE PROCEDURE sp_GetNamePuesto(
	@Opc                char(1),
	@Id_Puesto			int = null,
	@Nombre				varchar(30) = null,
	@No_Departamento	int = null,
	@ID_RFC             varchar(13) = null
)
AS

BEGIN
		
		IF @Opc = 'S'
		BEGIN
			SELECT Id_Puesto ,
				   Nombre,
				   No_Departamento ,
				   ID_RFC	 		   
				 FROM Puestos 
				 WHERE No_Departamento = @No_Departamento AND ID_RFC = @ID_RFC 
				 AND Activo = 1
		END
END

GO
IF EXISTS (SELECT * FROM sysobjects WHERE name = 'sp_Puestos' AND type = 'P' )
BEGIN
	DROP PROCEDURE sp_Puestos
END
GO 
CREATE PROCEDURE sp_Puestos(
	@Opc				char(1),
	@Id_Puesto			int = null,
	@Nombre				varchar(30) = null,
	@Nivel_Salarial		decimal(5,2) = null,
	@No_Departamento	int = null,
	@ID_RFC				varchar(13) = null
)
AS
		DECLARE @Activo		bit
				SET @Activo = 1

BEGIN
	
	IF @Opc = 'I'
	BEGIN
		
		INSERT INTO Puestos (Nombre, Nivel_Salarial,No_Departamento, ID_RFC, Activo )
		VALUES(@Nombre, @Nivel_Salarial, @No_Departamento, @ID_RFC, @Activo)

	END

	IF @Opc = 'M'
	BEGIN

		UPDATE Puestos  
				SET	 Nivel_Salarial   = @Nivel_Salarial 
			 WHERE Id_Puesto   = @Id_Puesto 
			AND Activo = @Activo

	END


	IF @Opc = 'X'
	BEGIN
		
		SELECT p.ID_Puesto 'Clave', 
			   P.Nombre, 
		       P.Nivel_Salarial 'Nivel Salarial',
			   P.No_Departamento 'No. Departamento', 
			   D.Nombre 'Departamento',
			   P.ID_RFC 'RFC Empresa',
			   E.Razon_Social 'Empresa',
			   P.Activo 
		FROM Puestos P
			INNER JOIN Departamentos D
			ON P.No_Departamento = D.No_Departamento
			INNER JOIN Empresas E
			ON  P.ID_RFC = E.ID_RFC 
		WHERE P.Activo = @Activo

	END

	IF @Opc = 'D' --////////////////////////////////////
	
			BEGIN
		
					DELETE 
					FROM Puestos
					WHERE ID_Puesto = @Id_Puesto
					AND Activo = @Activo
		
			END

END