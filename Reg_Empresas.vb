﻿Public Class Reg_Empresas
    Dim conexion As New EnlaceBD
    Dim tabla_empresas As New DataTable
    Dim val As New Funciones
    Dim v As Boolean = False
    Dim modificar As Boolean = False
    Dim id_E As String
    Dim obj As New EnlaceBD
    Dim result As Boolean = False


    Dim Frecuencia As New List(Of Integer)


    Private Sub Reg_Empresas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ESTADO_EMP.Items.Add("Aguascalientes")
        ESTADO_EMP.Items.Add("Baja California")
        ESTADO_EMP.Items.Add("Baja California Sur")
        ESTADO_EMP.Items.Add("Campeche")
        ESTADO_EMP.Items.Add("Chiapas")
        ESTADO_EMP.Items.Add("Chihuahua")
        ESTADO_EMP.Items.Add("Ciudad de Mexico")
        ESTADO_EMP.Items.Add("Coahuila")
        ESTADO_EMP.Items.Add("Colima")
        ESTADO_EMP.Items.Add("Durango")
        ESTADO_EMP.Items.Add("Guanajuato")
        ESTADO_EMP.Items.Add("Guerrero")
        ESTADO_EMP.Items.Add("Hidalgo")
        ESTADO_EMP.Items.Add("Jalisco")
        ESTADO_EMP.Items.Add("Michoacan")
        ESTADO_EMP.Items.Add("Morelos")
        ESTADO_EMP.Items.Add("Nayarit")
        ESTADO_EMP.Items.Add("Nuevo Leon")
        ESTADO_EMP.Items.Add("Oaxaca")
        ESTADO_EMP.Items.Add("Puebla")
        ESTADO_EMP.Items.Add("Queretaro")
        ESTADO_EMP.Items.Add("Quintana Roo")
        ESTADO_EMP.Items.Add("San Luis Potosi")
        ESTADO_EMP.Items.Add("Sonora")
        ESTADO_EMP.Items.Add("Sinaloa")
        ESTADO_EMP.Items.Add("Tabasco")
        ESTADO_EMP.Items.Add("Tlaxcala")
        ESTADO_EMP.Items.Add("Veracruz")
        ESTADO_EMP.Items.Add("Yucatan")
        ESTADO_EMP.Items.Add("Zacatecas")

        tabla_empresas = conexion.Get_Empresas()
        DG_EMPRESAS.DataSource = tabla_empresas



    End Sub

    Private Sub RAZON_SOCIAL_KeyPress(sender As Object, e As KeyPressEventArgs) Handles RAZON_SOCIAL.KeyPress, CALLE_EMP.KeyPress, COLONIA_EMP.KeyPress
        'Codigo para letras, numeros y espacios
        val.EvaluaSoloLetrasYNumerosConEspacios(e)

    End Sub

    Private Sub REG_PATRONAL_KeyPress(sender As Object, e As KeyPressEventArgs) Handles REG_PATRONAL.KeyPress, RFC_EMPRESA.KeyPress
        'Codigo para letras mayusculas y numeros
        val.EvaluaSoloLetrasMayusculasYNumeros(e)


    End Sub

    Private Sub MUNICIPIO_EMP_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MUNICIPIO_EMP.KeyPress
        'Codigo letras y espacios
        val.EvaluaSoloLetras(e)

    End Sub

    Private Sub CP_EMP_KeyPress(sender As Object, e As KeyPressEventArgs) Handles CP_EMP.KeyPress, NUMERO_EXT_EMP.KeyPress, PHONE_EMP.KeyPress
        'Codigo para solo numeros
        val.EvaluaSoloNumeros(e)
    End Sub

    Private Sub ACTUALIZAR_EMP_Click(sender As Object, e As EventArgs) Handles ACTUALIZAR_EMP.Click
        Dim opc As String
        Dim r_social As String
        Dim r_patronal As String
        Dim rfc As String
        Dim fecha_I_Op As String
        Dim telefono As String
        Dim calle As String
        Dim no_exterior As Integer
        Dim colonia As String
        Dim CP As Integer
        Dim estado As String
        Dim municipio As String
        Dim i As Integer = 0
        Frecuencia.Clear()

        v = False

        If MENSUAL.CheckState = 1 Or SEMANAL.CheckState = 1 Or QUINCENAL.CheckState = 1 Or CATORCENAL.CheckState = 1 Then
            i = 1
        End If

        If Trim(RAZON_SOCIAL.Text) = "" Or
           Trim(REG_PATRONAL.Text) = "" Or
           Trim(RFC_EMPRESA.Text) = "" Or
           Format(FECHA_EMP.Value, "yyyy") >= Date.Now.Date.Year - 9 Or
           Trim(CALLE_EMP.Text) = "" Or
           Trim(NUMERO_EXT_EMP.Text) = "" Or
           Trim(COLONIA_EMP.Text) = "" Or
           Trim(CP_EMP.Text) = "" Or
           Trim(ESTADO_EMP.Text) = "" Or
           Trim(MUNICIPIO_EMP.Text) = "" Or
           Len(Trim(CANTIDAD.Text)) = 0 Or Len(Trim(CANTIDAD1.Text)) = 0 Or Len(Trim(CANTIDAD2.Text)) = 0 Or Len(Trim(CANTIDAD3.Text)) = 0 Or Len(Trim(CANTIDAD4.Text)) = 0 Or
           i = 0 Then

            MessageBox.Show(" -Los campos con * son obligatorio" & vbCrLf & " -Su empresa debe de tener 10 años de antiguedad", "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Warning)

        Else


            If Len(Trim(NUMERO_EXT_EMP.Text)) <> 0 Or Len(Trim(PHONE_EMP.Text)) <> 0 Then
                If Len(Trim(PHONE_EMP.Text)) > 11 Or Len(Trim(NUMERO_EXT_EMP.Text)) > 6 Then
                    v = True
                End If
            End If

            If Len(Trim(RFC_EMPRESA.Text)) <> 13 Or Len(Trim(REG_PATRONAL.Text)) <> 13 Or Len(Trim(CP_EMP.Text)) <> 5 Then
                v = True
            End If

            If v Then

                MessageBox.Show("Tome en cuenta" & vbCrLf & "  *RFC y Registro Patronal son solo de 13 digitos " & vbCrLf & "  *Telefono es de 10 digitos" & vbCrLf & "  *El numero interior/exterior es de maximo 5 digitos" & vbCrLf & "  *CP es de 5 digitos", "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Warning)

            Else
                Dim monto As Decimal = CANTIDAD.Text

                Dim monto1 As Decimal = CANTIDAD1.Text

                Dim monto2 As Decimal = CANTIDAD2.Text

                Dim monto3 As Decimal = CANTIDAD3.Text

                Dim monto4 As Decimal = CANTIDAD4.Text

                r_social = RAZON_SOCIAL.Text
                r_patronal = REG_PATRONAL.Text
                rfc = RFC_EMPRESA.Text
                fecha_I_Op = Format(FECHA_EMP.Value, "yyyy-MM-dd")
                telefono = PHONE_EMP.Text
                calle = CALLE_EMP.Text
                colonia = COLONIA_EMP.Text
                estado = ESTADO_EMP.Text
                municipio = MUNICIPIO_EMP.Text
                CP = CP_EMP.Text

                If Len(Trim(NUMERO_EXT_EMP.Text)) <> 0 Then
                    no_exterior = NUMERO_EXT_EMP.Text
                End If

                If MENSUAL.CheckState = 1 Then
                    Frecuencia.Add(4)
                End If
                If SEMANAL.CheckState = 1 Then
                    Frecuencia.Add(1)
                End If
                If QUINCENAL.CheckState = 1 Then
                    Frecuencia.Add(2)
                End If
                If CATORCENAL.CheckState = 1 Then
                    Frecuencia.Add(3)
                End If

                If modificar = False Then
                    opc = "I"

                    result = obj.Add_Empresa(opc, r_social, r_patronal, rfc, fecha_I_Op, telefono, calle, no_exterior, colonia, CP, estado, municipio, Frecuencia, monto2, monto3, monto, monto4, monto1)

                    If result Then
                        MessageBox.Show("La Empresa ha sido añadido exitosamente", "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        CANTIDAD.Clear()
                        CANTIDAD1.Clear()
                        CANTIDAD2.Clear()
                        CANTIDAD3.Clear()
                        CANTIDAD4.Clear()

                        RAZON_SOCIAL.Clear()
                        REG_PATRONAL.Clear()
                        RFC_EMPRESA.Clear()
                        FECHA_EMP.Value = Date.Today
                        PHONE_EMP.Clear()
                        CALLE_EMP.Clear()
                        COLONIA_EMP.Clear()
                        MUNICIPIO_EMP.Clear()
                        CP_EMP.Clear()
                        NUMERO_EXT_EMP.Clear()
                        MENSUAL.Enabled() = True
                        CATORCENAL.Enabled() = True
                        QUINCENAL.Enabled() = True
                        SEMANAL.Enabled() = True
                        MENSUAL.Checked() = False
                        CATORCENAL.Checked() = False
                        QUINCENAL.Checked() = False
                        SEMANAL.Checked() = False

                    Else
                        MessageBox.Show("Hubo un error, su Empresa no se añadio", "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)


                    End If

                    Else
                        opc = "M"

                    result = obj.Edit_Empresa(opc, r_social, r_patronal, rfc, fecha_I_Op, telefono, calle, no_exterior, colonia, CP, estado, municipio, Frecuencia, monto2, monto3, monto, monto4, monto1)

                    If result Then
                        MessageBox.Show("La Empresa ha sido modificado exitosamente", "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        CANTIDAD.Clear()
                        CANTIDAD1.Clear()
                        CANTIDAD2.Clear()
                        CANTIDAD3.Clear()
                        CANTIDAD4.Clear()

                        RAZON_SOCIAL.Clear()
                        REG_PATRONAL.Clear()
                        RFC_EMPRESA.Clear()
                        FECHA_EMP.Value = Date.Today
                        PHONE_EMP.Clear()
                        CALLE_EMP.Clear()
                        COLONIA_EMP.Clear()
                        MUNICIPIO_EMP.Clear()
                        CP_EMP.Clear()
                        NUMERO_EXT_EMP.Clear()
                        MENSUAL.Enabled() = True
                        CATORCENAL.Enabled() = True
                        QUINCENAL.Enabled() = True
                        SEMANAL.Enabled() = True
                        MENSUAL.Checked() = False
                        CATORCENAL.Checked() = False
                        QUINCENAL.Checked() = False
                        SEMANAL.Checked() = False
                        modificar = False
                    Else
                        MessageBox.Show("Hubo un error, su Empresa no se modificado", "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                    End If

                End If

                tabla_empresas = conexion.Get_Empresas()
                DG_EMPRESAS.DataSource = tabla_empresas

            End If

        End If

    End Sub

    Private Sub DG_EMPRESAS_Click(sender As Object, e As EventArgs) Handles DG_EMPRESAS.Click


        If (Len(Trim(DG_EMPRESAS.CurrentRow.Cells.Item(0).Value.ToString())) <> 0) Then

            MENSUAL.Enabled() = False
            CATORCENAL.Enabled() = False
            QUINCENAL.Enabled() = False
            SEMANAL.Enabled() = False
            modificar = True

            RFC_EMPRESA.Text = DG_EMPRESAS.CurrentRow.Cells.Item(0).Value.ToString()
            RAZON_SOCIAL.Text = DG_EMPRESAS.CurrentRow.Cells.Item(1).Value.ToString()
            REG_PATRONAL.Text = DG_EMPRESAS.CurrentRow.Cells.Item(2).Value.ToString()
            FECHA_EMP.Value = DG_EMPRESAS.CurrentRow.Cells.Item(3).Value.ToString()
            PHONE_EMP.Text = DG_EMPRESAS.CurrentRow.Cells.Item(4).Value.ToString()
            CALLE_EMP.Text = DG_EMPRESAS.CurrentRow.Cells.Item(5).Value.ToString()
            NUMERO_EXT_EMP.Text = DG_EMPRESAS.CurrentRow.Cells.Item(6).Value.ToString()
            COLONIA_EMP.Text = DG_EMPRESAS.CurrentRow.Cells.Item(7).Value.ToString()
            ESTADO_EMP.Text = DG_EMPRESAS.CurrentRow.Cells.Item(8).Value.ToString()
            CP_EMP.Text = DG_EMPRESAS.CurrentRow.Cells.Item(9).Value.ToString()
            MUNICIPIO_EMP.Text = DG_EMPRESAS.CurrentRow.Cells.Item(10).Value.ToString()

            CANTIDAD.Text = DG_EMPRESAS.CurrentRow.Cells.Item(13).Value.ToString()

            CANTIDAD1.Text = DG_EMPRESAS.CurrentRow.Cells.Item(15).Value.ToString()

            CANTIDAD2.Text = DG_EMPRESAS.CurrentRow.Cells.Item(11).Value.ToString()

            CANTIDAD3.Text = DG_EMPRESAS.CurrentRow.Cells.Item(12).Value.ToString()

            CANTIDAD4.Text = DG_EMPRESAS.CurrentRow.Cells.Item(14).Value.ToString()


            Dim tabla_frec As DataTable
            tabla_frec = conexion.Get_Frecuencia(RFC_EMPRESA.Text, "X")
            Dim frec As Integer
            frec = tabla_frec.Rows().Count

            For check As Integer = 0 To frec - 1 Step 1
                If tabla_frec.Rows(check).Item(1) = 1 Then
                    SEMANAL.Checked = True
                End If
                If tabla_frec.Rows(check).Item(1) = 2 Then
                    QUINCENAL.Checked = True
                End If
                If tabla_frec.Rows(check).Item(1) = 3 Then
                    CATORCENAL.Checked = True
                End If
                If tabla_frec.Rows(check).Item(1) = 4 Then
                    MENSUAL.Checked = True
                End If

            Next

        Else
            RAZON_SOCIAL.Clear()
            REG_PATRONAL.Clear()
            RFC_EMPRESA.Clear()
            FECHA_EMP.Value = Date.Today
            PHONE_EMP.Clear()
            CALLE_EMP.Clear()
            COLONIA_EMP.Clear()
            MUNICIPIO_EMP.Clear()
            CP_EMP.Clear()
            NUMERO_EXT_EMP.Clear()
            MENSUAL.Enabled() = True
            CATORCENAL.Enabled() = True
            QUINCENAL.Enabled() = True
            SEMANAL.Enabled() = True
            MENSUAL.Checked() = False
            CATORCENAL.Checked() = False
            QUINCENAL.Checked() = False
            SEMANAL.Checked() = False

            CANTIDAD.Clear()
            CANTIDAD1.Clear()
            CANTIDAD2.Clear()
            CANTIDAD3.Clear()
            CANTIDAD4.Clear()

            modificar = False

        End If
    End Sub

    Private Sub DELETE_EMP_Click(sender As Object, e As EventArgs) Handles DELETE_EMP.Click
        If (modificar = True) Then

            If MessageBox.Show("Seguro que Desea Eliminar", "Empresas", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then


                result = obj.Delete_Empresas(DG_EMPRESAS.CurrentRow.Cells.Item(0).Value.ToString())

                If result Then
                    MessageBox.Show("La Empresa ha sido eliminado exitosamente", "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    RAZON_SOCIAL.Clear()
                    REG_PATRONAL.Clear()
                    RFC_EMPRESA.Clear()
                    FECHA_EMP.Value = Date.Today
                    PHONE_EMP.Clear()
                    CALLE_EMP.Clear()
                    COLONIA_EMP.Clear()
                    MUNICIPIO_EMP.Clear()
                    CP_EMP.Clear()
                    NUMERO_EXT_EMP.Clear()
                    MENSUAL.Enabled() = True
                    CATORCENAL.Enabled() = True
                    QUINCENAL.Enabled() = True
                    SEMANAL.Enabled() = True
                    MENSUAL.Checked() = False
                    CATORCENAL.Checked() = False
                    QUINCENAL.Checked() = False
                    SEMANAL.Checked() = False

                    CANTIDAD.Clear()
                    CANTIDAD1.Clear()
                    CANTIDAD2.Clear()
                    CANTIDAD3.Clear()
                    CANTIDAD4.Clear()

                    tabla_empresas = conexion.Get_Empresas()
                    DG_EMPRESAS.DataSource = tabla_empresas
                    modificar = False
                Else
                    MessageBox.Show("Hubo un error, su Empresa no se elimino", "Empresas", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                End If

            Else
                RAZON_SOCIAL.Clear()
                REG_PATRONAL.Clear()
                RFC_EMPRESA.Clear()
                FECHA_EMP.Value = Date.Today
                PHONE_EMP.Clear()
                CALLE_EMP.Clear()
                COLONIA_EMP.Clear()
                MUNICIPIO_EMP.Clear()
                CP_EMP.Clear()
                NUMERO_EXT_EMP.Clear()
                MENSUAL.Enabled() = True
                CATORCENAL.Enabled() = True
                QUINCENAL.Enabled() = True
                SEMANAL.Enabled() = True
                MENSUAL.Checked() = False
                CATORCENAL.Checked() = False
                QUINCENAL.Checked() = False
                SEMANAL.Checked() = False

                CANTIDAD.Clear()
                CANTIDAD1.Clear()
                CANTIDAD2.Clear()
                CANTIDAD3.Clear()
                CANTIDAD4.Clear()

                tabla_empresas = conexion.Get_Empresas()
                DG_EMPRESAS.DataSource = tabla_empresas
                modificar = False
            End If

        End If

    End Sub

    Private Sub CANTIDAD_KeyPress(sender As Object, e As KeyPressEventArgs) Handles CANTIDAD.KeyPress
        'Codigo con numeros decimales
        Dim v As Boolean
        Dim Cadena As String = CANTIDAD.Text
        Dim Filtro As String = "1234567890"
        If Len(Cadena) > 0 Then
            Filtro += "."
        End If

        For Each caracter In Filtro
            If e.KeyChar = caracter Then
                e.Handled = False
                v = True
                Exit For
            Else
                e.Handled = True
                v = False
            End If
        Next

        If Char.IsControl(e.KeyChar) Then
            e.Handled = False
            v = True
        End If

        If e.KeyChar = "." And Not Cadena.IndexOf(".") Then
            e.Handled = True
            v = False
        End If

        If v = False Then
            MessageBox.Show("Ingrese solo  Numeros con un numero decimal" & vbNewLine & " No Ingrese espacios", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub CANTIDAD1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles CANTIDAD1.KeyPress
        'Codigo con numeros decimales
        Dim v As Boolean
        Dim Cadena As String = CANTIDAD1.Text
        Dim Filtro As String = "1234567890"
        If Len(Cadena) > 0 Then
            Filtro += "."
        End If

        For Each caracter In Filtro
            If e.KeyChar = caracter Then
                e.Handled = False
                v = True
                Exit For
            Else
                e.Handled = True
                v = False
            End If
        Next

        If Char.IsControl(e.KeyChar) Then
            e.Handled = False
            v = True
        End If

        If e.KeyChar = "." And Not Cadena.IndexOf(".") Then
            e.Handled = True
            v = False
        End If

        If v = False Then
            MessageBox.Show("Ingrese solo  Numeros con un numero decimal" & vbNewLine & " No Ingrese espacios", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub CANTIDAD2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles CANTIDAD2.KeyPress
        'Codigo con numeros decimales
        Dim v As Boolean
        Dim Cadena As String = CANTIDAD2.Text
        Dim Filtro As String = "1234567890"
        If Len(Cadena) > 0 Then
            Filtro += "."
        End If

        For Each caracter In Filtro
            If e.KeyChar = caracter Then
                e.Handled = False
                v = True
                Exit For
            Else
                e.Handled = True
                v = False
            End If
        Next

        If Char.IsControl(e.KeyChar) Then
            e.Handled = False
            v = True
        End If

        If e.KeyChar = "." And Not Cadena.IndexOf(".") Then
            e.Handled = True
            v = False
        End If

        If v = False Then
            MessageBox.Show("Ingrese solo  Numeros con un numero decimal" & vbNewLine & " No Ingrese espacios", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub CANTIDAD3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles CANTIDAD3.KeyPress
        'Codigo con numeros decimales
        Dim v As Boolean
        Dim Cadena As String = CANTIDAD3.Text
        Dim Filtro As String = "1234567890"
        If Len(Cadena) > 0 Then
            Filtro += "."
        End If

        For Each caracter In Filtro
            If e.KeyChar = caracter Then
                e.Handled = False
                v = True
                Exit For
            Else
                e.Handled = True
                v = False
            End If
        Next

        If Char.IsControl(e.KeyChar) Then
            e.Handled = False
            v = True
        End If

        If e.KeyChar = "." And Not Cadena.IndexOf(".") Then
            e.Handled = True
            v = False
        End If

        If v = False Then
            MessageBox.Show("Ingrese solo  Numeros con un numero decimal" & vbNewLine & " No Ingrese espacios", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub CANTIDAD4_KeyPress(sender As Object, e As KeyPressEventArgs) Handles CANTIDAD4.KeyPress
        'Codigo con numeros decimales
        Dim v As Boolean
        Dim Cadena As String = CANTIDAD3.Text
        Dim Filtro As String = "1234567890"
        If Len(Cadena) > 0 Then
            Filtro += "."
        End If

        For Each caracter In Filtro
            If e.KeyChar = caracter Then
                e.Handled = False
                v = True
                Exit For
            Else
                e.Handled = True
                v = False
            End If
        Next

        If Char.IsControl(e.KeyChar) Then
            e.Handled = False
            v = True
        End If

        If e.KeyChar = "." And Not Cadena.IndexOf(".") Then
            e.Handled = True
            v = False
        End If

        If v = False Then
            MessageBox.Show("Ingrese solo  Numeros con un numero decimal" & vbNewLine & " No Ingrese espacios", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub

End Class