﻿Public Class Rep_Nomina
    Dim conexion As New EnlaceBD
    Dim tabla As New DataTable
    Dim tabla_empresas As New DataTable
    Dim tablaD As New DataTable
    Dim tabla_sumatorias As New DataTable

    Private Sub Rep_Nomina_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        tabla = conexion.Get_Empresas()
        Me.EMPRESA_N.DataSource = tabla
        Me.EMPRESA_N.DisplayMember = "Nombre"
        Me.EMPRESA_N.ValueMember = "RFC"


        MES_N.Items.Add("Enero")
        MES_N.Items.Add("Febrero")
        MES_N.Items.Add("Marzo")
        MES_N.Items.Add("Abril")
        MES_N.Items.Add("Mayo")
        MES_N.Items.Add("Junio")
        MES_N.Items.Add("Julio")
        MES_N.Items.Add("Agosto")
        MES_N.Items.Add("Septiembre")
        MES_N.Items.Add("Octubre")
        MES_N.Items.Add("Noviembre")
        MES_N.Items.Add("Diciembre")
        MES_N.Text = "Enero"

    End Sub

    Private Sub EMPRESA_N_SelectedIndexChanged(sender As Object, e As EventArgs) Handles EMPRESA_N.SelectedIndexChanged
        Dim rfc_ As String
        Dim i As Integer
        Dim depa As Integer

        rfc_ = EMPRESA_N.Text
        tabla = conexion.Get_Empresas()         'obtengo el rfc de la empresa 
        i = tabla.Rows.Count
        For fila As Integer = 0 To i - 1 Step 1
            If tabla.Rows(fila).Item(1) = rfc_ Then
                rfc_ = tabla.Rows(fila).Item(0)
            End If
        Next

        tabla = conexion.Get_NameDepartamento(rfc_)
        i = tabla.Rows.Count
        If i = 0 Then
            Me.DEPA_N.DataSource = tabla
            Me.DEPA_N.DisplayMember = "Nombre"
            Me.DEPA_N.ValueMember = "No_Departamento"
        Else
            For fila As Integer = 0 To i - 1 Step 1
                If tabla.Rows(fila).Item(2) = rfc_ Then
                    depa = tabla.Rows(fila).Item(0)
                    Me.DEPA_N.DataSource = tabla
                    Me.DEPA_N.DisplayMember = "Nombre"
                    Me.DEPA_N.ValueMember = "No_Departamento"
                End If
            Next
        End If

        'AÑOS OPERANDO--------------------------------------

        AÑO_N.Items.Clear()
        Dim fecha_Inicio_Op As DateTime
        tabla_empresas = conexion.Get_AnioEmpresa(rfc_)
        If (tabla_empresas.Rows.Count <> 0) Then

            fecha_Inicio_Op = tabla_empresas.Rows(0).Item(1)
            AÑO_N.Items.Add(Format(fecha_Inicio_Op, "yyyy"))
            AÑO_N.Text = Format(fecha_Inicio_Op, "yyyy")
            Dim anio_Inicio As Integer = Format(fecha_Inicio_Op, "yyyy")
            Dim anios_Operando As Integer = Date.Now.Date.Year - Format(fecha_Inicio_Op, "yyyy")
            If (anios_Operando <> 0) Then
                For anio As Integer = 1 To anios_Operando Step 1
                    anio_Inicio = anio_Inicio + 1
                    AÑO_N.Items.Add(anio_Inicio)
                Next
            End If
        End If


    End Sub

    Private Sub BUSCAR_N_MouseClick(sender As Object, e As MouseEventArgs) Handles BUSCAR_N.MouseClick

        Dim rfc_ As String
        tabla_empresas = conexion.Get_Empresas()         'obtengo el rfc de la empresa 
        Dim i = tabla_empresas.Rows.Count
        For fila As Integer = 0 To i - 1 Step 1
            If tabla_empresas.Rows(fila).Item(1) = EMPRESA_N.Text Then
                rfc_ = tabla_empresas.Rows(fila).Item(0)
            End If
        Next

        Dim depa As Integer
        tablaD = conexion.Get_NameDepartamento(rfc_)
        Dim d1 = tablaD.Rows.Count

        For fila As Integer = 0 To d1 - 1 Step 1
            If tablaD.Rows(fila).Item(2) = rfc_ And tablaD.Rows(fila).Item(1) = DEPA_N.Text Then
                depa = tablaD.Rows(fila).Item(0)
            End If
        Next

        Dim mes As Integer = MES_N.SelectedIndex + 1
        Dim anio As Integer = AÑO_N.Text

        tabla_sumatorias = conexion.Get_RepNomina(rfc_, depa, mes, MES_N.Text, anio)
        i = tabla_sumatorias.Rows.Count
        DG_REP_NOMINA.DataSource = tabla_sumatorias


    End Sub
End Class