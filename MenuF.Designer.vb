﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MenuF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MenuF))
        Me.PANEL_DESPLAZAR = New System.Windows.Forms.Panel()
        Me.BN_MINIMIZAR = New System.Windows.Forms.Button()
        Me.BN_CLOSE = New System.Windows.Forms.Button()
        Me.TIME_MENU_O = New System.Windows.Forms.Timer(Me.components)
        Me.TIMER_MENU_M = New System.Windows.Forms.Timer(Me.components)
        Me.PANEL_SUBMENU = New System.Windows.Forms.Panel()
        Me.PANEL_VENTANAS = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.PANEL_MENU = New System.Windows.Forms.Panel()
        Me.BTN_PROC_NOMINA = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.BN_REPORTES = New System.Windows.Forms.Button()
        Me.BN_EMPLEADOS = New System.Windows.Forms.Button()
        Me.BN_REGISTRO = New System.Windows.Forms.Button()
        Me.BN_MENU = New System.Windows.Forms.PictureBox()
        Me.PANEL_DESPLAZAR.SuspendLayout()
        Me.PANEL_MENU.SuspendLayout()
        CType(Me.BN_MENU, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PANEL_DESPLAZAR
        '
        Me.PANEL_DESPLAZAR.BackColor = System.Drawing.Color.Goldenrod
        Me.PANEL_DESPLAZAR.Controls.Add(Me.BN_MINIMIZAR)
        Me.PANEL_DESPLAZAR.Controls.Add(Me.BN_CLOSE)
        Me.PANEL_DESPLAZAR.Dock = System.Windows.Forms.DockStyle.Top
        Me.PANEL_DESPLAZAR.Location = New System.Drawing.Point(0, 0)
        Me.PANEL_DESPLAZAR.Name = "PANEL_DESPLAZAR"
        Me.PANEL_DESPLAZAR.Size = New System.Drawing.Size(1100, 40)
        Me.PANEL_DESPLAZAR.TabIndex = 1
        '
        'BN_MINIMIZAR
        '
        Me.BN_MINIMIZAR.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BN_MINIMIZAR.BackgroundImage = CType(resources.GetObject("BN_MINIMIZAR.BackgroundImage"), System.Drawing.Image)
        Me.BN_MINIMIZAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BN_MINIMIZAR.FlatAppearance.BorderSize = 0
        Me.BN_MINIMIZAR.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BN_MINIMIZAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BN_MINIMIZAR.Location = New System.Drawing.Point(1026, -1)
        Me.BN_MINIMIZAR.Name = "BN_MINIMIZAR"
        Me.BN_MINIMIZAR.Size = New System.Drawing.Size(40, 40)
        Me.BN_MINIMIZAR.TabIndex = 1
        Me.BN_MINIMIZAR.UseVisualStyleBackColor = True
        '
        'BN_CLOSE
        '
        Me.BN_CLOSE.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BN_CLOSE.BackgroundImage = CType(resources.GetObject("BN_CLOSE.BackgroundImage"), System.Drawing.Image)
        Me.BN_CLOSE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BN_CLOSE.FlatAppearance.BorderSize = 0
        Me.BN_CLOSE.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BN_CLOSE.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red
        Me.BN_CLOSE.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BN_CLOSE.Location = New System.Drawing.Point(1061, -1)
        Me.BN_CLOSE.Name = "BN_CLOSE"
        Me.BN_CLOSE.Size = New System.Drawing.Size(40, 40)
        Me.BN_CLOSE.TabIndex = 0
        Me.BN_CLOSE.UseVisualStyleBackColor = True
        '
        'TIME_MENU_O
        '
        '
        'TIMER_MENU_M
        '
        '
        'PANEL_SUBMENU
        '
        Me.PANEL_SUBMENU.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.PANEL_SUBMENU.Dock = System.Windows.Forms.DockStyle.Top
        Me.PANEL_SUBMENU.Location = New System.Drawing.Point(200, 40)
        Me.PANEL_SUBMENU.Name = "PANEL_SUBMENU"
        Me.PANEL_SUBMENU.Size = New System.Drawing.Size(900, 34)
        Me.PANEL_SUBMENU.TabIndex = 0
        '
        'PANEL_VENTANAS
        '
        Me.PANEL_VENTANAS.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PANEL_VENTANAS.Location = New System.Drawing.Point(200, 74)
        Me.PANEL_VENTANAS.Name = "PANEL_VENTANAS"
        Me.PANEL_VENTANAS.Size = New System.Drawing.Size(900, 666)
        Me.PANEL_VENTANAS.TabIndex = 3
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Goldenrod
        Me.Panel4.Location = New System.Drawing.Point(0, 179)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(3, 47)
        Me.Panel4.TabIndex = 5
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Goldenrod
        Me.Panel5.Location = New System.Drawing.Point(0, 236)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(3, 47)
        Me.Panel5.TabIndex = 6
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Goldenrod
        Me.Panel6.Location = New System.Drawing.Point(0, 292)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(3, 47)
        Me.Panel6.TabIndex = 6
        '
        'Panel3
        '
        Me.Panel3.Location = New System.Drawing.Point(200, 34)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(754, 698)
        Me.Panel3.TabIndex = 1
        '
        'PANEL_MENU
        '
        Me.PANEL_MENU.BackColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.PANEL_MENU.Controls.Add(Me.BTN_PROC_NOMINA)
        Me.PANEL_MENU.Controls.Add(Me.Panel1)
        Me.PANEL_MENU.Controls.Add(Me.BN_REPORTES)
        Me.PANEL_MENU.Controls.Add(Me.Panel3)
        Me.PANEL_MENU.Controls.Add(Me.BN_EMPLEADOS)
        Me.PANEL_MENU.Controls.Add(Me.BN_REGISTRO)
        Me.PANEL_MENU.Controls.Add(Me.Panel6)
        Me.PANEL_MENU.Controls.Add(Me.Panel5)
        Me.PANEL_MENU.Controls.Add(Me.Panel4)
        Me.PANEL_MENU.Controls.Add(Me.BN_MENU)
        Me.PANEL_MENU.Dock = System.Windows.Forms.DockStyle.Left
        Me.PANEL_MENU.Location = New System.Drawing.Point(0, 40)
        Me.PANEL_MENU.Name = "PANEL_MENU"
        Me.PANEL_MENU.Size = New System.Drawing.Size(200, 700)
        Me.PANEL_MENU.TabIndex = 2
        '
        'BTN_PROC_NOMINA
        '
        Me.BTN_PROC_NOMINA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BTN_PROC_NOMINA.FlatAppearance.BorderSize = 0
        Me.BTN_PROC_NOMINA.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BTN_PROC_NOMINA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTN_PROC_NOMINA.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_PROC_NOMINA.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BTN_PROC_NOMINA.Image = CType(resources.GetObject("BTN_PROC_NOMINA.Image"), System.Drawing.Image)
        Me.BTN_PROC_NOMINA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BTN_PROC_NOMINA.Location = New System.Drawing.Point(3, 290)
        Me.BTN_PROC_NOMINA.Name = "BTN_PROC_NOMINA"
        Me.BTN_PROC_NOMINA.Size = New System.Drawing.Size(197, 50)
        Me.BTN_PROC_NOMINA.TabIndex = 10
        Me.BTN_PROC_NOMINA.Text = "Nomina"
        Me.BTN_PROC_NOMINA.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BTN_PROC_NOMINA.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Goldenrod
        Me.Panel1.Location = New System.Drawing.Point(0, 351)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(3, 47)
        Me.Panel1.TabIndex = 6
        '
        'BN_REPORTES
        '
        Me.BN_REPORTES.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BN_REPORTES.FlatAppearance.BorderSize = 0
        Me.BN_REPORTES.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BN_REPORTES.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BN_REPORTES.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BN_REPORTES.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BN_REPORTES.Image = CType(resources.GetObject("BN_REPORTES.Image"), System.Drawing.Image)
        Me.BN_REPORTES.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BN_REPORTES.Location = New System.Drawing.Point(3, 349)
        Me.BN_REPORTES.Name = "BN_REPORTES"
        Me.BN_REPORTES.Size = New System.Drawing.Size(197, 50)
        Me.BN_REPORTES.TabIndex = 9
        Me.BN_REPORTES.Text = "Reportes"
        Me.BN_REPORTES.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BN_REPORTES.UseVisualStyleBackColor = True
        '
        'BN_EMPLEADOS
        '
        Me.BN_EMPLEADOS.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BN_EMPLEADOS.FlatAppearance.BorderSize = 0
        Me.BN_EMPLEADOS.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BN_EMPLEADOS.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BN_EMPLEADOS.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BN_EMPLEADOS.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BN_EMPLEADOS.Image = CType(resources.GetObject("BN_EMPLEADOS.Image"), System.Drawing.Image)
        Me.BN_EMPLEADOS.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BN_EMPLEADOS.Location = New System.Drawing.Point(3, 178)
        Me.BN_EMPLEADOS.Name = "BN_EMPLEADOS"
        Me.BN_EMPLEADOS.Size = New System.Drawing.Size(197, 50)
        Me.BN_EMPLEADOS.TabIndex = 7
        Me.BN_EMPLEADOS.Text = " Empleados"
        Me.BN_EMPLEADOS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BN_EMPLEADOS.UseVisualStyleBackColor = True
        '
        'BN_REGISTRO
        '
        Me.BN_REGISTRO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BN_REGISTRO.FlatAppearance.BorderSize = 0
        Me.BN_REGISTRO.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BN_REGISTRO.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BN_REGISTRO.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BN_REGISTRO.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BN_REGISTRO.Image = CType(resources.GetObject("BN_REGISTRO.Image"), System.Drawing.Image)
        Me.BN_REGISTRO.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BN_REGISTRO.Location = New System.Drawing.Point(3, 235)
        Me.BN_REGISTRO.Name = "BN_REGISTRO"
        Me.BN_REGISTRO.Size = New System.Drawing.Size(197, 50)
        Me.BN_REGISTRO.TabIndex = 4
        Me.BN_REGISTRO.Text = "Gestion"
        Me.BN_REGISTRO.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BN_REGISTRO.UseVisualStyleBackColor = True
        '
        'BN_MENU
        '
        Me.BN_MENU.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BN_MENU.BackgroundImage = CType(resources.GetObject("BN_MENU.BackgroundImage"), System.Drawing.Image)
        Me.BN_MENU.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.BN_MENU.Location = New System.Drawing.Point(136, 0)
        Me.BN_MENU.Name = "BN_MENU"
        Me.BN_MENU.Size = New System.Drawing.Size(69, 57)
        Me.BN_MENU.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.BN_MENU.TabIndex = 0
        Me.BN_MENU.TabStop = False
        '
        'MenuF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1100, 740)
        Me.Controls.Add(Me.PANEL_VENTANAS)
        Me.Controls.Add(Me.PANEL_SUBMENU)
        Me.Controls.Add(Me.PANEL_MENU)
        Me.Controls.Add(Me.PANEL_DESPLAZAR)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "MenuF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Menu"
        Me.PANEL_DESPLAZAR.ResumeLayout(False)
        Me.PANEL_MENU.ResumeLayout(False)
        Me.PANEL_MENU.PerformLayout()
        CType(Me.BN_MENU, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PANEL_DESPLAZAR As Panel
    Friend WithEvents BN_MINIMIZAR As Button
    Friend WithEvents BN_CLOSE As Button
    Friend WithEvents TIME_MENU_O As Timer
    Friend WithEvents TIMER_MENU_M As Timer
    Friend WithEvents PANEL_SUBMENU As Panel
    Friend WithEvents PANEL_VENTANAS As Panel
    Friend WithEvents BN_MENU As PictureBox
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Panel5 As Panel
    Friend WithEvents Panel6 As Panel
    Friend WithEvents BN_REGISTRO As Button
    Friend WithEvents BN_EMPLEADOS As Button
    Friend WithEvents Panel3 As Panel
    Friend WithEvents BN_REPORTES As Button
    Friend WithEvents PANEL_MENU As Panel
    Friend WithEvents BTN_PROC_NOMINA As Button
    Friend WithEvents Panel1 As Panel
End Class
