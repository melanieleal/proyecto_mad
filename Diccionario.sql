SELECT
	t.name AS [TableName], 
	c.name AS [ColumnName],
	DEFAULT_DOMAIN()[DomainName],
	st.name AS [DataType], 
	isnull(ep.name, '-- add description here') AS [Unidad],
	c.max_length AS [MaxLength],
	CASE 
		WHEN c.is_computed = 0 THEN 'NO'
		ELSE 'SI'
	END AS Calculada,
	CASE 
		WHEN c.is_nullable = 0 THEN 'Obligatoria, NOT NULL'
		ELSE 'Default, NULL'
	END AS [Restriccion],
	CASE 
		WHEN c.is_identity = 0 THEN 'NO'
		ELSE 'SI'
	END AS [ES IDENTITY], 
	isnull(ep.value, '-- add description here') AS [Description]
FROM [sys].[tables] t
INNER JOIN [sys].[columns] c
	ON t.object_id= c.object_id 
INNER JOIN [sys].[systypes] st 
	ON c.system_type_id= st.xusertype 
INNER JOIN [sys].[objects] o 
	ON t.object_id= o.object_id 
LEFT JOIN [sys].[extended_properties] ep 
	ON o.object_id = ep.major_id 
	AND c.column_Id = ep.minor_id
WHERE t.name <> 'sysdiagrams' 
ORDER BY 
	t.name,
	c.column_Id





----------------------------------------------------------------------------------


EXEC sp_addextendedproperty
@name = N'MS_Description', @value = 'wenas23', --NO LOGRO HACER QUE EL WENAS23 SOLO SE PONGA EN DESCRIPCION Y NO EN UNIDAD
@level0type = N'Schema', @level0name = 'dbo',
@level1type = N'Table', @level1name = 'Banco',
@level2type = N'Column', @level2name = 'Nombre';

EXEC sp_addextendedproperty
@name = N'MS_Description', @value = 'Bancos',
@level0type = N'Schema', @level0name = 'dbo',
@level1type = N'Table', @level1name = 'Banco',
@level2type = N'Column', @level2name = 'ID_Banco';

EXEC sp_updateextendedproperty   
     @name = 'wenaas'   
    ,@level0type =  N'Schema'   
    ,@level0name = 'dbo'  
    ,@level1type = N'Table'  
    ,@level1name = 'Banco'  
    ,@level2type = N'Column'  
    ,@level2name = 'Nombre';  