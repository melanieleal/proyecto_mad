﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Resumen_Pagos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.AÑO = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.BUSCAR_H = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DG_RESUMEN_PAGO = New System.Windows.Forms.DataGridView()
        CType(Me.DG_RESUMEN_PAGO, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'AÑO
        '
        Me.AÑO.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.AÑO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.AÑO.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.AÑO.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AÑO.FormattingEnabled = True
        Me.AÑO.Location = New System.Drawing.Point(748, 50)
        Me.AÑO.Name = "AÑO"
        Me.AÑO.Size = New System.Drawing.Size(69, 25)
        Me.AÑO.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(708, 53)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(34, 16)
        Me.Label4.TabIndex = 103
        Me.Label4.Text = "Año"
        '
        'BUSCAR_H
        '
        Me.BUSCAR_H.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BUSCAR_H.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BUSCAR_H.Location = New System.Drawing.Point(804, 79)
        Me.BUSCAR_H.Name = "BUSCAR_H"
        Me.BUSCAR_H.Size = New System.Drawing.Size(69, 35)
        Me.BUSCAR_H.TabIndex = 104
        Me.BUSCAR_H.Text = "Buscar"
        Me.BUSCAR_H.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(31, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(185, 23)
        Me.Label1.TabIndex = 105
        Me.Label1.Text = "Resumen de Pagos"
        '
        'DG_RESUMEN_PAGO
        '
        Me.DG_RESUMEN_PAGO.AllowUserToDeleteRows = False
        Me.DG_RESUMEN_PAGO.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DG_RESUMEN_PAGO.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DG_RESUMEN_PAGO.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG_RESUMEN_PAGO.Location = New System.Drawing.Point(44, 147)
        Me.DG_RESUMEN_PAGO.Name = "DG_RESUMEN_PAGO"
        Me.DG_RESUMEN_PAGO.ReadOnly = True
        Me.DG_RESUMEN_PAGO.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DG_RESUMEN_PAGO.Size = New System.Drawing.Size(815, 427)
        Me.DG_RESUMEN_PAGO.TabIndex = 106
        '
        'Resumen_Pagos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(900, 666)
        Me.Controls.Add(Me.DG_RESUMEN_PAGO)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BUSCAR_H)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.AÑO)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Resumen_Pagos"
        Me.Text = "Resumen_Pagos"
        CType(Me.DG_RESUMEN_PAGO, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents AÑO As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents BUSCAR_H As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents DG_RESUMEN_PAGO As DataGridView
End Class
