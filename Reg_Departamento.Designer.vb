﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Reg_Departamento
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.DELETE_DEP = New System.Windows.Forms.Button()
        Me.ACTUALIZAR_DEP = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SUELDO_BASE = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.NAME_DEP = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.DG_DEPARTAMENTO = New System.Windows.Forms.DataGridView()
        Me.EMP_DEPA = New System.Windows.Forms.ComboBox()
        CType(Me.DG_DEPARTAMENTO, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DELETE_DEP
        '
        Me.DELETE_DEP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.DELETE_DEP.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DELETE_DEP.Location = New System.Drawing.Point(736, 325)
        Me.DELETE_DEP.Name = "DELETE_DEP"
        Me.DELETE_DEP.Size = New System.Drawing.Size(69, 35)
        Me.DELETE_DEP.TabIndex = 93
        Me.DELETE_DEP.Text = "Eliminar"
        Me.DELETE_DEP.UseVisualStyleBackColor = True
        '
        'ACTUALIZAR_DEP
        '
        Me.ACTUALIZAR_DEP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.ACTUALIZAR_DEP.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ACTUALIZAR_DEP.Location = New System.Drawing.Point(637, 325)
        Me.ACTUALIZAR_DEP.Name = "ACTUALIZAR_DEP"
        Me.ACTUALIZAR_DEP.Size = New System.Drawing.Size(69, 35)
        Me.ACTUALIZAR_DEP.TabIndex = 92
        Me.ACTUALIZAR_DEP.Text = "Actualizar"
        Me.ACTUALIZAR_DEP.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(62, 96)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(107, 16)
        Me.Label14.TabIndex = 91
        Me.Label14.Text = "Departamentos"
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(381, 476)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 17)
        Me.Label1.TabIndex = 89
        Me.Label1.Text = "Sueldo Base."
        '
        'SUELDO_BASE
        '
        Me.SUELDO_BASE.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.SUELDO_BASE.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.SUELDO_BASE.Location = New System.Drawing.Point(488, 475)
        Me.SUELDO_BASE.Name = "SUELDO_BASE"
        Me.SUELDO_BASE.Size = New System.Drawing.Size(82, 13)
        Me.SUELDO_BASE.TabIndex = 88
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(381, 438)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(187, 17)
        Me.Label2.TabIndex = 87
        Me.Label2.Text = "Nombre de Departamento."
        '
        'NAME_DEP
        '
        Me.NAME_DEP.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.NAME_DEP.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NAME_DEP.Location = New System.Drawing.Point(574, 438)
        Me.NAME_DEP.Name = "NAME_DEP"
        Me.NAME_DEP.Size = New System.Drawing.Size(231, 13)
        Me.NAME_DEP.TabIndex = 86
        '
        'Label25
        '
        Me.Label25.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(381, 403)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(67, 17)
        Me.Label25.TabIndex = 83
        Me.Label25.Text = "Empresa."
        '
        'DG_DEPARTAMENTO
        '
        Me.DG_DEPARTAMENTO.AllowUserToDeleteRows = False
        Me.DG_DEPARTAMENTO.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DG_DEPARTAMENTO.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DG_DEPARTAMENTO.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG_DEPARTAMENTO.Location = New System.Drawing.Point(134, 144)
        Me.DG_DEPARTAMENTO.Name = "DG_DEPARTAMENTO"
        Me.DG_DEPARTAMENTO.ReadOnly = True
        Me.DG_DEPARTAMENTO.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DG_DEPARTAMENTO.Size = New System.Drawing.Size(638, 150)
        Me.DG_DEPARTAMENTO.TabIndex = 107
        '
        'EMP_DEPA
        '
        Me.EMP_DEPA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.EMP_DEPA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EMP_DEPA.FormattingEnabled = True
        Me.EMP_DEPA.Location = New System.Drawing.Point(454, 402)
        Me.EMP_DEPA.Name = "EMP_DEPA"
        Me.EMP_DEPA.Size = New System.Drawing.Size(351, 21)
        Me.EMP_DEPA.TabIndex = 108
        '
        'Reg_Departamento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(900, 666)
        Me.Controls.Add(Me.EMP_DEPA)
        Me.Controls.Add(Me.DG_DEPARTAMENTO)
        Me.Controls.Add(Me.DELETE_DEP)
        Me.Controls.Add(Me.ACTUALIZAR_DEP)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.SUELDO_BASE)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.NAME_DEP)
        Me.Controls.Add(Me.Label25)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Reg_Departamento"
        Me.Text = "Reg_Departamento"
        CType(Me.DG_DEPARTAMENTO, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DELETE_DEP As Button
    Friend WithEvents ACTUALIZAR_DEP As Button
    Friend WithEvents Label14 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents SUELDO_BASE As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents NAME_DEP As TextBox
    Friend WithEvents Label25 As Label
    Friend WithEvents DG_DEPARTAMENTO As DataGridView
    Friend WithEvents EMP_DEPA As ComboBox
End Class
