--creacion de tablas
--CREATE DATABASE Proyect_MAD;


GO
USE Proyect_MAD 


	ALTER TABLE  Empleado DROP CONSTRAINT FK_TERMINACION_EMAIL;
	ALTER TABLE  Empleado DROP CONSTRAINT FK_PUESTO;
	ALTER TABLE  Empleado DROP CONSTRAINT FK_EMPRESA;
	ALTER TABLE  Empresas DROP CONSTRAINT FK_EMPLEADO;
	ALTER TABLE  Concepto DROP CONSTRAINT FK_TIPO_VALOR ;
	ALTER TABLE  Empleado DROP CONSTRAINT FK_BANCO ;
	ALTER TABLE  Empleado DROP CONSTRAINT FK_FREC_PAGO_EMPLEADO ;
	ALTER TABLE  Empresa_FrecPago DROP CONSTRAINT FK_EMPRESA_PAGO ;
	ALTER TABLE  Empresa_FrecPago DROP CONSTRAINT FK_FREC_PAGO ;
	ALTER TABLE  Departamentos DROP CONSTRAINT FK_DEPARTAMENTO1 ;
	ALTER TABLE  Departamentos DROP CONSTRAINT FK_DEPARTAMENTO2 ;
	ALTER TABLE  Puestos DROP CONSTRAINT FK_PUESTOS1 ;
	ALTER TABLE  Puestos DROP CONSTRAINT FK_PUESTOS2 ;
	ALTER TABLE Nomina DROP CONSTRAINT FK_RECIBO_NOMINA ;
	ALTER TABLE Deducciones DROP CONSTRAINT FK_DEDUCCIONES1 ;
    ALTER TABLE Nomina_Deduccion DROP CONSTRAINT FK_DEDUCCIONESN1 ;
    ALTER TABLE Nomina_Deduccion DROP CONSTRAINT FK_DEDUCCIONESN2;
	ALTER TABLE Percepciones DROP CONSTRAINT FK_PERCEPCIONES1;
	ALTER TABLE Nomina_Percepcion DROP CONSTRAINT FK_PERCEPCIONESN1;	
	ALTER TABLE Nomina_Percepcion DROP CONSTRAINT FK_PERCEPCIONESN2 ;		
	ALTER TABLE Incidencias_Empleados DROP CONSTRAINT FK_INCIDENCIAS_EMP1 ;
	ALTER TABLE Incidencias_Empleados DROP CONSTRAINT FK_INCIDENCIAS_EMP2 ;
	ALTER TABLE Resumen_Pago DROP CONSTRAINT FK_NO_EMPLEADO;
	
	

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Tipo_Valor') 
BEGIN
    DROP TABLE Tipo_Valor
END
CREATE TABLE Tipo_Valor(
    ID_Tipo_Valor        int identity (1,1) primary key, 
    Nombre               varchar(30) UNIQUE not null);
	

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Concepto') 
BEGIN
    DROP TABLE Concepto
END
CREATE TABLE Concepto(
    ID_Concepto          int identity (1,1) primary key, 
    Nombre               varchar(30) UNIQUE not null,  
    Monto                decimal (10,2) not null,
	ID_Tipo_Valor        int); --FK


IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Empresas') 
BEGIN
    DROP TABLE Empresas
END	
CREATE TABLE Empresas( 
	ID_RFC				 varchar(13) not null  primary key, 
	Razon_Social		 varchar(30) UNIQUE not null, 
	Registro_Patronal    char(20) UNIQUE  not null, 
	Fecha_Inicio_Op      date not null,
	No_Empleado          int, --FK
	Telefono			 char(10) null, 	
	Calle				 varchar(30) not null,
	No_Exterior			 int not null, 
	Colonia              varchar(30) not null, 
	Estado				 varchar(30) not null, 
	CP					 int not null,
	Municipio			 varchar(30) not null,
	IMSS                 decimal(7,2),
	ISR					 decimal(7,2),
	PVacacional          decimal(7,2),
	BonoG                decimal(7,2),
	PagoGE               decimal(7,2),
	Fecha_PN             date null,
	Activo               Bit
	);
	

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Frec_Pago') 
BEGIN
    DROP TABLE Frec_Pago
END	
CREATE TABLE Frec_Pago(
	ID_Frec_Pago        int identity (1,1)  primary key, 
	Nombre              varchar(30) UNIQUE
	);

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Empresa_FrecPago') 
BEGIN
    DROP TABLE Empresa_FrecPago
END	
CREATE TABLE Empresa_FrecPago(
	ID_Frec_Pago               int , --FK 
	ID_RFC					 varchar(13) --FK
	);

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Departamentos') 
BEGIN
    DROP TABLE Departamentos
END	
CREATE TABLE Departamentos(
	No_Departamento      int identity(100,1)  primary key, 
	Nombre               varchar(30)  not null, 
	Sueldo_Base          decimal(7,2) not null,
	Activo               Bit,
	ID_RFC			     varchar(13), --FK
	No_Empleado		   	 int  --FK
	);

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Puestos') 
BEGIN
    DROP TABLE Puestos
END	
CREATE TABLE Puestos(
	ID_Puesto            int identity(10,1)  primary key,  
	Nombre               varchar(30)  not null,  
	Nivel_Salarial		 decimal(7,2) not null, 
	Activo               Bit,
	No_Departamento      int , --FK
	ID_RFC				 varchar(13)  --FK
	);
	

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Banco') 
BEGIN
    DROP TABLE Banco
END	

CREATE TABLE Banco(
	ID_Banco	         int identity(1,1) primary key, 
	Nombre			     varchar(30) UNIQUE not null, 		
);

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Terminacion_Email') 
BEGIN
    DROP TABLE Terminacion_Email
END	

CREATE TABLE Terminacion_Email(
	ID_Terminacion_Email int identity(1,1) primary key, 
	Nombre			     varchar(30) UNIQUE not null, 	
);

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Empleado') 
BEGIN
    DROP TABLE Empleado
END	
CREATE TABLE Empleado(
	No_Empleado			 int identity(1000,1) primary key, 
	Nombre				 varchar(30) not null, 
	Ape_Paterno			 varchar(30) not null, 
	Ape_Materno			 varchar(30), 
	Fecha_Nacimiento     date, 
	Email                nvarchar(30) UNIQUE not null,
	ID_Terminacion_Email int, --FK
	Contraseña			 varchar(20) not null,  
	Fecha_Puesto         date,
	Calle				 varchar(30) not null,   
	No_Casa              int not null,  
	Colonia			     varchar(30) not null,   
	Estado			     varchar(30) not null,   
	CP				     int not null,  
	Municipio		     varchar(30) not null,
	Telefono		     char(10),
	Fecha_Contrato       date not null,
	RFC                  varchar(13) UNIQUE not null,
	CURP                 varchar(18) UNIQUE not null,
	NSS                  varchar(11) UNIQUE not null,
	No_Cuenta_Banco      varchar(24) UNIQUE not null,
	ID_Banco	         int,  --FK
	ID_Puesto			 int,   --FK
	ID_RFC			     varchar(13), --FK
	ID_Frec_Pago         int, --FK
	No_Departamento		 int,
	Gerente_General      bit default 0,
	Gerente_Empresa      bit,
	Gerente_Depto        bit,
	Activo               Bit
	); 

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Nomina') 
BEGIN
    DROP TABLE Nomina
END	
CREATE TABLE Nomina(
    ID_Nomina            int  identity (10010,1)  primary key, 
    Sueldo_Bruto         decimal(10,2), 
	Sueldo_Neto          decimal(10,2),
    DiasT                int,
	IMSS                 decimal(7,2),
	ISR					 decimal(7,2),
	fecha_PN             date,
    No_Empleado          int, --FK
	SueldoD              decimal(10,2)
	);


IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Deducciones') 
BEGIN
    DROP TABLE Deducciones
END	
CREATE TABLE Deducciones(
    ID_Deduccion           int identity(1,1) primary key,
    ID_Concepto            int, --FK
	Activo                 Bit
	);

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Nomina_Deduccion') 
BEGIN
    DROP TABLE Nomina_Deduccion
END	
CREATE TABLE Nomina_Deduccion(
	ID_Nomina_Deduccion    int identity(1,1) primary key,
    ID_Deduccion           int , --FK
     ID_Nomina             int,  --FK
	fecha                date
	);

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Percepciones') 
BEGIN
    DROP TABLE Percepciones
END	
CREATE TABLE Percepciones(
    ID_Percepcion        int identity(1,1) primary key, 
    ID_Concepto          int,  --FK
	Activo               Bit
	);
	
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Nomina_Percepcion') 
BEGIN
    DROP TABLE Nomina_Percepcion
END	
CREATE TABLE Nomina_Percepcion(
	ID_Nomina_Percepcion int identity(1,1) primary key,
    ID_Percepcion        int , --FK
     ID_Nomina            int,  --FK
	fecha                date

);

IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Incidencias') 
BEGIN
    DROP TABLE Incidencias
END		
CREATE TABLE Incidencias(
    ID_Incidencias       int identity (1,1)  primary key,
    Nombre               varchar(30) UNIQUE not null
	);


IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Incidencias_Empleados') 
BEGIN
    DROP TABLE Incidencias_Empleados
END	
CREATE TABLE Incidencias_Empleados(
    ID_Emp_Incidencias   int identity (1,1) primary key, 
    ID_Incidencias       int ,  --FK
    No_Empleado          int ,  --FK
	Fecha	             date,
	Activo               Bit
	);
			
IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Resumen_Pago') 
BEGIN
    DROP TABLE Resumen_Pago
END	
CREATE TABLE Resumen_Pago(
	ID_Mes                  int identity(1,1) primary key,
	Mes                     varchar(30),
	Anio					int,
	No_Empleado				int, --FK
	RFC                     varchar(13),                 
	NSS						varchar(11),
	Suma_IMSS               decimal(10,2),
	Suma_ISR				decimal(10,2),
	Suma_Sueldo_Bruto       decimal(10,2),
	Suma_Sueldo_Neto        decimal(10,2)
	);


    ALTER TABLE  Empleado add CONSTRAINT FK_BANCO foreign key (ID_Banco) references Banco(ID_Banco);	
	ALTER TABLE  Empresas add CONSTRAINT FK_EMPLEADO foreign key (No_Empleado) references Empleado(No_Empleado);
	ALTER TABLE  Concepto add CONSTRAINT FK_TIPO_VALOR foreign key (ID_Tipo_Valor) references Tipo_Valor(ID_Tipo_Valor);	
	ALTER TABLE  Empleado add CONSTRAINT FK_EMPRESA foreign key (ID_RFC) references Empresas(ID_RFC);
	ALTER TABLE  Empleado add CONSTRAINT FK_TERMINACION_EMAIL foreign key (ID_Terminacion_Email) references Terminacion_Email(ID_Terminacion_Email);
	ALTER TABLE  Empleado add CONSTRAINT FK_PUESTO foreign key (ID_Puesto) references Puestos(ID_Puesto);
	ALTER TABLE  Empleado add CONSTRAINT FK_FREC_PAGO_EMPLEADO  foreign key (ID_Frec_Pago) references Frec_Pago(ID_Frec_Pago);
	ALTER TABLE  Empresa_FrecPago add CONSTRAINT FK_EMPRESA_PAGO foreign key (ID_RFC) references Empresas(ID_RFC);
	ALTER TABLE  Empresa_FrecPago add CONSTRAINT FK_FREC_PAGO foreign key (ID_Frec_Pago ) references Frec_Pago (ID_Frec_Pago );
	ALTER TABLE  Departamentos add CONSTRAINT FK_DEPARTAMENTO1 foreign key (No_Empleado) references Empleado(No_Empleado);
	ALTER TABLE  Departamentos add CONSTRAINT FK_DEPARTAMENTO2 foreign key (ID_RFC) references Empresas(ID_RFC);
	ALTER TABLE  Puestos add CONSTRAINT FK_PUESTOS1 foreign key (No_Departamento) references Departamentos(No_Departamento);
	ALTER TABLE  Puestos add CONSTRAINT FK_PUESTOS2 foreign key (ID_RFC) references Empresas(ID_RFC);
	ALTER TABLE Nomina add CONSTRAINT FK_RECIBO_NOMINA foreign key (No_Empleado) references Empleado(No_Empleado);
	ALTER TABLE Deducciones add CONSTRAINT FK_DEDUCCIONES1 foreign key (ID_Concepto) references Concepto(ID_Concepto);
	ALTER TABLE Nomina_Deduccion add CONSTRAINT FK_DEDUCCIONESN1 foreign key ( ID_Nomina   ) references Nomina( ID_Nomina  );
    ALTER TABLE Nomina_Deduccion add CONSTRAINT FK_DEDUCCIONESN2 foreign key (ID_Deduccion ) references Deducciones(ID_Deduccion);
	ALTER TABLE Percepciones add CONSTRAINT FK_PERCEPCIONES1 foreign key (ID_Concepto) references Concepto(ID_Concepto);
	ALTER TABLE Nomina_Percepcion add CONSTRAINT FK_PERCEPCIONESN1 foreign key ( ID_Nomina   ) references Nomina( ID_Nomina  );	
	ALTER TABLE Nomina_Percepcion add CONSTRAINT FK_PERCEPCIONESN2 foreign key (ID_Percepcion ) references Percepciones(ID_Percepcion);		
	ALTER TABLE Incidencias_Empleados add CONSTRAINT FK_INCIDENCIAS_EMP1 foreign key (ID_Incidencias) references Incidencias(ID_Incidencias);
	ALTER TABLE Incidencias_Empleados add CONSTRAINT FK_INCIDENCIAS_EMP2 foreign key (No_Empleado) references Empleado(No_Empleado);

	ALTER TABLE Resumen_Pago add CONSTRAINT FK_NO_EMPLEADO foreign key (No_Empleado) references Empleado(No_Empleado);
	
---INSERT DE DATOS
INSERT INTO Resumen_Pago (Mes)
VALUES ('Enero'),
('Febrero'),
('Marzo'),
('Abril'),
('Mayo'),
('Junio'),
('Julio'),
('Agosto'),
('Septiembre'),
('Octubre'),
('Noviembre'),
('Diciembre')

EXEC sp_Terminacion_Email 'I', null, '@gmail.com'
EXEC sp_Terminacion_Email 'I', null, '@hotmail.com'
EXEC sp_Terminacion_Email 'I', null, '@outlook.com'
EXEC sp_Terminacion_Email 'I', null, '@hotmail.es'

EXEC sp_Banco 'I', null, 'Santander'
EXEC sp_Banco 'I', null, 'Banamex'
EXEC sp_Banco 'I', null, 'Afirme'
EXEC sp_Banco 'I', null, 'HSBC'
EXEC sp_Banco 'I', null, 'Banorte'
EXEC sp_Banco 'I', null, 'Scotiabank'
EXEC sp_Banco 'I', null, 'America Express'

EXEC sp_Tipo_Valor 'I', null, 'Porcentaje'
EXEC sp_Tipo_Valor 'I', null, 'Monto Fijo'

INSERT INTO Frec_Pago  (Nombre)
VALUES ('Semanal'),
       ('Quincenal'),
       ('Catorcenal'),
       ('Mensual')

INSERT INTO Incidencias (Nombre)
VALUES ('Vacaciones'),
	   ('Falta'),
	   ('Accidente'),
	   ('Otro')


