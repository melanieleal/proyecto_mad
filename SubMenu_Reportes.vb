﻿Public Class SubMenu_Reportes

    'mostrar ventanas en un panel
    Private Sub AbrirForm(ByVal formHijo As Object)
        If MenuF.PANEL_VENTANAS.Controls.Count > 0 Then
            MenuF.PANEL_VENTANAS.Controls.RemoveAt(0)         'remueve lo que haya en el panel
        End If
        Dim formH As Form = TryCast(formHijo, Form)
        formH.TopLevel = False
        formH.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        formH.Dock = DockStyle.Fill
        MenuF.PANEL_VENTANAS.Controls.Add(formH)              'añade la nueva ventana al panel
        MenuF.PANEL_VENTANAS.Tag = formH
        formH.Show()
    End Sub

    Private Sub BN_NOMINAG_Click(sender As Object, e As EventArgs) Handles BN_NOMINAG.Click
        AbrirForm(New Nomina_General)
    End Sub

    Private Sub BN_NOMINA_Click(sender As Object, e As EventArgs) Handles BN_NOMINA.Click
        AbrirForm(New Rep_Nomina)
    End Sub

    Private Sub BN_HEADCOUNTER_Click(sender As Object, e As EventArgs) Handles BN_HEADCOUNTER.Click
        AbrirForm(New Rep_Headcounter)
    End Sub

End Class