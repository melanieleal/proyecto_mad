GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'Reporte_Nomina_General' and type = 'V')
BEGIN
	DROP VIEW Reporte_Nomina_General
END
GO 
CREATE VIEW Reporte_Nomina_General

AS					


SELECT  UPPER(CONCAT( E.Nombre, ' ',E.Ape_Paterno,' ', E.Ape_Materno) )'Nombre',
        D.Nombre 'Departamento',
		P.Nombre 'Puesto',
		E.Fecha_Contrato 'Fecha de Contrato',
		E.Fecha_Nacimiento 'Fecha de Nacimiento',

		dbo.fnSueldoGE(E.ID_RFC, E.No_Empleado)'Salario Diario',

		E.ID_RFC
		FROM Empleado E
		INNER JOIN Departamentos D
		ON E.No_Departamento = D.No_Departamento 
		INNER JOIN Puestos P
		ON E.ID_Puesto = P.ID_Puesto 
		INNER JOIN Empresas W
		ON E.ID_RFC = W.ID_RFC 

	WHERE E.ID_RFC = W.ID_RFC 
----------------------------------------------------------------------------------------------------------------------
GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'Reporte_Headcounter1' and type = 'V')
BEGIN
	DROP VIEW Reporte_Headcounter1
END

GO
CREATE VIEW Reporte_Headcounter1

AS

SELECT 
		
		W.Razon_Social 'Empresa',
		W.ID_RFC,
		D.Nombre 'Departamento',
		D.No_Departamento,
		D.No_Empleado 'Gerente Departamento',
		P.Nombre 'Puesto',
		P.ID_Puesto 'ID_Puesto'
		FROM Departamentos D
		INNER JOIN Empresas W
		ON D.ID_RFC = W.ID_RFC 
		INNER JOIN Puestos P
		ON D.No_Departamento = P.No_Departamento

----------------------------------------------------------------------------------------------------------------------
GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'Reporte_Headcounter2' and type = 'V')
BEGIN
	DROP VIEW Reporte_Headcounter2
END

GO
CREATE VIEW Reporte_Headcounter2

AS

SELECT 
		
		W.Razon_Social 'Empresa',
		W.ID_RFC,
		D.Nombre 'Departamento',
		D.No_Departamento,
		D.No_Empleado 'Gerente Departamento',
		D.No_Departamento 'Numero Departamento',
		W.Fecha_PN 'Fecha de �ltima ejecuci�n del proceso de n�mina'
		FROM Departamentos D
		INNER JOIN Empresas W
		ON D.ID_RFC = W.ID_RFC 

-------------------------------------------------------------------------------------------

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'Reporte_Nomina' and type = 'V')
BEGIN
	DROP VIEW Reporte_Nomina
END

GO
CREATE VIEW Reporte_Nomina

AS

SELECT 
		
		W.Razon_Social 'Empresa',
		W.ID_RFC,
		D.Nombre 'Departamento',
		D.No_Departamento,
		D.No_Departamento 'Numero Departamento',
		D.No_Empleado 'Gerente Departamento'
		FROM Departamentos D
		INNER JOIN Empresas W
		ON D.ID_RFC = W.ID_RFC 


-------------------------------------------------------------------------------------------

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'Resumen_Pagos' and type = 'V')
BEGIN
	DROP VIEW Resumen_Pagos
END

GO
CREATE VIEW Resumen_Pagos

AS

SELECT 
		
		
		RFC,
		NSS,
		N.fecha_PN 'Fecha que le han pagado'
		FROM Empleado E
		INNER JOIN Nomina N
		ON E.No_Empleado = N.No_Empleado 
		WHERE  E.No_Empleado = N.No_Empleado 

--------------------------------------------------------------------------------------------

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'Empresa_Proceso_Nomina' and type = 'V')
BEGIN
	DROP VIEW Empresa_Proceso_Nomina
END

GO
CREATE VIEW Empresa_Proceso_Nomina

AS

		SELECT 
		

			   E.ID_RFC 'ID_RFC', 
			   N.Fecha_PN 'Fecha_PN',
			   E.ID_Frec_Pago 'ID_Frec_Pago'

		FROM Empleado E
		INNER JOIN Nomina N
		ON E.No_Empleado = N.No_Empleado 
		INNER JOIN Empresas  W
		ON E.ID_RFC = W.ID_RFC 
		INNER JOIN Frec_Pago  F
		ON E.ID_Frec_Pago = F.ID_Frec_Pago  

	
---------------------------------------------------------------------------------------------
