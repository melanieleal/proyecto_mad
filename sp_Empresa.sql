use Proyect_MAD 

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_Frec_Pago' AND type = 'P') 
BEGIN
    DROP PROCEDURE sp_Frec_Pago;
END

GO
CREATE PROCEDURE sp_Frec_Pago(
	@Opc                 char(1),
	@Nombre              varchar(30) = null,
	@ID_RFC     		 varchar(13) = null,
	@ID_Frec_Pago		 int = null
)
AS

BEGIN
	
	IF @Opc = 'I'
	BEGIN
		
		INSERT INTO Empresa_FrecPago(ID_RFC, ID_Frec_Pago)
		VALUES (@ID_RFC, @ID_Frec_Pago)

	END

	IF @Opc = 'D'
	BEGIN
		
		DELETE 
		FROM Empresa_FrecPago 
		WHERE ID_RFC = @ID_RFC

	END

		IF @Opc = 'X'
	BEGIN
		
		SELECT  
     	ID_RFC ,
		ID_Frec_Pago
		FROM Empresa_FrecPago 
		where ID_RFC = @ID_RFC
	
	END

			IF @Opc = 'Y'
	BEGIN
		
		SELECT  
     	ID_Frec_Pago,
		Nombre
		FROM Frec_Pago 

	
	END

END

-----------------------------------------------------------------------
GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_Empresa' AND type = 'P') 
BEGIN
    DROP PROCEDURE sp_Empresa;
END

GO
CREATE PROCEDURE sp_Empresa(
	@Opc                 char(1),
	@ID_RFC				 varchar(13) = null, 
	@Razon_Social		 varchar(30) = null, 
	@Registro_Patronal   char(20) = null, 
	@Fecha_Inicio_Op     date = null,
	@Telefono			 char(10) = null, 
	@Calle				 varchar(30) = null,
	@No_Exterior		 int = null, 
	@Colonia             varchar(30) = null, 
	@Estado				 varchar(30) = null, 
	@CP					 int = null,
	@Municipio			 varchar(30) = null,
	@IMSS                decimal(7,2) = null,
	@ISR			     decimal(7,2) = null,
	@PVacacional         decimal(7,2) = null,
	@BonoG               decimal(7,2) = null,
	@PagoGE              decimal(7,2) = null,
	@Fecha_PN            date = null,
	@No_Empleado          int = null
) 
AS
		DECLARE @Activo  Bit 
			SET @Activo = 1;
		DECLARE @ID_Frec_Pago int
BEGIN

	IF @Opc = 'I'
	
	BEGIN
	    
		INSERT INTO Empresas(ID_RFC, Razon_Social,Registro_Patronal, Fecha_Inicio_Op, Telefono,Calle, No_Exterior, Colonia, Estado, CP, Municipio, IMSS, ISR, PVacacional, BonoG, PagoGE, Activo) 
		VALUES (@ID_RFC, @Razon_Social, @Registro_Patronal, @Fecha_Inicio_Op, @Telefono ,  @Calle , @No_Exterior, @Colonia, @Estado, @CP, @Municipio, @IMSS, @ISR, @PVacacional , @BonoG, @PagoGE, @Activo)
	END

	IF @Opc = 'D'
	
	BEGIN
		
		DELETE 
		FROM Empresas 
		WHERE ID_RFC = @ID_RFC
			AND Activo = @Activo			
	END

	IF @Opc = 'M' 
	
	BEGIN
		
			UPDATE Empresas 
				SET	 
						Razon_Social = ISNULL( @Razon_Social, Razon_Social), 
						Registro_Patronal = ISNULL( @Registro_Patronal, Registro_Patronal), 
						Fecha_Inicio_Op = ISNULL(@Fecha_Inicio_Op,Fecha_Inicio_Op),
						Telefono =  ISNULL(@Telefono, Telefono),
						Calle = ISNULL( @Calle, Calle),  
						No_Exterior =  ISNULL(@No_Exterior, No_Exterior), 
						Colonia = ISNULL(@Colonia,Colonia),  
						Estado = ISNULL(@Estado,Estado),  
						CP = ISNULL( @CP, CP), 
						Municipio = ISNULL(@Municipio, Municipio),
						IMSS = ISNULL(@IMSS, IMSS),
						ISR	= ISNULL(@ISR, ISR),
						PVacacional  = ISNULL(@PVacacional , PVacacional ),
						BonoG   = ISNULL(@BonoG, BonoG),
						PagoGE   = ISNULL(@PagoGE, PagoGE),
						No_Empleado = ISNULL(@No_Empleado, No_Empleado ) 

			WHERE ID_RFC = @ID_RFC
			AND Activo = @Activo

	END

	IF  @Opc = 'X'
	BEGIN
	
			SELECT 
				   ID_RFC 'RFC',  --0
				   Razon_Social 'Nombre', --1
				   Registro_Patronal 'Registro Patronal', --2
				   Fecha_Inicio_Op 'Inicio de Operaciones', --3
				   Telefono, --4
				   Calle,  --5
				   No_Exterior 'No. Exterior', --6 
			       Colonia, --7
				   Estado, --8
				   CP, --9
				   Municipio, --10
				   IMSS, --11
				   ISR,  --12
				   BonoG 'Bono Genernte', --13
				   PagoGE 'Pago Gerente Empresa',  --14
				   PVacacional 'Prima Vacacional', --15
				   No_Empleado 'Gerente', --16
				   Activo, 	--17
				   Fecha_PN 'Fecha Proceso de Nomina' --18
				FROM Empresas 
					WHERE Activo = @Activo
					ORDER BY Razon_Social

	END


	IF  @Opc = 'A'---------------------------------------------------
	BEGIN
	
			SELECT 
				   ID_RFC, 
				   Fecha_Inicio_Op
				FROM Empresas 
					WHERE ID_RFC = @ID_RFC

	END

	IF  @Opc = 'G'---------------------------------------------------
	BEGIN
	
			UPDATE Empresas 
				SET	 
					No_Empleado = @No_Empleado 
				WHERE ID_RFC = @ID_RFC
	END

	IF  @Opc = 'P'---------------------------------------------------
	BEGIN
	
			UPDATE Empresas 
				SET	 
					Fecha_PN = @Fecha_PN
				WHERE ID_RFC = @ID_RFC
	END

END

------------------------------------------------------------------------------
