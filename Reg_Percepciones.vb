﻿Public Class Reg_Percepciones
    Dim val As New Funciones
    Dim conexion As New EnlaceBD
    Dim tabla As New DataTable
    Dim obj As New EnlaceBD
    Dim result As Boolean = False
    Dim modificar As Boolean = False
    Dim id As String
    Dim idC As String
    Dim PD As String


    Private Sub NAME_PERCEPCIONES_KeyPress(sender As Object, e As KeyPressEventArgs) Handles NAME_PERCE_DEDU.KeyPress
        'Codigo letras y espacios
        val.EvaluaSoloLetras(e)
    End Sub

    Private Sub CANTIDAD_PERCEPCIONES_KeyPress(sender As Object, e As KeyPressEventArgs) Handles CANTIDAD.KeyPress
        'Codigo con numeros decimales
        Dim v As Boolean
        Dim Cadena As String = CANTIDAD.Text
        Dim Filtro As String = "1234567890"
        If Len(Cadena) > 0 Then
            Filtro += "."
        End If

        For Each caracter In Filtro
            If e.KeyChar = caracter Then
                e.Handled = False
                v = True
                Exit For
            Else
                e.Handled = True
                v = False
            End If
        Next

        If Char.IsControl(e.KeyChar) Then
            e.Handled = False
            v = True
        End If

        If e.KeyChar = "." And Not Cadena.IndexOf(".") Then
            e.Handled = True
            v = False
        End If

        If v = False Then
            MessageBox.Show("Ingrese solo  Numeros con un numero decimal" & vbNewLine & " No Ingrese espacios", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub Reg_Percepciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        tabla = conexion.Get_Tipo_Valor()
        Me.FORMA.DataSource = tabla
        Me.FORMA.DisplayMember = "Nombre"
        Me.FORMA.ValueMember = "ID_Tipo_Valor"

        tabla = conexion.Get_Deduccion()
        DG_DEDUCCIONES.DataSource = tabla

        tabla = conexion.Get_Percepcion()
        DG_PERCEPCIONES.DataSource = tabla

    End Sub

    Private Sub ACTUALIZAR_PER_Click(sender As Object, e As EventArgs) Handles ACTUALIZAR_PER.Click
        Dim opc As String
        Dim name As String
        Dim monto As Decimal
        Dim i As Integer

        i = 0
        If CHECK_DEDU.CheckState = 1 Or CHECK_PERCE.CheckState = 1 Then
            i = 1
        End If

        If Trim(FORMA.Text) = "" Or
          Trim(NAME_PERCE_DEDU.Text) = "" Or
          Trim(CANTIDAD.Text) = "" Or i = 0 Or CHECK_DEDU.CheckState = 1 And CHECK_PERCE.CheckState = 1 Then

            MessageBox.Show(" -Ingrese todos los campos" & vbCrLf & " -Marque un solo check de Percepciones o Deducciones", "Percepciones/Deducciones", MessageBoxButtons.OK, MessageBoxIcon.Warning)

        Else

            If FORMA.Text = "Porcentaje" Then
                i = 1
            Else
                i = 2
            End If

            name = NAME_PERCE_DEDU.Text
            monto = CANTIDAD.Text

            If CHECK_DEDU.CheckState = 1 Then

                If modificar Then
                    opc = "M"

                    result = obj.Edit_Deduccion_Percepcion(opc, id, monto, i)
                    If result Then
                        MessageBox.Show("La Deducción se modifico con exito", "Deducción", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        NAME_PERCE_DEDU.Clear()
                        CANTIDAD.Clear()
                        CHECK_DEDU.Checked = False
                        CHECK_DEDU.Enabled = True
                        NAME_PERCE_DEDU.Enabled = True
                        modificar = False

                    End If
                Else
                    opc = "I"

                    result = obj.Add_Deduccion(opc, name, monto, i)

                    If result Then
                        MessageBox.Show("La Deducción se ha sido añadido exitosamente", "Deducción", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        NAME_PERCE_DEDU.Clear()
                        CANTIDAD.Clear()
                        CHECK_DEDU.Checked = False
                    Else
                        MessageBox.Show("Hubo un error, su Deducción no se añadio", "Deducción", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                End If

                tabla = conexion.Get_Deduccion()
                DG_DEDUCCIONES.DataSource = tabla

            Else
                If modificar Then
                    opc = "M"
                    result = obj.Edit_Deduccion_Percepcion(opc, id, monto, i)
                    If result Then
                        MessageBox.Show("La Percepción se modifico con exito", "Percepción", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        NAME_PERCE_DEDU.Clear()
                        CANTIDAD.Clear()
                        CHECK_PERCE.Checked = False
                        CHECK_PERCE.Enabled = True
                        NAME_PERCE_DEDU.Enabled = True
                        modificar = False

                    End If

                Else

                    opc = "I"

                    result = obj.Add_Percepcion(opc, name, monto, i)

                    If result Then
                        MessageBox.Show("La Percepción se ha sido añadido exitosamente", "Percepción", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        NAME_PERCE_DEDU.Clear()
                        CANTIDAD.Clear()
                        CHECK_PERCE.Checked = False
                    Else
                        MessageBox.Show("Hubo un error, su Percepción no se añadio", "Percepción", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If

                End If

                tabla = conexion.Get_Percepcion()
                DG_PERCEPCIONES.DataSource = tabla

            End If

        End If
    End Sub

    Private Sub DG_PERCEPCIONES_Click(sender As Object, e As EventArgs) Handles DG_PERCEPCIONES.Click

        If (Len(DG_PERCEPCIONES.CurrentRow.Cells.Item(0).Value.ToString()) <> 0) Then

            PD = "P"
            modificar = True
            CHECK_PERCE.Checked = True
            CHECK_PERCE.Enabled = False
            NAME_PERCE_DEDU.Enabled = False

            idC = DG_PERCEPCIONES.CurrentRow.Cells.Item(0).Value.ToString()
            id = DG_PERCEPCIONES.CurrentRow.Cells.Item(1).Value.ToString()
            NAME_PERCE_DEDU.Text = DG_PERCEPCIONES.CurrentRow.Cells.Item(2).Value.ToString()
            CANTIDAD.Text = DG_PERCEPCIONES.CurrentRow.Cells.Item(5).Value.ToString()
            FORMA.Text = DG_PERCEPCIONES.CurrentRow.Cells.Item(4).Value.ToString()

        Else
            NAME_PERCE_DEDU.Clear()
            CANTIDAD.Clear()
            CHECK_PERCE.Checked = False
            CHECK_PERCE.Enabled = True
            NAME_PERCE_DEDU.Enabled = True
            modificar = False

        End If

    End Sub

    Private Sub DG_DEDUCCIONES_Click(sender As Object, e As EventArgs) Handles DG_DEDUCCIONES.Click


        If (Len(Trim(DG_DEDUCCIONES.CurrentRow.Cells.Item(0).Value.ToString())) <> 0) Then
            modificar = True
            CHECK_DEDU.Checked = True
            CHECK_DEDU.Enabled = False
            NAME_PERCE_DEDU.Enabled = False

            PD = "D"
            idC = DG_DEDUCCIONES.CurrentRow.Cells.Item(0).Value.ToString()
            id = DG_DEDUCCIONES.CurrentRow.Cells.Item(1).Value.ToString()
            NAME_PERCE_DEDU.Text = DG_DEDUCCIONES.CurrentRow.Cells.Item(2).Value.ToString()
            CANTIDAD.Text = DG_DEDUCCIONES.CurrentRow.Cells.Item(5).Value.ToString()
            FORMA.Text = DG_DEDUCCIONES.CurrentRow.Cells.Item(4).Value.ToString()

        Else

            NAME_PERCE_DEDU.Clear()
            CANTIDAD.Clear()
            CHECK_DEDU.Checked = False
            CHECK_DEDU.Enabled = True
            NAME_PERCE_DEDU.Enabled = True
            modificar = False

        End If
    End Sub

    Private Sub DELETE_PER_Click(sender As Object, e As EventArgs) Handles DELETE_PER.Click
        If modificar = True Then


            If PD = "P" Then

                If MessageBox.Show("Seguro que Desea Eliminar", "Percepcion", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    result = conexion.Delete_Percepcion(idC)
                    If result Then
                        MessageBox.Show("Percepcion elimanada con Exito", "Percepcion", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        NAME_PERCE_DEDU.Clear()
                        CANTIDAD.Clear()
                        CHECK_PERCE.Checked = False
                        CHECK_PERCE.Enabled = True
                        NAME_PERCE_DEDU.Enabled = True
                        tabla = conexion.Get_Percepcion()
                        DG_PERCEPCIONES.DataSource = tabla

                    Else
                        MessageBox.Show("Hubo un Error", "Percepcion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                    End If
                Else
                    NAME_PERCE_DEDU.Clear()
                    CANTIDAD.Clear()
                    CHECK_PERCE.Checked = False
                    CHECK_PERCE.Enabled = True
                    NAME_PERCE_DEDU.Enabled = True

                End If

            Else

                If MessageBox.Show("Seguro que Desea Eliminar", "Deduccion", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

                    result = conexion.Delete_Deduccion(idC)
                    If result Then
                        MessageBox.Show("Deduccion elimanada con Exito", "Deduccion", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        NAME_PERCE_DEDU.Clear()
                        CANTIDAD.Clear()
                        CHECK_DEDU.Checked = False
                        CHECK_DEDU.Enabled = True
                        NAME_PERCE_DEDU.Enabled = True
                        tabla = conexion.Get_Deduccion()
                        DG_DEDUCCIONES.DataSource = tabla

                    Else
                        MessageBox.Show("Hubo un Error", "Deduccion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If

                Else
                    NAME_PERCE_DEDU.Clear()
                    CANTIDAD.Clear()
                    CHECK_DEDU.Checked = False
                    CHECK_DEDU.Enabled = True
                    NAME_PERCE_DEDU.Enabled = True

                End If

            End If




                modificar = False
        End If


    End Sub

End Class