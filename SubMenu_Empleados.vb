﻿Imports iTextSharp.text.pdf
Imports iTextSharp.text
Imports System.IO

Public Class SubMenu_Empleados

    'mostrar ventanas en un panel
    Private Sub AbrirForm(ByVal formHijo As Object)
        If MenuF.PANEL_VENTANAS.Controls.Count > 0 Then
            MenuF.PANEL_VENTANAS.Controls.RemoveAt(0)         'remueve lo que haya en el panel
        End If
        Dim formH As Form = TryCast(formHijo, Form)
        formH.TopLevel = False
        formH.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        formH.Dock = DockStyle.Fill
        MenuF.PANEL_VENTANAS.Controls.Add(formH)              'añade la nueva ventana al panel
        MenuF.PANEL_VENTANAS.Tag = formH
        formH.Show()
    End Sub

    Private Sub BN_NOMINAG_Click(sender As Object, e As EventArgs) Handles BN_NOMINAG.Click
        AbrirForm(New Resumen_Pagos)
    End Sub

    Private Sub BN_NOMINA_Click(sender As Object, e As EventArgs) Handles BN_NOMINA.Click

        Dim pdfN As String = Login.USERNAME.Text & ".pdf"
        If File.Exists(pdfN) Then
            Process.Start(pdfN)
        Else
            MessageBox.Show("Su Recibo de Nomina no se ha efectuado", "Recibo de Nomina", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If


    End Sub

    Private Sub BN_INCIDENCIAS_Click(sender As Object, e As EventArgs) Handles BN_INCIDENCIAS.Click
        AbrirForm(New Solicitudes)
    End Sub


End Class