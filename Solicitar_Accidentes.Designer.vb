﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Solicitar_Accidentes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.FECHA_INCIDENTE = New System.Windows.Forms.DateTimePicker()
        Me.ACTUALIZAR_DEP = New System.Windows.Forms.Button()
        Me.CONSECUENCIA = New System.Windows.Forms.TextBox()
        Me.INCIDENTE_1 = New System.Windows.Forms.CheckBox()
        Me.INCIDENTE_2 = New System.Windows.Forms.CheckBox()
        Me.INCIDENTE_3 = New System.Windows.Forms.CheckBox()
        Me.INCIDENTE_4 = New System.Windows.Forms.CheckBox()
        Me.INCIDENTE_5 = New System.Windows.Forms.CheckBox()
        Me.INCIDENTE_6 = New System.Windows.Forms.CheckBox()
        Me.INCIDENTE_7 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(30, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 17)
        Me.Label2.TabIndex = 92
        Me.Label2.Text = "Fecha "
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(374, 157)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(105, 17)
        Me.Label6.TabIndex = 95
        Me.Label6.Text = "Consecuencia."
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(38, 102)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(79, 17)
        Me.Label8.TabIndex = 97
        Me.Label8.Text = "Incidencia."
        '
        'FECHA_INCIDENTE
        '
        Me.FECHA_INCIDENTE.Location = New System.Drawing.Point(98, 45)
        Me.FECHA_INCIDENTE.Name = "FECHA_INCIDENTE"
        Me.FECHA_INCIDENTE.Size = New System.Drawing.Size(200, 20)
        Me.FECHA_INCIDENTE.TabIndex = 98
        '
        'ACTUALIZAR_DEP
        '
        Me.ACTUALIZAR_DEP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ACTUALIZAR_DEP.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ACTUALIZAR_DEP.Location = New System.Drawing.Point(800, 456)
        Me.ACTUALIZAR_DEP.Name = "ACTUALIZAR_DEP"
        Me.ACTUALIZAR_DEP.Size = New System.Drawing.Size(69, 36)
        Me.ACTUALIZAR_DEP.TabIndex = 99
        Me.ACTUALIZAR_DEP.Text = "Aceptar"
        Me.ACTUALIZAR_DEP.UseVisualStyleBackColor = True
        '
        'CONSECUENCIA
        '
        Me.CONSECUENCIA.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CONSECUENCIA.Location = New System.Drawing.Point(416, 194)
        Me.CONSECUENCIA.Multiline = True
        Me.CONSECUENCIA.Name = "CONSECUENCIA"
        Me.CONSECUENCIA.Size = New System.Drawing.Size(306, 144)
        Me.CONSECUENCIA.TabIndex = 100
        '
        'INCIDENTE_1
        '
        Me.INCIDENTE_1.AutoSize = True
        Me.INCIDENTE_1.Location = New System.Drawing.Point(72, 141)
        Me.INCIDENTE_1.Name = "INCIDENTE_1"
        Me.INCIDENTE_1.Size = New System.Drawing.Size(54, 17)
        Me.INCIDENTE_1.TabIndex = 101
        Me.INCIDENTE_1.Text = "Golpe"
        Me.INCIDENTE_1.UseVisualStyleBackColor = True
        '
        'INCIDENTE_2
        '
        Me.INCIDENTE_2.AutoSize = True
        Me.INCIDENTE_2.Location = New System.Drawing.Point(72, 164)
        Me.INCIDENTE_2.Name = "INCIDENTE_2"
        Me.INCIDENTE_2.Size = New System.Drawing.Size(53, 17)
        Me.INCIDENTE_2.TabIndex = 102
        Me.INCIDENTE_2.Text = "Caida"
        Me.INCIDENTE_2.UseVisualStyleBackColor = True
        '
        'INCIDENTE_3
        '
        Me.INCIDENTE_3.AutoSize = True
        Me.INCIDENTE_3.Location = New System.Drawing.Point(71, 187)
        Me.INCIDENTE_3.Name = "INCIDENTE_3"
        Me.INCIDENTE_3.Size = New System.Drawing.Size(81, 17)
        Me.INCIDENTE_3.TabIndex = 103
        Me.INCIDENTE_3.Text = "Quemadura"
        Me.INCIDENTE_3.UseVisualStyleBackColor = True
        '
        'INCIDENTE_4
        '
        Me.INCIDENTE_4.AutoSize = True
        Me.INCIDENTE_4.Location = New System.Drawing.Point(71, 210)
        Me.INCIDENTE_4.Name = "INCIDENTE_4"
        Me.INCIDENTE_4.Size = New System.Drawing.Size(89, 17)
        Me.INCIDENTE_4.TabIndex = 104
        Me.INCIDENTE_4.Text = "Estres laboral"
        Me.INCIDENTE_4.UseVisualStyleBackColor = True
        '
        'INCIDENTE_5
        '
        Me.INCIDENTE_5.AutoSize = True
        Me.INCIDENTE_5.Location = New System.Drawing.Point(71, 233)
        Me.INCIDENTE_5.Name = "INCIDENTE_5"
        Me.INCIDENTE_5.Size = New System.Drawing.Size(111, 17)
        Me.INCIDENTE_5.TabIndex = 105
        Me.INCIDENTE_5.Text = "Electrocutamiento"
        Me.INCIDENTE_5.UseVisualStyleBackColor = True
        '
        'INCIDENTE_6
        '
        Me.INCIDENTE_6.AutoSize = True
        Me.INCIDENTE_6.Location = New System.Drawing.Point(71, 256)
        Me.INCIDENTE_6.Name = "INCIDENTE_6"
        Me.INCIDENTE_6.Size = New System.Drawing.Size(51, 17)
        Me.INCIDENTE_6.TabIndex = 106
        Me.INCIDENTE_6.Text = "Corte"
        Me.INCIDENTE_6.UseVisualStyleBackColor = True
        '
        'INCIDENTE_7
        '
        Me.INCIDENTE_7.AutoSize = True
        Me.INCIDENTE_7.Location = New System.Drawing.Point(70, 279)
        Me.INCIDENTE_7.Name = "INCIDENTE_7"
        Me.INCIDENTE_7.Size = New System.Drawing.Size(83, 17)
        Me.INCIDENTE_7.TabIndex = 111
        Me.INCIDENTE_7.Text = "Enfermedad"
        Me.INCIDENTE_7.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(70, 302)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(49, 17)
        Me.CheckBox1.TabIndex = 112
        Me.CheckBox1.Text = "Falta"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Solicitar_Accidentes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(900, 523)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.INCIDENTE_7)
        Me.Controls.Add(Me.INCIDENTE_6)
        Me.Controls.Add(Me.INCIDENTE_5)
        Me.Controls.Add(Me.INCIDENTE_4)
        Me.Controls.Add(Me.INCIDENTE_3)
        Me.Controls.Add(Me.INCIDENTE_2)
        Me.Controls.Add(Me.INCIDENTE_1)
        Me.Controls.Add(Me.CONSECUENCIA)
        Me.Controls.Add(Me.ACTUALIZAR_DEP)
        Me.Controls.Add(Me.FECHA_INCIDENTE)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Solicitar_Accidentes"
        Me.Text = "Solicitar_Accidentes"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label2 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents FECHA_INCIDENTE As DateTimePicker
    Friend WithEvents ACTUALIZAR_DEP As Button
    Friend WithEvents CONSECUENCIA As TextBox
    Friend WithEvents INCIDENTE_1 As CheckBox
    Friend WithEvents INCIDENTE_2 As CheckBox
    Friend WithEvents INCIDENTE_3 As CheckBox
    Friend WithEvents INCIDENTE_4 As CheckBox
    Friend WithEvents INCIDENTE_5 As CheckBox
    Friend WithEvents INCIDENTE_6 As CheckBox
    Friend WithEvents INCIDENTE_7 As CheckBox
    Friend WithEvents CheckBox1 As CheckBox
End Class
