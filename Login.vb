﻿Public Class Login
    Dim val As New Funciones
    Dim conexion As New EnlaceBD
    Dim result As Boolean
    Dim tabla As New DataTable
    Dim user As Integer

    Private Sub BN_CLOSE_Click(sender As Object, e As EventArgs) Handles BN_CLOSE.Click
        Me.Close()
    End Sub

    Private Sub BN_MINIMIZAR_Click(sender As Object, e As EventArgs) Handles BN_MINIMIZAR.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub AbrirForm(ByVal formHijo As Object)
        If MenuF.PANEL_VENTANAS.Controls.Count > 0 Then
            MenuF.PANEL_VENTANAS.Controls.RemoveAt(0)         'remueve lo que haya en el panel
        End If
        Dim formH As Form = TryCast(formHijo, Form)
        formH.TopLevel = False
        formH.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        formH.Dock = DockStyle.Fill
        MenuF.PANEL_VENTANAS.Controls.Add(formH)              'añade la nueva ventana al panel
        MenuF.PANEL_VENTANAS.Tag = formH
        formH.Show()
    End Sub


    Private Sub BN_LOGIN_Click(sender As Object, e As EventArgs) Handles BN_LOGIN.Click

        If Trim(USERNAME.Text) = "" Or Trim(PASSWORD.Text) = "" Then
            MessageBox.Show("Ingrese usuario y contraseña", "Inicio Sesion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Dim user As Integer = USERNAME.Text
            Dim pass As String = PASSWORD.Text

            tabla = conexion.Get_Login(user, pass)
            Dim i As Integer = tabla.Rows().Count

            If i = 1 Then
                Me.Hide()
                MenuF.Show()

                If tabla.Rows(0).Item(2) = False And tabla.Rows(0).Item(3) = False Then

                    MenuF.BN_REGISTRO.Enabled = False
                    MenuF.BN_REPORTES.Enabled = False
                    MenuF.BTN_PROC_NOMINA.Enabled = False

                End If

                AbrirForm(New Bienvenido)

            Else
                MessageBox.Show("Error al ingresar usuario y contraseña", "Inicio Sesion", MessageBoxButtons.OK, MessageBoxIcon.Warning)

            End If
        End If
    End Sub

    Private Sub USERNAME_KeyPress(sender As Object, e As KeyPressEventArgs) Handles USERNAME.KeyPress
        val.EvaluaSoloNumeros(e)
    End Sub

End Class

