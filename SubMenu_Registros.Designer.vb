﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SubMenu_Registros
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.BN_EMPRESA = New System.Windows.Forms.Button()
        Me.BN_EMPLEADOS = New System.Windows.Forms.Button()
        Me.BN_DEPA = New System.Windows.Forms.Button()
        Me.BN_PUESTOS = New System.Windows.Forms.Button()
        Me.PERCE_DEDU = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'BN_EMPRESA
        '
        Me.BN_EMPRESA.FlatAppearance.BorderSize = 0
        Me.BN_EMPRESA.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Goldenrod
        Me.BN_EMPRESA.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.BN_EMPRESA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BN_EMPRESA.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BN_EMPRESA.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BN_EMPRESA.Location = New System.Drawing.Point(-1, 0)
        Me.BN_EMPRESA.Name = "BN_EMPRESA"
        Me.BN_EMPRESA.Size = New System.Drawing.Size(121, 34)
        Me.BN_EMPRESA.TabIndex = 0
        Me.BN_EMPRESA.Text = "Empresas"
        Me.BN_EMPRESA.UseVisualStyleBackColor = True
        '
        'BN_EMPLEADOS
        '
        Me.BN_EMPLEADOS.FlatAppearance.BorderSize = 0
        Me.BN_EMPLEADOS.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Goldenrod
        Me.BN_EMPLEADOS.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.BN_EMPLEADOS.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BN_EMPLEADOS.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BN_EMPLEADOS.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BN_EMPLEADOS.Location = New System.Drawing.Point(126, 0)
        Me.BN_EMPLEADOS.Name = "BN_EMPLEADOS"
        Me.BN_EMPLEADOS.Size = New System.Drawing.Size(121, 34)
        Me.BN_EMPLEADOS.TabIndex = 1
        Me.BN_EMPLEADOS.Text = "Empleados"
        Me.BN_EMPLEADOS.UseVisualStyleBackColor = True
        '
        'BN_DEPA
        '
        Me.BN_DEPA.FlatAppearance.BorderSize = 0
        Me.BN_DEPA.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Goldenrod
        Me.BN_DEPA.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.BN_DEPA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BN_DEPA.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BN_DEPA.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BN_DEPA.Location = New System.Drawing.Point(253, 0)
        Me.BN_DEPA.Name = "BN_DEPA"
        Me.BN_DEPA.Size = New System.Drawing.Size(121, 34)
        Me.BN_DEPA.TabIndex = 2
        Me.BN_DEPA.Text = "Departamentos"
        Me.BN_DEPA.UseVisualStyleBackColor = True
        '
        'BN_PUESTOS
        '
        Me.BN_PUESTOS.FlatAppearance.BorderSize = 0
        Me.BN_PUESTOS.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Goldenrod
        Me.BN_PUESTOS.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.BN_PUESTOS.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BN_PUESTOS.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BN_PUESTOS.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.BN_PUESTOS.Location = New System.Drawing.Point(380, 0)
        Me.BN_PUESTOS.Name = "BN_PUESTOS"
        Me.BN_PUESTOS.Size = New System.Drawing.Size(121, 34)
        Me.BN_PUESTOS.TabIndex = 3
        Me.BN_PUESTOS.Text = "Puestos"
        Me.BN_PUESTOS.UseVisualStyleBackColor = True
        '
        'PERCE_DEDU
        '
        Me.PERCE_DEDU.FlatAppearance.BorderSize = 0
        Me.PERCE_DEDU.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Goldenrod
        Me.PERCE_DEDU.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.PERCE_DEDU.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.PERCE_DEDU.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PERCE_DEDU.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.PERCE_DEDU.Location = New System.Drawing.Point(507, 0)
        Me.PERCE_DEDU.Name = "PERCE_DEDU"
        Me.PERCE_DEDU.Size = New System.Drawing.Size(206, 34)
        Me.PERCE_DEDU.TabIndex = 4
        Me.PERCE_DEDU.Text = "Percepciones/Deducciones"
        Me.PERCE_DEDU.UseVisualStyleBackColor = True
        '
        'SubMenu_Registros
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(754, 34)
        Me.Controls.Add(Me.PERCE_DEDU)
        Me.Controls.Add(Me.BN_PUESTOS)
        Me.Controls.Add(Me.BN_DEPA)
        Me.Controls.Add(Me.BN_EMPLEADOS)
        Me.Controls.Add(Me.BN_EMPRESA)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "SubMenu_Registros"
        Me.Text = "SubMenu_Registros"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents BN_EMPRESA As Button
    Friend WithEvents BN_EMPLEADOS As Button
    Friend WithEvents BN_DEPA As Button
    Friend WithEvents BN_PUESTOS As Button
    Friend WithEvents PERCE_DEDU As Button
End Class
