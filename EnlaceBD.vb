﻿Imports System.Data
Imports System.Data.SqlClient

Public Class EnlaceBD
    Private aux As String
    Private conexion As SqlConnection
    'Private conexionNomina As SqlConnection
    Private tabla As DataTable = New DataTable
    Private adaptador As SqlDataAdapter = New SqlDataAdapter
    Private comandosql As SqlCommand = New SqlCommand

    Private Sub conectar()
        'Dim DBConnection As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("SQL").ConnectionString)        
        conexion = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("desarrollo").ConnectionString)

    End Sub

    Private Sub desconectar()
        conexion.Close()
    End Sub

    Public Function Get_Login(ByVal user As Integer, ByVal password As String) As DataTable
        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Login", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int)
            parametro1.Value = user
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@Contraseña", SqlDbType.VarChar, 20)
            parametro2.Value = password

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try

        Return data
    End Function

    Public Function Get_Empresas() As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Empresa", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim Mostrar As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            Mostrar.Value = "X"

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data
    End Function

    Public Function Get_AnioEmpresa(ByVal rfc As String) As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Empresa", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim Mostrar As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            Mostrar.Value = "A"

            Dim paramtero1 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            paramtero1.Value = rfc

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data
    End Function

    Public Function Get_Frecuencia(ByVal rfc As String, ByVal Opc As String) As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Frec_Pago", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            If (Opc = "X") Then 'Si no se utilizara la tabla Frec_Pago (donde viene el nombre de las frecuencias) entonces:
                Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                parametro1.Value = "X"

                Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
                parametro2.Value = rfc
            End If

            If (Opc = "Y") Then
                Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                parametro3.Value = "Y"
            End If

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data
    End Function

    Public Function Add_Empresa_Frecuencia(ByVal Id_Empresa As String,
                                ByVal Frecuencia As Integer) As Boolean

        Dim _bool As Boolean = True
        Try
            conectar()
            comandosql = New SqlCommand("sp_Frec_Pago", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "I"
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            parametro2.Value = Id_Empresa
            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@ID_Frec_Pago", SqlDbType.Int)
            parametro3.Value = Frecuencia

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool
    End Function

    Public Function Delete_Empresa_Frecuencia(ByVal id As String) As Boolean

        Dim _bool As Boolean = True
        Try
            conectar()
            comandosql = New SqlCommand("sp_Frec_Pago", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "D"
            Dim _id As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            _id.Value = id

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool

    End Function

    Public Function Add_Empresa(ByVal opc As String,
                                ByVal razon_social As String,
                                ByVal reg_patronal As String,
                                ByVal rfc As String,
                                ByVal fecha_Inicio_Op As String,
                                ByVal telefono As String,
                                ByVal calle As String,
                                ByVal no_exterior As Integer,
                                ByVal colonia As String,
                                ByVal CP As Integer,
                                ByVal estado As String,
                                ByVal municipio As String, ByVal Frecuencia As List(Of Integer),
                                ByVal IMSS As Decimal,
                                ByVal ISR As Decimal,
                                ByVal BonoG As Decimal,
                                ByVal PagoGE As Decimal,
                                ByVal PVacacional As Decimal) As Boolean

        Dim _bool As Boolean = True
        Dim frec As String

        Try
            conectar()
            comandosql = New SqlCommand("sp_Empresa", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = opc
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            parametro2.Value = rfc
            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@Razon_Social", SqlDbType.VarChar, 30)
            parametro3.Value = razon_social
            Dim parametro4 As SqlParameter = comandosql.Parameters.Add("@Registro_Patronal", SqlDbType.VarChar, 30)
            parametro4.Value = reg_patronal
            Dim parametro5 As SqlParameter = comandosql.Parameters.Add("@Fecha_Inicio_Op", SqlDbType.Date)
            parametro5.Value = fecha_Inicio_Op
            Dim parametro6 As SqlParameter = comandosql.Parameters.Add("@Telefono", SqlDbType.Char, 10)
            parametro6.Value = telefono
            Dim parametro7 As SqlParameter = comandosql.Parameters.Add("@Calle", SqlDbType.VarChar, 30)
            parametro7.Value = calle
            Dim parametro9 As SqlParameter = comandosql.Parameters.Add("@No_Exterior", SqlDbType.Int, 5)
            parametro9.Value = no_exterior
            Dim parametro10 As SqlParameter = comandosql.Parameters.Add("@Colonia", SqlDbType.VarChar, 30)
            parametro10.Value = colonia
            Dim parametro11 As SqlParameter = comandosql.Parameters.Add("@Estado", SqlDbType.VarChar, 30)
            parametro11.Value = estado
            Dim parametro12 As SqlParameter = comandosql.Parameters.Add("@CP", SqlDbType.Int, 6)
            parametro12.Value = CP
            Dim parametro13 As SqlParameter = comandosql.Parameters.Add("@Municipio", SqlDbType.VarChar, 20)
            parametro13.Value = municipio
            Dim parametro14 As SqlParameter = comandosql.Parameters.Add("@IMSS", SqlDbType.Decimal)
            parametro14.Value = IMSS
            Dim parametro15 As SqlParameter = comandosql.Parameters.Add("@ISR", SqlDbType.Decimal)
            parametro15.Value = ISR
            Dim parametro16 As SqlParameter = comandosql.Parameters.Add("@PVacacional", SqlDbType.Decimal)
            parametro16.Value = PVacacional
            Dim parametro17 As SqlParameter = comandosql.Parameters.Add("@BonoG", SqlDbType.Decimal)
            parametro17.Value = BonoG
            Dim parametro18 As SqlParameter = comandosql.Parameters.Add("@PagoGE", SqlDbType.Decimal)
            parametro18.Value = PagoGE

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try

        If _bool Then
            For Each frec In Frecuencia
                Add_Empresa_Frecuencia(rfc, frec)
            Next

        End If

        Return _bool
    End Function

    Public Function Edit_Empresa(ByVal opc As String,
                               ByVal razon_social As String,
                               ByVal reg_patronal As String,
                               ByVal rfc As String,
                               ByVal fecha_Inicio_Op As String,
                               ByVal telefono As String,
                               ByVal calle As String,
                               ByVal no_exterior As Integer,
                               ByVal colonia As String,
                               ByVal CP As Integer,
                               ByVal estado As String,
                               ByVal municipio As String,
                               ByVal Frecuencia As List(Of Integer),
                               ByVal IMSS As Decimal,
                               ByVal ISR As Decimal,
                               ByVal BonoG As Decimal,
                               ByVal PagoGE As Decimal,
                               ByVal PVacacional As Decimal) As Boolean

        Dim _bool As Boolean = True
        Try
            conectar()
            comandosql = New SqlCommand("sp_Empresa", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = opc
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.Char, 13)
            parametro2.Value = rfc
            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@Razon_Social", SqlDbType.VarChar, 30)
            parametro3.Value = razon_social
            Dim parametro4 As SqlParameter = comandosql.Parameters.Add("@Registro_Patronal", SqlDbType.VarChar, 30)
            parametro4.Value = reg_patronal
            Dim parametro5 As SqlParameter = comandosql.Parameters.Add("@Fecha_Inicio_Op", SqlDbType.Date)
            parametro5.Value = fecha_Inicio_Op
            Dim parametro6 As SqlParameter = comandosql.Parameters.Add("@Telefono", SqlDbType.Char, 10)
            parametro6.Value = telefono
            Dim parametro7 As SqlParameter = comandosql.Parameters.Add("@Calle", SqlDbType.VarChar, 30)
            parametro7.Value = calle
            Dim parametro9 As SqlParameter = comandosql.Parameters.Add("@No_Exterior", SqlDbType.Int, 5)
            parametro9.Value = no_exterior
            Dim parametro10 As SqlParameter = comandosql.Parameters.Add("@Colonia", SqlDbType.VarChar, 30)
            parametro10.Value = colonia
            Dim parametro11 As SqlParameter = comandosql.Parameters.Add("@Estado", SqlDbType.VarChar, 30)
            parametro11.Value = estado
            Dim parametro12 As SqlParameter = comandosql.Parameters.Add("@CP", SqlDbType.Int, 6)
            parametro12.Value = CP
            Dim parametro13 As SqlParameter = comandosql.Parameters.Add("@Municipio", SqlDbType.VarChar, 20)
            parametro13.Value = municipio
            Dim parametro14 As SqlParameter = comandosql.Parameters.Add("@IMSS", SqlDbType.Decimal)
            parametro14.Value = IMSS
            Dim parametro15 As SqlParameter = comandosql.Parameters.Add("@ISR", SqlDbType.Decimal)
            parametro15.Value = ISR
            Dim parametro16 As SqlParameter = comandosql.Parameters.Add("@PVacacional", SqlDbType.Decimal)
            parametro16.Value = PVacacional
            Dim parametro17 As SqlParameter = comandosql.Parameters.Add("@BonoG", SqlDbType.Decimal)
            parametro17.Value = BonoG
            Dim parametro18 As SqlParameter = comandosql.Parameters.Add("@PagoGE", SqlDbType.Decimal)
            parametro18.Value = PagoGE

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try

        Return _bool
    End Function


    Public Function add_EmpresaGerente(ByVal rfc As String,
                                       ByVal no_Emp As String) As Boolean

        Dim _bool As Boolean = True
        Try
            conectar()
            comandosql = New SqlCommand("sp_Empresa", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "G"
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.Char, 13)
            parametro2.Value = rfc
            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int, 10)
            parametro3.Value = no_Emp

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try

        Return _bool
    End Function

    Public Function add_DeptoGerente(ByVal No_Departamento As String,
                                       ByVal no_Emp As String) As Boolean

        Dim _bool As Boolean = True
        Try
            conectar()
            comandosql = New SqlCommand("sp_Departamentos", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "G"
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Departamento", SqlDbType.Int, 13)
            parametro2.Value = No_Departamento
            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int)
            parametro3.Value = no_Emp

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try

        Return _bool
    End Function

    Public Function Delete_Empresas(ByVal id As String) As Boolean

        Dim _bool As Boolean = True
        Try
            conectar()
            comandosql = New SqlCommand("sp_Empresa", conexion)
            comandosql.CommandType = CommandType.StoredProcedure


            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "D"
            Dim _id As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            _id.Value = id

            'Delete_Empresa_Frecuencia(id)
            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool

    End Function

    Public Function Get_Bancos() As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Banco", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim Mostrar As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            Mostrar.Value = "X"

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data
    End Function

    Public Function Get_Terminacion_Email() As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Terminacion_Email", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim Mostrar As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            Mostrar.Value = "X"

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data
    End Function

    Public Function Get_Tipo_Valor() As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Tipo_Valor", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim Mostrar As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            Mostrar.Value = "X"

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data
    End Function

    Public Function Add_Deduccion(ByVal opc As String, ByVal Nombre As String, ByVal Monto As Decimal, ByVal i As Integer) As Boolean

        Dim _bool As Boolean = True

        Try
            conectar()
            comandosql = New SqlCommand("sp_Deduccion", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = opc
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@Nombre", SqlDbType.VarChar, 30)
            parametro2.Value = Nombre
            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@Monto", SqlDbType.Decimal)
            parametro3.Value = Monto
            Dim parametro4 As SqlParameter = comandosql.Parameters.Add("@ID_Tipo_Valor", SqlDbType.Int)
            parametro4.Value = i

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool
    End Function

    Public Function Edit_Deduccion_Percepcion(ByVal opc As String, ByVal id As Integer, ByVal Monto As Decimal, ByVal i As Integer) As Boolean

        Dim _bool As Boolean = True

        Try
            conectar()
            comandosql = New SqlCommand("sp_EditConcepto", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = opc
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@ID_Concepto", SqlDbType.VarChar, 30)
            parametro2.Value = id
            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@Monto", SqlDbType.Decimal)
            parametro3.Value = Monto
            Dim parametro4 As SqlParameter = comandosql.Parameters.Add("@ID_Tipo_Valor", SqlDbType.Int)
            parametro4.Value = i

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool
    End Function

    Public Function Get_Deduccion() As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Deduccion", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim Mostrar As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            Mostrar.Value = "X"

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data
    End Function

    Public Function Get_DeduccionT() As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Deduccion", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim Mostrar As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            Mostrar.Value = "T"

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data
    End Function

    Public Function Add_Percepcion(ByVal opc As String, ByVal Nombre As String, ByVal Monto As Decimal, ByVal i As Integer) As Boolean

        Dim _bool As Boolean = True

        Try
            conectar()
            comandosql = New SqlCommand("sp_Percepcion", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = opc
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@Nombre", SqlDbType.VarChar, 30)
            parametro2.Value = Nombre
            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@Monto", SqlDbType.Decimal)
            parametro3.Value = Monto
            Dim parametro4 As SqlParameter = comandosql.Parameters.Add("@ID_Tipo_Valor", SqlDbType.Int)
            parametro4.Value = i

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool
    End Function

    Public Function Get_Percepcion() As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Percepcion", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim opc As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            opc.Value = "X"

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Get_PercepcionT() As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Percepcion", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim opc As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            opc.Value = "T"

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Delete_Percepcion(ByVal idC As Integer) As Boolean

        Dim _bool As Boolean = True
        Try
            conectar()
            comandosql = New SqlCommand("sp_Percepcion", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "D"
            Dim _id As SqlParameter = comandosql.Parameters.Add("@ID_Percepcion", SqlDbType.Int)
            _id.Value = idC

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool

    End Function

    Public Function Delete_Deduccion(ByVal idC As Integer) As Boolean

        Dim _bool As Boolean = True
        Try
            conectar()
            comandosql = New SqlCommand("sp_Deduccion", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "D"
            Dim _id As SqlParameter = comandosql.Parameters.Add("@ID_Deduccion", SqlDbType.Int)
            _id.Value = idC

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool

    End Function

    Public Function Add_Departamento(ByVal opc As String, ByVal name As String, ByVal sueldoB As Decimal, ByVal rfc As String) As Boolean

        Dim _bool As Boolean = True

        Try
            conectar()
            comandosql = New SqlCommand("sp_Departamentos", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = opc
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@Nombre", SqlDbType.VarChar, 30)
            parametro2.Value = name
            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@Sueldo_Base", SqlDbType.Decimal)
            parametro3.Value = sueldoB
            Dim parametro4 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            parametro4.Value = rfc

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool

    End Function

    Public Function Delete_Departamento(ByVal No_Departamento As Integer) As Boolean

        Dim _bool As Boolean = True
        Try
            conectar()
            comandosql = New SqlCommand("sp_Departamentos", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "D"
            Dim _id As SqlParameter = comandosql.Parameters.Add("@No_Departamento", SqlDbType.Int)
            _id.Value = No_Departamento

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool

    End Function

    Public Function Edit_Departamento(ByVal opc As String, ByVal id As Integer, ByVal sueldoB As Decimal) As Boolean

        Dim _bool As Boolean = True

        Try
            conectar()
            comandosql = New SqlCommand("sp_Departamentos", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = opc
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Departamento", SqlDbType.Int)
            parametro2.Value = id
            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@Sueldo_Base", SqlDbType.Decimal)
            parametro3.Value = sueldoB

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool

    End Function

    Public Function Get_Departamento() As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Departamentos", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim opc As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            opc.Value = "X"

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Get_Empleados() As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Empleados", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim Mostrar As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            Mostrar.Value = "X"

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Get_AnioEmpleado(ByVal No_Empleado As Integer) As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Empleados", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim Mostrar As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            Mostrar.Value = "A"

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int, 10)
            parametro1.Value = No_Empleado

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Get_Gerentes(ByVal Opc As String, ByVal ID_RFC As String, ByVal No_Depto As String) As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_GetGerentes", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            If (Opc = "G") Then
                Dim Mostrar As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar.Value = "G"
            End If


            If (Opc = "E") Then
                Dim Mostrar As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar.Value = "E"

                Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
                parametro1.Value = ID_RFC
            End If

            If (Opc = "D") Then
                Dim Mostrar As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar.Value = "D"

                Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@No_Departamento", SqlDbType.Int, 13)
                parametro1.Value = No_Depto
            End If


            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Get_EmpleadosValidacion(ByVal Opc As String,
                                            ByVal CURP As String,
                                            ByVal RFC As String,
                                            ByVal NSS As String,
                                            ByVal NO_CUENTA_BANCO As String,
                                            ByVal EMAIL As String,
                                            ByVal No_Empleado As Integer,
                                            ByVal ID_RFC As String,
                                            ByVal No_Departamento As Integer) As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_GetValidacionEmpleado", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            If (Opc = "C") Then
                Dim Mostrar1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar1.Value = "C"

                Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@CURP", SqlDbType.VarChar, 18)
                parametro1.Value = CURP
            End If

            If (Opc = "R") Then
                Dim Mostrar2 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar2.Value = "R"

                Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@RFC", SqlDbType.VarChar, 13)
                parametro2.Value = RFC
            End If

            If (Opc = "N") Then
                Dim Mostrar3 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar3.Value = "N"

                Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@NSS", SqlDbType.VarChar, 11)
                parametro3.Value = NSS
            End If

            If (Opc = "B") Then
                Dim Mostrar4 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar4.Value = "B"

                Dim parametro4 As SqlParameter = comandosql.Parameters.Add("@No_Cuenta_Banco", SqlDbType.VarChar, 24)
                parametro4.Value = NO_CUENTA_BANCO
            End If

            If (Opc = "E") Then
                Dim Mostrar5 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar5.Value = "E"

                Dim parametro5 As SqlParameter = comandosql.Parameters.Add("@Email", SqlDbType.NVarChar, 30)
                parametro5.Value = EMAIL
            End If

            If (Opc = "G") Then
                Dim Mostrar6 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar6.Value = "G"

                Dim parametro6 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int, 10)
                parametro6.Value = No_Empleado
            End If

            If (Opc = "M") Then
                Dim Mostrar7 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar7.Value = "M"

                Dim parametro7 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int, 10)
                parametro7.Value = No_Empleado

                Dim parametro8 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
                parametro8.Value = ID_RFC
            End If

            If (Opc = "D") Then
                Dim Mostrar9 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar9.Value = "D"

                Dim parametro10 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int, 10)
                parametro10.Value = No_Empleado

                Dim parametro11 As SqlParameter = comandosql.Parameters.Add("@No_Departamento", SqlDbType.Int, 10)
                parametro11.Value = No_Departamento
            End If

            If (Opc = "J") Then
                Dim Mostrar6 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar6.Value = "J"

            End If

            If (Opc = "K") Then
                Dim Mostrar7 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar7.Value = "K"

                Dim parametro8 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
                parametro8.Value = ID_RFC
            End If

            If (Opc = "L") Then
                Dim Mostrar9 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar9.Value = "L"


                Dim parametro11 As SqlParameter = comandosql.Parameters.Add("@No_Departamento", SqlDbType.Int, 10)
                parametro11.Value = No_Departamento
            End If

            If (Opc = "W") Then
                Dim Mostrar9 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar9.Value = "W"
            End If

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Get_EmpleadosValidacionModificar(ByVal Opc As String,
                                                     ByVal No_Empleado As Integer,
                                                     ByVal CURP As String,
                                                     ByVal RFC As String,
                                                     ByVal NSS As String,
                                                     ByVal NO_CUENTA_BANCO As String,
                                                     ByVal EMAIL As String) As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_GetValidacionEmpleadoModificar", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            If (Opc = "C") Then
                Dim Mostrar1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar1.Value = "C"

                Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@CURP", SqlDbType.VarChar, 18)
                parametro1.Value = CURP

                Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.VarChar, 18)
                parametro2.Value = No_Empleado
            End If

            If (Opc = "R") Then
                Dim Mostrar2 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar2.Value = "R"

                Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@RFC", SqlDbType.VarChar, 13)
                parametro1.Value = RFC

                Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.VarChar, 18)
                parametro2.Value = No_Empleado
            End If

            If (Opc = "N") Then
                Dim Mostrar3 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar3.Value = "N"

                Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@NSS", SqlDbType.VarChar, 11)
                parametro1.Value = NSS

                Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.VarChar, 18)
                parametro2.Value = No_Empleado
            End If

            If (Opc = "B") Then
                Dim Mostrar4 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar4.Value = "B"

                Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@No_Cuenta_Banco", SqlDbType.VarChar, 24)
                parametro1.Value = NO_CUENTA_BANCO

                Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.VarChar, 18)
                parametro2.Value = No_Empleado
            End If

            If (Opc = "E") Then
                Dim Mostrar5 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                Mostrar5.Value = "E"

                Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Email", SqlDbType.NVarChar, 30)
                parametro1.Value = EMAIL

                Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.VarChar, 18)
                parametro2.Value = No_Empleado
            End If

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function modificarGerentes(ByVal Opc As String, ByVal No_Empleado As Integer) As Boolean

        Dim _bool As Boolean = True

        Try
            conectar()
            comandosql = New SqlCommand("sp_GetValidacionGerentes", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = Opc
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.VarChar, 30)
            parametro2.Value = No_Empleado

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool

    End Function

    Public Function Get_NameDepartamento(ByVal rfc As String) As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("SP_NameDepartamento", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim opc As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            opc.Value = "S"
            Dim _rfc As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            _rfc.Value = rfc

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data
    End Function

    Public Function Add_Empleado(ByVal OpcAUX As String, 'UTILIZO ESTA FUNCION TANTO PARA AÑADIR COMO PARA MODIFICAR EMPLEADOS
                                    ByVal No_EmpleadoAUX2 As Integer,
                                    ByVal NombreAUX2 As String,
                                    ByVal Ape_PaternoAUX2 As String,
                                    ByVal Ape_MaternoAUX2 As String,
                                    ByVal Fecha_NacimientoAUX2 As String,
                                    ByVal EmailAUX2 As String,
                                    ByVal ID_Terminacion_EmailAUX2 As Integer,
                                    ByVal ContraseñaAUX2 As String,
                                    ByVal Fecha_PuestoAUX2 As String,
                                    ByVal CalleAUX2 As String,
                                    ByVal No_CasaAUX2 As Integer,
                                    ByVal ColoniaAUX2 As String,
                                    ByVal EstadoAUX2 As String,
                                    ByVal CP_AUX2 As Integer,
                                    ByVal MunicipioAUX2 As String,
                                    ByVal TelefonoAUX2 As String,
                                    ByVal Fecha_ContratoAUX2 As String,
                                    ByVal RFC_EMPLEADO_AUX2 As String,
                                    ByVal CURP_AUX2 As String,
                                    ByVal NSS_AUX2 As String,
                                    ByVal No_Cuenta_Banco_AUX2 As String,
                                    ByVal ID_Banco_AUX2 As Integer,
                                    ByVal ID_Puesto_AUX2 As Integer,
                                    ByVal ID_RFC_AUX2 As String,
                                    ByVal ID_Frec_PagoAUX2 As String,
                                    ByVal No_Departamento_AUX2 As Integer,
                                    ByVal GERENTE_EMPRESA_AUX2 As Boolean,
                                    ByVal GERENTE_DEPTO_AUX2 As Boolean) As Boolean


        Dim _bool As Boolean = True

        Try

            conectar()

            comandosql = New SqlCommand("sp_Empleados", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = OpcAUX
            If (OpcAUX = "M") Then
                Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int, 10)
                parametro2.Value = No_EmpleadoAUX2
            End If
            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@Nombre", SqlDbType.VarChar, 30)
            parametro3.Value = NombreAUX2
            Dim parametro4 As SqlParameter = comandosql.Parameters.Add("@Ape_Paterno", SqlDbType.VarChar, 30)
            parametro4.Value = Ape_PaternoAUX2
            Dim parametro5 As SqlParameter = comandosql.Parameters.Add("@Ape_Materno", SqlDbType.VarChar, 30)
            parametro5.Value = Ape_MaternoAUX2
            Dim parametro6 As SqlParameter = comandosql.Parameters.Add("@Fecha_Nacimiento", SqlDbType.Date)
            parametro6.Value = Fecha_NacimientoAUX2
            Dim parametro7 As SqlParameter = comandosql.Parameters.Add("@Email", SqlDbType.NVarChar, 30)
            parametro7.Value = EmailAUX2
            Dim parametro8 As SqlParameter = comandosql.Parameters.Add("@ID_Terminacion_Email", SqlDbType.Int, 10)
            parametro8.Value = ID_Terminacion_EmailAUX2
            Dim parametro9 As SqlParameter = comandosql.Parameters.Add("@Contraseña", SqlDbType.VarChar, 30)
            parametro9.Value = ContraseñaAUX2
            Dim parametro10 As SqlParameter = comandosql.Parameters.Add("@Fecha_Puesto", SqlDbType.Date)
            parametro10.Value = Fecha_PuestoAUX2
            Dim parametro11 As SqlParameter = comandosql.Parameters.Add("@Calle", SqlDbType.VarChar, 30)
            parametro11.Value = CalleAUX2
            Dim parametro12 As SqlParameter = comandosql.Parameters.Add("@No_Casa", SqlDbType.Int, 10)
            parametro12.Value = No_CasaAUX2
            Dim parametro13 As SqlParameter = comandosql.Parameters.Add("@Colonia", SqlDbType.VarChar, 20)
            parametro13.Value = ColoniaAUX2
            Dim parametro14 As SqlParameter = comandosql.Parameters.Add("@Estado", SqlDbType.VarChar, 20)
            parametro14.Value = EstadoAUX2
            Dim parametro15 As SqlParameter = comandosql.Parameters.Add("@CP", SqlDbType.Int, 5)
            parametro15.Value = CP_AUX2
            Dim parametro16 As SqlParameter = comandosql.Parameters.Add("@Municipio", SqlDbType.VarChar, 20)
            parametro16.Value = MunicipioAUX2
            Dim parametro17 As SqlParameter = comandosql.Parameters.Add("@Telefono", SqlDbType.Char, 10)
            parametro17.Value = TelefonoAUX2
            Dim parametro18 As SqlParameter = comandosql.Parameters.Add("@Fecha_Contrato", SqlDbType.Date)
            parametro18.Value = Fecha_ContratoAUX2
            Dim parametro19 As SqlParameter = comandosql.Parameters.Add("@RFC", SqlDbType.VarChar, 13)
            parametro19.Value = RFC_EMPLEADO_AUX2
            Dim parametro20 As SqlParameter = comandosql.Parameters.Add("@CURP", SqlDbType.VarChar, 18)
            parametro20.Value = CURP_AUX2
            Dim parametro21 As SqlParameter = comandosql.Parameters.Add("@NSS", SqlDbType.VarChar, 11)
            parametro21.Value = NSS_AUX2
            Dim parametro22 As SqlParameter = comandosql.Parameters.Add("@No_Cuenta_Banco", SqlDbType.VarChar, 24)
            parametro22.Value = No_Cuenta_Banco_AUX2
            Dim parametro23 As SqlParameter = comandosql.Parameters.Add("@ID_Banco", SqlDbType.Int, 10)
            parametro23.Value = ID_Banco_AUX2
            Dim parametro24 As SqlParameter = comandosql.Parameters.Add("@ID_Puesto", SqlDbType.Int, 10)
            parametro24.Value = ID_Puesto_AUX2
            Dim parametro25 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            parametro25.Value = ID_RFC_AUX2
            Dim parametro26 As SqlParameter = comandosql.Parameters.Add("@ID_Frec_Pago", SqlDbType.VarChar, 13)
            parametro26.Value = ID_Frec_PagoAUX2
            Dim parametro27 As SqlParameter = comandosql.Parameters.Add("@No_Departamento", SqlDbType.Int, 10)
            parametro27.Value = No_Departamento_AUX2

            Dim GG As SqlParameter = comandosql.Parameters.Add("@Gerente_General", SqlDbType.Bit, 1)
            GG.Value = False

            Dim parametro29 As SqlParameter = comandosql.Parameters.Add("@Gerente_Empresa", SqlDbType.Bit, 1)
            parametro29.Value = GERENTE_EMPRESA_AUX2
            Dim parametro30 As SqlParameter = comandosql.Parameters.Add("@Gerente_Depto", SqlDbType.Bit, 1)
            parametro30.Value = GERENTE_DEPTO_AUX2

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool

    End Function

    Public Function Delete_Empleado(ByVal No_Empleado As String) As Boolean

        Dim _bool As Boolean = True
        Try
            conectar()
            comandosql = New SqlCommand("sp_Empleados", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "D"
            Dim _id As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int)
            _id.Value = No_Empleado

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool

    End Function

    Public Function Get_EmpleadoEmpresa(ByVal rfc As String) As DataTable
        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_GetEmpleadoEmpresa", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim rfc_ As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            rfc_.Value = rfc

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try

        Return data
    End Function

    Public Function Get_Empleados_Empresa_Frec(ByVal rfc As String, ByVal frec As Integer) As DataTable
        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_GetEmpleEmpF", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim rfc_ As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            rfc_.Value = rfc
            Dim frec_ As SqlParameter = comandosql.Parameters.Add("@ID_Frec_Pago", SqlDbType.Int)
            frec_.Value = frec

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try

        Return data
    End Function

    Public Function Add_Puesto(ByVal opc As String, ByVal name As String, ByVal nivelS As Decimal, ByVal rfc As String, ByVal depa As Integer) As Boolean

        Dim _bool As Boolean = True

        Try

            comandosql = New SqlCommand("sp_Puestos", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = opc
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@Nombre", SqlDbType.VarChar, 30)
            parametro2.Value = name
            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@Nivel_Salarial", SqlDbType.Decimal)
            parametro3.Value = nivelS
            Dim parametro4 As SqlParameter = comandosql.Parameters.Add("@No_Departamento", SqlDbType.Int)
            parametro4.Value = depa
            Dim parametro5 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            parametro5.Value = rfc
            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool

    End Function

    Public Function Delete_Puesto(ByVal ID_Puesto As Integer) As Boolean

        Dim _bool As Boolean = True
        Try
            conectar()
            comandosql = New SqlCommand("sp_Puestos", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "D"
            Dim _id As SqlParameter = comandosql.Parameters.Add("@Id_Puesto", SqlDbType.Int)
            _id.Value = ID_Puesto

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool

    End Function

    Public Function Edit_Puesto(ByVal opc As String, ByVal id As Integer, ByVal nivelS As Decimal) As Boolean

        Dim _bool As Boolean = True

        Try
            conectar()
            comandosql = New SqlCommand("sp_Puestos", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = opc
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@Id_Puesto", SqlDbType.Int)
            parametro2.Value = id
            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@Nivel_Salarial", SqlDbType.Decimal)
            parametro3.Value = nivelS

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool

    End Function

    Public Function Get_Puesto() As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Puestos", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim opc As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            opc.Value = "X"

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Get_NamePuesto(ByVal depa As String, ByVal rfc As String) As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_GetNamePuesto", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim opc As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            opc.Value = "S"
            Dim _depa As SqlParameter = comandosql.Parameters.Add("@No_Departamento", SqlDbType.Int)
            _depa.Value = depa
            Dim rfc_ As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            rfc_.Value = rfc

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Add_ProcesoNomina(ByVal opc As String, ByVal no_Emp As Integer, ByVal fecha_PN As Date) As Boolean

        Dim bool_ As Boolean = True

        Try
            conectar()
            comandosql = New SqlCommand("sp_ProcesoNomina", conexion)
            comandosql.CommandType = CommandType.StoredProcedure


            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = opc
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int)
            parametro2.Value = no_Emp
            Dim parametro9 As SqlParameter = comandosql.Parameters.Add("@fecha_PN", SqlDbType.Date)
            parametro9.Value = fecha_PN



            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()
        Catch ex As Exception
            bool_ = False
        Finally
            desconectar()
        End Try

        Return bool_
    End Function

    Public Function Add_ProcesoNomina_T(ByVal opc As String, ByVal nomina As Integer, ByVal no_Emp As Integer, ByVal sueldo_B As Decimal, ByVal sueldo_N As Decimal, ByVal dias As Integer, ByVal IMSS As Decimal, ByVal ISR As Decimal, ByVal fecha_PN As Date, ByVal sueldoD As Decimal) As Boolean

        Dim bool_ As Boolean = True

        Try
            conectar()
            comandosql = New SqlCommand("sp_ProcesoNomina", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = opc
            Dim n As SqlParameter = comandosql.Parameters.Add("@ID_Nomina", SqlDbType.Int)
            n.Value = nomina
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int)
            parametro2.Value = no_Emp
            Dim parametro4 As SqlParameter = comandosql.Parameters.Add("@Sueldo_Bruto", SqlDbType.Decimal)
            parametro4.Value = sueldo_B
            Dim parametro5 As SqlParameter = comandosql.Parameters.Add("@Sueldo_Neto", SqlDbType.Decimal)
            parametro5.Value = sueldo_N
            Dim parametro6 As SqlParameter = comandosql.Parameters.Add("@DiasT", SqlDbType.Int)
            parametro6.Value = dias
            Dim parametro7 As SqlParameter = comandosql.Parameters.Add("@IMSS", SqlDbType.Decimal)
            parametro7.Value = IMSS
            Dim parametro8 As SqlParameter = comandosql.Parameters.Add("@ISR", SqlDbType.Decimal)
            parametro8.Value = ISR
            Dim parametro9 As SqlParameter = comandosql.Parameters.Add("@fecha_PN", SqlDbType.Date)
            parametro9.Value = fecha_PN
            Dim parametro10 As SqlParameter = comandosql.Parameters.Add("@SueldoD", SqlDbType.Decimal)
            parametro10.Value = sueldoD




            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()
        Catch ex As Exception
            bool_ = False
        Finally
            desconectar()
        End Try

        Return bool_
    End Function

    Public Function Add_ProcesoNomina_D(ByVal opc As String, ByVal no_emp As Integer, ByVal D_P As Integer, ByVal fecha As Date) As Boolean

        Dim bool_ As Boolean = True

        Try
            conectar()
            comandosql = New SqlCommand("sp_ProcesoNomina", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = opc
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@ID_Nomina", SqlDbType.Int)
            parametro2.Value = no_emp

            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@ID_Deduccion", SqlDbType.Int)
            parametro3.Value = D_P
            Dim parametro4 As SqlParameter = comandosql.Parameters.Add("@fecha_PN", SqlDbType.Date)
            parametro4.Value = fecha


            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()
        Catch ex As Exception
            bool_ = False
        Finally
            desconectar()
        End Try

        Return bool_
    End Function

    Public Function Add_ProcesoNomina_P(ByVal opc As String, ByVal no_emp As Integer, ByVal D_P As Integer, ByVal fecha As Date) As Boolean

        Dim bool_ As Boolean = True

        Try
            conectar()
            comandosql = New SqlCommand("sp_ProcesoNomina", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = opc
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@ID_Nomina", SqlDbType.Int)
            parametro2.Value = no_emp

            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@ID_Percepcion", SqlDbType.Int)
            parametro3.Value = D_P
            Dim parametro4 As SqlParameter = comandosql.Parameters.Add("@fecha_PN", SqlDbType.Date)
            parametro4.Value = fecha

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()
        Catch ex As Exception
            bool_ = False
        Finally
            desconectar()
        End Try

        Return bool_
    End Function

    Public Function Get_Ultima_Nomina_E(ByVal frec As Integer, ByVal rfc_ As String) As DataTable

        Dim data As New DataTable


        Try
            conectar()
            comandosql = New SqlCommand("sp_ProcesoNomina", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "V"

            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            parametro3.Value = rfc_
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@ID_Frec_Pago", SqlDbType.Int)
            parametro2.Value = frec

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Get_Nomina() As DataTable

        Dim data As New DataTable


        Try
            conectar()
            comandosql = New SqlCommand("sp_ProcesoNomina", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "X"

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function


    Public Function Get_NominaEmpleado(ByVal no_Emp As Integer, ByVal fecha As Date) As DataTable
        Dim data As New DataTable
        Try
            conectar()
            comandosql = New SqlCommand("sp_ProcesoNomina", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "N"

            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int)
            parametro2.Value = no_Emp
            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@fecha_PN", SqlDbType.Date)
            parametro3.Value = fecha


            adaptador.SelectCommand = comandosql
            adaptador.Fill(Data)

        Catch ex As Exception
            desconectar()
        End Try
        Return Data

    End Function

    Public Function Get_Nomina2(ByVal No_Empleado As Integer) As DataTable

        Dim data As New DataTable


        Try
            conectar()
            comandosql = New SqlCommand("sp_ProcesoNomina", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "S"

            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int)
            parametro2.Value = No_Empleado

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Get_Nomina_Percepcion_Deduccion(ByVal opc As String, ByVal D_P As Integer, ByVal fecha As Integer) As DataTable

        Dim data As New DataTable


        Try
            conectar()
            comandosql = New SqlCommand("sp_ProcesoNomina", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            If opc = "Y" Then   ''''percepciones
                Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
                parametro1.Value = opc
                Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@ID_Nomina", SqlDbType.Int)
                parametro2.Value = D_P
                Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@frec", SqlDbType.Int)
                parametro3.Value = fecha

            Else
                Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1) '''deducciones
                parametro1.Value = opc
                Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@ID_Nomina", SqlDbType.Int)
                parametro2.Value = D_P
                Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@frec", SqlDbType.Int)
                parametro3.Value = fecha

            End If

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Get_Nomina_Percepcion(ByVal D_P As Integer) As DataTable

        Dim data As New DataTable


        Try
            conectar()
            comandosql = New SqlCommand("sp_GetNominaPer", conexion)
            comandosql.CommandType = CommandType.StoredProcedure


            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@ID_Nomina", SqlDbType.Int)
            parametro2.Value = D_P

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Get_Nomina_Deduccion(ByVal D_P As Integer) As DataTable

        Dim data As New DataTable


        Try
            conectar()
            comandosql = New SqlCommand("sp_GetNominaDedu", conexion)
            comandosql.CommandType = CommandType.StoredProcedure


            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@ID_Nomina", SqlDbType.Int)
            parametro2.Value = D_P

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Get_SalarioD(ByVal no_Emp As Integer) As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_GetSalarioD", conexion)
            comandosql.CommandType = CommandType.StoredProcedure


            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int)
            parametro2.Value = no_Emp


            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Get_Incidencias() As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Incidencias", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "X"

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Add_IncidenciaEmpleado(ByVal incidencia As Integer, ByVal id_Emp As Integer, ByVal fecha As Date) As Boolean

        Dim bool_ As Boolean = True

        Try
            conectar()
            comandosql = New SqlCommand("sp_Incidencias_Empleados", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "I"
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@ID_Incidencias", SqlDbType.Int)
            parametro2.Value = incidencia
            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int)
            parametro3.Value = id_Emp
            Dim parametro4 As SqlParameter = comandosql.Parameters.Add("@Fecha", SqlDbType.Date)
            parametro4.Value = fecha

            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()
        Catch ex As Exception
            bool_ = False
        Finally
            desconectar()
        End Try

        Return bool_
    End Function

    Public Function Get_IncidenciasEmpleado(ByVal no_Emp As Integer) As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Incidencias_Empleados", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = "S"

            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int)
            parametro2.Value = no_Emp

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function actualizar_FechaProcesoNomina(ByVal opc As String, ByVal ID_RFC As String, ByVal Fecha As Date) As Boolean

        Dim _bool As Boolean = True

        Try
            conectar()
            comandosql = New SqlCommand("sp_Empresa", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@Opc", SqlDbType.Char, 1)
            parametro1.Value = opc
            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            parametro2.Value = ID_RFC
            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@Fecha_PN", SqlDbType.Date)
            parametro3.Value = Fecha


            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool
    End Function

    Public Function Get_RepNominaGeneral(ByVal ID_RFC As String, ByVal mes As Integer, ByVal anio As Integer) As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_Reporte_Nomina_General", conexion)
            comandosql.CommandType = CommandType.StoredProcedure


            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            parametro1.Value = ID_RFC

            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@Mes", SqlDbType.Int)
            parametro2.Value = mes

            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@Anio", SqlDbType.Int)
            parametro3.Value = anio

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Get_RepHeadcounter1(ByVal ID_RFC As String, ByVal No_Departamento As Integer, ByVal mes As Integer, ByVal anio As Integer) As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_ReporteHeadcounter1", conexion)
            comandosql.CommandType = CommandType.StoredProcedure


            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            parametro1.Value = ID_RFC

            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Departamento", SqlDbType.Int)
            parametro2.Value = No_Departamento

            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@Mes", SqlDbType.Int)
            parametro3.Value = mes

            Dim parametro4 As SqlParameter = comandosql.Parameters.Add("@Anio", SqlDbType.Int)
            parametro4.Value = anio

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Get_RepHeadcounter2(ByVal ID_RFC As String, ByVal No_Departamento As Integer, ByVal mes As Integer, ByVal anio As Integer) As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_ReporteHeadcounter2", conexion)
            comandosql.CommandType = CommandType.StoredProcedure


            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            parametro1.Value = ID_RFC

            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Departamento", SqlDbType.Int)
            parametro2.Value = No_Departamento

            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@Mes", SqlDbType.Int)
            parametro3.Value = mes

            Dim parametro4 As SqlParameter = comandosql.Parameters.Add("@Anio", SqlDbType.Int)
            parametro4.Value = anio

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function Get_RepNomina(ByVal ID_RFC As String, ByVal No_Departamento As Integer, ByVal mes As Integer, ByVal mes_nombre As String, ByVal anio As Integer) As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_ReporteNomina", conexion)
            comandosql.CommandType = CommandType.StoredProcedure


            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@ID_RFC", SqlDbType.VarChar, 13)
            parametro1.Value = ID_RFC

            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@No_Departamento", SqlDbType.Int)
            parametro2.Value = No_Departamento

            Dim parametro3 As SqlParameter = comandosql.Parameters.Add("@Mes", SqlDbType.Int)
            parametro3.Value = mes

            Dim parametro4 As SqlParameter = comandosql.Parameters.Add("@Mes_Nombre", SqlDbType.VarChar, 30)
            parametro4.Value = mes_nombre

            Dim parametro5 As SqlParameter = comandosql.Parameters.Add("@Anio", SqlDbType.Int)
            parametro5.Value = anio

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

    Public Function actualizar_ResumenPagosEmpl(ByVal No_Empleado As Integer, ByVal Anio As Integer) As Boolean

        Dim _bool As Boolean = True

        Try
            conectar()
            comandosql = New SqlCommand("sp_Resumen_Pagos", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            Dim parametro1 As SqlParameter = comandosql.Parameters.Add("@No_Empleado", SqlDbType.Int)
            parametro1.Value = No_Empleado

            Dim parametro2 As SqlParameter = comandosql.Parameters.Add("@Anio", SqlDbType.Int)
            parametro2.Value = Anio


            conexion.Open()
            adaptador.InsertCommand = comandosql
            comandosql.ExecuteNonQuery()

        Catch ex As SqlException
            _bool = False
        Finally
            desconectar()
        End Try
        Return _bool
    End Function

    Public Function Get_ResumenPagosEmpl() As DataTable

        Dim data As New DataTable

        Try
            conectar()
            comandosql = New SqlCommand("sp_GetResumen_Pagos", conexion)
            comandosql.CommandType = CommandType.StoredProcedure

            adaptador.SelectCommand = comandosql
            adaptador.Fill(data)

        Catch ex As Exception
            desconectar()
        End Try
        Return data

    End Function

End Class
