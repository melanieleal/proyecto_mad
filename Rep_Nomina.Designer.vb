﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Rep_Nomina
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BUSCAR_N = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.EMPRESA_N = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DEPA_N = New System.Windows.Forms.ComboBox()
        Me.MES_N = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.AÑO_N = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DG_REP_NOMINA = New System.Windows.Forms.DataGridView()
        CType(Me.DG_REP_NOMINA, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BUSCAR_N
        '
        Me.BUSCAR_N.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BUSCAR_N.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BUSCAR_N.Location = New System.Drawing.Point(789, 121)
        Me.BUSCAR_N.Name = "BUSCAR_N"
        Me.BUSCAR_N.Size = New System.Drawing.Size(69, 35)
        Me.BUSCAR_N.TabIndex = 88
        Me.BUSCAR_N.Text = "Buscar"
        Me.BUSCAR_N.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(504, 85)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 16)
        Me.Label2.TabIndex = 87
        Me.Label2.Text = "Mes"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(65, 89)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 16)
        Me.Label1.TabIndex = 86
        Me.Label1.Text = "Empresa."
        '
        'EMPRESA_N
        '
        Me.EMPRESA_N.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.EMPRESA_N.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EMPRESA_N.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EMPRESA_N.FormattingEnabled = True
        Me.EMPRESA_N.Location = New System.Drawing.Point(148, 85)
        Me.EMPRESA_N.Name = "EMPRESA_N"
        Me.EMPRESA_N.Size = New System.Drawing.Size(207, 25)
        Me.EMPRESA_N.TabIndex = 84
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(65, 119)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(105, 16)
        Me.Label3.TabIndex = 90
        Me.Label3.Text = "Departamento."
        '
        'DEPA_N
        '
        Me.DEPA_N.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DEPA_N.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.DEPA_N.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DEPA_N.FormattingEnabled = True
        Me.DEPA_N.Location = New System.Drawing.Point(176, 115)
        Me.DEPA_N.Name = "DEPA_N"
        Me.DEPA_N.Size = New System.Drawing.Size(207, 25)
        Me.DEPA_N.TabIndex = 89
        '
        'MES_N
        '
        Me.MES_N.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MES_N.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.MES_N.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MES_N.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MES_N.FormattingEnabled = True
        Me.MES_N.Location = New System.Drawing.Point(544, 82)
        Me.MES_N.Name = "MES_N"
        Me.MES_N.Size = New System.Drawing.Size(120, 25)
        Me.MES_N.TabIndex = 91
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(681, 85)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(34, 16)
        Me.Label4.TabIndex = 92
        Me.Label4.Text = "Año"
        '
        'AÑO_N
        '
        Me.AÑO_N.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.AÑO_N.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.AÑO_N.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.AÑO_N.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AÑO_N.FormattingEnabled = True
        Me.AÑO_N.Location = New System.Drawing.Point(721, 81)
        Me.AÑO_N.Name = "AÑO_N"
        Me.AÑO_N.Size = New System.Drawing.Size(63, 25)
        Me.AÑO_N.TabIndex = 93
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(12, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(195, 23)
        Me.Label5.TabIndex = 99
        Me.Label5.Text = "Reporte de Nomina."
        '
        'DG_REP_NOMINA
        '
        Me.DG_REP_NOMINA.AllowUserToDeleteRows = False
        Me.DG_REP_NOMINA.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DG_REP_NOMINA.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DG_REP_NOMINA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG_REP_NOMINA.Location = New System.Drawing.Point(42, 182)
        Me.DG_REP_NOMINA.Name = "DG_REP_NOMINA"
        Me.DG_REP_NOMINA.ReadOnly = True
        Me.DG_REP_NOMINA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DG_REP_NOMINA.Size = New System.Drawing.Size(815, 427)
        Me.DG_REP_NOMINA.TabIndex = 100
        '
        'Rep_Nomina
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(900, 666)
        Me.Controls.Add(Me.DG_REP_NOMINA)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.AÑO_N)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.MES_N)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.DEPA_N)
        Me.Controls.Add(Me.BUSCAR_N)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.EMPRESA_N)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Rep_Nomina"
        Me.Text = "Rep_Nomina"
        CType(Me.DG_REP_NOMINA, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BUSCAR_N As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents EMPRESA_N As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents DEPA_N As ComboBox
    Friend WithEvents MES_N As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents AÑO_N As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents DG_REP_NOMINA As DataGridView
End Class
