﻿Public Class Reg_Departamento
    Dim conexion As New EnlaceBD
    Dim tabla As New DataTable
    Dim val As New Funciones

    Dim obj As New EnlaceBD
    Dim result As Boolean = False
    Dim modificar As Boolean = False
    Dim id As Integer

    Private Sub NAME_DEP_KeyPress(sender As Object, e As KeyPressEventArgs) Handles NAME_DEP.KeyPress
        'Codigo letras y espacios
        val.EvaluaSoloLetras(e)
    End Sub

    Private Sub SUELDO_BASE_KeyPress(sender As Object, e As KeyPressEventArgs) Handles SUELDO_BASE.KeyPress
        'Codigo con numeros decimales
        Dim Cadena As String = SUELDO_BASE.Text
        Dim v As Boolean
        Dim Filtro As String = "1234567890"
        If Len(Cadena) > 0 Then
            Filtro += "."
        End If

        For Each caracter In Filtro
            If e.KeyChar = caracter Then
                e.Handled = False
                v = True
                Exit For
            Else
                e.Handled = True
                v = False
            End If
        Next

        If Char.IsControl(e.KeyChar) Then
            e.Handled = False
            v = True
        End If

        If e.KeyChar = "." And Not Cadena.IndexOf(".") Then
            e.Handled = True
            v = False
        End If

        If v = False Then
            MessageBox.Show("Ingrese solo  Numeros con un numero decimal" & vbNewLine & " No Ingrese espacios", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub Reg_Departamento_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        tabla = conexion.Get_Empresas()
        Me.EMP_DEPA.DataSource = tabla
        Me.EMP_DEPA.DisplayMember = "Nombre"
        Me.EMP_DEPA.ValueMember = "RFC"

        tabla = conexion.Get_Departamento()
        DG_DEPARTAMENTO.DataSource = tabla

    End Sub

    Private Sub ACTUALIZAR_DEP_Click(sender As Object, e As EventArgs) Handles ACTUALIZAR_DEP.Click
        Dim name As String
        Dim sueldoB As Decimal
        Dim rfc As String
        Dim opc As String
        Dim i As Integer
        Dim v As Boolean = False


        If (Len(NAME_DEP.Text) = 0 Or Len(SUELDO_BASE.Text) = 0) Then
            MessageBox.Show("Ingrese todos los campos", "Departamento", MessageBoxButtons.OK, MessageBoxIcon.Warning)

        Else

            name = NAME_DEP.Text
            sueldoB = SUELDO_BASE.Text
            rfc = EMP_DEPA.Text

            tabla = conexion.Get_Empresas()         'obtengo el rfc de la empresa 
            i = tabla.Rows.Count
            For fila As Integer = 0 To i - 1 Step 1
                If tabla.Rows(fila).Item(1) = rfc Then
                    rfc = tabla.Rows(fila).Item(0)
                End If
            Next                                     'obtengo el rfc de la empresa 

            If modificar Then
                opc = "M"

                result = obj.Edit_Departamento(opc, id, sueldoB)

                If result Then
                    MessageBox.Show("Departamento modificado con exito", "Departamento", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    NAME_DEP.Clear()
                    SUELDO_BASE.Clear()
                    NAME_DEP.Enabled = True
                    EMP_DEPA.Enabled = True
                    modificar = False
                Else
                    MessageBox.Show("Hubo un error, su departamento no se modifico", "Departamento", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If

            Else
                opc = "I"
                tabla = conexion.Get_NameDepartamento(rfc)  'saber si ya existe el departamento en la empresa
                i = tabla.Rows.Count
                For fila As Integer = 0 To i - 1 Step 1
                    If tabla.Rows(fila).Item(1) = name Then
                        v = True
                    End If
                Next                                        'saber si ya existe el departamento en la empresa

                If v Then
                    MessageBox.Show("El Departamento ya existe en la empresa por favor ingrese otro", "Departamento", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                Else
                    result = obj.Add_Departamento(opc, name, sueldoB, rfc)

                    If result Then
                        MessageBox.Show("Departamento agregado con exito", "Departamento", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        NAME_DEP.Clear()
                        SUELDO_BASE.Clear()
                    Else
                        MessageBox.Show("Hubo un error, su departamento no se añadio" & vbCrLf & "Verifique que su nombre de departamento no exista en la Empresa", "Departamento", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If

                End If
            End If

            tabla = conexion.Get_Departamento()
            DG_DEPARTAMENTO.DataSource = tabla

        End If

    End Sub

    Private Sub DG_DEPARTAMENTO_Click(sender As Object, e As EventArgs) Handles DG_DEPARTAMENTO.Click


        If (Len(Trim(DG_DEPARTAMENTO.CurrentRow.Cells.Item(0).Value.ToString())) <> 0) Then
            NAME_DEP.Enabled = False
            EMP_DEPA.Enabled = False
            modificar = True

            id = DG_DEPARTAMENTO.CurrentRow.Cells.Item(0).Value.ToString()
            NAME_DEP.Text = DG_DEPARTAMENTO.CurrentRow.Cells.Item(1).Value.ToString()
            EMP_DEPA.Text = DG_DEPARTAMENTO.CurrentRow.Cells.Item(4).Value.ToString()
            SUELDO_BASE.Text = DG_DEPARTAMENTO.CurrentRow.Cells.Item(2).Value.ToString()

        Else

            NAME_DEP.Clear()
            SUELDO_BASE.Clear()
            NAME_DEP.Enabled = True
            EMP_DEPA.Enabled = True

        End If

    End Sub

    Private Sub DELETE_DEP_Click(sender As Object, e As EventArgs) Handles DELETE_DEP.Click
        If (modificar = True) Then
            If MessageBox.Show("Seguro que Desea Eliminar", "Departamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then


                result = obj.Delete_Departamento(DG_DEPARTAMENTO.CurrentRow.Cells.Item(0).Value.ToString())

                If result Then
                    MessageBox.Show("El Departamento ha sido eliminado exitosamente", "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    NAME_DEP.Clear()
                    SUELDO_BASE.Clear()
                    NAME_DEP.Enabled = True
                    EMP_DEPA.Enabled = True
                    modificar = False

                    tabla = conexion.Get_Departamento()
                    DG_DEPARTAMENTO.DataSource = tabla
                Else
                    MessageBox.Show("Hubo un error, su Departamento no se elimino", "Departamentos", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                End If

            Else
                NAME_DEP.Clear()
                SUELDO_BASE.Clear()
                NAME_DEP.Enabled = True
                EMP_DEPA.Enabled = True
                modificar = False
            End If
        End If
    End Sub

End Class