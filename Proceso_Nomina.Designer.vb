﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Proceso_Nomina
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.DG_PERCE_NOMINA = New System.Windows.Forms.DataGridView()
        Me.DG_DEDU_NOMINA = New System.Windows.Forms.DataGridView()
        Me.DG_EMPLEADOS_NOMINA = New System.Windows.Forms.DataGridView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CB_EMPRESA_NOMINA = New System.Windows.Forms.ComboBox()
        Me.CB_FREC_NOMINA = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.PROC_NOMINA = New System.Windows.Forms.Button()
        Me.AÑADIR_DEDU = New System.Windows.Forms.Button()
        Me.AÑADIR_PERCE = New System.Windows.Forms.Button()
        Me.FECHA_ACTUAL = New System.Windows.Forms.Label()
        Me.FECHA1 = New System.Windows.Forms.Label()
        Me.FECHA2 = New System.Windows.Forms.Label()
        Me.FECHA3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        CType(Me.DG_PERCE_NOMINA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DG_DEDU_NOMINA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DG_EMPLEADOS_NOMINA, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DG_PERCE_NOMINA
        '
        Me.DG_PERCE_NOMINA.AllowUserToDeleteRows = False
        Me.DG_PERCE_NOMINA.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.DG_PERCE_NOMINA.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DG_PERCE_NOMINA.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DG_PERCE_NOMINA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG_PERCE_NOMINA.Location = New System.Drawing.Point(507, 222)
        Me.DG_PERCE_NOMINA.Name = "DG_PERCE_NOMINA"
        Me.DG_PERCE_NOMINA.ReadOnly = True
        Me.DG_PERCE_NOMINA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DG_PERCE_NOMINA.Size = New System.Drawing.Size(344, 134)
        Me.DG_PERCE_NOMINA.TabIndex = 0
        '
        'DG_DEDU_NOMINA
        '
        Me.DG_DEDU_NOMINA.AllowUserToDeleteRows = False
        Me.DG_DEDU_NOMINA.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.DG_DEDU_NOMINA.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DG_DEDU_NOMINA.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DG_DEDU_NOMINA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG_DEDU_NOMINA.Location = New System.Drawing.Point(507, 452)
        Me.DG_DEDU_NOMINA.Name = "DG_DEDU_NOMINA"
        Me.DG_DEDU_NOMINA.ReadOnly = True
        Me.DG_DEDU_NOMINA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DG_DEDU_NOMINA.Size = New System.Drawing.Size(344, 134)
        Me.DG_DEDU_NOMINA.TabIndex = 1
        '
        'DG_EMPLEADOS_NOMINA
        '
        Me.DG_EMPLEADOS_NOMINA.AllowUserToDeleteRows = False
        Me.DG_EMPLEADOS_NOMINA.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.DG_EMPLEADOS_NOMINA.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DG_EMPLEADOS_NOMINA.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DG_EMPLEADOS_NOMINA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG_EMPLEADOS_NOMINA.Location = New System.Drawing.Point(60, 202)
        Me.DG_EMPLEADOS_NOMINA.Name = "DG_EMPLEADOS_NOMINA"
        Me.DG_EMPLEADOS_NOMINA.ReadOnly = True
        Me.DG_EMPLEADOS_NOMINA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DG_EMPLEADOS_NOMINA.Size = New System.Drawing.Size(344, 374)
        Me.DG_EMPLEADOS_NOMINA.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(504, 426)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(92, 16)
        Me.Label4.TabIndex = 112
        Me.Label4.Text = "Deducciones"
        '
        'Label14
        '
        Me.Label14.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(504, 202)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(95, 16)
        Me.Label14.TabIndex = 111
        Me.Label14.Text = "Percepciones"
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(71, 166)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 16)
        Me.Label1.TabIndex = 113
        Me.Label1.Text = "Empleados"
        '
        'CB_EMPRESA_NOMINA
        '
        Me.CB_EMPRESA_NOMINA.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.CB_EMPRESA_NOMINA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_EMPRESA_NOMINA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CB_EMPRESA_NOMINA.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CB_EMPRESA_NOMINA.FormattingEnabled = True
        Me.CB_EMPRESA_NOMINA.Location = New System.Drawing.Point(84, 51)
        Me.CB_EMPRESA_NOMINA.Name = "CB_EMPRESA_NOMINA"
        Me.CB_EMPRESA_NOMINA.Size = New System.Drawing.Size(208, 25)
        Me.CB_EMPRESA_NOMINA.TabIndex = 114
        '
        'CB_FREC_NOMINA
        '
        Me.CB_FREC_NOMINA.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.CB_FREC_NOMINA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_FREC_NOMINA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CB_FREC_NOMINA.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CB_FREC_NOMINA.FormattingEnabled = True
        Me.CB_FREC_NOMINA.Location = New System.Drawing.Point(84, 102)
        Me.CB_FREC_NOMINA.Name = "CB_FREC_NOMINA"
        Me.CB_FREC_NOMINA.Size = New System.Drawing.Size(208, 25)
        Me.CB_FREC_NOMINA.TabIndex = 115
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(25, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 16)
        Me.Label2.TabIndex = 116
        Me.Label2.Text = "Empresa"
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(25, 83)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(142, 16)
        Me.Label3.TabIndex = 117
        Me.Label3.Text = "Frecuencia de Pago."
        '
        'PROC_NOMINA
        '
        Me.PROC_NOMINA.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PROC_NOMINA.Location = New System.Drawing.Point(725, 102)
        Me.PROC_NOMINA.Name = "PROC_NOMINA"
        Me.PROC_NOMINA.Size = New System.Drawing.Size(126, 38)
        Me.PROC_NOMINA.TabIndex = 118
        Me.PROC_NOMINA.Text = "Efectuar Proceso"
        Me.PROC_NOMINA.UseVisualStyleBackColor = True
        '
        'AÑADIR_DEDU
        '
        Me.AÑADIR_DEDU.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.AÑADIR_DEDU.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AÑADIR_DEDU.Location = New System.Drawing.Point(776, 592)
        Me.AÑADIR_DEDU.Name = "AÑADIR_DEDU"
        Me.AÑADIR_DEDU.Size = New System.Drawing.Size(75, 37)
        Me.AÑADIR_DEDU.TabIndex = 119
        Me.AÑADIR_DEDU.Text = "Añadir"
        Me.AÑADIR_DEDU.UseVisualStyleBackColor = True
        '
        'AÑADIR_PERCE
        '
        Me.AÑADIR_PERCE.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.AÑADIR_PERCE.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AÑADIR_PERCE.Location = New System.Drawing.Point(776, 362)
        Me.AÑADIR_PERCE.Name = "AÑADIR_PERCE"
        Me.AÑADIR_PERCE.Size = New System.Drawing.Size(75, 37)
        Me.AÑADIR_PERCE.TabIndex = 120
        Me.AÑADIR_PERCE.Text = "Añadir"
        Me.AÑADIR_PERCE.UseVisualStyleBackColor = True
        '
        'FECHA_ACTUAL
        '
        Me.FECHA_ACTUAL.AutoSize = True
        Me.FECHA_ACTUAL.Location = New System.Drawing.Point(753, 7)
        Me.FECHA_ACTUAL.Name = "FECHA_ACTUAL"
        Me.FECHA_ACTUAL.Size = New System.Drawing.Size(49, 13)
        Me.FECHA_ACTUAL.TabIndex = 121
        Me.FECHA_ACTUAL.Text = "Domingo"
        '
        'FECHA1
        '
        Me.FECHA1.AutoSize = True
        Me.FECHA1.Location = New System.Drawing.Point(801, 8)
        Me.FECHA1.Name = "FECHA1"
        Me.FECHA1.Size = New System.Drawing.Size(19, 13)
        Me.FECHA1.TabIndex = 122
        Me.FECHA1.Text = "11"
        '
        'FECHA2
        '
        Me.FECHA2.AutoSize = True
        Me.FECHA2.Location = New System.Drawing.Point(825, 8)
        Me.FECHA2.Name = "FECHA2"
        Me.FECHA2.Size = New System.Drawing.Size(19, 13)
        Me.FECHA2.TabIndex = 123
        Me.FECHA2.Text = "11"
        '
        'FECHA3
        '
        Me.FECHA3.AutoSize = True
        Me.FECHA3.Location = New System.Drawing.Point(856, 8)
        Me.FECHA3.Name = "FECHA3"
        Me.FECHA3.Size = New System.Drawing.Size(37, 13)
        Me.FECHA3.TabIndex = 124
        Me.FECHA3.Text = "Fecha"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(818, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(10, 13)
        Me.Label5.TabIndex = 125
        Me.Label5.Text = "-"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(845, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(10, 13)
        Me.Label6.TabIndex = 126
        Me.Label6.Text = "-"
        '
        'Proceso_Nomina
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(900, 666)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.FECHA3)
        Me.Controls.Add(Me.FECHA2)
        Me.Controls.Add(Me.FECHA1)
        Me.Controls.Add(Me.FECHA_ACTUAL)
        Me.Controls.Add(Me.AÑADIR_PERCE)
        Me.Controls.Add(Me.AÑADIR_DEDU)
        Me.Controls.Add(Me.PROC_NOMINA)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.CB_FREC_NOMINA)
        Me.Controls.Add(Me.CB_EMPRESA_NOMINA)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.DG_EMPLEADOS_NOMINA)
        Me.Controls.Add(Me.DG_DEDU_NOMINA)
        Me.Controls.Add(Me.DG_PERCE_NOMINA)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Proceso_Nomina"
        Me.Text = "Reg_Gerente"
        CType(Me.DG_PERCE_NOMINA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DG_DEDU_NOMINA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DG_EMPLEADOS_NOMINA, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DG_PERCE_NOMINA As DataGridView
    Friend WithEvents DG_DEDU_NOMINA As DataGridView
    Friend WithEvents DG_EMPLEADOS_NOMINA As DataGridView
    Friend WithEvents Label4 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents CB_EMPRESA_NOMINA As ComboBox
    Friend WithEvents CB_FREC_NOMINA As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents PROC_NOMINA As Button
    Friend WithEvents AÑADIR_DEDU As Button
    Friend WithEvents AÑADIR_PERCE As Button
    Friend WithEvents FECHA_ACTUAL As Label
    Friend WithEvents FECHA1 As Label
    Friend WithEvents FECHA2 As Label
    Friend WithEvents FECHA3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
End Class
