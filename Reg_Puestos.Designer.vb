﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Reg_Puestos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.EMPRESA_PUESTO = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.DEPA_PUESTO = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.NAME_PUESTO = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.NIVEL_SALARIAL = New System.Windows.Forms.TextBox()
        Me.DELETE_PUESTO = New System.Windows.Forms.Button()
        Me.ACTUALIZAR_PUESTO = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.DG_PUESTOS = New System.Windows.Forms.DataGridView()
        CType(Me.DG_PUESTOS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label25
        '
        Me.Label25.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(86, 104)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(67, 17)
        Me.Label25.TabIndex = 51
        Me.Label25.Text = "Empresa."
        '
        'EMPRESA_PUESTO
        '
        Me.EMPRESA_PUESTO.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.EMPRESA_PUESTO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.EMPRESA_PUESTO.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EMPRESA_PUESTO.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EMPRESA_PUESTO.FormattingEnabled = True
        Me.EMPRESA_PUESTO.Location = New System.Drawing.Point(182, 103)
        Me.EMPRESA_PUESTO.Name = "EMPRESA_PUESTO"
        Me.EMPRESA_PUESTO.Size = New System.Drawing.Size(328, 25)
        Me.EMPRESA_PUESTO.TabIndex = 50
        '
        'Label11
        '
        Me.Label11.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(86, 139)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(109, 17)
        Me.Label11.TabIndex = 55
        Me.Label11.Text = "Departamento."
        '
        'DEPA_PUESTO
        '
        Me.DEPA_PUESTO.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.DEPA_PUESTO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DEPA_PUESTO.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.DEPA_PUESTO.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DEPA_PUESTO.FormattingEnabled = True
        Me.DEPA_PUESTO.Location = New System.Drawing.Point(201, 137)
        Me.DEPA_PUESTO.Name = "DEPA_PUESTO"
        Me.DEPA_PUESTO.Size = New System.Drawing.Size(309, 25)
        Me.DEPA_PUESTO.TabIndex = 54
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(396, 411)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(136, 17)
        Me.Label2.TabIndex = 57
        Me.Label2.Text = "Nombre del Puesto."
        '
        'NAME_PUESTO
        '
        Me.NAME_PUESTO.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.NAME_PUESTO.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NAME_PUESTO.Location = New System.Drawing.Point(538, 411)
        Me.NAME_PUESTO.Name = "NAME_PUESTO"
        Me.NAME_PUESTO.Size = New System.Drawing.Size(282, 13)
        Me.NAME_PUESTO.TabIndex = 56
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(396, 451)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(94, 17)
        Me.Label1.TabIndex = 59
        Me.Label1.Text = "Nivel Salarial."
        '
        'NIVEL_SALARIAL
        '
        Me.NIVEL_SALARIAL.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.NIVEL_SALARIAL.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NIVEL_SALARIAL.Location = New System.Drawing.Point(496, 450)
        Me.NIVEL_SALARIAL.Name = "NIVEL_SALARIAL"
        Me.NIVEL_SALARIAL.Size = New System.Drawing.Size(82, 13)
        Me.NIVEL_SALARIAL.TabIndex = 58
        '
        'DELETE_PUESTO
        '
        Me.DELETE_PUESTO.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.DELETE_PUESTO.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DELETE_PUESTO.Location = New System.Drawing.Point(751, 501)
        Me.DELETE_PUESTO.Name = "DELETE_PUESTO"
        Me.DELETE_PUESTO.Size = New System.Drawing.Size(69, 35)
        Me.DELETE_PUESTO.TabIndex = 81
        Me.DELETE_PUESTO.Text = "Eliminar"
        Me.DELETE_PUESTO.UseVisualStyleBackColor = True
        '
        'ACTUALIZAR_PUESTO
        '
        Me.ACTUALIZAR_PUESTO.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.ACTUALIZAR_PUESTO.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ACTUALIZAR_PUESTO.Location = New System.Drawing.Point(656, 501)
        Me.ACTUALIZAR_PUESTO.Name = "ACTUALIZAR_PUESTO"
        Me.ACTUALIZAR_PUESTO.Size = New System.Drawing.Size(69, 35)
        Me.ACTUALIZAR_PUESTO.TabIndex = 80
        Me.ACTUALIZAR_PUESTO.Text = "Actualizar"
        Me.ACTUALIZAR_PUESTO.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(61, 71)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(59, 16)
        Me.Label14.TabIndex = 79
        Me.Label14.Text = "Puestos."
        '
        'DG_PUESTOS
        '
        Me.DG_PUESTOS.AllowUserToDeleteRows = False
        Me.DG_PUESTOS.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DG_PUESTOS.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DG_PUESTOS.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DG_PUESTOS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG_PUESTOS.Location = New System.Drawing.Point(52, 209)
        Me.DG_PUESTOS.Name = "DG_PUESTOS"
        Me.DG_PUESTOS.ReadOnly = True
        Me.DG_PUESTOS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DG_PUESTOS.Size = New System.Drawing.Size(782, 120)
        Me.DG_PUESTOS.TabIndex = 83
        '
        'Reg_Puestos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(900, 666)
        Me.Controls.Add(Me.DG_PUESTOS)
        Me.Controls.Add(Me.DELETE_PUESTO)
        Me.Controls.Add(Me.ACTUALIZAR_PUESTO)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.NIVEL_SALARIAL)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.NAME_PUESTO)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.DEPA_PUESTO)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.EMPRESA_PUESTO)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Reg_Puestos"
        Me.Text = "Reg_Puestos"
        CType(Me.DG_PUESTOS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label25 As Label
    Friend WithEvents EMPRESA_PUESTO As ComboBox
    Friend WithEvents Label11 As Label
    Friend WithEvents DEPA_PUESTO As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents NAME_PUESTO As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents NIVEL_SALARIAL As TextBox
    Friend WithEvents DELETE_PUESTO As Button
    Friend WithEvents ACTUALIZAR_PUESTO As Button
    Friend WithEvents Label14 As Label
    Friend WithEvents DG_PUESTOS As DataGridView
End Class
