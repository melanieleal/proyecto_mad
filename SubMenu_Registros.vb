﻿Public Class SubMenu_Registros

    Private Sub SubMenu_Registros_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim tabla As New DataTable
        Dim conexion As New EnlaceBD

        tabla = conexion.Get_Login(Login.USERNAME.Text, Login.PASSWORD.Text)
        If tabla.Rows(0).Item(3) = True Then

            BN_EMPRESA.Enabled = False
        Else
            BN_EMPRESA.Enabled = True
        End If

    End Sub


    'mostrar ventanas en un panel
    Private Sub AbrirForm(ByVal formHijo As Object)
        If MenuF.PANEL_VENTANAS.Controls.Count > 0 Then
            MenuF.PANEL_VENTANAS.Controls.RemoveAt(0)         'remueve lo que haya en el panel
        End If
        Dim formH As Form = TryCast(formHijo, Form)
        formH.TopLevel = False
        formH.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        formH.Dock = DockStyle.Fill
        MenuF.PANEL_VENTANAS.Controls.Add(formH)              'añade la nueva ventana al panel
        MenuF.PANEL_VENTANAS.Tag = formH
        formH.Show()
    End Sub

    Private Sub BN_EMPRESA_Click(sender As Object, e As EventArgs) Handles BN_EMPRESA.Click
        AbrirForm(New Reg_Empresas)
    End Sub

    Private Sub BN_EMPLEADOS_Click(sender As Object, e As EventArgs) Handles BN_EMPLEADOS.Click
        AbrirForm(New Reg_Empleados)
    End Sub

    Private Sub BN_DEPA_Click(sender As Object, e As EventArgs) Handles BN_DEPA.Click
        AbrirForm(New Reg_Departamento)
    End Sub

    Private Sub BN_PUESTOS_Click(sender As Object, e As EventArgs) Handles BN_PUESTOS.Click
        AbrirForm(New Reg_Puestos)
    End Sub

    Private Sub BN_PERCEPCIONES_Click(sender As Object, e As EventArgs) Handles PERCE_DEDU.Click
        AbrirForm(New Reg_Percepciones)
    End Sub

End Class