use Proyect_MAD 

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_Tipo_Valor' AND type = 'P') 
BEGIN
    DROP PROCEDURE sp_Tipo_Valor;
END

GO
CREATE PROCEDURE sp_Tipo_Valor(
	
	@Opc					 char(1),
	@ID_Tipo_Valor           int = null, 
    @Nombre                  varchar(30) = null
	 
)
AS
BEGIN

		IF @Opc = 'I'
			BEGIN
			
				INSERT INTO Tipo_Valor (Nombre)					
				VALUES (@Nombre)
			END

		IF @Opc = 'X'
			BEGIN
			
				SELECT ID_Tipo_Valor, 
					   Nombre		
					FROM Tipo_Valor 
				
			END

		IF @Opc = 'S'
		BEGIN
			
			SELECT ID_Tipo_Valor, 
					Nombre		
			FROM Tipo_Valor 
			where @ID_Tipo_Valor = ID_Tipo_Valor
		END
END

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_EditConcepto' AND type = 'P') 
BEGIN
    DROP PROCEDURE sp_EditConcepto;
END

GO
CREATE PROCEDURE sp_EditConcepto(	
	@Opc				 char(1),
	@ID_Concepto         int = null,
    @Monto               decimal (5,2)  = null,
	@ID_Tipo_Valor       int = null
	
		 
)
as 
begin
	IF @Opc = 'M' 
	
		BEGIN
		
				UPDATE Concepto 
					SET		
							Monto = ISNULL( @Monto , Monto),
							ID_Tipo_Valor = ISNULL(@ID_Tipo_Valor , ID_Tipo_Valor )
				WHERE ID_Concepto = @ID_Concepto
	

		END

end


GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_Deduccion' AND type = 'P') 
BEGIN
    DROP PROCEDURE sp_Deduccion;
END

GO
CREATE PROCEDURE sp_Deduccion(	
	@Opc				  char(1),
	@ID_Deduccion         int = null,
    @Nombre              varchar(30) = null,
    @Monto               decimal (5,2)  = null,
	@ID_Tipo_Valor       int = null
		 
)
AS
			DECLARE @Activo              bit
			    set @Activo = 1
			DECLARE	@ID_Concepto         int 
BEGIN

IF @Opc = 'I'
	
	BEGIN
	    

	    
		INSERT INTO Concepto(Nombre, Monto, ID_Tipo_Valor)
		VALUES (@Nombre, @Monto, @ID_Tipo_Valor)
		SET @ID_Concepto = SCOPE_IDENTITY()

		INSERT INTO Deducciones (ID_Concepto , Activo)
		VALUES (@ID_Concepto, @Activo)

	END

	IF @Opc = 'D'
	
	BEGIN
		
		UPDATE Deducciones 
		SET Activo = 0
		WHERE ID_Deduccion = @ID_Deduccion
			
		
	END


	IF  @Opc = 'X'
	BEGIN
	
			SELECT D.ID_Deduccion 'Clave de Deducci�n',
			       C.ID_Concepto 'Clave de Concepto',
			       C.Nombre,
				   T.ID_Tipo_Valor 'Clave Forma de Monto',
				   T.Nombre  'Forma de Monto',
				   C.Monto,
				   D.Activo 	
				FROM Deducciones D
				INNER JOIN   Concepto C
				ON C.ID_Concepto = D.ID_Concepto
				INNER JOIN Tipo_Valor T
				ON T.ID_Tipo_Valor = C.ID_Tipo_Valor
				WHERE Activo = @Activo 
	END

	IF  @Opc = 'T'
	BEGIN
	
			SELECT D.ID_Deduccion 'Clave de Deducci�n',
			       C.ID_Concepto 'Clave de Concepto',
			       C.Nombre,
				   T.ID_Tipo_Valor 'Clave Forma de Monto',
				   T.Nombre  'Forma de Monto',
				   C.Monto,
				   D.Activo 	
				FROM Deducciones D
				INNER JOIN   Concepto C
				ON C.ID_Concepto = D.ID_Concepto
				INNER JOIN Tipo_Valor T
				ON T.ID_Tipo_Valor = C.ID_Tipo_Valor
					 
	END

END

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_Percepcion' AND type = 'P') 
BEGIN
    DROP PROCEDURE sp_Percepcion;
END

GO
CREATE PROCEDURE sp_Percepcion(	
	@Opc				   char(1),
	@ID_Percepcion         int = null,
    @Nombre                varchar(30) = null,
    @Monto                 decimal (10,2)  = null,
	@ID_Tipo_Valor         int = null
		 
)
AS
			DECLARE @Activo              bit
			    set @Activo = 1
			DECLARE	@ID_Concepto         int 
BEGIN

IF @Opc = 'I'
	
	BEGIN
	    	    
		INSERT INTO Concepto(Nombre, Monto, ID_Tipo_Valor)
		VALUES (@Nombre, @Monto, @ID_Tipo_Valor)
		SET @ID_Concepto = SCOPE_IDENTITY()

		INSERT INTO Percepciones (ID_Concepto , Activo)
		VALUES (@ID_Concepto, @Activo)

	END

	IF @Opc = 'D'
	
	BEGIN
		
		UPDATE Percepciones 
		SET Activo = 0
		WHERE ID_Percepcion = @ID_Percepcion 
					
	END


	IF  @Opc = 'X'
	BEGIN
	
			SELECT P.ID_Percepcion 'Clave de Percepci�n', --0
			       C.ID_Concepto 'Clave de Concepto',--1
			       C.Nombre,--2
				   T.ID_Tipo_Valor 'Clave Forma de Monto', --3
				   T.Nombre  'Forma de Monto', --4
				   C.Monto, --5
				   P.Activo 	
				FROM Percepciones P
				INNER JOIN  Concepto C
				ON C.ID_Concepto = P.ID_Concepto
				INNER JOIN Tipo_Valor T
				ON T.ID_Tipo_Valor = C.ID_Tipo_Valor
				WHERE Activo = @Activo 
	END

	IF  @Opc = 'T'
	BEGIN
	
			SELECT P.ID_Percepcion 'Clave de Percepci�n', --0
			       C.ID_Concepto 'Clave de Concepto',--1
			       C.Nombre,--2
				   T.ID_Tipo_Valor 'Clave Forma de Monto', --3
				   T.Nombre  'Forma de Monto', --4
				   C.Monto, --5
				   P.Activo 	
				FROM Percepciones P
				INNER JOIN  Concepto C
				ON C.ID_Concepto = P.ID_Concepto
				INNER JOIN Tipo_Valor T
				ON T.ID_Tipo_Valor = C.ID_Tipo_Valor

	END
END

