﻿GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_Reporte_Nomina_General' and type = 'P')
BEGIN
	DROP PROCEDURE sp_Reporte_Nomina_General
END
GO 
CREATE PROCEDURE sp_Reporte_Nomina_General(
	@ID_RFC  varchar(13) = null,
	@Mes     int = null,
	@Anio    int = null
)
AS
BEGIN
	BEGIN
		SELECT [Nombre],
        [Departamento],
		[Puesto],
		[Fecha de Contrato],
		[Fecha de Nacimiento],
		[Salario Diario] 	   
		FROM Reporte_Nomina_General
		WHERE ID_RFC = @ID_RFC
		 AND ((YEAR([Fecha de Contrato]) < @Anio) OR (YEAR([Fecha de Contrato]) = @Anio AND MONTH([Fecha de Contrato]) <= @Mes))

	END
END

------------------------------------------------------------------------------------------------------------------------------

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_ReporteHeadcounter1' and type = 'P')
BEGIN
	DROP PROCEDURE sp_ReporteHeadcounter1
END
GO 
CREATE PROCEDURE sp_ReporteHeadcounter1(
	@ID_RFC           varchar(13) = null,
	@No_Departamento  int = null,
	@Mes     int = null,
	@Anio    int = null
)
AS
BEGIN
	BEGIN
		SELECT [Empresa],
        [Departamento],
		[Gerente Departamento],
		[Puesto],
		dbo.fnCantidadEmplPuesto([ID_Puesto], @Mes, @Anio) 'Cantidad de empleados en el puesto'
		FROM Reporte_Headcounter1
		WHERE ID_RFC = @ID_RFC AND No_Departamento = @No_Departamento
		
	END
END

------------------------------------------------------------------------------------------------------------------------------

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_ReporteHeadcounter2' and type = 'P')
BEGIN
	DROP PROCEDURE sp_ReporteHeadcounter2
END
GO 
CREATE PROCEDURE sp_ReporteHeadcounter2(
	@ID_RFC           varchar(13) = null,
	@No_Departamento  int = null,
	@Mes     int = null,
	@Anio    int = null
)
AS
BEGIN
	BEGIN
		SELECT [Empresa],
        [Departamento],
		[Gerente Departamento],
		dbo.fnCantidadEmplDepto([Numero Departamento], @Mes, @Anio) 'Cantidad de empleados en el departamento',
		[Fecha de última ejecución del proceso de nómina]   
		FROM Reporte_Headcounter2
		WHERE ID_RFC = @ID_RFC AND No_Departamento = @No_Departamento
		
	END
END

------------------------------------------------------------------------------------------------------------------------------

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_ReporteNomina' and type = 'P')
BEGIN
	DROP PROCEDURE sp_ReporteNomina
END
GO 
CREATE PROCEDURE sp_ReporteNomina(
	@ID_RFC             varchar(13) = null,
	@No_Departamento    int = null,
	@Mes				int = null,
	@Mes_Nombre         varchar(30) = null,
	@Anio               int = null
)
AS
BEGIN
	BEGIN
		SELECT [Empresa],
        [Departamento],
		[Gerente Departamento],
		@Anio 'Año',
		@Mes_Nombre 'Mes',
		dbo.fnSumatoriaIMSS([Numero Departamento], @Mes, @Anio) '∑Pago IMSS',
		dbo.fnSumatoriaISR([Numero Departamento], @Mes, @Anio) '∑ISR',
		dbo.fnSumatoriaSueldoBruto([Numero Departamento], @Mes, @Anio) '∑Sueldo Bruto',
		dbo.fnSumatoriaSueldoNeto([Numero Departamento], @Mes, @Anio) '∑Sueldo Neto'  
		FROM Reporte_Nomina
		WHERE ID_RFC = @ID_RFC AND No_Departamento = @No_Departamento
		
	END
END

------------------------------------------------------------------------------------------------------------------------------

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_Resumen_Pagos' and type = 'P')
BEGIN
	DROP PROCEDURE sp_Resumen_Pagos
END
GO 
CREATE PROCEDURE sp_Resumen_Pagos(
	@No_Empleado        int = null,
	@Anio               int = null
)
AS
BEGIN

			DECLARE @mes int,  @RFC varchar(13), @NSS varchar(11)

			SELECT 
			@RFC = RFC,
			@NSS = NSS
			FROM Empleado
			WHERE No_Empleado = @No_Empleado


			UPDATE Resumen_Pago
			SET Anio = @Anio

			UPDATE Resumen_Pago
			SET No_Empleado = @No_Empleado
			
			UPDATE Resumen_Pago
			SET RFC = @RFC

			UPDATE Resumen_Pago
			SET NSS = @NSS

			--IMSS--------------------------------------------------------------------------------------------------------------

			SELECT @mes = 1
			WHILE @mes < 13
			    BEGIN
					 UPDATE Resumen_Pago
					 SET Suma_IMSS = dbo.fnSumatoria_Empl_IMSS(@No_Empleado, @mes, @Anio)
					 WHERE ID_Mes = @mes
					 SET @mes = @mes + 1;
				END

			--ISR--------------------------------------------------------------------------------------------------------------

			SELECT @mes = 1
			WHILE @mes < 13
			    BEGIN
					 UPDATE Resumen_Pago
					 SET Suma_ISR = dbo.fnSumatoria_Empl_ISR(@No_Empleado, @mes, @Anio)
					 WHERE ID_Mes = @mes
					 SET @mes = @mes + 1;
				END

			--Sueldo Bruto--------------------------------------------------------------------------------------------------------------

			SELECT @mes = 1
			WHILE @mes < 13
			    BEGIN
					 UPDATE Resumen_Pago
					 SET Suma_Sueldo_Bruto = dbo.fnSumatoria_Empl_SueldoBruto(@No_Empleado, @mes, @Anio)
					 WHERE ID_Mes = @mes
					 SET @mes = @mes + 1;
				END

			--Sueldo Neto--------------------------------------------------------------------------------------------------------------

			SELECT @mes = 1
			WHILE @mes < 13
			    BEGIN
					 UPDATE Resumen_Pago
					 SET Suma_Sueldo_Neto = dbo.fnSumatoria_Empl_SueldoNeto(@No_Empleado, @mes, @Anio)
					 WHERE ID_Mes = @mes
					 SET @mes = @mes + 1;
				END

			
END

------------------------------------------------------------------------------------------------------------------------------

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_GetResumen_Pagos' and type = 'P')
BEGIN
	DROP PROCEDURE sp_GetResumen_Pagos
END
GO 

CREATE PROCEDURE sp_GetResumen_Pagos
AS
BEGIN
			SELECT 
			RFC,
			NSS,
			Anio 'Año',
			Mes,
			Suma_IMSS '∑Pago IMSS',
			Suma_ISR '∑ISR',
			Suma_Sueldo_Bruto '∑Sueldo Bruto',
			Suma_Sueldo_Neto '∑Sueldo Neto'
			FROM Resumen_Pago
	
END