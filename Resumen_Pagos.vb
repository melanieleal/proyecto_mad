﻿Public Class Resumen_Pagos
    Dim conexion As New EnlaceBD
    Dim tabla As New DataTable
    Dim tabla_ResumenPago As New DataTable
    Dim rfc_ As String
    Dim i As Integer
    Dim No_Empleado As Integer = Login.USERNAME.Text


    Private Sub Resumen_Pagos_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim fecha_Inicio_Op As DateTime
        tabla = conexion.Get_AnioEmpleado(No_Empleado)
        If (tabla.Rows.Count <> 0) Then

            fecha_Inicio_Op = tabla.Rows(0).Item(1)
            AÑO.Items.Add(Format(fecha_Inicio_Op, "yyyy"))
            AÑO.Text = Format(fecha_Inicio_Op, "yyyy")
            Dim anio_Inicio As Integer = Format(fecha_Inicio_Op, "yyyy")
            Dim anios_Operando As Integer = Date.Now.Date.Year - Format(fecha_Inicio_Op, "yyyy")
            If (anios_Operando <> 0) Then
                For anio As Integer = 1 To anios_Operando Step 1
                    anio_Inicio = anio_Inicio + 1
                    AÑO.Items.Add(anio_Inicio)
                Next
            End If
        End If
    End Sub

    Private Sub BUSCAR_H_MouseClick(sender As Object, e As MouseEventArgs) Handles BUSCAR_H.MouseClick

        conexion.actualizar_ResumenPagosEmpl(No_Empleado, AÑO.Text)

        tabla_ResumenPago = conexion.Get_ResumenPagosEmpl()
        DG_RESUMEN_PAGO.DataSource = tabla_ResumenPago

    End Sub
End Class