use Proyect_MAD 

GO 
IF EXISTS( SELECT 1 FROM sysobjects WHERE name = 'sp_Incidencias' and type = 'P')
BEGIN
	DROP PROCEDURE sp_Incidencias
END
GO 
CREATE PROCEDURE sp_Incidencias(
	@Opc            char(1),
	@ID_Incidencias int = null,
	@Nombre         varchar(30) = null
)
AS
BEGIN

	IF @Opc = 'X'
	BEGIN
		SELECT ID_Incidencias,
			   Nombre
		FROM Incidencias
		ORDER BY ID_Incidencias 
	END

END

GO 
IF EXISTS( SELECT 1 FROM sysobjects WHERE name = 'sp_Incidencias_Empleados' and type = 'P')
BEGIN
	DROP PROCEDURE sp_Incidencias_Empleados
END
GO 
CREATE PROCEDURE sp_Incidencias_Empleados(
	@Opc	char(1),
	@ID_Incidencias int = null,
	@No_Empleado int = null,
	@Fecha       date = null
)
AS

		DECLARE @Activo  bit
		SET @Activo = 1
BEGIN
	IF @Opc = 'I'
	BEGIN
		INSERT INTO Incidencias_Empleados(ID_Incidencias, No_Empleado, Fecha, Activo )
		VALUES(@ID_Incidencias, @No_Empleado, @Fecha, @Activo)
	END

		IF @Opc = 'S'
	BEGIN
		SELECT ID_Incidencias,
			   No_Empleado,
			   Fecha
		FROM Incidencias_Empleados 
		WHERE No_Empleado = @No_Empleado  
	END

	IF @Opc = 'D'
	BEGIN
		DELETE  
		FROM Incidencias_Empleados
		WHERE No_Empleado = @No_Empleado 
	END
END

