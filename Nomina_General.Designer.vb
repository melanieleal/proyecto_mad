﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Nomina_General
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CB_EMPRESA = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BUSCAR_NG = New System.Windows.Forms.Button()
        Me.AÑO_N = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.MES_N = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DG_NOMINA_GENERAL = New System.Windows.Forms.DataGridView()
        CType(Me.DG_NOMINA_GENERAL, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CB_EMPRESA
        '
        Me.CB_EMPRESA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CB_EMPRESA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CB_EMPRESA.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CB_EMPRESA.FormattingEnabled = True
        Me.CB_EMPRESA.Location = New System.Drawing.Point(138, 83)
        Me.CB_EMPRESA.Name = "CB_EMPRESA"
        Me.CB_EMPRESA.Size = New System.Drawing.Size(207, 25)
        Me.CB_EMPRESA.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(55, 87)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Empresa."
        '
        'BUSCAR_NG
        '
        Me.BUSCAR_NG.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BUSCAR_NG.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BUSCAR_NG.Location = New System.Drawing.Point(780, 112)
        Me.BUSCAR_NG.Name = "BUSCAR_NG"
        Me.BUSCAR_NG.Size = New System.Drawing.Size(69, 35)
        Me.BUSCAR_NG.TabIndex = 82
        Me.BUSCAR_NG.Text = "Buscar"
        Me.BUSCAR_NG.UseVisualStyleBackColor = True
        '
        'AÑO_N
        '
        Me.AÑO_N.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.AÑO_N.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.AÑO_N.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.AÑO_N.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AÑO_N.FormattingEnabled = True
        Me.AÑO_N.Location = New System.Drawing.Point(653, 83)
        Me.AÑO_N.Name = "AÑO_N"
        Me.AÑO_N.Size = New System.Drawing.Size(63, 25)
        Me.AÑO_N.TabIndex = 97
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(613, 87)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(34, 16)
        Me.Label4.TabIndex = 96
        Me.Label4.Text = "Año"
        '
        'MES_N
        '
        Me.MES_N.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MES_N.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.MES_N.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MES_N.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MES_N.FormattingEnabled = True
        Me.MES_N.Location = New System.Drawing.Point(476, 83)
        Me.MES_N.Name = "MES_N"
        Me.MES_N.Size = New System.Drawing.Size(120, 25)
        Me.MES_N.TabIndex = 95
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(436, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 16)
        Me.Label2.TabIndex = 94
        Me.Label2.Text = "Mes"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(275, 23)
        Me.Label3.TabIndex = 98
        Me.Label3.Text = "Reporte de Nomina General."
        '
        'DG_NOMINA_GENERAL
        '
        Me.DG_NOMINA_GENERAL.AllowUserToDeleteRows = False
        Me.DG_NOMINA_GENERAL.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DG_NOMINA_GENERAL.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DG_NOMINA_GENERAL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG_NOMINA_GENERAL.Location = New System.Drawing.Point(33, 174)
        Me.DG_NOMINA_GENERAL.Name = "DG_NOMINA_GENERAL"
        Me.DG_NOMINA_GENERAL.ReadOnly = True
        Me.DG_NOMINA_GENERAL.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DG_NOMINA_GENERAL.Size = New System.Drawing.Size(816, 442)
        Me.DG_NOMINA_GENERAL.TabIndex = 99
        '
        'Nomina_General
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(900, 666)
        Me.Controls.Add(Me.DG_NOMINA_GENERAL)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.AÑO_N)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.MES_N)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.BUSCAR_NG)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CB_EMPRESA)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Nomina_General"
        Me.Text = "Nomina_General"
        CType(Me.DG_NOMINA_GENERAL, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CB_EMPRESA As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents BUSCAR_NG As Button
    Friend WithEvents AÑO_N As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents MES_N As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents DG_NOMINA_GENERAL As DataGridView
End Class
