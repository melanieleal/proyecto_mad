﻿Public Class Solicitudes
    Dim conexion As New EnlaceBD
    Dim tabla As New DataTable

    Private Sub Solicitudes_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        tabla = conexion.Get_Incidencias()
        Me.TIPO.DataSource = tabla
        Me.TIPO.DisplayMember = "Nombre"
        Me.TIPO.ValueMember = "ID_Incidencias"

        tabla = conexion.Get_Empleados()
        Dim i As Integer = tabla.Rows().Count
        For fila As Integer = 0 To i - 1 Step 1
            If tabla.Rows(fila).Item(0) = Login.USERNAME.Text Then
                NAME_EMP.Text = tabla.Rows(fila).Item(1) & " " & tabla.Rows(fila).Item(2) & " " & tabla.Rows(fila).Item(3)
            End If
        Next

        ID_EMP.Text = Login.USERNAME.Text

    End Sub

    Private Sub CONSECUENCIA_KeyPress(sender As Object, e As KeyPressEventArgs) Handles CONSECUENCIA.KeyPress
        'Codigo letras, numeros y espacios
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
            MessageBox.Show("Ingrese solo Letras y Numeros", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub ACTUALIZAR_DEP_Click(sender As Object, e As EventArgs) Handles ACTUALIZAR_DEP.Click
        Dim result As Boolean

        Dim incidencia As Integer
        Dim fecha As String

        tabla = conexion.Get_Incidencias()
        Dim i As Integer = tabla.Rows().Count
        For fila As Integer = 0 To i - 1 Step 1
            If tabla.Rows(fila).Item(1) = TIPO.Text Then
                incidencia = tabla.Rows(fila).Item(0)
            End If
        Next

        If TIPO.Text = "Falta" Or TIPO.Text = "Accidente" Or TIPO.Text = "Otro" Then
            fecha = Format(FECHA_V1.Value, "yyyy-MM-dd")

            If Format(FECHA_V1.Value, "yyyy-MM-dd") > Date.Now.Date Then
                MessageBox.Show("No puede ingresar una fecha mayor a la actual", "Incidencias", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                result = conexion.Add_IncidenciaEmpleado(incidencia, Login.USERNAME.Text, fecha)
                CONSECUENCIA.Clear()
                FECHA_V1.Value = Date.Now.Date
                TIPO.Text = "Vacaciones"


                If result Then
                    MessageBox.Show("Incidencia agregada con exito", "Incidencia", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            End If

        Else
            fecha = Format(FECHA_V1.Value, "yyyy-MM-dd")


            If Format(FECHA_V1.Value, "yyyy-MM-dd") < Date.Now.Date Then
                MessageBox.Show("No puede ingresar una fecha menor a la actual", "Incidencias", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else

                Dim no_Emp = ID_EMP.Text
                tabla = conexion.Get_IncidenciasEmpleado(no_Emp)
                i = tabla.Rows().Count

                Dim id As Integer
                Dim tablaI As New DataTable
                tablaI = conexion.Get_Incidencias()
                Dim inci As Integer = tablaI.Rows().Count

                For filaI As Integer = 0 To inci - 1 Step 1
                    If tablaI.Rows(filaI).Item(1) = TIPO.Text Then
                        id = tablaI.Rows(filaI).Item(0)
                    End If
                Next


                Dim v As Boolean = False
                If i > 0 Then
                    For fila As Integer = 0 To i - 1 Step 1

                        Dim fechaI As Date = tabla.Rows(fila).Item(2)
                        Dim anio As String = Format(fechaI, "yyyy")
                        If tabla.Rows(fila).Item(0) = id And anio = Date.Now.Date.Year Then
                            v = True
                        End If
                    Next
                End If

                If v = False Then
                    result = conexion.Add_IncidenciaEmpleado(incidencia, no_Emp, fecha)
                    CONSECUENCIA.Clear()
                    TIPO.Text = "Vacaciones"
                    FECHA_V1.Value = Date.Now.Date


                    If result Then
                        MessageBox.Show("Incidencia agregada con exito", "Incidencia", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If

                Else
                    MessageBox.Show("Solo puede solicitar vacaciones una vez al año", "Incidencia", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            End If

        End If

    End Sub

End Class