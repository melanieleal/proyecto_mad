﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Solicitudes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.ACTUALIZAR_DEP = New System.Windows.Forms.Button()
        Me.TIPO = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.FECHA_V1 = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CONSECUENCIA = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ID_EMP = New System.Windows.Forms.Label()
        Me.NAME_EMP = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 39)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(110, 17)
        Me.Label4.TabIndex = 114
        Me.Label4.Text = "No.  Empleado."
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 17)
        Me.Label2.TabIndex = 113
        Me.Label2.Text = "Nombre."
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Panel2.Location = New System.Drawing.Point(12, 113)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(876, 2)
        Me.Panel2.TabIndex = 117
        '
        'ACTUALIZAR_DEP
        '
        Me.ACTUALIZAR_DEP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ACTUALIZAR_DEP.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ACTUALIZAR_DEP.Location = New System.Drawing.Point(806, 580)
        Me.ACTUALIZAR_DEP.Name = "ACTUALIZAR_DEP"
        Me.ACTUALIZAR_DEP.Size = New System.Drawing.Size(69, 36)
        Me.ACTUALIZAR_DEP.TabIndex = 118
        Me.ACTUALIZAR_DEP.Text = "Aceptar"
        Me.ACTUALIZAR_DEP.UseVisualStyleBackColor = True
        '
        'TIPO
        '
        Me.TIPO.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TIPO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.TIPO.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.TIPO.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TIPO.FormattingEnabled = True
        Me.TIPO.Location = New System.Drawing.Point(89, 237)
        Me.TIPO.Name = "TIPO"
        Me.TIPO.Size = New System.Drawing.Size(194, 25)
        Me.TIPO.TabIndex = 125
        '
        'Label5
        '
        Me.Label5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(55, 216)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(38, 17)
        Me.Label5.TabIndex = 124
        Me.Label5.Text = "Tipo."
        '
        'FECHA_V1
        '
        Me.FECHA_V1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FECHA_V1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FECHA_V1.Location = New System.Drawing.Point(89, 416)
        Me.FECHA_V1.Name = "FECHA_V1"
        Me.FECHA_V1.Size = New System.Drawing.Size(121, 20)
        Me.FECHA_V1.TabIndex = 122
        '
        'Label8
        '
        Me.Label8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(62, 391)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(51, 17)
        Me.Label8.TabIndex = 119
        Me.Label8.Text = "Fecha."
        '
        'CONSECUENCIA
        '
        Me.CONSECUENCIA.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CONSECUENCIA.Location = New System.Drawing.Point(460, 298)
        Me.CONSECUENCIA.Multiline = True
        Me.CONSECUENCIA.Name = "CONSECUENCIA"
        Me.CONSECUENCIA.Size = New System.Drawing.Size(306, 144)
        Me.CONSECUENCIA.TabIndex = 127
        '
        'Label7
        '
        Me.Label7.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(418, 261)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(105, 17)
        Me.Label7.TabIndex = 126
        Me.Label7.Text = "Consecuencia."
        '
        'ID_EMP
        '
        Me.ID_EMP.AutoSize = True
        Me.ID_EMP.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.ID_EMP.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ID_EMP.Location = New System.Drawing.Point(124, 39)
        Me.ID_EMP.Name = "ID_EMP"
        Me.ID_EMP.Size = New System.Drawing.Size(29, 17)
        Me.ID_EMP.TabIndex = 128
        Me.ID_EMP.Text = "011"
        '
        'NAME_EMP
        '
        Me.NAME_EMP.AutoSize = True
        Me.NAME_EMP.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.NAME_EMP.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NAME_EMP.Location = New System.Drawing.Point(83, 69)
        Me.NAME_EMP.Name = "NAME_EMP"
        Me.NAME_EMP.Size = New System.Drawing.Size(29, 17)
        Me.NAME_EMP.TabIndex = 129
        Me.NAME_EMP.Text = "011"
        '
        'Solicitudes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(900, 666)
        Me.Controls.Add(Me.NAME_EMP)
        Me.Controls.Add(Me.ID_EMP)
        Me.Controls.Add(Me.CONSECUENCIA)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.TIPO)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.FECHA_V1)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.ACTUALIZAR_DEP)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Solicitudes"
        Me.Text = "Incidencias"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label4 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents ACTUALIZAR_DEP As Button
    Friend WithEvents TIPO As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents FECHA_V1 As DateTimePicker
    Friend WithEvents Label8 As Label
    Friend WithEvents CONSECUENCIA As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents ID_EMP As Label
    Friend WithEvents NAME_EMP As Label
End Class
