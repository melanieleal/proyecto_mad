﻿Public Class Nomina_General

    Dim conexion As New EnlaceBD
    Dim tabla_empresas As New DataTable
    Dim tabla_empleados As New DataTable
    Dim i As Integer

    Private Sub Nomina_General_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        tabla_empresas = conexion.Get_Empresas()
        Me.CB_EMPRESA.DataSource = tabla_empresas
        Me.CB_EMPRESA.DisplayMember = "Nombre"
        Me.CB_EMPRESA.ValueMember = "RFC"

        MES_N.Items.Add("Enero")
        MES_N.Items.Add("Febrero")
        MES_N.Items.Add("Marzo")
        MES_N.Items.Add("Abril")
        MES_N.Items.Add("Mayo")
        MES_N.Items.Add("Junio")
        MES_N.Items.Add("Julio")
        MES_N.Items.Add("Agosto")
        MES_N.Items.Add("Septiembre")
        MES_N.Items.Add("Octubre")
        MES_N.Items.Add("Noviembre")
        MES_N.Items.Add("Diciembre")
        MES_N.Text = "Enero"

    End Sub

    Private Sub CB_EMPRESA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_EMPRESA.SelectedIndexChanged
        AÑO_N.Items.Clear()

        Dim rfc_ As String = CB_EMPRESA.Text
        tabla_empresas = conexion.Get_Empresas()         'obtengo el rfc de la empresa 
        i = tabla_empresas.Rows.Count
        For fila As Integer = 0 To i - 1 Step 1
            If tabla_empresas.Rows(fila).Item(1) = rfc_ Then
                rfc_ = tabla_empresas.Rows(fila).Item(0)
            End If
        Next

        Dim fecha_Inicio_Op As DateTime
        tabla_empresas = conexion.Get_AnioEmpresa(rfc_)
        If (tabla_empresas.Rows.Count <> 0) Then

            fecha_Inicio_Op = tabla_empresas.Rows(0).Item(1)
            AÑO_N.Items.Add(Format(fecha_Inicio_Op, "yyyy"))
            AÑO_N.Text = Format(fecha_Inicio_Op, "yyyy")
            Dim anio_Inicio As Integer = Format(fecha_Inicio_Op, "yyyy")
            Dim anios_Operando As Integer = Date.Now.Date.Year - Format(fecha_Inicio_Op, "yyyy")
            If (anios_Operando <> 0) Then
                For anio As Integer = 1 To anios_Operando Step 1
                    anio_Inicio = anio_Inicio + 1
                    AÑO_N.Items.Add(anio_Inicio)
                Next
            End If
        End If

    End Sub

    Private Sub BUSCAR_NG_MouseClick(sender As Object, e As MouseEventArgs) Handles BUSCAR_NG.MouseClick

        Dim rfc_ As String = CB_EMPRESA.Text
        tabla_empresas = conexion.Get_Empresas()         'obtengo el rfc de la empresa 
        i = tabla_empresas.Rows.Count
        For fila As Integer = 0 To i - 1 Step 1
            If tabla_empresas.Rows(fila).Item(1) = CB_EMPRESA.Text Then
                rfc_ = tabla_empresas.Rows(fila).Item(0)
            End If
        Next

        Dim mes As Integer = MES_N.SelectedIndex + 1
        Dim anio As Integer = AÑO_N.Text

        tabla_empleados = conexion.Get_RepNominaGeneral(rfc_, mes, anio)
        i = tabla_empleados.Rows.Count
        DG_NOMINA_GENERAL.DataSource = tabla_empleados


    End Sub
End Class