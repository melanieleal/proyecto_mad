﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Rep_Headcounter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.AÑO_H = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.MES_H = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DEPA_H = New System.Windows.Forms.ComboBox()
        Me.BUSCAR_H = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.EMPRESA_H = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DG_HEADCOUNTER_PUESTOS = New System.Windows.Forms.DataGridView()
        Me.DG_HEADCOUNTER_DEPA = New System.Windows.Forms.DataGridView()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        CType(Me.DG_HEADCOUNTER_PUESTOS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DG_HEADCOUNTER_DEPA, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'AÑO_H
        '
        Me.AÑO_H.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.AÑO_H.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.AÑO_H.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.AÑO_H.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AÑO_H.FormattingEnabled = True
        Me.AÑO_H.Location = New System.Drawing.Point(721, 81)
        Me.AÑO_H.Name = "AÑO_H"
        Me.AÑO_H.Size = New System.Drawing.Size(63, 25)
        Me.AÑO_H.TabIndex = 103
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(681, 85)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(34, 16)
        Me.Label4.TabIndex = 102
        Me.Label4.Text = "Año"
        '
        'MES_H
        '
        Me.MES_H.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MES_H.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.MES_H.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.MES_H.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MES_H.FormattingEnabled = True
        Me.MES_H.Location = New System.Drawing.Point(544, 82)
        Me.MES_H.Name = "MES_H"
        Me.MES_H.Size = New System.Drawing.Size(120, 25)
        Me.MES_H.TabIndex = 101
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(65, 119)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(105, 16)
        Me.Label3.TabIndex = 100
        Me.Label3.Text = "Departamento."
        '
        'DEPA_H
        '
        Me.DEPA_H.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DEPA_H.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.DEPA_H.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DEPA_H.FormattingEnabled = True
        Me.DEPA_H.Location = New System.Drawing.Point(176, 115)
        Me.DEPA_H.Name = "DEPA_H"
        Me.DEPA_H.Size = New System.Drawing.Size(207, 25)
        Me.DEPA_H.TabIndex = 99
        '
        'BUSCAR_H
        '
        Me.BUSCAR_H.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BUSCAR_H.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BUSCAR_H.Location = New System.Drawing.Point(789, 121)
        Me.BUSCAR_H.Name = "BUSCAR_H"
        Me.BUSCAR_H.Size = New System.Drawing.Size(69, 35)
        Me.BUSCAR_H.TabIndex = 98
        Me.BUSCAR_H.Text = "Buscar"
        Me.BUSCAR_H.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(504, 85)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 16)
        Me.Label2.TabIndex = 97
        Me.Label2.Text = "Mes"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(65, 89)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 16)
        Me.Label1.TabIndex = 96
        Me.Label1.Text = "Empresa."
        '
        'EMPRESA_H
        '
        Me.EMPRESA_H.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.EMPRESA_H.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EMPRESA_H.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EMPRESA_H.FormattingEnabled = True
        Me.EMPRESA_H.Location = New System.Drawing.Point(148, 85)
        Me.EMPRESA_H.Name = "EMPRESA_H"
        Me.EMPRESA_H.Size = New System.Drawing.Size(207, 25)
        Me.EMPRESA_H.TabIndex = 95
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(12, 17)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(243, 23)
        Me.Label5.TabIndex = 105
        Me.Label5.Text = "Reporte de Headcounter."
        '
        'DG_HEADCOUNTER_PUESTOS
        '
        Me.DG_HEADCOUNTER_PUESTOS.AllowUserToDeleteRows = False
        Me.DG_HEADCOUNTER_PUESTOS.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DG_HEADCOUNTER_PUESTOS.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DG_HEADCOUNTER_PUESTOS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG_HEADCOUNTER_PUESTOS.Location = New System.Drawing.Point(31, 437)
        Me.DG_HEADCOUNTER_PUESTOS.Name = "DG_HEADCOUNTER_PUESTOS"
        Me.DG_HEADCOUNTER_PUESTOS.ReadOnly = True
        Me.DG_HEADCOUNTER_PUESTOS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DG_HEADCOUNTER_PUESTOS.Size = New System.Drawing.Size(850, 205)
        Me.DG_HEADCOUNTER_PUESTOS.TabIndex = 106
        '
        'DG_HEADCOUNTER_DEPA
        '
        Me.DG_HEADCOUNTER_DEPA.AllowUserToDeleteRows = False
        Me.DG_HEADCOUNTER_DEPA.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DG_HEADCOUNTER_DEPA.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DG_HEADCOUNTER_DEPA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG_HEADCOUNTER_DEPA.Location = New System.Drawing.Point(31, 186)
        Me.DG_HEADCOUNTER_DEPA.Name = "DG_HEADCOUNTER_DEPA"
        Me.DG_HEADCOUNTER_DEPA.ReadOnly = True
        Me.DG_HEADCOUNTER_DEPA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DG_HEADCOUNTER_DEPA.Size = New System.Drawing.Size(850, 205)
        Me.DG_HEADCOUNTER_DEPA.TabIndex = 107
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(28, 167)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(105, 16)
        Me.Label6.TabIndex = 108
        Me.Label6.Text = "Departamento."
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(28, 409)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(55, 16)
        Me.Label7.TabIndex = 109
        Me.Label7.Text = "Puestos"
        '
        'Rep_Headcounter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(900, 666)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.DG_HEADCOUNTER_DEPA)
        Me.Controls.Add(Me.DG_HEADCOUNTER_PUESTOS)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.AÑO_H)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.MES_H)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.DEPA_H)
        Me.Controls.Add(Me.BUSCAR_H)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.EMPRESA_H)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Rep_Headcounter"
        Me.Text = "Rep_Headcounter"
        CType(Me.DG_HEADCOUNTER_PUESTOS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DG_HEADCOUNTER_DEPA, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents AÑO_H As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents MES_H As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents DEPA_H As ComboBox
    Friend WithEvents BUSCAR_H As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents EMPRESA_H As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents DG_HEADCOUNTER_PUESTOS As DataGridView
    Friend WithEvents DG_HEADCOUNTER_DEPA As DataGridView
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
End Class
