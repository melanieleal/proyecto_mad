
DROP FUNCTION fnSalarioD

GO
CREATE FUNCTION fnSalarioD(@No_Empleado int)
RETURNS decimal(10,2)

AS

BEGIN

		DECLARE @SueldoD  decimal(10,2)
		SELECT

			  @SueldoD = P.Nivel_Salarial * D.Sueldo_Base 

		FROM Empleado E
			INNER JOIN Puestos P
			ON E.ID_Puesto = P.ID_Puesto 
			INNER JOIN Departamentos  D
			ON E.No_Departamento = D.No_Departamento
		WHERE E.No_Empleado = @No_Empleado  

	RETURN @SueldoD
END
------------------------------------------------------------------------------------------------------------------------

DROP FUNCTION fnCantidadEmplPuesto

GO
CREATE FUNCTION fnCantidadEmplPuesto(@ID_Puesto int, @Mes int, @Anio int)
RETURNS int

AS

BEGIN

        DECLARE @CantidadEmpleados  int

		SELECT
			  @CantidadEmpleados = COUNT (No_Empleado)
			  FROM Empleado 
			  WHERE ID_Puesto = @ID_Puesto 
			  AND ((YEAR(Fecha_Contrato) < @Anio) OR (YEAR(Fecha_Contrato) = @Anio AND MONTH(Fecha_Contrato) <= @Mes))
		 

	RETURN @CantidadEmpleados
END

------------------------------------------------------------------------------------------------------------------------

DROP FUNCTION fnCantidadEmplDepto
GO
CREATE FUNCTION fnCantidadEmplDepto(@No_Departamento int, @Mes int, @Anio int)
RETURNS int

AS

BEGIN

        DECLARE @CantidadEmpleados  int

		SELECT
			  @CantidadEmpleados = COUNT (No_Empleado)
			  FROM Empleado 
			  WHERE No_Departamento = @No_Departamento
			  AND ((YEAR(Fecha_Contrato) < @Anio) OR (YEAR(Fecha_Contrato) = @Anio AND MONTH(Fecha_Contrato) <= @Mes))

	RETURN @CantidadEmpleados
END

------------------------------------------------------------------------------------------------------------------------

DROP FUNCTION fnSumatoriaIMSS
GO
CREATE FUNCTION fnSumatoriaIMSS(@No_Departamento int, @Mes int, @Anio int)
RETURNS int

AS

BEGIN
        DECLARE @Sumatoria  decimal(10,2)

		SELECT
			  @Sumatoria = ISNULL(SUM (IMSS), 0)
			  FROM Nomina N
			  INNER JOIN Empleado E
			  ON N.No_Empleado = E.No_Empleado
			  WHERE E.No_Departamento = @No_Departamento
			  AND (@Mes = MONTH(fecha_PN) AND @Anio = YEAR(fecha_PN))




	   RETURN @Sumatoria


END

------------------------------------------------------------------------------------------------------------------------

DROP FUNCTION fnSumatoriaISR
GO
CREATE FUNCTION fnSumatoriaISR(@No_Departamento int, @Mes int, @Anio int)
RETURNS int

AS

BEGIN
        DECLARE @Sumatoria  decimal(10,2)

		SELECT
			  @Sumatoria = ISNULL(SUM (ISR), 0)
			  FROM Nomina N
			  INNER JOIN Empleado E
			  ON N.No_Empleado = E.No_Empleado
			  WHERE E.No_Departamento = @No_Departamento
			  AND (@Mes = MONTH(fecha_PN) AND @Anio = YEAR(fecha_PN))
			 


	        RETURN @Sumatoria


END

------------------------------------------------------------------------------------------------------------------------

DROP FUNCTION fnSumatoriaSueldoBruto
GO
CREATE FUNCTION fnSumatoriaSueldoBruto(@No_Departamento int, @Mes int, @Anio int)
RETURNS int

AS

BEGIN
        DECLARE @Sumatoria  decimal(10,2)

		SELECT
			  @Sumatoria = ISNULL(SUM (Sueldo_Bruto), 0)
			  FROM Nomina N
			  INNER JOIN Empleado E
			  ON N.No_Empleado = E.No_Empleado
			  WHERE E.No_Departamento = @No_Departamento
			  AND (@Mes = MONTH(fecha_PN) AND @Anio = YEAR(fecha_PN))



	        RETURN @Sumatoria


END

------------------------------------------------------------------------------------------------------------------------

DROP FUNCTION fnSumatoriaSueldoNeto
GO
CREATE FUNCTION fnSumatoriaSueldoNeto(@No_Departamento int, @Mes int, @Anio int)
RETURNS int

AS

BEGIN
        DECLARE @Sumatoria  decimal(10,2)

		SELECT
			  @Sumatoria = ISNULL(SUM (Sueldo_Neto), 0)
			  FROM Nomina N
			  INNER JOIN Empleado E
			  ON N.No_Empleado = E.No_Empleado
			  WHERE E.No_Departamento = @No_Departamento AND (@Mes = MONTH(fecha_PN) AND @Anio = YEAR(fecha_PN))


	        RETURN @Sumatoria


END
------------------------------------------------------------------------------------------------------------------------

DROP FUNCTION fnSumatoria_Empl_IMSS
GO
CREATE FUNCTION fnSumatoria_Empl_IMSS(@No_Empleado int, @Mes int, @Anio int)
RETURNS int

AS

BEGIN
        DECLARE @Sumatoria  decimal(10,2)

		SELECT
			  @Sumatoria = SUM (IMSS)
			  FROM Nomina
			  WHERE No_Empleado = @No_Empleado AND (@Mes = MONTH(fecha_PN) AND @Anio = YEAR(fecha_PN))


	     RETURN @Sumatoria


END

------------------------------------------------------------------------------------------------------------------------

DROP FUNCTION fnSumatoria_Empl_ISR
GO
CREATE FUNCTION fnSumatoria_Empl_ISR(@No_Empleado int, @Mes int, @Anio int)
RETURNS int

AS

BEGIN
        DECLARE @Sumatoria  decimal(10,2)

		SELECT
			  @Sumatoria = SUM (ISR)
			  FROM Nomina
			  WHERE No_Empleado = @No_Empleado AND (@Mes = MONTH(fecha_PN) AND @Anio = YEAR(fecha_PN))


	     RETURN @Sumatoria


END

------------------------------------------------------------------------------------------------------------------------

DROP FUNCTION fnSumatoria_Empl_SueldoBruto
GO
CREATE FUNCTION fnSumatoria_Empl_SueldoBruto(@No_Empleado int, @Mes int, @Anio int)
RETURNS int

AS

BEGIN
        DECLARE @Sumatoria  decimal(10,2)

		SELECT
			  @Sumatoria = SUM (Sueldo_Bruto)
			  FROM Nomina
			  WHERE No_Empleado = @No_Empleado AND (@Mes = MONTH(fecha_PN) AND @Anio = YEAR(fecha_PN))


	     RETURN @Sumatoria


END

------------------------------------------------------------------------------------------------------------------------

DROP FUNCTION fnSumatoria_Empl_SueldoNeto
GO
CREATE FUNCTION fnSumatoria_Empl_SueldoNeto(@No_Empleado int, @Mes int, @Anio int)
RETURNS int

AS

BEGIN
        DECLARE @Sumatoria  decimal(10,2)

		SELECT
			  @Sumatoria = SUM (Sueldo_Neto)
			  FROM Nomina
			  WHERE No_Empleado = @No_Empleado AND (@Mes = MONTH(fecha_PN) AND @Anio = YEAR(fecha_PN))


	     RETURN @Sumatoria


END

--------------------------------------------------------------------------------------------------------
DROP FUNCTION fnSueldoGE

GO
CREATE FUNCTION fnSueldoGE(@ID_RFC varchar(13), @No_Empleado int)
RETURNS decimal(10,2)

AS

BEGIN
        DECLARE @SueldoG decimal(10,2) 

		DECLARE @GE  bit 

			SELECT
				@GE = Gerente_Empresa  
			FROM Empleado 
			WHERE No_Empleado  = @No_Empleado 

	IF @GE = 1
	BEGIN
        
		SELECT
			@SueldoG = PagoGE 
		FROM Empresas 
		WHERE ID_RFC = @ID_RFC
	END 

	IF @GE = 0
	BEGIN
		SELECT
		@SueldoG = dbo.fnSalarioD(@No_Empleado)
		FROM Empleado 
	END

	RETURN @SueldoG
END

--------------------------------------------------------------------------------------------------------
