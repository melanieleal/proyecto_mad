﻿Public Class Rep_Headcounter
    Dim conexion As New EnlaceBD
    Dim tabla As New DataTable
    Dim tabla_empresas As New DataTable
    Dim tablaD As New DataTable
    Dim tabla_empleados_depa As New DataTable
    Dim tabla_empleados_puestos As New DataTable

    Private Sub Rep_Headcounter_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        tabla = conexion.Get_Empresas()
        Me.EMPRESA_H.DataSource = tabla
        Me.EMPRESA_H.DisplayMember = "Nombre"
        Me.EMPRESA_H.ValueMember = "RFC"


        MES_H.Items.Add("Enero")
        MES_H.Items.Add("Febrero")
        MES_H.Items.Add("Marzo")
        MES_H.Items.Add("Abril")
        MES_H.Items.Add("Mayo")
        MES_H.Items.Add("Junio")
        MES_H.Items.Add("Julio")
        MES_H.Items.Add("Agosto")
        MES_H.Items.Add("Septiembre")
        MES_H.Items.Add("Octubre")
        MES_H.Items.Add("Noviembre")
        MES_H.Items.Add("Diciembre")

        MES_H.Text = "Enero"

    End Sub

    Private Sub EMPRESA_H_SelectedIndexChanged(sender As Object, e As EventArgs) Handles EMPRESA_H.SelectedIndexChanged
        Dim rfc_ As String
        Dim i As Integer
        Dim depa As Integer


        rfc_ = EMPRESA_H.Text
        tabla = conexion.Get_Empresas()         'obtengo el rfc de la empresa 
        i = tabla.Rows.Count
        For fila As Integer = 0 To i - 1 Step 1
            If tabla.Rows(fila).Item(1) = rfc_ Then
                rfc_ = tabla.Rows(fila).Item(0)
            End If
        Next

        tabla = conexion.Get_NameDepartamento(rfc_)
        i = tabla.Rows.Count
        If i = 0 Then
            Me.DEPA_H.DataSource = tabla
            Me.DEPA_H.DisplayMember = "Nombre"
            Me.DEPA_H.ValueMember = "No_Departamento"
        Else
            For fila As Integer = 0 To i - 1 Step 1
                If tabla.Rows(fila).Item(2) = rfc_ Then
                    depa = tabla.Rows(fila).Item(0)
                    Me.DEPA_H.DataSource = tabla
                    Me.DEPA_H.DisplayMember = "Nombre"
                    Me.DEPA_H.ValueMember = "No_Departamento"
                End If
            Next
        End If

        'AÑOS OPERANDO -------------------------------------------------------------

        AÑO_H.Items.Clear()
        Dim fecha_Inicio_Op As DateTime
        tabla_empresas = conexion.Get_AnioEmpresa(rfc_)
        If (tabla_empresas.Rows.Count <> 0) Then

            fecha_Inicio_Op = tabla_empresas.Rows(0).Item(1)
            AÑO_H.Items.Add(Format(fecha_Inicio_Op, "yyyy"))
            AÑO_H.Text = Format(fecha_Inicio_Op, "yyyy")
            Dim anio_Inicio As Integer = Format(fecha_Inicio_Op, "yyyy")
            Dim anios_Operando As Integer = Date.Now.Date.Year - Format(fecha_Inicio_Op, "yyyy")
            If (anios_Operando <> 0) Then
                For anio As Integer = 1 To anios_Operando Step 1
                    anio_Inicio = anio_Inicio + 1
                    AÑO_H.Items.Add(anio_Inicio)
                Next
            End If
        End If


    End Sub

    Private Sub BUSCAR_H_MouseClick(sender As Object, e As MouseEventArgs) Handles BUSCAR_H.MouseClick

        Dim rfc_ As String
        tabla_empresas = conexion.Get_Empresas()         'obtengo el rfc de la empresa 
        Dim i = tabla_empresas.Rows.Count
        For fila As Integer = 0 To i - 1 Step 1
            If tabla_empresas.Rows(fila).Item(1) = EMPRESA_H.Text Then
                rfc_ = tabla_empresas.Rows(fila).Item(0)
            End If
        Next

        Dim depa As Integer
        tablaD = conexion.Get_NameDepartamento(rfc_)
        Dim d1 = tablaD.Rows.Count

        For fila As Integer = 0 To d1 - 1 Step 1
            If tablaD.Rows(fila).Item(2) = rfc_ And tablaD.Rows(fila).Item(1) = DEPA_H.Text Then
                depa = tablaD.Rows(fila).Item(0)
            End If
        Next

        Dim mes As Integer = MES_H.SelectedIndex + 1
        Dim anio As Integer = AÑO_H.Text

        tabla_empleados_puestos = conexion.Get_RepHeadcounter1(rfc_, depa, mes, anio)
        i = tabla_empleados_puestos.Rows.Count
        DG_HEADCOUNTER_PUESTOS.DataSource = tabla_empleados_puestos


        tabla_empleados_depa = conexion.Get_RepHeadcounter2(rfc_, depa, mes, anio)
        i = tabla_empleados_depa.Rows.Count
        DG_HEADCOUNTER_DEPA.DataSource = tabla_empleados_depa

    End Sub
End Class