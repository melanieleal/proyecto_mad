﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Login
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Login))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.BN_MINIMIZAR = New System.Windows.Forms.Button()
        Me.BN_CLOSE = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.USERNAME = New System.Windows.Forms.TextBox()
        Me.PASSWORD = New System.Windows.Forms.TextBox()
        Me.BN_LOGIN = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Goldenrod
        Me.Panel1.Controls.Add(Me.BN_MINIMIZAR)
        Me.Panel1.Controls.Add(Me.BN_CLOSE)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(377, 34)
        Me.Panel1.TabIndex = 0
        '
        'BN_MINIMIZAR
        '
        Me.BN_MINIMIZAR.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BN_MINIMIZAR.BackgroundImage = CType(resources.GetObject("BN_MINIMIZAR.BackgroundImage"), System.Drawing.Image)
        Me.BN_MINIMIZAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.BN_MINIMIZAR.FlatAppearance.BorderSize = 0
        Me.BN_MINIMIZAR.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BN_MINIMIZAR.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BN_MINIMIZAR.Location = New System.Drawing.Point(306, 0)
        Me.BN_MINIMIZAR.Name = "BN_MINIMIZAR"
        Me.BN_MINIMIZAR.Size = New System.Drawing.Size(34, 34)
        Me.BN_MINIMIZAR.TabIndex = 3
        Me.BN_MINIMIZAR.UseVisualStyleBackColor = True
        '
        'BN_CLOSE
        '
        Me.BN_CLOSE.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BN_CLOSE.BackgroundImage = CType(resources.GetObject("BN_CLOSE.BackgroundImage"), System.Drawing.Image)
        Me.BN_CLOSE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.BN_CLOSE.FlatAppearance.BorderSize = 0
        Me.BN_CLOSE.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BN_CLOSE.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red
        Me.BN_CLOSE.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BN_CLOSE.Location = New System.Drawing.Point(342, 0)
        Me.BN_CLOSE.Name = "BN_CLOSE"
        Me.BN_CLOSE.Size = New System.Drawing.Size(35, 34)
        Me.BN_CLOSE.TabIndex = 2
        Me.BN_CLOSE.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = CType(resources.GetObject("PictureBox1.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(86, 64)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(198, 180)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'USERNAME
        '
        Me.USERNAME.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.USERNAME.Location = New System.Drawing.Point(14, 9)
        Me.USERNAME.Name = "USERNAME"
        Me.USERNAME.Size = New System.Drawing.Size(198, 13)
        Me.USERNAME.TabIndex = 2
        '
        'PASSWORD
        '
        Me.PASSWORD.BackColor = System.Drawing.SystemColors.Window
        Me.PASSWORD.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.PASSWORD.Location = New System.Drawing.Point(14, 10)
        Me.PASSWORD.Name = "PASSWORD"
        Me.PASSWORD.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PASSWORD.Size = New System.Drawing.Size(198, 13)
        Me.PASSWORD.TabIndex = 3
        Me.PASSWORD.UseSystemPasswordChar = True
        '
        'BN_LOGIN
        '
        Me.BN_LOGIN.BackColor = System.Drawing.Color.DarkGoldenrod
        Me.BN_LOGIN.FlatAppearance.BorderSize = 0
        Me.BN_LOGIN.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BN_LOGIN.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Goldenrod
        Me.BN_LOGIN.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BN_LOGIN.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BN_LOGIN.ForeColor = System.Drawing.Color.LightGray
        Me.BN_LOGIN.Location = New System.Drawing.Point(61, 440)
        Me.BN_LOGIN.Name = "BN_LOGIN"
        Me.BN_LOGIN.Size = New System.Drawing.Size(249, 38)
        Me.BN_LOGIN.TabIndex = 4
        Me.BN_LOGIN.Text = "Login"
        Me.BN_LOGIN.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightGray
        Me.Label1.Location = New System.Drawing.Point(60, 313)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(123, 17)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "No. de Empleado"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightGray
        Me.Label2.Location = New System.Drawing.Point(61, 368)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 17)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Password"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.Window
        Me.Panel2.Controls.Add(Me.USERNAME)
        Me.Panel2.Location = New System.Drawing.Point(61, 333)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(249, 32)
        Me.Panel2.TabIndex = 7
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.Window
        Me.Panel3.Controls.Add(Me.PASSWORD)
        Me.Panel3.Location = New System.Drawing.Point(61, 388)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(249, 32)
        Me.Panel3.TabIndex = 8
        '
        'Login
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(377, 533)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BN_LOGIN)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Login"
        Me.Opacity = 0.9R
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login"
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents USERNAME As TextBox
    Friend WithEvents PASSWORD As TextBox
    Friend WithEvents BN_LOGIN As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents BN_MINIMIZAR As Button
    Friend WithEvents BN_CLOSE As Button
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel3 As Panel
End Class
