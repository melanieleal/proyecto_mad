﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Reg_Percepciones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.DELETE_PER = New System.Windows.Forms.Button()
        Me.ACTUALIZAR_PER = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CANTIDAD = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.NAME_PERCE_DEDU = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.FORMA = New System.Windows.Forms.ComboBox()
        Me.DG_PERCEPCIONES = New System.Windows.Forms.DataGridView()
        Me.DG_DEDUCCIONES = New System.Windows.Forms.DataGridView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CHECK_PERCE = New System.Windows.Forms.CheckBox()
        Me.CHECK_DEDU = New System.Windows.Forms.CheckBox()
        CType(Me.DG_PERCEPCIONES, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DG_DEDUCCIONES, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DELETE_PER
        '
        Me.DELETE_PER.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.DELETE_PER.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DELETE_PER.Location = New System.Drawing.Point(819, 89)
        Me.DELETE_PER.Name = "DELETE_PER"
        Me.DELETE_PER.Size = New System.Drawing.Size(69, 35)
        Me.DELETE_PER.TabIndex = 103
        Me.DELETE_PER.Text = "Eliminar"
        Me.DELETE_PER.UseVisualStyleBackColor = True
        '
        'ACTUALIZAR_PER
        '
        Me.ACTUALIZAR_PER.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.ACTUALIZAR_PER.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ACTUALIZAR_PER.Location = New System.Drawing.Point(733, 89)
        Me.ACTUALIZAR_PER.Name = "ACTUALIZAR_PER"
        Me.ACTUALIZAR_PER.Size = New System.Drawing.Size(69, 35)
        Me.ACTUALIZAR_PER.TabIndex = 102
        Me.ACTUALIZAR_PER.Text = "Actualizar"
        Me.ACTUALIZAR_PER.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(217, 174)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(95, 16)
        Me.Label14.TabIndex = 101
        Me.Label14.Text = "Percepciones"
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(29, 95)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 17)
        Me.Label1.TabIndex = 99
        Me.Label1.Text = "Cantidad."
        '
        'CANTIDAD
        '
        Me.CANTIDAD.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.CANTIDAD.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CANTIDAD.Location = New System.Drawing.Point(136, 94)
        Me.CANTIDAD.Name = "CANTIDAD"
        Me.CANTIDAD.Size = New System.Drawing.Size(82, 13)
        Me.CANTIDAD.TabIndex = 98
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(29, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 17)
        Me.Label2.TabIndex = 97
        Me.Label2.Text = "Nombre."
        '
        'NAME_PERCE_DEDU
        '
        Me.NAME_PERCE_DEDU.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.NAME_PERCE_DEDU.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NAME_PERCE_DEDU.Location = New System.Drawing.Point(136, 38)
        Me.NAME_PERCE_DEDU.Name = "NAME_PERCE_DEDU"
        Me.NAME_PERCE_DEDU.Size = New System.Drawing.Size(231, 13)
        Me.NAME_PERCE_DEDU.TabIndex = 96
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(29, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 17)
        Me.Label3.TabIndex = 106
        Me.Label3.Text = "Forma."
        '
        'FORMA
        '
        Me.FORMA.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.FORMA.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.FORMA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.FORMA.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FORMA.FormattingEnabled = True
        Me.FORMA.Location = New System.Drawing.Point(136, 62)
        Me.FORMA.Name = "FORMA"
        Me.FORMA.Size = New System.Drawing.Size(121, 25)
        Me.FORMA.TabIndex = 107
        '
        'DG_PERCEPCIONES
        '
        Me.DG_PERCEPCIONES.AllowUserToDeleteRows = False
        Me.DG_PERCEPCIONES.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DG_PERCEPCIONES.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DG_PERCEPCIONES.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DG_PERCEPCIONES.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG_PERCEPCIONES.Location = New System.Drawing.Point(333, 160)
        Me.DG_PERCEPCIONES.Name = "DG_PERCEPCIONES"
        Me.DG_PERCEPCIONES.ReadOnly = True
        Me.DG_PERCEPCIONES.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DG_PERCEPCIONES.Size = New System.Drawing.Size(537, 173)
        Me.DG_PERCEPCIONES.TabIndex = 108
        '
        'DG_DEDUCCIONES
        '
        Me.DG_DEDUCCIONES.AllowUserToDeleteRows = False
        Me.DG_DEDUCCIONES.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DG_DEDUCCIONES.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DG_DEDUCCIONES.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DG_DEDUCCIONES.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG_DEDUCCIONES.Location = New System.Drawing.Point(333, 357)
        Me.DG_DEDUCCIONES.Name = "DG_DEDUCCIONES"
        Me.DG_DEDUCCIONES.ReadOnly = True
        Me.DG_DEDUCCIONES.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DG_DEDUCCIONES.Size = New System.Drawing.Size(537, 173)
        Me.DG_DEDUCCIONES.TabIndex = 109
        '
        'Label4
        '
        Me.Label4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(217, 374)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(92, 16)
        Me.Label4.TabIndex = 110
        Me.Label4.Text = "Deducciones"
        '
        'CHECK_PERCE
        '
        Me.CHECK_PERCE.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CHECK_PERCE.AutoSize = True
        Me.CHECK_PERCE.Location = New System.Drawing.Point(201, 176)
        Me.CHECK_PERCE.Name = "CHECK_PERCE"
        Me.CHECK_PERCE.Size = New System.Drawing.Size(15, 14)
        Me.CHECK_PERCE.TabIndex = 111
        Me.CHECK_PERCE.UseVisualStyleBackColor = True
        '
        'CHECK_DEDU
        '
        Me.CHECK_DEDU.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CHECK_DEDU.AutoSize = True
        Me.CHECK_DEDU.Location = New System.Drawing.Point(201, 376)
        Me.CHECK_DEDU.Name = "CHECK_DEDU"
        Me.CHECK_DEDU.Size = New System.Drawing.Size(15, 14)
        Me.CHECK_DEDU.TabIndex = 112
        Me.CHECK_DEDU.UseVisualStyleBackColor = True
        '
        'Reg_Percepciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(900, 666)
        Me.Controls.Add(Me.CHECK_DEDU)
        Me.Controls.Add(Me.CHECK_PERCE)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.DG_DEDUCCIONES)
        Me.Controls.Add(Me.DG_PERCEPCIONES)
        Me.Controls.Add(Me.FORMA)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.DELETE_PER)
        Me.Controls.Add(Me.ACTUALIZAR_PER)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CANTIDAD)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.NAME_PERCE_DEDU)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Reg_Percepciones"
        Me.Text = "Reg_Percepciones"
        CType(Me.DG_PERCEPCIONES, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DG_DEDUCCIONES, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DELETE_PER As Button
    Friend WithEvents ACTUALIZAR_PER As Button
    Friend WithEvents Label14 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents CANTIDAD As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents NAME_PERCE_DEDU As TextBox
    Friend WithEvents Label25 As Label
    Friend WithEvents EMP_PERCE_DEDU As ListBox
    Friend WithEvents Label3 As Label
    Friend WithEvents FORMA As ComboBox
    Friend WithEvents DG_PERCEPCIONES As DataGridView
    Friend WithEvents DG_DEDUCCIONES As DataGridView
    Friend WithEvents Label4 As Label
    Friend WithEvents CHECK_PERCE As CheckBox
    Friend WithEvents CHECK_DEDU As CheckBox
End Class
