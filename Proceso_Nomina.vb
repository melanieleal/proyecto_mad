﻿Imports iTextSharp.text.pdf
Imports iTextSharp.text
Imports System.IO

Public Class Proceso_Nomina

    Dim funcion As New Funciones
    Dim conexion As New EnlaceBD
    Dim tabla As New DataTable
    Dim obj As New EnlaceBD
    Dim result As Boolean = False
    Dim i As Integer
    Dim no_Emp As Integer
    Dim D As Integer
    Dim P As Integer

    Dim nameE As String
    Dim frec As Integer
    Dim nomina As Integer


    Dim IMSS As Decimal
    Dim ISR As Decimal
    Dim BONOG As Decimal
    Dim Prima As Decimal
    Dim SalarioGE As Decimal
    Dim rfc1 As String
    Dim fechaPN As Date

    Dim empF As Integer


    Private Sub Proceso_Nomina_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CB_FREC_NOMINA.Items.Clear()

        tabla = conexion.Get_Empresas()
        Me.CB_EMPRESA_NOMINA.DataSource = tabla
        Me.CB_EMPRESA_NOMINA.DisplayMember = "Nombre"
        Me.CB_EMPRESA_NOMINA.ValueMember = "RFC"

        tabla = conexion.Get_Deduccion()
        Me.DG_DEDU_NOMINA.DataSource = tabla

        tabla = conexion.Get_Percepcion()
        Me.DG_PERCE_NOMINA.DataSource = tabla

        FECHA_ACTUAL.Text = DateTime.Now.ToString("dddd")
        FECHA1.Text = DateTime.Now.ToString("dd")
        FECHA2.Text = DateTime.Now.ToString("MM")
        FECHA3.Text = DateTime.Now.ToString("yyyy")

    End Sub

    Private Sub CB_EMPRESA_NOMINA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_EMPRESA_NOMINA.SelectedIndexChanged
        CB_FREC_NOMINA.Items.Clear()

        tabla = conexion.Get_Empresas()

        i = tabla.Rows.Count
        For fila As Integer = 0 To i - 1 Step 1
            If tabla.Rows(fila).Item(1) = CB_EMPRESA_NOMINA.Text Then
                rfc1 = tabla.Rows(fila).Item(0)
                IMSS = tabla.Rows(fila).Item(11)
                ISR = tabla.Rows(fila).Item(12)
                BONOG = tabla.Rows(fila).Item(13)
                SalarioGE = tabla.Rows(fila).Item(14)
                Prima = tabla.Rows(fila).Item(15)

                empF = fila

                Dim tablaF As New DataTable
                    tablaF = conexion.Get_Frecuencia(rfc1, "X")
                    Dim j As Integer = tablaF.Rows.Count
                    For filaF As Integer = 0 To j - 1 Step 1
                        If tablaF.Rows(filaF).Item(1) = 1 Then
                            Me.CB_FREC_NOMINA.Items.Add("Semanal")
                            If filaF = 0 Then
                                CB_FREC_NOMINA.Text = "Semanal"
                            End If
                        End If
                        If tablaF.Rows(filaF).Item(1) = 2 Then
                            Me.CB_FREC_NOMINA.Items.Add("Quincenal")
                            If filaF = 0 Then
                                CB_FREC_NOMINA.Text = "Quincenal"
                            End If
                        End If
                        If tablaF.Rows(filaF).Item(1) = 3 Then
                            Me.CB_FREC_NOMINA.Items.Add("Catorcenal")
                            If filaF = 0 Then
                                CB_FREC_NOMINA.Text = "Catorcenal"
                            End If
                        End If
                        If tablaF.Rows(filaF).Item(1) = 4 Then
                            Me.CB_FREC_NOMINA.Items.Add("Mensual")
                            If filaF = 0 Then
                                CB_FREC_NOMINA.Text = "Mensual"
                            End If
                        End If
                    Next

                End If
        Next
    End Sub

    Private Sub CB_FREC_NOMINA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CB_FREC_NOMINA.SelectedIndexChanged
        Dim tablaF As DataTable = conexion.Get_Frecuencia("", "Y")
        For fila As Integer = 0 To 3 Step 1
            If tablaF.Rows(fila).Item(1) = CB_FREC_NOMINA.Text Then
                frec = tablaF.Rows(fila).Item(0)
            End If
        Next

        Dim tablaEE As New DataTable
        tablaEE = conexion.Get_Empleados_Empresa_Frec(rfc1, frec)
        DG_EMPLEADOS_NOMINA.DataSource = tablaEE

    End Sub

    Private Sub PROC_NOMINA_Click(sender As Object, e As EventArgs) Handles PROC_NOMINA.Click

        Dim frecuencia As String = CB_FREC_NOMINA.Text
        Dim v As Boolean = True
        Dim dias As Integer
        Dim boolP As Boolean
        Dim bono As Boolean
        Dim frec_ As Integer
        Dim actual As Date = DateTime.Now.ToString("dd/MM/yyyy")

        Dim PerM As New List(Of Decimal)
        Dim DedM As New List(Of Decimal)

        Dim vf As Integer

        If frecuencia = "Semanal" Then
            frec_ = 6
            vf = 1

        ElseIf frecuencia = "Quincenal" Then
            frec_ = 15
            vf = 2

        ElseIf frecuencia = "Catorcenal" Then
            frec_ = 14
            vf = 3

        ElseIf frecuencia = "Mensual" Then
            frec_ = 30
            vf = 4

        End If


        Dim tablaEE As New DataTable
        tablaEE = conexion.Get_Empleados_Empresa_Frec(rfc1, frec)
        Dim emp As Integer = tablaEE.Rows().Count
        Dim cont As Integer = 0


        Dim tablaV = conexion.Get_Ultima_Nomina_E(vf, rfc1)
        Dim vv As Integer = tablaV.Rows.Count

        If vv <> 0 Then

            'Dim tablaEF As New DataTable
            'tablaEF = conexion.Get_Empresas()
            'If Not DBNull.Value.Equals(tablaEF.Rows(empF).Item(18)) Then
            '    fechaPN = tablaEF.Rows(empF).Item(18)
            'End If


            'If Len(fechaPN) <> 0 Then
            '    If DateDiff(DateInterval.Day, fechaPN, actual) < frec_ Then
            '        v = False
            '    End If
            'End If

            If DateDiff(DateInterval.Day, tablaV.Rows(vv - 1).Item(1), actual) < frec_ Then
                v = False
            End If


        End If


        If v And Len(Trim(CB_EMPRESA_NOMINA.Text)) <> 0 And Len(Trim(CB_FREC_NOMINA.Text)) <> 0 Then

            Dim empleadosN As New List(Of Integer)

            While (emp <> cont)

                boolP = False
                bono = False
                PerM.Clear()
                DedM.Clear()

                no_Emp = tablaEE.Rows(cont).Item(0)                       ''obtengo numero de empleado

                If DateDiff(DateInterval.Day, tablaEE.Rows(cont).Item(12), actual) < frec_ Then  ''dias que trabajo
                    dias = DateDiff(DateInterval.Day, tablaEE.Rows(cont).Item(12), actual) + 1
                Else
                    dias = frec_
                End If

                tabla = conexion.Get_NominaEmpleado(no_Emp, "1753-01-01")
                i = tabla.Rows().Count

                If i = 0 Then
                    result = obj.Add_ProcesoNomina("I", no_Emp, actual)

                    tabla = conexion.Get_NominaEmpleado(no_Emp, actual)   ''''obtener el numero de nomina
                    For fila As Integer = 0 To tabla.Rows().Count - 1 Step 1
                        If tabla.Rows(fila).Item(1) = no_Emp And tabla.Rows(fila).Item(7) = actual Then
                            nomina = tabla.Rows(fila).Item(0)
                        End If
                    Next

                Else

                    tabla = conexion.Get_NominaEmpleado(no_Emp, "1753-01-01")   ''''obtener el numero de nomina
                    For fila As Integer = 0 To tabla.Rows().Count - 1 Step 1
                        If tabla.Rows(fila).Item(1) = no_Emp And tabla.Rows(fila).Item(7) = "1753-01-01" Then
                            nomina = tabla.Rows(fila).Item(0)
                        End If
                    Next

                End If

                For filaG As Integer = 0 To emp - 1 Step 1                '''Si es gerente de departamento se le da un bono
                    If tablaEE.Rows(cont).Item(24) = True Then
                        bono = True
                    End If
                Next

                Dim id As Integer
                Dim tablaIE As New DataTable
                tablaIE = conexion.Get_IncidenciasEmpleado(no_Emp)
                Dim j As Integer = tablaIE.Rows().Count
                Dim vacaciones As Integer = 0

                For filaIE As Integer = 0 To j - 1 Step 1
                    id = tablaIE.Rows(filaIE).Item(0)

                    Dim name As String
                    Dim tablaI As New DataTable
                    tablaI = conexion.Get_Incidencias()
                    Dim inci As Integer = tablaI.Rows().Count

                    For filaI As Integer = 0 To inci - 1 Step 1
                        If tablaI.Rows(filaI).Item(0) = id Then
                            name = tablaI.Rows(filaI).Item(1)

                            If name <> "Vacaciones" Then
                                dias = dias - 1
                            Else
                                boolP = True
                            End If
                        End If
                    Next
                Next

                '''Obtener el salario''
                Dim tablaS As New DataTable
                tablaS = conexion.Get_SalarioD(no_Emp)

                Dim salarioD As Decimal
                If tablaEE.Rows(cont).Item(23) = True Then
                    salarioD = SalarioGE
                Else
                    salarioD = tablaS.Rows(0).Item(0)
                End If


                Dim salarioBruto As Decimal = salarioD * dias
                Dim salarioNeto As Decimal
                Dim MontoP As Decimal = 0
                Dim MontoD As Decimal = 0

                Dim per As Decimal = 0
                Dim ded As Decimal = 0

                Dim tablaD As New DataTable
                tablaD = conexion.Get_Nomina_Percepcion_Deduccion("Z", nomina, frec_)
                Dim d As Integer = tablaD.Rows().Count
                If d > 0 Then

                    For fila As Integer = 0 To d - 1 Step 1

                        Dim tablaCD As New DataTable
                        tablaCD = conexion.Get_DeduccionT()
                        Dim cd As Integer = tablaCD.Rows().Count
                        For filaCD As Integer = 0 To cd - 1 Step 1

                            If tablaD.Rows(fila).Item(1) = tablaCD.Rows(filaCD).Item(0) Then

                                Dim deduccion As Decimal = tablaCD.Rows(filaCD).Item(5)
                                If tablaCD.Rows(filaCD).Item(4) = "Porcentaje" Then
                                    deduccion = deduccion / 100
                                    MontoD = MontoD + (salarioBruto * deduccion)
                                    ded = salarioBruto * deduccion
                                Else
                                    MontoD = MontoD + deduccion
                                    ded = deduccion
                                End If

                                DedM.Add(ded)

                            End If
                        Next

                    Next
                Else
                    MontoD = 0
                End If

                Dim tablaP As New DataTable
                tablaP = conexion.Get_Nomina_Percepcion_Deduccion("Y", nomina, frec_)
                Dim p As Integer = tablaP.Rows().Count

                If p > 0 Then
                    For fila As Integer = 0 To p - 1 Step 1

                        Dim tablaCP As New DataTable
                        tablaCP = conexion.Get_PercepcionT()
                        Dim cp = tablaCP.Rows().Count
                        For filaCP As Integer = 0 To cp - 1 Step 1
                            If tablaP.Rows(fila).Item(1) = tablaCP.Rows(filaCP).Item(0) Then
                                Dim percepcion As Decimal = tablaCP.Rows(filaCP).Item(5)

                                If tablaCP.Rows(filaCP).Item(4) = "Porcentaje" Then
                                    percepcion = percepcion / 100
                                    MontoP = MontoP + (salarioBruto * percepcion)
                                    per = salarioBruto * percepcion
                                Else
                                    MontoP = MontoP + percepcion
                                    per = percepcion
                                End If

                                PerM.Add(per)

                            End If
                        Next

                    Next

                Else
                    MontoP = 0
                End If

                If DateDiff(DateInterval.Day, tablaEE.Rows(cont).Item(12), actual) > 3 Then ''''
                    IMSS = funcion.Monto(salarioBruto, IMSS)
                    ISR = funcion.Monto(salarioBruto, ISR)
                Else
                    IMSS = 0
                    ISR = 0
                End If

                BONOG = funcion.Monto(salarioBruto, BONOG)
                Prima = funcion.Monto(salarioBruto, Prima)


                If boolP Then
                    salarioNeto = salarioBruto + MontoP - MontoD - IMSS - ISR + Prima
                End If

                If bono Then
                    salarioNeto = salarioBruto + MontoP - MontoD - IMSS - ISR + BONOG
                End If

                If boolP = False And bono = False Then
                    salarioNeto = salarioBruto + MontoP - MontoD - IMSS - ISR
                End If


                result = conexion.Add_ProcesoNomina_T("M", nomina, no_Emp, salarioBruto, salarioNeto, dias, IMSS, ISR, actual, salarioD)
                empleadosN.Add(no_Emp)
                funcion.pdf(no_Emp, DedM, PerM, MontoP, MontoD, boolP, bono, actual)


                If result = False Then
                    MessageBox.Show("No se pudo efectuar su proceso de nomina del empleado: " & vbLf & no_Emp, "Proceso de Nomina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If

                cont = cont + 1
            End While

            MessageBox.Show("Proceso de Pago efectuado con exito, verifique en la carpeta su PDF y CSV", "Proceso de Nomina", MessageBoxButtons.OK, MessageBoxIcon.Information)
            conexion.actualizar_FechaProcesoNomina("P", rfc1, actual)

#Region "Documentacion"

            nameE = CB_EMPRESA_NOMINA.Text
            Dim filePDF As String = nameE & "" & ".pdf"

            '''''''''''''''''CREACION DE PDF''''''''''''''''''''''

            Dim pdfDoc As New Document(iTextSharp.text.PageSize.A4, 15.0F, 15.0F, 30.0F, 30.0F)
            Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(filePDF, FileMode.Create))
            pdfDoc.Open()

            Dim titulo As Paragraph = New Paragraph()
            titulo.Font = FontFactory.GetFont(FontFactory.TIMES, 18.0F, BaseColor.BLACK)
            titulo.Add("Proceso de Nomina")
            pdfDoc.Add(titulo)
            pdfDoc.Add(New Paragraph(" "))

            Dim empresa As Paragraph = New Paragraph()
            empresa.Font = FontFactory.GetFont(FontFactory.TIMES_BOLD, 14.0F, BaseColor.DARK_GRAY)
            empresa.Add("           Empresa. " & " " & nameE & vbNewLine & "           RFC. " & " " & rfc1)
            pdfDoc.Add(empresa)
            pdfDoc.Add(New Paragraph(" "))

            Dim table As PdfPTable = New PdfPTable(7)
            table.AddCell("Folio")
            table.AddCell("No. Empleado")
            table.AddCell("Nombre")
            table.AddCell("Fecha")
            table.AddCell("Sueldo Neto")
            table.AddCell("Banco")
            table.AddCell("No. Cuenta")

            tabla = conexion.Get_Nomina()
            i = tabla.Rows().Count
            Dim num As Integer = 0

            For fila As Integer = 0 To i - 1 Step 1

                If num < empleadosN.Count() Then
                    If tabla.Rows(fila).Item(3) = actual And empleadosN(num) = tabla.Rows(fila).Item(1) Then  ''

                        table.AddCell(tabla.Rows(fila).Item(0))
                        table.AddCell(tabla.Rows(fila).Item(1))
                        table.AddCell(tabla.Rows(fila).Item(9))
                        table.AddCell(actual)
                        table.AddCell(tabla.Rows(fila).Item(6))
                        table.AddCell(tabla.Rows(fila).Item(11))
                        table.AddCell(tabla.Rows(fila).Item(10))
                        num = num + 1

                    End If
                End If

            Next

            pdfDoc.Add(table)
            pdfDoc.Close()

            Process.Start(filePDF)

            '''''''''''''''''''''''''CREACION DE CSV''''''''''''''''''''''''''''''''

            Dim fileCSV As String = nameE & "" & ".csv"
            If File.Exists(fileCSV) Then
                My.Computer.FileSystem.DeleteFile(fileCSV)
            End If

            Dim CSV As StreamWriter = File.AppendText(fileCSV)
            CSV.WriteLine("Folio ; No. Empleado ; Nombre ; Fecha ; Sueldo Neto ; Banco ; No. Cuenta")

            tabla = conexion.Get_Nomina()
            i = tabla.Rows().Count
            num = 0

            For fila As Integer = 0 To i - 1 Step 1
                If num < empleadosN.Count() Then
                    If tabla.Rows(fila).Item(3) = actual And empleadosN(num) = tabla.Rows(fila).Item(1) Then  ''

                        CSV.WriteLine(tabla.Rows(fila).Item(0) & " ; " & tabla.Rows(fila).Item(1) & " ; " & tabla.Rows(fila).Item(9) & " ; " & actual & " ; " & tabla.Rows(fila).Item(6) & " ; " & tabla.Rows(fila).Item(11) & " ; " & " -" & tabla.Rows(fila).Item(10) & "- ")
                        num = num + 1
                    End If
                End If
            Next

            CSV.Close()
            Process.Start(fileCSV)
#End Region

            empleadosN.Clear()
        Else
            MessageBox.Show("No se pudo efectuar su proceso de nomina, verifique que :  " & vbNewLine & " " & vbNewLine & "    -Ingreso la empresa y frecuencia de pago." & vbNewLine & "    - Ya efectuo su proceso de Nomina, vuelva a efectualo hasta el " & DateAdd(DateInterval.Day, frec_, actual), "Proceso de Nomina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub DG_PERCE_NOMINA_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DG_PERCE_NOMINA.CellClick

        If Len(DG_PERCE_NOMINA.CurrentRow.Cells.Item(0).Value.ToString()) <> 0 Then
            P = DG_PERCE_NOMINA.CurrentRow.Cells.Item(0).Value.ToString()
        Else
            P = 0
        End If

    End Sub

    Private Sub DG_DEDU_NOMINA_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DG_DEDU_NOMINA.CellClick

        If Len(DG_DEDU_NOMINA.CurrentRow.Cells.Item(0).Value.ToString()) <> 0 Then
            D = DG_DEDU_NOMINA.CurrentRow.Cells.Item(0).Value.ToString()
        Else
            D = 0
        End If

    End Sub

    Private Sub DG_EMPLEADOS_NOMINA_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DG_EMPLEADOS_NOMINA.CellClick

        If Len(DG_EMPLEADOS_NOMINA.CurrentRow.Cells.Item(0).Value.ToString()) <> 0 Then
            no_Emp = DG_EMPLEADOS_NOMINA.CurrentRow.Cells.Item(0).Value.ToString()
        Else
            no_Emp = 0
        End If

    End Sub

    Private Sub AÑADIR_PERCE_Click(sender As Object, e As EventArgs) Handles AÑADIR_PERCE.Click
        Dim val As Boolean = True
        nameE = CB_EMPRESA_NOMINA.Text

        If Len(nameE) = 0 Or Len(frec) = 0 Or no_Emp = 0 Or D = 0 And P = 0 Then
            MessageBox.Show("- Ingrese la Empresa y Frecuencia de Pago para obtener a los Empleados" & vbNewLine & "- Seleccione al Empleado y a alguna Percepcion para ejecutar el Proceso de Nomina", "Proceso de Nomina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        Else

            If P > 0 Then


                tabla = conexion.Get_NominaEmpleado(no_Emp, "1753-01-01")
                i = tabla.Rows().Count

                If i = 0 Then
                    result = obj.Add_ProcesoNomina("I", no_Emp, "1753-01-01")
                End If


                tabla = conexion.Get_NominaEmpleado(no_Emp, "1753-01-01")   ''''obtener el numero de nomina
                i = tabla.Rows().Count
                For fila As Integer = 0 To i - 1 Step 1
                    If tabla.Rows(fila).Item(1) = no_Emp And tabla.Rows(fila).Item(7) = "1753-01-01" Then
                        nomina = tabla.Rows(fila).Item(0)
                    End If
                Next



                Dim tablaVP = conexion.Get_Nomina_Percepcion(nomina)

                For fila As Integer = 0 To tablaVP.Rows().Count - 1 Step 1
                    If tablaVP.Rows(fila).Item(1) = P Then
                        MessageBox.Show("La Percepcion ya existe en la nomina", "Proceso de Nomina", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        val = False
                    End If

                Next

                If val Then
                    result = obj.Add_ProcesoNomina_P("P", nomina, P, DateTime.Now.ToString)
                    If result Then
                        MessageBox.Show("Se ha añadido con exito una Percepcion a su Nomina", "Proceso de Nomina", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    Else
                        MessageBox.Show("Se ha producido un error", "Proceso de Nomina", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                    End If
                End If

            End If

        End If

    End Sub

    Private Sub AÑADIR_DEDU_Click(sender As Object, e As EventArgs) Handles AÑADIR_DEDU.Click
        nameE = CB_EMPRESA_NOMINA.Text
        Dim val As Boolean = True

        If Len(nameE) = 0 Or Len(frec) = 0 Or no_Emp = 0 Or D = 0 And P = 0 Then
            MessageBox.Show("- Ingrese la Empresa y Frecuencia de Pago para obtener a los Empleados" & vbNewLine & "- Seleccione al Empleado y a alguna  Deduccion para ejecutar el Proceso de Nomina", "Proceso de Nomina", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        Else

            If D > 0 Then


                tabla = conexion.Get_NominaEmpleado(no_Emp, "1753-01-01")
                i = tabla.Rows().Count

                If i = 0 Then
                    result = obj.Add_ProcesoNomina("I", no_Emp, "1753-01-01")
                End If


                tabla = conexion.Get_NominaEmpleado(no_Emp, "1753-01-01")   ''''obtener el numero de nomina
                i = tabla.Rows().Count
                For fila As Integer = 0 To i - 1 Step 1
                    If tabla.Rows(fila).Item(1) = no_Emp And tabla.Rows(fila).Item(7) = "1753-01-01" Then
                        nomina = tabla.Rows(fila).Item(0)
                    End If
                Next


                Dim tablaVD = conexion.Get_Nomina_Deduccion(nomina)
                For fila As Integer = 0 To tablaVD.Rows().Count - 1 Step 1
                    If tablaVD.Rows(fila).Item(1) = D Then

                        MessageBox.Show("La deduccion ya existe en la nomina", "Proceso de Nomina", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        val = False

                    End If

                Next

                If val Then
                    result = obj.Add_ProcesoNomina_D("D", nomina, D, DateTime.Now.ToString)

                    If result Then
                        MessageBox.Show("Se ha añadido con exito una Deduccion a su Nomina", "Proceso de Nomina", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    Else
                        MessageBox.Show("Se ha producido un error", "Proceso de Nomina", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                    End If
                End If


            End If

        End If

    End Sub


End Class