﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Reg_Empresas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PHONE_EMP = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.CP_EMP = New System.Windows.Forms.TextBox()
        Me.MUNICIPIO_EMP = New System.Windows.Forms.TextBox()
        Me.COLONIA_EMP = New System.Windows.Forms.TextBox()
        Me.CALLE_EMP = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.RAZON_SOCIAL = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.REG_PATRONAL = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.FECHA_EMP = New System.Windows.Forms.DateTimePicker()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.ACTUALIZAR_EMP = New System.Windows.Forms.Button()
        Me.DELETE_EMP = New System.Windows.Forms.Button()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.ESTADO_EMP = New System.Windows.Forms.ComboBox()
        Me.QUINCENAL = New System.Windows.Forms.CheckBox()
        Me.CATORCENAL = New System.Windows.Forms.CheckBox()
        Me.MENSUAL = New System.Windows.Forms.CheckBox()
        Me.SEMANAL = New System.Windows.Forms.CheckBox()
        Me.DG_EMPRESAS = New System.Windows.Forms.DataGridView()
        Me.RFC_EMPRESA = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.NUMERO_EXT_EMP = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.CANTIDAD1 = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.CANTIDAD3 = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.CANTIDAD2 = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.CANTIDAD = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.CANTIDAD4 = New System.Windows.Forms.TextBox()
        CType(Me.DG_EMPRESAS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(22, 390)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 17)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Telefono."
        '
        'PHONE_EMP
        '
        Me.PHONE_EMP.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.PHONE_EMP.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.PHONE_EMP.Location = New System.Drawing.Point(93, 390)
        Me.PHONE_EMP.Name = "PHONE_EMP"
        Me.PHONE_EMP.Size = New System.Drawing.Size(149, 13)
        Me.PHONE_EMP.TabIndex = 7
        '
        'Panel2
        '
        Me.Panel2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Panel2.Location = New System.Drawing.Point(129, 466)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(474, 1)
        Me.Panel2.TabIndex = 60
        '
        'Label22
        '
        Me.Label22.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(403, 525)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(31, 17)
        Me.Label22.TabIndex = 59
        Me.Label22.Text = "CP."
        '
        'Label10
        '
        Me.Label10.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(23, 597)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(56, 17)
        Me.Label10.TabIndex = 58
        Me.Label10.Text = "Estado."
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(23, 562)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(74, 17)
        Me.Label9.TabIndex = 57
        Me.Label9.Text = "Municipio."
        '
        'Label7
        '
        Me.Label7.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(22, 529)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 17)
        Me.Label7.TabIndex = 55
        Me.Label7.Text = "Colonia."
        '
        'Label6
        '
        Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(23, 495)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 17)
        Me.Label6.TabIndex = 54
        Me.Label6.Text = "Calle."
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 455)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(114, 16)
        Me.Label5.TabIndex = 53
        Me.Label5.Text = "Domicilio Fiscal."
        '
        'CP_EMP
        '
        Me.CP_EMP.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.CP_EMP.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CP_EMP.Location = New System.Drawing.Point(437, 524)
        Me.CP_EMP.Name = "CP_EMP"
        Me.CP_EMP.Size = New System.Drawing.Size(96, 13)
        Me.CP_EMP.TabIndex = 52
        '
        'MUNICIPIO_EMP
        '
        Me.MUNICIPIO_EMP.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.MUNICIPIO_EMP.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MUNICIPIO_EMP.Location = New System.Drawing.Point(103, 559)
        Me.MUNICIPIO_EMP.Name = "MUNICIPIO_EMP"
        Me.MUNICIPIO_EMP.Size = New System.Drawing.Size(244, 13)
        Me.MUNICIPIO_EMP.TabIndex = 50
        '
        'COLONIA_EMP
        '
        Me.COLONIA_EMP.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.COLONIA_EMP.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.COLONIA_EMP.Location = New System.Drawing.Point(92, 529)
        Me.COLONIA_EMP.Name = "COLONIA_EMP"
        Me.COLONIA_EMP.Size = New System.Drawing.Size(277, 13)
        Me.COLONIA_EMP.TabIndex = 49
        '
        'CALLE_EMP
        '
        Me.CALLE_EMP.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.CALLE_EMP.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CALLE_EMP.Location = New System.Drawing.Point(75, 495)
        Me.CALLE_EMP.Name = "CALLE_EMP"
        Me.CALLE_EMP.Size = New System.Drawing.Size(294, 13)
        Me.CALLE_EMP.TabIndex = 47
        '
        'Panel1
        '
        Me.Panel1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Panel1.Location = New System.Drawing.Point(138, 241)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(474, 1)
        Me.Panel1.TabIndex = 62
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 231)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(132, 16)
        Me.Label1.TabIndex = 61
        Me.Label1.Text = "Datos de Contacto."
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(403, 495)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 17)
        Me.Label3.TabIndex = 64
        Me.Label3.Text = "No. Exterior."
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(22, 268)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(94, 17)
        Me.Label4.TabIndex = 66
        Me.Label4.Text = "Razon Social."
        '
        'RAZON_SOCIAL
        '
        Me.RAZON_SOCIAL.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.RAZON_SOCIAL.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RAZON_SOCIAL.Location = New System.Drawing.Point(123, 268)
        Me.RAZON_SOCIAL.Name = "RAZON_SOCIAL"
        Me.RAZON_SOCIAL.Size = New System.Drawing.Size(404, 13)
        Me.RAZON_SOCIAL.TabIndex = 65
        '
        'Label11
        '
        Me.Label11.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(22, 297)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(122, 17)
        Me.Label11.TabIndex = 68
        Me.Label11.Text = "Registro Patronal."
        '
        'REG_PATRONAL
        '
        Me.REG_PATRONAL.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.REG_PATRONAL.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.REG_PATRONAL.Location = New System.Drawing.Point(152, 297)
        Me.REG_PATRONAL.Name = "REG_PATRONAL"
        Me.REG_PATRONAL.Size = New System.Drawing.Size(167, 13)
        Me.REG_PATRONAL.TabIndex = 67
        '
        'Label12
        '
        Me.Label12.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(22, 325)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(37, 17)
        Me.Label12.TabIndex = 70
        Me.Label12.Text = "RFC."
        '
        'Label13
        '
        Me.Label13.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(22, 356)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(194, 17)
        Me.Label13.TabIndex = 73
        Me.Label13.Text = "Fecha inicio de operaciones."
        '
        'FECHA_EMP
        '
        Me.FECHA_EMP.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.FECHA_EMP.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FECHA_EMP.Location = New System.Drawing.Point(225, 356)
        Me.FECHA_EMP.Name = "FECHA_EMP"
        Me.FECHA_EMP.Size = New System.Drawing.Size(94, 20)
        Me.FECHA_EMP.TabIndex = 72
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(31, 20)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(74, 16)
        Me.Label14.TabIndex = 75
        Me.Label14.Text = "Empresas."
        '
        'ACTUALIZAR_EMP
        '
        Me.ACTUALIZAR_EMP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ACTUALIZAR_EMP.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ACTUALIZAR_EMP.Location = New System.Drawing.Point(700, 196)
        Me.ACTUALIZAR_EMP.Name = "ACTUALIZAR_EMP"
        Me.ACTUALIZAR_EMP.Size = New System.Drawing.Size(69, 35)
        Me.ACTUALIZAR_EMP.TabIndex = 76
        Me.ACTUALIZAR_EMP.Text = "Actualizar"
        Me.ACTUALIZAR_EMP.UseVisualStyleBackColor = True
        '
        'DELETE_EMP
        '
        Me.DELETE_EMP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DELETE_EMP.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DELETE_EMP.Location = New System.Drawing.Point(799, 196)
        Me.DELETE_EMP.Name = "DELETE_EMP"
        Me.DELETE_EMP.Size = New System.Drawing.Size(69, 35)
        Me.DELETE_EMP.TabIndex = 77
        Me.DELETE_EMP.Text = "Eliminar"
        Me.DELETE_EMP.UseVisualStyleBackColor = True
        '
        'Label15
        '
        Me.Label15.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(379, 356)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(213, 17)
        Me.Label15.TabIndex = 79
        Me.Label15.Text = "Forma de Pago a Trabajadores."
        '
        'ESTADO_EMP
        '
        Me.ESTADO_EMP.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.ESTADO_EMP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ESTADO_EMP.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ESTADO_EMP.FormattingEnabled = True
        Me.ESTADO_EMP.Location = New System.Drawing.Point(85, 594)
        Me.ESTADO_EMP.Name = "ESTADO_EMP"
        Me.ESTADO_EMP.Size = New System.Drawing.Size(200, 21)
        Me.ESTADO_EMP.TabIndex = 81
        '
        'QUINCENAL
        '
        Me.QUINCENAL.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.QUINCENAL.AutoSize = True
        Me.QUINCENAL.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.QUINCENAL.Location = New System.Drawing.Point(420, 386)
        Me.QUINCENAL.Name = "QUINCENAL"
        Me.QUINCENAL.Size = New System.Drawing.Size(88, 21)
        Me.QUINCENAL.TabIndex = 82
        Me.QUINCENAL.Text = "Quincenal"
        Me.QUINCENAL.UseVisualStyleBackColor = True
        '
        'CATORCENAL
        '
        Me.CATORCENAL.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.CATORCENAL.AutoSize = True
        Me.CATORCENAL.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CATORCENAL.Location = New System.Drawing.Point(420, 409)
        Me.CATORCENAL.Name = "CATORCENAL"
        Me.CATORCENAL.Size = New System.Drawing.Size(95, 21)
        Me.CATORCENAL.TabIndex = 83
        Me.CATORCENAL.Text = "Catorcenal"
        Me.CATORCENAL.UseVisualStyleBackColor = True
        '
        'MENSUAL
        '
        Me.MENSUAL.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.MENSUAL.AutoSize = True
        Me.MENSUAL.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MENSUAL.Location = New System.Drawing.Point(519, 386)
        Me.MENSUAL.Name = "MENSUAL"
        Me.MENSUAL.Size = New System.Drawing.Size(76, 21)
        Me.MENSUAL.TabIndex = 84
        Me.MENSUAL.Text = "Mensual"
        Me.MENSUAL.UseVisualStyleBackColor = True
        '
        'SEMANAL
        '
        Me.SEMANAL.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.SEMANAL.AutoSize = True
        Me.SEMANAL.Font = New System.Drawing.Font("Century Gothic", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SEMANAL.Location = New System.Drawing.Point(519, 409)
        Me.SEMANAL.Name = "SEMANAL"
        Me.SEMANAL.Size = New System.Drawing.Size(79, 21)
        Me.SEMANAL.TabIndex = 85
        Me.SEMANAL.Text = "Semanal"
        Me.SEMANAL.UseVisualStyleBackColor = True
        '
        'DG_EMPRESAS
        '
        Me.DG_EMPRESAS.AllowUserToDeleteRows = False
        Me.DG_EMPRESAS.AllowUserToResizeColumns = False
        Me.DG_EMPRESAS.AllowUserToResizeRows = False
        Me.DG_EMPRESAS.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DG_EMPRESAS.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DG_EMPRESAS.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DG_EMPRESAS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG_EMPRESAS.Location = New System.Drawing.Point(116, 41)
        Me.DG_EMPRESAS.MultiSelect = False
        Me.DG_EMPRESAS.Name = "DG_EMPRESAS"
        Me.DG_EMPRESAS.ReadOnly = True
        Me.DG_EMPRESAS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DG_EMPRESAS.Size = New System.Drawing.Size(734, 150)
        Me.DG_EMPRESAS.TabIndex = 86
        '
        'RFC_EMPRESA
        '
        Me.RFC_EMPRESA.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.RFC_EMPRESA.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RFC_EMPRESA.Location = New System.Drawing.Point(65, 327)
        Me.RFC_EMPRESA.Name = "RFC_EMPRESA"
        Me.RFC_EMPRESA.Size = New System.Drawing.Size(177, 13)
        Me.RFC_EMPRESA.TabIndex = 87
        '
        'Label16
        '
        Me.Label16.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(540, 268)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(14, 17)
        Me.Label16.TabIndex = 88
        Me.Label16.Text = "*"
        '
        'Label17
        '
        Me.Label17.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(325, 295)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(14, 17)
        Me.Label17.TabIndex = 89
        Me.Label17.Text = "*"
        '
        'Label18
        '
        Me.Label18.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(248, 327)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(14, 17)
        Me.Label18.TabIndex = 90
        Me.Label18.Text = "*"
        '
        'NUMERO_EXT_EMP
        '
        Me.NUMERO_EXT_EMP.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.NUMERO_EXT_EMP.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NUMERO_EXT_EMP.Location = New System.Drawing.Point(494, 495)
        Me.NUMERO_EXT_EMP.Name = "NUMERO_EXT_EMP"
        Me.NUMERO_EXT_EMP.Size = New System.Drawing.Size(51, 13)
        Me.NUMERO_EXT_EMP.TabIndex = 63
        '
        'Label19
        '
        Me.Label19.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(325, 360)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(14, 17)
        Me.Label19.TabIndex = 91
        Me.Label19.Text = "*"
        '
        'Label20
        '
        Me.Label20.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(375, 492)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(14, 17)
        Me.Label20.TabIndex = 92
        Me.Label20.Text = "*"
        '
        'Label21
        '
        Me.Label21.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(375, 529)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(14, 17)
        Me.Label21.TabIndex = 93
        Me.Label21.Text = "*"
        '
        'Label23
        '
        Me.Label23.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(351, 559)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(14, 17)
        Me.Label23.TabIndex = 94
        Me.Label23.Text = "*"
        '
        'Label24
        '
        Me.Label24.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(551, 494)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(14, 17)
        Me.Label24.TabIndex = 95
        Me.Label24.Text = "*"
        '
        'Label25
        '
        Me.Label25.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(539, 521)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(14, 17)
        Me.Label25.TabIndex = 96
        Me.Label25.Text = "*"
        '
        'Label26
        '
        Me.Label26.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(296, 598)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(14, 17)
        Me.Label26.TabIndex = 97
        Me.Label26.Text = "*"
        '
        'Label27
        '
        Me.Label27.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(598, 358)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(14, 17)
        Me.Label27.TabIndex = 98
        Me.Label27.Text = "*"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(684, 393)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(124, 17)
        Me.Label8.TabIndex = 141
        Me.Label8.Text = "Prima Vacacional"
        '
        'Label28
        '
        Me.Label28.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(710, 426)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(75, 17)
        Me.Label28.TabIndex = 140
        Me.Label28.Text = "Cantidad."
        '
        'CANTIDAD1
        '
        Me.CANTIDAD1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CANTIDAD1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CANTIDAD1.Location = New System.Drawing.Point(792, 428)
        Me.CANTIDAD1.Name = "CANTIDAD1"
        Me.CANTIDAD1.Size = New System.Drawing.Size(82, 13)
        Me.CANTIDAD1.TabIndex = 139
        '
        'Label29
        '
        Me.Label29.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(684, 526)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(25, 17)
        Me.Label29.TabIndex = 138
        Me.Label29.Text = "ISR"
        '
        'Label30
        '
        Me.Label30.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(710, 550)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(75, 17)
        Me.Label30.TabIndex = 137
        Me.Label30.Text = "Cantidad."
        '
        'CANTIDAD3
        '
        Me.CANTIDAD3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CANTIDAD3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CANTIDAD3.Location = New System.Drawing.Point(792, 554)
        Me.CANTIDAD3.Name = "CANTIDAD3"
        Me.CANTIDAD3.Size = New System.Drawing.Size(82, 13)
        Me.CANTIDAD3.TabIndex = 136
        '
        'Label31
        '
        Me.Label31.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(684, 463)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(98, 17)
        Me.Label31.TabIndex = 135
        Me.Label31.Text = "Impuesto IMSS"
        '
        'Label32
        '
        Me.Label32.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(710, 499)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(75, 17)
        Me.Label32.TabIndex = 134
        Me.Label32.Text = "Cantidad."
        '
        'CANTIDAD2
        '
        Me.CANTIDAD2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CANTIDAD2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CANTIDAD2.Location = New System.Drawing.Point(792, 499)
        Me.CANTIDAD2.Name = "CANTIDAD2"
        Me.CANTIDAD2.Size = New System.Drawing.Size(82, 13)
        Me.CANTIDAD2.TabIndex = 133
        '
        'Label33
        '
        Me.Label33.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(684, 324)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(97, 17)
        Me.Label33.TabIndex = 132
        Me.Label33.Text = "Bono Gerente"
        '
        'Label34
        '
        Me.Label34.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(710, 361)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(75, 17)
        Me.Label34.TabIndex = 131
        Me.Label34.Text = "Cantidad."
        '
        'CANTIDAD
        '
        Me.CANTIDAD.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CANTIDAD.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CANTIDAD.Location = New System.Drawing.Point(792, 360)
        Me.CANTIDAD.Name = "CANTIDAD"
        Me.CANTIDAD.Size = New System.Drawing.Size(82, 13)
        Me.CANTIDAD.TabIndex = 130
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Panel3.Location = New System.Drawing.Point(740, 295)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(100, 1)
        Me.Panel3.TabIndex = 129
        '
        'Label35
        '
        Me.Label35.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(756, 269)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(64, 16)
        Me.Label35.TabIndex = 128
        Me.Label35.Text = "Empresa"
        '
        'Panel4
        '
        Me.Panel4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(37, Byte), Integer), CType(CType(46, Byte), Integer), CType(CType(59, Byte), Integer))
        Me.Panel4.Location = New System.Drawing.Point(656, 241)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1, 400)
        Me.Panel4.TabIndex = 130
        '
        'Label36
        '
        Me.Label36.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(684, 361)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(14, 17)
        Me.Label36.TabIndex = 142
        Me.Label36.Text = "*"
        '
        'Label37
        '
        Me.Label37.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.Location = New System.Drawing.Point(684, 432)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(14, 17)
        Me.Label37.TabIndex = 143
        Me.Label37.Text = "*"
        '
        'Label38
        '
        Me.Label38.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(684, 501)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(14, 17)
        Me.Label38.TabIndex = 144
        Me.Label38.Text = "*"
        '
        'Label39
        '
        Me.Label39.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(684, 553)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(14, 17)
        Me.Label39.TabIndex = 145
        Me.Label39.Text = "*"
        '
        'Label40
        '
        Me.Label40.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(684, 629)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(14, 17)
        Me.Label40.TabIndex = 149
        Me.Label40.Text = "*"
        '
        'Label41
        '
        Me.Label41.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(684, 592)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(158, 17)
        Me.Label41.TabIndex = 148
        Me.Label41.Text = "Pago Gerente Empresa"
        '
        'Label42
        '
        Me.Label42.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Century Gothic", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(710, 629)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(75, 17)
        Me.Label42.TabIndex = 147
        Me.Label42.Text = "Cantidad."
        '
        'CANTIDAD4
        '
        Me.CANTIDAD4.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CANTIDAD4.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CANTIDAD4.Location = New System.Drawing.Point(792, 628)
        Me.CANTIDAD4.Name = "CANTIDAD4"
        Me.CANTIDAD4.Size = New System.Drawing.Size(82, 13)
        Me.CANTIDAD4.TabIndex = 146
        '
        'Reg_Empresas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(900, 666)
        Me.Controls.Add(Me.Label40)
        Me.Controls.Add(Me.Label41)
        Me.Controls.Add(Me.Label42)
        Me.Controls.Add(Me.CANTIDAD4)
        Me.Controls.Add(Me.Label39)
        Me.Controls.Add(Me.Label38)
        Me.Controls.Add(Me.Label37)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.CANTIDAD1)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.CANTIDAD3)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.CANTIDAD2)
        Me.Controls.Add(Me.Label33)
        Me.Controls.Add(Me.Label34)
        Me.Controls.Add(Me.CANTIDAD)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Label35)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.RFC_EMPRESA)
        Me.Controls.Add(Me.DG_EMPRESAS)
        Me.Controls.Add(Me.SEMANAL)
        Me.Controls.Add(Me.MENSUAL)
        Me.Controls.Add(Me.CATORCENAL)
        Me.Controls.Add(Me.QUINCENAL)
        Me.Controls.Add(Me.ESTADO_EMP)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.DELETE_EMP)
        Me.Controls.Add(Me.ACTUALIZAR_EMP)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.FECHA_EMP)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.REG_PATRONAL)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.RAZON_SOCIAL)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.NUMERO_EXT_EMP)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.CP_EMP)
        Me.Controls.Add(Me.MUNICIPIO_EMP)
        Me.Controls.Add(Me.COLONIA_EMP)
        Me.Controls.Add(Me.CALLE_EMP)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PHONE_EMP)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Reg_Empresas"
        Me.Text = "Reg_Empresas"
        CType(Me.DG_EMPRESAS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label2 As Label
    Friend WithEvents PHONE_EMP As TextBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label22 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents CP_EMP As TextBox
    Friend WithEvents MUNICIPIO_EMP As TextBox
    Friend WithEvents COLONIA_EMP As TextBox
    Friend WithEvents CALLE_EMP As TextBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents RAZON_SOCIAL As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents REG_PATRONAL As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents FECHA_EMP As DateTimePicker
    Friend WithEvents Label14 As Label
    Friend WithEvents ACTUALIZAR_EMP As Button
    Friend WithEvents DELETE_EMP As Button
    Friend WithEvents Label15 As Label
    Friend WithEvents ESTADO_EMP As ComboBox
    Friend WithEvents QUINCENAL As CheckBox
    Friend WithEvents CATORCENAL As CheckBox
    Friend WithEvents MENSUAL As CheckBox
    Friend WithEvents SEMANAL As CheckBox
    Friend WithEvents DG_EMPRESAS As DataGridView
    Friend WithEvents RFC_EMPRESA As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents NUMERO_EXT_EMP As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents CANTIDAD1 As TextBox
    Friend WithEvents Label29 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents CANTIDAD3 As TextBox
    Friend WithEvents Label31 As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents CANTIDAD2 As TextBox
    Friend WithEvents Label33 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents CANTIDAD As TextBox
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Label35 As Label
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Label36 As Label
    Friend WithEvents Label37 As Label
    Friend WithEvents Label38 As Label
    Friend WithEvents Label39 As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents Label41 As Label
    Friend WithEvents Label42 As Label
    Friend WithEvents CANTIDAD4 As TextBox
End Class
