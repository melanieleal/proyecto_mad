﻿Option Explicit On
Imports System.Windows
Imports System
Imports System.IO


Imports iTextSharp.text.pdf
Imports iTextSharp.text

Public Class Funciones
    Inherits EnlaceBD


    Public Function EvaluaSoloNumeros(e As KeyPressEventArgs)
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
            MessageBox.Show("Ingrese solo Numeros", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

        Return 0
    End Function

    Public Function EvaluaSoloLetras(e As KeyPressEventArgs)
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
            MessageBox.Show("Ingrese solo Letras", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
        Return 0
    End Function

    Public Function EvaluaSoloLetrasYNumerosConEspacios(e As KeyPressEventArgs)
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
            MessageBox.Show("Ingrese solo Letras, Numeros o Espacios", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
        Return 0
    End Function

    Public Function EvaluaSoloLetrasMayusculasYNumeros(e As KeyPressEventArgs)
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
            MessageBox.Show("Ingrese solo Letras Mayusculas y Numeros", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

        If Char.IsLower(e.KeyChar) Then
            e.Handled = True
            MessageBox.Show("Ingrese solo Letras Mayusculas y Numeros", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
        Return 0
    End Function

    Public Function EvaluaSoloLetrasYNumerosSinEspacio(e As KeyPressEventArgs)
        If Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
            MessageBox.Show("Ingrese solo Letras y Numeros /n No ingrese espacios", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
        Return 0
    End Function

    Public Function Monto(ByVal salario As Decimal, ByVal monto_ As Decimal)

        If monto_ < 1 Then
            monto_ = salario * monto_
        End If

        Return monto_
    End Function



    Public Function pdf(ByVal no_Emp As Integer, ByVal dedm As List(Of Decimal), ByVal perm As List(Of Decimal), ByVal MontoP As Decimal, ByVal MontoD As Decimal, ByVal primaV As Boolean, ByVal bonoG As Boolean, ByVal fecha As Date)
        Dim tablaN As New DataTable
        Dim tablaE As New DataTable
        Dim tablaEmple As New DataTable
        Dim conexion As New EnlaceBD
        Dim InfoEmp As New List(Of String)
        Dim InfoE As New List(Of String)
        Dim frecuencia As Integer
        Dim bool As Boolean = True

        Dim diasT As Integer
        Dim d As Integer
        Dim p As Integer

        Dim i As Integer
        Dim prima As Decimal
        Dim bono As Decimal

        Dim tablaD As New DataTable
        Dim tablaP As New DataTable

        Dim n As Integer

        InfoE.Clear()
        InfoEmp.Clear()

        tablaE = conexion.Get_Empresas()
        Dim empresa As Integer = tablaE.Rows.Count

        tablaEmple = conexion.Get_Empleados()
        Dim empleados As Integer = tablaEmple.Rows.Count

        For filaEmp As Integer = 0 To empleados - 1 Step 1
            If tablaEmple.Rows(filaEmp).Item(0) = no_Emp Then
                InfoEmp.Add(tablaEmple.Rows(filaEmp).Item(1) & " " & tablaEmple.Rows(filaEmp).Item(2) & " " & tablaEmple.Rows(filaEmp).Item(3))
                InfoEmp.Add(tablaEmple.Rows(filaEmp).Item(17)) ''rfc
                InfoEmp.Add(tablaEmple.Rows(filaEmp).Item(18)) ''curp
                InfoEmp.Add(tablaEmple.Rows(filaEmp).Item(19))  ''nss
                InfoEmp.Add(tablaEmple.Rows(filaEmp).Item(24)) ''puesto

                If tablaEmple.Rows(filaEmp).Item(28) = "Semanal" Then
                    frecuencia = 6

                ElseIf tablaEmple.Rows(filaEmp).Item(28) = "Quincenal" Then
                    frecuencia = 15

                ElseIf tablaEmple.Rows(filaEmp).Item(28) = "Catorcenal" Then
                    frecuencia = 14

                ElseIf tablaEmple.Rows(filaEmp).Item(28) = "Mensual" Then
                    frecuencia = 30

                End If

                InfoEmp.Add(tablaEmple.Rows(filaEmp).Item(30))  ''departamento
                InfoEmp.Add(tablaEmple.Rows(filaEmp).Item(16))  ''fecha contrato

                For filaE As Integer = 0 To empresa - 1 Step 1
                    If tablaE.Rows(filaE).Item(0) = tablaEmple.Rows(filaEmp).Item(25) Then

                        InfoE.Add(tablaE.Rows(filaE).Item(0))  ''rfc empresa
                        InfoE.Add(tablaE.Rows(filaE).Item(1))  ''nombre empresa
                        InfoE.Add(tablaE.Rows(filaE).Item(2)) ''reg patronal
                        InfoE.Add(tablaE.Rows(filaE).Item(5) & " " & tablaE.Rows(filaE).Item(6) & ", " & tablaE.Rows(filaE).Item(7) & ", " & tablaE.Rows(filaE).Item(10) & ", " & " CP. " & tablaE.Rows(filaE).Item(9) & ", " & tablaE.Rows(filaE).Item(8)) ''direccion

                        If primaV Then
                            prima = tablaE.Rows(filaE).Item(15)
                        End If

                        If bonoG Then
                            bono = tablaE.Rows(filaE).Item(13)
                        End If


                    End If
                Next

                tablaN = conexion.Get_NominaEmpleado(no_Emp, fecha)
                n = tablaN.Rows.Count

                diasT = tablaN.Rows(0).Item(4)


                tablaD = conexion.Get_Nomina_Percepcion_Deduccion("Z", tablaN.Rows(0).Item(0), frecuencia)
                d = tablaD.Rows.Count

                tablaP = conexion.Get_Nomina_Percepcion_Deduccion("Y", tablaN.Rows(0).Item(0), frecuencia)
                p = tablaP.Rows.Count

            End If
        Next




        ''''''''''''PDF RECIBO DE NOMINA''''''''''''''''''''''''

        Dim pdfN As String = no_Emp & ".pdf"
        Dim pdfDoc As New Document(iTextSharp.text.PageSize.A4, 15.0F, 15.0F, 30.0F, 30.0F)
        Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(pdfN, FileMode.Create))

        Dim Font8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL))
        Dim Font10 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.NORMAL))
        Dim FontB8 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD))
        Dim FontB10 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD))
        Dim FontB12 As New Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD))
        Dim Vacio As PdfPCell = New PdfPCell(New Phrase(" "))
        Vacio.Border = 0

        pdfDoc.Open()

        Dim DatosE As PdfPTable = New PdfPTable(1)
        DatosE.WidthPercentage = 95


#Region "Encabezado"

        Dim Col1 As PdfPCell = New PdfPCell(New Phrase("                                                                                                                      Folio. " & tablaN.Rows(0).Item(0), FontB12))
        Col1.Border = 0
        DatosE.AddCell(Col1)

        Col1 = New PdfPCell(New Phrase(InfoE(1), FontB12))
        Col1.Border = 0
        DatosE.AddCell(Col1)

        Dim Col3 As PdfPCell = New PdfPCell(New Phrase("RFC. " & InfoE(0), FontB8))
        Col3.Border = 0
        DatosE.AddCell(Col3)

        Dim Col4 As PdfPCell = New PdfPCell(New Phrase("Reg Patronal. " & InfoE(2), FontB8))
        Col4.Border = 0
        DatosE.AddCell(Col4)

        Dim Col5 As PdfPCell = New PdfPCell(New Phrase(InfoE(3), FontB8))
        Col5.Border = 0
        DatosE.AddCell(Col5)
        DatosE.AddCell(Vacio)
        DatosE.AddCell(Vacio)

        pdfDoc.Add(DatosE)

#End Region

        Dim text As New Paragraph()
        text.Add("R E C I B O   D E   N O M I N A")
        text.Alignment = Element.ALIGN_CENTER
        pdfDoc.Add(text)

        Dim linea As Chunk = New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(5.0F, 100.0F, BaseColor.RED, Element.ALIGN_CENTER, 0F))
        pdfDoc.Add(linea)

        pdfDoc.Add(New Paragraph(vbNewLine & "                Fecha de Pago. " & tablaN.Rows(0).Item(7) & "                                                                                                                Periodo de Pago. " & DateAdd("d", -diasT, fecha) & " - " & tablaN.Rows(0).Item(7), FontB8))

        linea = New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(2.0F, 100.0F, BaseColor.GRAY, Element.ALIGN_CENTER, 0F))
        pdfDoc.Add(linea)

#Region "Empleado"

        Dim DatosEmp As PdfPTable = New PdfPTable(2)
        DatosEmp.WidthPercentage = 95

        Dim widths As Single() = New Single() {67.0F, 20.0F}
        DatosEmp.SetWidths(widths)


        Col1 = New PdfPCell(New Phrase("No. Empleado   " & no_Emp, FontB8))
        Col1.Border = 0
        DatosEmp.AddCell(Col1)
        DatosEmp.AddCell(Vacio)
        DatosEmp.AddCell(Vacio)
        DatosEmp.AddCell(Vacio)


        Dim Col2 As PdfPCell = New PdfPCell(New Phrase("    " & InfoEmp(0), Font10))
        Col2.Border = 0
        DatosEmp.AddCell(Col2)
        DatosEmp.AddCell(Vacio)

        Col3 = New PdfPCell(New Phrase("    CURP.   " & InfoEmp(2), Font8))
        Col3.Border = 0
        DatosEmp.AddCell(Col3)
        Dim Col9 = New PdfPCell(New Phrase("Salario Diario.   " & tablaN.Rows(0).Item(8), Font8))
        Col9.Border = 0
        DatosEmp.AddCell(Col9)

        Col4 = New PdfPCell(New Phrase("    RFC.   " & InfoEmp(1), Font8))
        Col4.Border = 0
        DatosEmp.AddCell(Col4)
        Dim Col13 = New PdfPCell(New Phrase("Salario Bruto.   " & tablaN.Rows(0).Item(2), Font8))
        Col13.Border = 0
        DatosEmp.AddCell(Col13)

        Col5 = New PdfPCell(New Phrase("    NSS.   " & InfoEmp(3), Font8))
        Col5.Border = 0
        DatosEmp.AddCell(Col5)
        Dim Col12 = New PdfPCell(New Phrase("Días Trabajados.   " & diasT, Font8))
        Col12.Border = 0
        DatosEmp.AddCell(Col12)

        Dim Col6 As PdfPCell = New PdfPCell(New Phrase("    Puesto.   " & InfoEmp(4), Font8))
        Col6.Border = 0
        DatosEmp.AddCell(Col6)
        DatosEmp.AddCell(Vacio)


        Dim Col7 As PdfPCell = New PdfPCell(New Phrase("    Departamento.   " & InfoEmp(5), Font8))
        Col7.Border = 0
        DatosEmp.AddCell(Col7)
        DatosEmp.AddCell(Vacio)

        Dim Col8 As PdfPCell = New PdfPCell(New Phrase("    Fecha de Contrato.   " & InfoEmp(6), Font8))
        Col8.Border = 0
        DatosEmp.AddCell(Col8)
        DatosEmp.AddCell(Vacio)

        pdfDoc.Add(DatosEmp)

#End Region

        linea = New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(2.0F, 100.0F, BaseColor.GRAY, Element.ALIGN_CENTER, 0F))
        pdfDoc.Add(linea)

        pdfDoc.Add(Chunk.NEWLINE)
        pdfDoc.Add(Chunk.NEWLINE)

        Dim text1 As New Paragraph()
        text1.Font = New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD))
        text1.Add("D e d u c c i o n e s")
        text1.Alignment = Element.ALIGN_CENTER
        pdfDoc.Add(text1)
        linea = New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(0.1F, 70.0F, BaseColor.DARK_GRAY, Element.ALIGN_CENTER, 0F))
        pdfDoc.Add(linea)

#Region "Deducciones"

        Dim Ded As PdfPTable = New PdfPTable(3)
        Ded.WidthPercentage = 60

        widths = New Single() {10.0F, 40.0F, 10.0F}
        Ded.SetWidths(widths)

        Col1 = New PdfPCell(New Phrase("Clave", FontB8))
        Col1.Border = 0
        Ded.AddCell(Col1)
        Col2 = New PdfPCell(New Phrase("Concepto", FontB8))
        Col2.Border = 0
        Ded.AddCell(Col2)
        Col3 = New PdfPCell(New Phrase("Importe", FontB8))
        Col3.Border = 0
        Ded.AddCell(Col3)

        pdfDoc.Add(Ded)
        linea = New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(2.0F, 70.0F, BaseColor.DARK_GRAY, Element.ALIGN_CENTER, 0F))
        pdfDoc.Add(linea)

        i = 0
        Dim Ded1 As PdfPTable = New PdfPTable(3)
        Ded1.WidthPercentage = 60

        widths = New Single() {10.0F, 40.0F, 10.0F}
        Ded1.SetWidths(widths)


        Dim Col10 = New PdfPCell(New Phrase("24", Font8))
        Col10.Border = 0
        Ded1.AddCell(Col10)
        Dim Col11 = New PdfPCell(New Phrase("ISR.   ", Font8))
        Col11.Border = 0
        Ded1.AddCell(Col11)
        Col11 = New PdfPCell(New Phrase(tablaN.Rows(0).Item(6), Font8))
        Col11.Border = 0
        Ded1.AddCell(Col11)

        Col10 = New PdfPCell(New Phrase("36", Font8))
        Col10.Border = 0
        Ded1.AddCell(Col10)
        Col12 = New PdfPCell(New Phrase("Impuesto IMSS.   ", Font8))
        Col12.Border = 0
        Ded1.AddCell(Col12)
        Col12 = New PdfPCell(New Phrase(tablaN.Rows(0).Item(5), Font8))
        Col12.Border = 0
        Ded1.AddCell(Col12)

        If (d > 0) Then
            For filaD As Integer = 0 To d - 1 Step 1

                Dim tablaC As New DataTable
                tablaC = conexion.Get_DeduccionT()
                Dim c_dp As Integer = tablaC.Rows().Count
                For filaC As Integer = 0 To c_dp - 1 Step 1
                    If tablaD.Rows(filaD).Item(1) = tablaC.Rows(filaC).Item(0) Then

                        Col4 = New PdfPCell(New Phrase(tablaC.Rows(filaC).Item(0), Font8))
                        Col4.Border = 0
                        Ded1.AddCell(Col4)
                        Col5 = New PdfPCell(New Phrase(tablaC.Rows(filaC).Item(2), Font8))
                        Col5.Border = 0
                        Ded1.AddCell(Col5)
                        Col6 = New PdfPCell(New Phrase(Decimal.Round(dedm(i), 2), Font8))
                        Col6.Border = 0
                        Ded1.AddCell(Col6)

                        i = i + 1
                    End If
                Next

            Next
        End If
        pdfDoc.Add(Ded1)

        pdfDoc.Add(Chunk.NEWLINE)
        linea = New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(2.0F, 70.0F, BaseColor.DARK_GRAY, Element.ALIGN_CENTER, 0F))
        pdfDoc.Add(linea)


        Dim DedT As PdfPTable = New PdfPTable(3)
        DedT.WidthPercentage = 60

        widths = New Single() {40.0F, 10.0F, 10.0F}
        DedT.SetWidths(widths)

        DedT.AddCell(Vacio)
        Dim total As PdfPCell = New PdfPCell(New Phrase("Total. ", FontB10))
        total.Border = 0
        DedT.AddCell(total)
        total = New PdfPCell(New Phrase(Decimal.Round(MontoD + tablaN.Rows(0).Item(5) + tablaN.Rows(0).Item(6), 2), Font10))
        total.Border = 0
        DedT.AddCell(total)

        pdfDoc.Add(DedT)

#End Region

        pdfDoc.Add(Chunk.NEWLINE)
        pdfDoc.Add(Chunk.NEWLINE)

        Dim text2 As New Paragraph()
        text2.Font = New Font(FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD))
        text2.Add("P e r c e p c i o n e s")
        text2.Alignment = Element.ALIGN_CENTER
        pdfDoc.Add(text2)
        linea = New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(0.1F, 70.0F, BaseColor.DARK_GRAY, Element.ALIGN_CENTER, 0F))
        pdfDoc.Add(linea)

#Region "Percepciones"

        Dim Per As PdfPTable = New PdfPTable(3)
        Per.WidthPercentage = 60

        widths = New Single() {10.0F, 40.0F, 10.0F}
        Per.SetWidths(widths)

        Col1 = New PdfPCell(New Phrase("Clave", FontB8))
        Col1.Border = 0
        Per.AddCell(Col1)
        Col2 = New PdfPCell(New Phrase("Concepto", FontB8))
        Col2.Border = 0
        Per.AddCell(Col2)
        Col3 = New PdfPCell(New Phrase("Importe", FontB8))
        Col3.Border = 0
        Per.AddCell(Col3)

        pdfDoc.Add(Per)
        linea = New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(2.0F, 70.0F, BaseColor.DARK_GRAY, Element.ALIGN_CENTER, 0F))
        pdfDoc.Add(linea)


        Dim Per1 As PdfPTable = New PdfPTable(3)
        Per1.WidthPercentage = 60

        If bonoG Then

            Col10 = New PdfPCell(New Phrase("54", Font8))
            Col10.Border = 0
            Per1.AddCell(Col10)
            Col12 = New PdfPCell(New Phrase("Bono Gerente", Font8))
            Col12.Border = 0
            Per1.AddCell(Col12)
            Col12 = New PdfPCell(New Phrase(bono, Font8))
            Col12.Border = 0
            Per1.AddCell(Col12)

            MontoP = MontoP + bono

        End If

        If primaV Then

            Col10 = New PdfPCell(New Phrase("65", Font8))
            Col10.Border = 0
            Per1.AddCell(Col10)
            Col12 = New PdfPCell(New Phrase("Prima Vacacional", Font8))
            Col12.Border = 0
            Per1.AddCell(Col12)
            Col12 = New PdfPCell(New Phrase(prima, Font8))
            Col12.Border = 0
            Per1.AddCell(Col12)

            MontoP = MontoP + prima
        End If


        i = 0
        widths = New Single() {10.0F, 40.0F, 10.0F}
        Per1.SetWidths(widths)
        If (p > 0) Then
            For filaP As Integer = 0 To p - 1 Step 1

                Dim tablaC As New DataTable
                tablaC = conexion.Get_PercepcionT()
                Dim c_dp As Integer = tablaC.Rows().Count

                For filaC As Integer = 0 To c_dp - 1 Step 1

                    If tablaP.Rows(filaP).Item(1) = tablaC.Rows(filaC).Item(0) Then

                        Col4 = New PdfPCell(New Phrase(tablaC.Rows(filaC).Item(0), Font8))
                        Col4.Border = 0
                        Per1.AddCell(Col4)
                        Col5 = New PdfPCell(New Phrase(tablaC.Rows(filaC).Item(2), Font8))
                        Col5.Border = 0
                        Per1.AddCell(Col5)
                        Col6 = New PdfPCell(New Phrase(Decimal.Round(perm(i), 2), Font8))
                        Col6.Border = 0
                        Per1.AddCell(Col6)

                        i = i + 1
                    End If
                Next

            Next
        End If


        pdfDoc.Add(Per1)

        pdfDoc.Add(Chunk.NEWLINE)
        linea = New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(2.0F, 70.0F, BaseColor.DARK_GRAY, Element.ALIGN_CENTER, 0F))
        pdfDoc.Add(linea)

        Dim PerT As PdfPTable = New PdfPTable(3)
        PerT.WidthPercentage = 60

        widths = New Single() {40.0F, 10.0F, 10.0F}
        PerT.SetWidths(widths)

        PerT.AddCell(Vacio)
        Col1 = New PdfPCell(New Phrase("Total. ", FontB10))
        Col1.Border = 0
        PerT.AddCell(Col1)
        Col2 = New PdfPCell(New Phrase(Decimal.Round(MontoP, 2), Font10))
        Col2.Border = 0
        PerT.AddCell(Col2)

        pdfDoc.Add(PerT)

#End Region

        pdfDoc.Add(Chunk.NEWLINE)
        pdfDoc.Add(Chunk.NEWLINE)

        linea = New Chunk(New iTextSharp.text.pdf.draw.LineSeparator(2.0F, 100.0F, BaseColor.GRAY, Element.ALIGN_CENTER, 0F))
        pdfDoc.Add(linea)

        Dim totalF As PdfPTable = New PdfPTable(4)
        totalF.WidthPercentage = 95

        widths = New Single() {20.0F, 27.0F, 30.0F, 15.0F}
        totalF.SetWidths(widths)

        totalF.AddCell(Vacio)
        Col1 = New PdfPCell(New Phrase("  Total. " & vbNewLine & vbNewLine & tablaN.Rows(0).Item(3), FontB12))
        Col1.Border = 0
        totalF.AddCell(Col1)
        Col2 = New PdfPCell(New Phrase("         Total con Letra." & vbNewLine & vbNewLine & Letras(tablaN.Rows(0).Item(3).ToString), FontB12))
        Col2.Border = 0
        totalF.AddCell(Col2)
        totalF.AddCell(Vacio)

        pdfDoc.Add(totalF)

        pdfDoc.Close()


        Return 0
    End Function



    Public Function Letras(ByVal numero As String) As String

        '********Declara variables de tipo cadena************
        Dim palabras, entero, dec, flag As String

        '********Declara variables de tipo entero***********
        Dim num, dec1, x, y As Integer


        flag = "N"

        '**********Número Negativo***********
        If Mid(numero, 1, 1) = "-" Then
            numero = Mid(numero, 2, numero.ToString.Length - 1).ToString
            palabras = "menos "
        End If

        '**********Si tiene ceros a la izquierda*************
        For x = 1 To numero.ToString.Length
            If Mid(numero, 1, 1) = "0" Then
                numero = Trim(Mid(numero, 2, numero.ToString.Length).ToString)
                If Trim(numero.ToString.Length) = 0 Then palabras = ""
            Else
                Exit For
            End If
        Next

        '*********Dividir parte entera y decimal************
        For y = 1 To Len(numero)
            If Mid(numero, y, 1) = "." Then
                flag = "S"
            Else
                If flag = "N" Then
                    entero = entero + Mid(numero, y, 1)
                Else
                    dec = dec + Mid(numero, y, 1)
                End If
            End If
        Next y

        If Len(dec) = 1 Then dec = dec & "0"

        '**********proceso de conversión***********
        flag = "N"

        If Val(numero) <= 999999999 Then
            For y = Len(entero) To 1 Step -1
                num = Len(entero) - (y - 1)
                Select Case y
                    Case 3, 6, 9
                        '**********Asigna las palabras para las centenas***********
                        Select Case Mid(entero, num, 1)
                            Case "1"
                                If Mid(entero, num + 1, 1) = "0" And Mid(entero, num + 2, 1) = "0" Then
                                    palabras = palabras & "cien "
                                Else
                                    palabras = palabras & "ciento "
                                End If
                            Case "2"
                                palabras = palabras & "doscientos "
                            Case "3"
                                palabras = palabras & "trescientos "
                            Case "4"
                                palabras = palabras & "cuatrocientos "
                            Case "5"
                                palabras = palabras & "quinientos "
                            Case "6"
                                palabras = palabras & "seiscientos "
                            Case "7"
                                palabras = palabras & "setecientos "
                            Case "8"
                                palabras = palabras & "ochocientos "
                            Case "9"
                                palabras = palabras & "novecientos "
                        End Select
                    Case 2, 5, 8
                        '*********Asigna las palabras para las decenas************
                        Select Case Mid(entero, num, 1)
                            Case "1"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    flag = "S"
                                    palabras = palabras & "diez "
                                End If
                                If Mid(entero, num + 1, 1) = "1" Then
                                    flag = "S"
                                    palabras = palabras & "once "
                                End If
                                If Mid(entero, num + 1, 1) = "2" Then
                                    flag = "S"
                                    palabras = palabras & "doce "
                                End If
                                If Mid(entero, num + 1, 1) = "3" Then
                                    flag = "S"
                                    palabras = palabras & "trece "
                                End If
                                If Mid(entero, num + 1, 1) = "4" Then
                                    flag = "S"
                                    palabras = palabras & "catorce "
                                End If
                                If Mid(entero, num + 1, 1) = "5" Then
                                    flag = "S"
                                    palabras = palabras & "quince "
                                End If
                                If Mid(entero, num + 1, 1) > "5" Then
                                    flag = "N"
                                    palabras = palabras & "dieci"
                                End If
                            Case "2"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "veinte "
                                    flag = "S"
                                Else
                                    palabras = palabras & "veinti"
                                    flag = "N"
                                End If
                            Case "3"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "treinta "
                                    flag = "S"
                                Else
                                    palabras = palabras & "treinta y "
                                    flag = "N"
                                End If
                            Case "4"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "cuarenta "
                                    flag = "S"
                                Else
                                    palabras = palabras & "cuarenta y "
                                    flag = "N"
                                End If
                            Case "5"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "cincuenta "
                                    flag = "S"
                                Else
                                    palabras = palabras & "cincuenta y "
                                    flag = "N"
                                End If
                            Case "6"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "sesenta "
                                    flag = "S"
                                Else
                                    palabras = palabras & "sesenta y "
                                    flag = "N"
                                End If
                            Case "7"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "setenta "
                                    flag = "S"
                                Else
                                    palabras = palabras & "setenta y "
                                    flag = "N"
                                End If
                            Case "8"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "ochenta "
                                    flag = "S"
                                Else
                                    palabras = palabras & "ochenta y "
                                    flag = "N"
                                End If
                            Case "9"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "noventa "
                                    flag = "S"
                                Else
                                    palabras = palabras & "noventa y "
                                    flag = "N"
                                End If
                        End Select
                    Case 1, 4, 7
                        '*********Asigna las palabras para las unidades*********
                        Select Case Mid(entero, num, 1)
                            Case "1"
                                If flag = "N" Then
                                    If y = 1 Then
                                        palabras = palabras & "uno "
                                    Else
                                        palabras = palabras & "un "
                                    End If
                                End If
                            Case "2"
                                If flag = "N" Then palabras = palabras & "dos "
                            Case "3"
                                If flag = "N" Then palabras = palabras & "tres "
                            Case "4"
                                If flag = "N" Then palabras = palabras & "cuatro "
                            Case "5"
                                If flag = "N" Then palabras = palabras & "cinco "
                            Case "6"
                                If flag = "N" Then palabras = palabras & "seis "
                            Case "7"
                                If flag = "N" Then palabras = palabras & "siete "
                            Case "8"
                                If flag = "N" Then palabras = palabras & "ocho "
                            Case "9"
                                If flag = "N" Then palabras = palabras & "nueve "
                        End Select
                End Select

                '***********Asigna la palabra mil***************
                If y = 4 Then
                    If Mid(entero, 6, 1) <> "0" Or Mid(entero, 5, 1) <> "0" Or Mid(entero, 4, 1) <> "0" Or
                    (Mid(entero, 6, 1) = "0" And Mid(entero, 5, 1) = "0" And Mid(entero, 4, 1) = "0" And
                    Len(entero) <= 6) Then palabras = palabras & "mil "
                End If

                '**********Asigna la palabra millón*************
                If y = 7 Then
                    If Len(entero) = 7 And Mid(entero, 1, 1) = "1" Then
                        palabras = palabras & "millón "
                    Else
                        palabras = palabras & "millones "
                    End If
                End If
            Next y


            '**********Une la parte entera y la parte decimal*************
            If dec <> "" Then

                For y = Len(dec) To 1 Step -1
                    dec1 = Len(dec) - (y - 1)
                    Select Case y
                        Case 2, 5, 8
                            '*********Asigna las palabras para las decenas************
                            Select Case Mid(dec, dec1, 1)
                                Case "1"
                                    If Mid(dec, dec1 + 1, 1) = "0" Then
                                        flag = "S"
                                        palabras = palabras & "con " & "diez "
                                    End If
                                    If Mid(dec, dec1 + 1, 1) = "1" Then
                                        flag = "S"
                                        palabras = palabras & "con " & "once "
                                    End If
                                    If Mid(dec, dec1 + 1, 1) = "2" Then
                                        flag = "S"
                                        palabras = palabras & "con " & "doce "
                                    End If
                                    If Mid(dec, dec1 + 1, 1) = "3" Then
                                        flag = "S"
                                        palabras = palabras & "con " & "trece "
                                    End If
                                    If Mid(dec, dec1 + 1, 1) = "4" Then
                                        flag = "S"
                                        palabras = palabras & "con " & "catorce "
                                    End If
                                    If Mid(dec, dec1 + 1, 1) = "5" Then
                                        flag = "S"
                                        palabras = palabras & "con " & "quince "
                                    End If
                                    If Mid(dec, dec1 + 1, 1) > "5" Then
                                        flag = "N"
                                        palabras = palabras & "con " & "dieci"
                                    End If
                                Case "2"
                                    If Mid(dec, dec1 + 1, 1) = "0" Then
                                        palabras = palabras & "con " & "veinte "
                                        flag = "S"
                                    Else
                                        palabras = palabras & "con " & "veinti"
                                        flag = "N"
                                    End If
                                Case "3"
                                    If Mid(dec, dec1 + 1, 1) = "0" Then
                                        palabras = palabras & "con " & "treinta "
                                        flag = "S"
                                    Else
                                        palabras = palabras & "con " & "treinta y "
                                        flag = "N"
                                    End If
                                Case "4"
                                    If Mid(dec, dec1 + 1, 1) = "0" Then
                                        palabras = palabras & "con " & "cuarenta "
                                        flag = "S"
                                    Else
                                        palabras = palabras & "con " & "cuarenta y "
                                        flag = "N"
                                    End If
                                Case "5"
                                    If Mid(dec, dec1 + 1, 1) = "0" Then
                                        palabras = palabras & "con " & "cincuenta "
                                        flag = "S"
                                    Else
                                        palabras = palabras & "con " & "cincuenta y "
                                        flag = "N"
                                    End If
                                Case "6"
                                    If Mid(dec, dec1 + 1, 1) = "0" Then
                                        palabras = palabras & "con " & "sesenta "
                                        flag = "S"
                                    Else
                                        palabras = palabras & "con " & "sesenta y "
                                        flag = "N"
                                    End If
                                Case "7"
                                    If Mid(dec, dec1 + 1, 1) = "0" Then
                                        palabras = palabras & "con " & "setenta "
                                        flag = "S"
                                    Else
                                        palabras = palabras & "con " & "setenta y "
                                        flag = "N"
                                    End If
                                Case "8"
                                    If Mid(dec, dec1 + 1, 1) = "0" Then
                                        palabras = palabras & "con " & "ochenta "
                                        flag = "S"
                                    Else
                                        palabras = palabras & "con " & "ochenta y "
                                        flag = "N"
                                    End If
                                Case "9"
                                    If Mid(dec, dec1 + 1, 1) = "0" Then
                                        palabras = palabras & "con " & "noventa "
                                        flag = "S"
                                    Else
                                        palabras = palabras & "con " & "noventa y "
                                        flag = "N"
                                    End If
                            End Select
                        Case 1, 4, 7
                            '*********Asigna las palabras para las unidades*********
                            Select Case Mid(dec, dec1, 1)
                                Case "1"
                                    If flag = "N" Then
                                        If y = 1 Then
                                            palabras = palabras & "uno "
                                        Else
                                            palabras = palabras & "un "
                                        End If
                                    End If
                                Case "2"
                                    If flag = "N" Then palabras = palabras & "dos "
                                Case "3"
                                    If flag = "N" Then palabras = palabras & "tres "
                                Case "4"
                                    If flag = "N" Then palabras = palabras & "cuatro "
                                Case "5"
                                    If flag = "N" Then palabras = palabras & "cinco "
                                Case "6"
                                    If flag = "N" Then palabras = palabras & "seis "
                                Case "7"
                                    If flag = "N" Then palabras = palabras & "siete "
                                Case "8"
                                    If flag = "N" Then palabras = palabras & "ocho "
                                Case "9"
                                    If flag = "N" Then palabras = palabras & "nueve "
                            End Select
                    End Select
                Next

                If dec = "00" Then
                    Letras = palabras
                Else
                    Letras = palabras & "centavos"
                End If
            End If

        Else
            Letras = ""
        End If

        Return Letras
    End Function


End Class
