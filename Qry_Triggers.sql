DROP TRIGGER tDeleteEmpleados

GO
CREATE  TRIGGER tDeleteEmpleados ON Puestos
INSTEAD OF DELETE
AS
BEGIN 
	
	declare @ID_Puesto int
			
    SELECT 
	@ID_Puesto = ID_Puesto
	FROM DELETED
	
	UPDATE Empleado
	SET Activo = 0
	WHERE ID_Puesto = @ID_Puesto

	UPDATE Puestos
	SET Activo = 0
	WHERE ID_Puesto = @ID_Puesto

END

-----------------------------------------------------------------------------------------------------------------------
DROP TRIGGER tDeletePuestos

GO
CREATE  TRIGGER tDeletePuestos ON Departamentos
INSTEAD OF DELETE
AS
BEGIN 
	
	declare @No_Departamento int
			
    SELECT 
	@No_Departamento = No_Departamento
	FROM DELETED

	DELETE
	FROM Puestos
	WHERE No_Departamento = @No_Departamento

	UPDATE Departamentos
	SET Activo = 0
	WHERE No_Departamento = @No_Departamento

END

------------------------------------------------------------------------------------------------------------
DROP TRIGGER tDelete_Depas_Puestos

GO
CREATE  TRIGGER tDelete_Depas_Puestos ON Empresas
INSTEAD OF DELETE
AS
BEGIN 
	
	declare @ID_RFC varchar(13)
			
    SELECT 
	@ID_RFC = ID_RFC
	FROM DELETED

	DELETE
	FROM Departamentos
	WHERE ID_RFC = @ID_RFC

	DELETE 
	FROM Empresa_FrecPago 
	WHERE ID_RFC = @ID_RFC

	UPDATE Empresas
	SET Activo = 0
	WHERE ID_RFC = @ID_RFC


END

------------------------------------------------------------------------
