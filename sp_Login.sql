use Proyect_MAD 

GO
IF EXISTS(SELECT 1 FROM sysobjects  WHERE name = 'sp_Login' and type = 'P')
BEGIN
	DROP PROCEDURE sp_Login
END
GO
CREATE PROCEDURE sp_Login(
		@No_Empleado    int = null,
		@Contraseņa     varchar(20) = null
)
AS
BEGIN
		SELECT No_Empleado, 
		       Contraseņa,
			   Gerente_General,
			   Gerente_Empresa		
		FROM Empleado 
		WHERE No_Empleado = @No_Empleado 
		and Contraseņa = @Contraseņa 
END