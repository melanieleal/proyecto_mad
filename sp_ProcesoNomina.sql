use Proyect_MAD 

GO
IF EXISTS( Select 1  FROM sysobjects WHERE name = 'sp_GetSalarioD' AND type = 'P')
BEGIN
	DROP PROCEDURE sp_GetSalarioD
END
GO
CREATE PROCEDURE sp_GetSalarioD(
	@No_Empleado	int = null
)
AS 
BEGIN
	SELECT dbo.fnSalarioD (@No_Empleado)
END

GO
IF EXISTS( Select 1  FROM sysobjects WHERE name = 'sp_ProcesoNomina' AND type = 'P')
BEGIN
	DROP PROCEDURE sp_ProcesoNomina
END
GO
CREATE PROCEDURE sp_ProcesoNomina(
	@Opc			char(1),
	@ID_Nomina		int = null,
	@No_Empleado	int = null,
	@ID_Percepcion	int = null,
	@ID_Deduccion	int = null,
	@Sueldo_Bruto   decimal(10,2) = null, 
	@Sueldo_Neto    decimal(10,2) = null,
	@DiasT          int = null,
	@IMSS           decimal(7,2) = null,
	@ISR			decimal(7,2) = null,
	@fecha_PN       date = null,
	@frec           int = null,
	@SueldoD        decimal(10,2) = null,
	@ID_RFC			varchar(13) = null,
	@ID_Frec_Pago   int = null
)
AS 
BEGIN
		IF @Opc = 'I'
		BEGIN
			
			INSERT INTO Nomina ( No_Empleado, Sueldo_Bruto, Sueldo_Neto,DiasT,IMSS, ISR,fecha_PN, SueldoD)
			VALUES(@No_Empleado, @Sueldo_Bruto,@Sueldo_Neto, @DiasT, @IMSS, @ISR,@fecha_PN, @SueldoD )

		END


		IF @Opc = 'M'
		BEGIN
			
			UPDATE  Nomina 
				SET  No_Empleado = ISNULL(@No_Empleado, No_Empleado),
				     Sueldo_Bruto = ISNULL(@Sueldo_Bruto, Sueldo_Bruto), 
					 Sueldo_Neto = ISNULL(@Sueldo_Neto, Sueldo_Neto),
					 DiasT = ISNULL(@DiasT, DiasT),
					 IMSS = ISNULL(@IMSS, IMSS), 
					 ISR = ISNULL(@ISR, ISR),
					 fecha_PN = ISNULL(@fecha_PN, fecha_PN),
					 SueldoD = ISNULL(@SueldoD, SueldoD)
			WHERE ID_Nomina = @ID_Nomina 

		END

		IF @Opc = 'S'
		BEGIN 
			    SELECT
					   ID_Nomina, 
					   No_Empleado,
					   Sueldo_Bruto,
					   Sueldo_Neto ,
					   DiasT,
					   IMSS,
					   ISR,
					   fecha_PN,
					   SueldoD 
				FROM Nomina
				WHERE  No_Empleado = @No_Empleado 
		END

		IF @Opc = 'N'
		BEGIN 
			    SELECT
					   ID_Nomina, 
					   No_Empleado,
					   Sueldo_Bruto,
					   Sueldo_Neto ,
					   DiasT,
					   IMSS,
					   ISR,
					   fecha_PN,
					   SueldoD 
				FROM Nomina
				WHERE  No_Empleado = @No_Empleado and fecha_PN = @fecha_PN
		END

		IF @Opc = 'X'
		BEGIN 
			    SELECT
					   N.ID_Nomina,  --0
					   N.No_Empleado, --1
					   N.DiasT, --2
					   N.fecha_PN, --3
					   E.ID_RFC, --4
					   N.Sueldo_Bruto, --5
					   N.Sueldo_Neto, --6
					   N.IMSS, --7
					   N.ISR,		--8		
					   CONCAT(UPPER(E.Nombre), ' ' , UPPER(E.Ape_Paterno), ' ' ,UPPER(E.Ape_Materno)), --9
					   E.No_Cuenta_Banco, --10
					   B.Nombre,   --11
					   N.SueldoD ---12
				FROM Nomina N
				INNER JOIN Empleado E
				ON N.No_Empleado = E.No_Empleado 
				INNER JOIN Banco B
				ON E.ID_Banco  = B.ID_Banco  
		END


		IF @Opc = 'D' ----a�adir deduccion a la nomina
		BEGIN

			INSERT INTO Nomina_Deduccion(ID_Nomina, ID_Deduccion, fecha)
			VALUES(@ID_Nomina, @ID_Deduccion, @fecha_PN)

		END

		IF @Opc = 'P' -----A�adir percepcion a la nomina
		BEGIN
			
			INSERT INTO Nomina_Percepcion(ID_Nomina, ID_Percepcion, fecha)
			VALUES(@ID_Nomina, @ID_Percepcion, @fecha_PN)

		END


		IF @Opc = 'Y'---Obtenemos las percepciones de su nomina
		BEGIN 
			    SELECT
					   ID_Nomina, 
					   ID_Percepcion,
					   fecha					    
				FROM Nomina_Percepcion
				WHERE  ID_Nomina = ID_Nomina and fecha BETWEEN DATEADD(DAY, -@frec, GETDATE()) AND GETDATE()
		END

		IF @Opc = 'Z' ----Obtenemos las deducciones de su nomina
		BEGIN 
			    SELECT
					   ID_Nomina, 
					   ID_Deduccion,
					   fecha					   
				FROM Nomina_Deduccion
				WHERE  ID_Nomina = ID_Nomina and fecha BETWEEN DATEADD(DAY, -@frec, GETDATE()) AND GETDATE()
		END


		IF @Opc = 'V'
		BEGIN
			  SELECT
					  [ID_RFC], 
					  [Fecha_PN],
				      [ID_Frec_Pago]				    
				FROM Empresa_Proceso_Nomina
				WHERE  ID_RFC = @ID_RFC and ID_Frec_Pago = @ID_Frec_Pago  
		END
END


GO
IF EXISTS( Select 1  FROM sysobjects WHERE name = 'sp_GetNominaDedu' AND type = 'P')
BEGIN
	DROP PROCEDURE sp_GetNominaDedu
END
GO
CREATE PROCEDURE sp_GetNominaDedu(	
	@ID_Nomina		int = null	
)
AS 
BEGIN
			
			SELECT
					   ID_Nomina, 
					   ID_Deduccion,
					   fecha					   
				FROM Nomina_Deduccion
				WHERE  ID_Nomina = ID_Nomina 

END		

GO
IF EXISTS( Select 1  FROM sysobjects WHERE name = 'sp_GetNominaPer' AND type = 'P')
BEGIN
	DROP PROCEDURE sp_GetNominaPer
END
GO
CREATE PROCEDURE sp_GetNominaPer(	
	@ID_Nomina		int = null	
)
AS 
BEGIN
			
			SELECT
					   ID_Nomina, 
					   ID_Percepcion,
					   fecha					   
				FROM Nomina_Percepcion 
				WHERE  ID_Nomina = ID_Nomina 

END	
