use Proyect_MAD 



GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_Banco' AND type = 'P') 
BEGIN
    DROP PROCEDURE sp_Banco;
END

GO
CREATE PROCEDURE sp_Banco(
	
	@Opc					 char(1),
	@ID_Banco				 int = null, 
	@Nombre			         varchar(30) = null
)
AS
BEGIN

		IF @Opc = 'I'
			BEGIN
			
				INSERT INTO Banco (Nombre)					
				VALUES (@Nombre)
			END

		IF @Opc = 'X'
			BEGIN
			
				SELECT ID_Banco, 
					   Nombre 'Banco'		
					FROM Banco 
				
			END

		IF @Opc = 'S'
			BEGIN
			
				SELECT ID_Banco, 
					   Nombre 'Banco'	
					FROM Banco 
				WHERE ID_Banco = @ID_Banco
				RETURN @ID_Banco
			END
END


------------------------------------------------------------------------------------------------------------------------------

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_Terminacion_Email' AND type = 'P') 
BEGIN
    DROP PROCEDURE sp_Terminacion_Email;
END

GO
CREATE PROCEDURE sp_Terminacion_Email(
	
	@Opc					 char(1),
	@ID_Terminacion_Email    int = null, 
	@Nombre			         varchar(30) = null
)
AS
BEGIN

		IF @Opc = 'I'
			BEGIN
			
				INSERT INTO Terminacion_Email (Nombre)					
				VALUES (@Nombre)
			END

		IF @Opc = 'X'
			BEGIN
			
				SELECT ID_Terminacion_Email, 
					   Nombre	
					FROM Terminacion_Email 
				
			END

		IF @Opc = 'S'
			BEGIN
			
				SELECT ID_Terminacion_Email, 
					   Nombre	
					FROM Terminacion_Email 
				WHERE ID_Terminacion_Email = @ID_Terminacion_Email
				RETURN @ID_Terminacion_Email
			END
END

--------------------------------------------------------------------------------------------------------------------------------------
GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_Empleados' AND type = 'P') 
BEGIN
    DROP PROCEDURE sp_Empleados;
END

GO
CREATE PROCEDURE sp_Empleados(

	@Opc					char(1),
    @No_Empleado			int = null,
	@Nombre					varchar(30) = null, 
	@Ape_Paterno			varchar(30) = null, 
	@Ape_Materno			varchar(30) = null, 
	@Fecha_Nacimiento		date = null,
	@Email					nvarchar(30) = null,
	@ID_Terminacion_Email	int = null, --FK
	@Contraseña				varchar(20) = null,  
	@Fecha_Puesto			date = null,
	@Calle					varchar(30) = null,   
	@No_Casa				int = null,  
	@Colonia				varchar(30) = null,   
	@Estado					varchar(30) = null,   
	@CP						int = null,  
	@Municipio				varchar(30) = null,
	@Telefono				char(10) = null,
	@Fecha_Contrato			date = null,
	@RFC					varchar(13) = null,
	@CURP					varchar(18) = null,
	@NSS					varchar(11) = null,
	@No_Cuenta_Banco		varchar(24) = null,
	@ID_Banco				int = null,  --FK
	@ID_Puesto				int = null,   --FK
	@ID_RFC					varchar(13) = null, --FK
	@ID_Frec_Pago			int = null, --FK
	@No_Departamento		int = null,
	@Gerente_General		bit = null,
	@Gerente_Empresa		bit = null,
	@Gerente_Depto			bit = null)
As
		DECLARE @Activo  Bit
			SET	@Activo = 1

BEGIN

		IF @Opc = 'I'--/////////////////////////////
			BEGIN

					INSERT INTO Empleado (Nombre, Ape_Paterno, Ape_Materno, Fecha_Nacimiento, Email, ID_Terminacion_Email, Contraseña, Fecha_Puesto, Calle, No_Casa, Colonia, Estado, CP, Municipio, Telefono, Fecha_Contrato, RFC, CURP, NSS, No_Cuenta_Banco, ID_Banco, ID_Puesto, ID_RFC, ID_Frec_Pago, No_Departamento, Gerente_General, Gerente_Empresa, Gerente_Depto, Activo)
							VALUES(@Nombre, @Ape_Paterno, @Ape_Materno, @Fecha_Nacimiento, @Email, @ID_Terminacion_Email, @Contraseña, @Fecha_Puesto, @Calle, @No_Casa, @Colonia, @Estado, @CP, @Municipio, @Telefono, @Fecha_Contrato, @RFC, @CURP, @NSS, @No_Cuenta_Banco, @ID_Banco, @ID_Puesto, @ID_RFC, @ID_Frec_Pago, @No_Departamento, @Gerente_General,  @Gerente_Empresa, @Gerente_Depto, @Activo)
							
			END

		
		IF @Opc = 'D' --////////////////////////////////////
	
			BEGIN
		
					DELETE 
					FROM Empleado 
					WHERE No_Empleado = @No_Empleado
					AND Activo = @Activo
		
			END

		IF @Opc = 'M' --////////////////////////////////////////
	
			BEGIN
		
			UPDATE Empleado 
				SET		Nombre = ISNULL(@Nombre, Nombre),
						Ape_Paterno= ISNULL(@Ape_Paterno, Ape_Paterno), 
						Ape_Materno= ISNULL(@Ape_Materno, Ape_Materno), 
						Fecha_Nacimiento= ISNULL(@Fecha_Nacimiento, Fecha_Nacimiento), 
						Email= ISNULL(@Email, Email), 
						ID_Terminacion_Email= ISNULL(@ID_Terminacion_Email, ID_Terminacion_Email), 
						Contraseña = ISNULL(@Contraseña, Contraseña), 
						Fecha_Puesto= ISNULL(@Fecha_Puesto, Fecha_Puesto), 
						Calle= ISNULL(@Calle, Calle), 
						No_Casa= ISNULL(@No_Casa, No_Casa), 
						Colonia= ISNULL(@Colonia, Colonia), 
						Estado= ISNULL(@Estado, Estado), 
						CP= ISNULL(@CP, CP), 
						Municipio= ISNULL(@Municipio, Municipio), 
						Telefono= ISNULL(@Telefono, Telefono), 
						Fecha_Contrato= ISNULL(@Fecha_Contrato, Fecha_Contrato), 
						RFC= ISNULL(@RFC, RFC), 
						CURP= ISNULL(@CURP, CURP), 
						NSS= ISNULL(@NSS, NSS), 
						No_Cuenta_Banco= ISNULL(@No_Cuenta_Banco, No_Cuenta_Banco), 
						ID_Banco= ISNULL(@ID_Banco, ID_Banco), 
						ID_Puesto= ISNULL(@ID_Puesto, ID_Puesto), 
						ID_RFC= ISNULL(@ID_RFC, ID_RFC), 
						ID_Frec_Pago= ISNULL(@ID_Frec_Pago, ID_Frec_Pago),
						No_Departamento = ISNULL(@No_Departamento, No_Departamento),
						Gerente_General= ISNULL(@Gerente_General, Gerente_General), 
						Gerente_Empresa= ISNULL(@Gerente_Empresa, Gerente_Empresa), 
						Gerente_Depto= ISNULL(@Gerente_Depto, Gerente_Depto),
						Activo= ISNULL(@Activo, Activo)

				WHERE No_Empleado = @No_Empleado
					AND Activo = @Activo

			END

		IF  @Opc = 'X' --//////////////////////////////////////
			
			BEGIN
	
				SELECT  E.No_Empleado 'No. Empleado', --0
					    UPPER(E.Nombre) 'Nombre', --1
						UPPER(E.Ape_Paterno) 'Ape. Paterno', --2
						UPPER(E.Ape_Materno) 'Ape. Materno', --3
						E.Fecha_Nacimiento 'Fecha de Nacimiento', --4
						E.Email, --5
						T.Nombre ' ', --6
						E.Contraseña, --7
						E.Fecha_Puesto 'Fecha en que Obtuvo Puesto', --8
						E.Calle, --9
						E.No_Casa 'No. Casa', --10
						E.Colonia, --11
						E.Estado, --12
						E.CP, --13
						E.Municipio, --14
						E.Telefono, --15
						E.Fecha_Contrato 'Fecha de Contrato', --16
						E.RFC, --17
						E.CURP, --18
						E.NSS, --19
                        E.ID_Banco 'Clave Banco', --20
						B.Nombre 'Banco', --21
						E.No_Cuenta_Banco 'No. de Cuenta', 	--22
						E.ID_Puesto 'Clave Puesto', --23				
						P.Nombre 'Puesto', --24
						E.ID_RFC 'RFC Empresa', --25
						W.Razon_Social 'Empresa',--26
						E.ID_Frec_Pago 'Clave de frecuencia de pago', --27
						F.Nombre 'Frecuencia de pago', --28						 				
						P.No_Departamento, --29
						D.Nombre 'Departamento', --30
						E.Gerente_General, --31
						E.Gerente_Empresa, --32
						E.Gerente_Depto, --33
						E.Activo
				FROM Empleado E
					 INNER JOIN Terminacion_Email T
					 ON E.ID_Terminacion_Email = T.ID_Terminacion_Email
					 INNER JOIN Banco B
					 ON E.ID_Banco = B.ID_Banco 
					 INNER JOIN Puestos P
					 ON E.ID_Puesto = P.ID_Puesto
					  INNER JOIN Empresas  W
					 ON E.ID_RFC  = W.ID_RFC
					  INNER JOIN Departamentos  D
					 ON E.No_Departamento = D.No_Departamento
					 INNER JOIN Frec_Pago  F
					 ON E.ID_Frec_Pago = F.ID_Frec_Pago
				WHERE E.Activo = @Activo

			END

			IF  @Opc = 'Y' --//////////////////////////////////////
			
			BEGIN
	
				SELECT 
					    No_Empleado,
						(Nombre + ' ' +
						Ape_Paterno + ' ' + 
						Ape_Materno) 'Nombre'
				FROM Empleado 
					WHERE Activo = @Activo
			END

			IF  @Opc = 'A' --////////////////////////////////////// OBTENER LA FECHA
			
			BEGIN
	
				SELECT 
					    No_Empleado,
						Fecha_Contrato
				FROM Empleado 
					WHERE No_Empleado = @No_Empleado
			END

END
-----------------------------------------------------------------------------------------
GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_GetEmpleadoEmpresa' AND type = 'P') 
BEGIN
    DROP PROCEDURE sp_GetEmpleadoEmpresa;
END

GO
CREATE PROCEDURE sp_GetEmpleadoEmpresa(
	@ID_RFC			varchar(13) = null
)
AS
BEGIN	


				SELECT  No_Empleado 'No. Empleado', --0
					    CONCAT (UPPER(Nombre), ' ', 
						UPPER(Ape_Paterno), ' ', 
						UPPER(Ape_Materno)) 'Nombre',--1
						Fecha_Nacimiento 'Fecha de Nacimiento',--2
						Contraseña, --3
						Fecha_Puesto 'Fecha en que Obtuvo Puesto', --4
						Calle, --5
						No_Casa 'No. Casa', --6
						Colonia, --7
						Estado, --8
						CP, --9
						Municipio, --10
						Telefono, --11
						Fecha_Contrato 'Fecha de Contrato', --12 
						RFC, --13
						CURP, --14
						NSS, --15
                        ID_Banco 'Clave Banco', --16
						No_Cuenta_Banco 'No. de Cuenta', --17 	
						ID_Puesto 'Clave Puesto', 			--18	
						ID_RFC 'RFC Empresa', --19
						ID_Frec_Pago 'Tipo de frecuencia de pago', --20
						No_Departamento 'No. Departamento',				--21		
						Gerente_General,  --22
						Gerente_Empresa, --23
						Gerente_Depto,--24
						Activo				--25  
																
				FROM Empleado 
				WHERE ID_RFC = @ID_RFC 
				AND Activo = 1
					
END
------------------------------------------------------------------------------------------------------

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_GetEmpleEmpF' AND type = 'P') 
BEGIN
    DROP PROCEDURE sp_GetEmpleEmpF;
END

GO
CREATE PROCEDURE sp_GetEmpleEmpF(
	@ID_RFC			varchar(13) = null,
	@ID_Frec_Pago   int = null
)
AS
BEGIN	


				SELECT  No_Empleado 'No. Empleado', --0
					    CONCAT (UPPER(Nombre), ' ', 
						UPPER(Ape_Paterno), ' ', 
						UPPER(Ape_Materno)) 'Nombre',--1
						Fecha_Nacimiento 'Fecha de Nacimiento',--2
						Contraseña, --3
						Fecha_Puesto 'Fecha en que Obtuvo Puesto', --4
						Calle, --5
						No_Casa 'No. Casa', --6
						Colonia, --7
						Estado, --8
						CP, --9
						Municipio, --10
						Telefono, --11
						Fecha_Contrato 'Fecha de Contrato', --12
						RFC, --13
						CURP, --14
						NSS, --15
                        ID_Banco 'Clave Banco', --16
						No_Cuenta_Banco 'No. de Cuenta', --17 	
						ID_Puesto 'Clave Puesto', 			--18	
						ID_RFC 'RFC Empresa', --19
						ID_Frec_Pago 'Tipo de frecuencia de pago', --20
						No_Departamento 'No. Departamento',				--21		
						Gerente_General,  --22
						Gerente_Empresa, --23
						Gerente_Depto,--24
						Activo				--25  
																
				FROM Empleado 
				WHERE ID_RFC = @ID_RFC and ID_Frec_Pago = @ID_Frec_Pago 
				AND Activo = 1
					
END
------------------------------------------------------------------------------------------------------

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_GetValidacionEmpleado' AND type = 'P') 
BEGIN
    DROP PROCEDURE sp_GetValidacionEmpleado;
END

GO
CREATE PROCEDURE sp_GetValidacionEmpleado(
	@Opc					char(1),
	@CURP                 varchar(18) = null,
	@RFC                  varchar(13) = null,
	@NSS                  varchar(11) = null,
	@No_Cuenta_Banco      varchar(24)= null,
	@Email                nvarchar(30) = null,
	@No_Empleado		  int = null,
	@ID_RFC			      varchar(13) = null,
	@No_Departamento		int = null
)
AS
BEGIN	

			IF  @Opc = 'C' --//////////////////////////////////////
			
			BEGIN
				SELECT  
				CURP  												
				FROM Empleado 
				WHERE CURP = @CURP 
			END

			
			IF  @Opc = 'R' --//////////////////////////////////////
			
			BEGIN
				SELECT  
				RFC  												
				FROM Empleado 
				WHERE RFC = @RFC 
			END
			
			IF  @Opc = 'N' --//////////////////////////////////////
			
			BEGIN
				SELECT  
				NSS  												
				FROM Empleado 
				WHERE NSS = @NSS 
			END
			
			IF  @Opc = 'B' --//////////////////////////////////////
			
			BEGIN
				SELECT  
				No_Cuenta_Banco  												
				FROM Empleado 
				WHERE No_Cuenta_Banco = @No_Cuenta_Banco 
			END

			IF  @Opc = 'E' --//////////////////////////////////////
			
			BEGIN
				SELECT  
				Email  												
				FROM Empleado 
				WHERE Email = @Email 
			END

			IF  @Opc = 'G' --////////////////////////////////////// AQUI BUSCO QUIEN ES EL GERENTE GENERAL
			
			BEGIN
				SELECT  
				No_Empleado,
				Gerente_General  												
				FROM Empleado 
				WHERE Gerente_General = 1 AND No_Empleado <> @No_Empleado
			END

			IF  @Opc = 'M' --////////////////////////////////////// AQUI BUSCO QUIEN ES EL GERENTE DE EMPRESA
			
			BEGIN
				SELECT  
				No_Empleado,
				Gerente_Empresa  												
				FROM Empleado 
				WHERE Gerente_Empresa = 1 AND ID_RFC = @ID_RFC AND No_Empleado <> @No_Empleado
			END

			IF  @Opc = 'D' --////////////////////////////////////// AQUI BUSCO QUIEN ES EL GERENTE DE DEPARTAMENTO
			
			BEGIN
				SELECT  
				No_Empleado,
				Gerente_Depto 												
				FROM Empleado 
				WHERE Gerente_Depto = 1 AND No_Departamento = @No_Departamento AND No_Empleado <> @No_Empleado
			END

			IF  @Opc = 'J' --////////////////////////////////////// AQUI BUSCO QUIEN ES EL GERENTE GENERAL CUANDO APENAS INSERTA
			
			BEGIN
				SELECT  
				No_Empleado,
				Gerente_General  												
				FROM Empleado 
				WHERE Gerente_General = 1 AND No_Empleado <> IDENT_CURRENT('Empleado')
			END

			IF  @Opc = 'K' --////////////////////////////////////// AQUI BUSCO QUIEN ES EL GERENTE DE EMPRESA CUANDO APENAS INSERTA
			
			BEGIN
				SELECT  
				No_Empleado,
				Gerente_Empresa  												
				FROM Empleado 
				WHERE Gerente_Empresa = 1 AND ID_RFC = @ID_RFC AND No_Empleado <> IDENT_CURRENT('Empleado')
			END

			IF  @Opc = 'L' --////////////////////////////////////// AQUI BUSCO QUIEN ES EL GERENTE DE DEPARTAMENTO CUANDO APENAS INSERTA
			
			BEGIN
				SELECT  
				No_Empleado,
				Gerente_Depto,
				No_Departamento 												
				FROM Empleado 
				WHERE Gerente_Depto = 1 AND No_Departamento = 100 AND No_Empleado <> IDENT_CURRENT('Empleado')
			END

			IF  @Opc = 'W' --////////////////////////////////////// BUSCAR EL EMPLEADO RECIEN REGISTRADO
			
			BEGIN
				SELECT  
				No_Empleado												
				FROM Empleado 
				WHERE No_Empleado = IDENT_CURRENT('Empleado')
			END
END

------------------------------------------------------------------------------------------------------

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_GetValidacionEmpleadoModificar' AND type = 'P') 
BEGIN
    DROP PROCEDURE sp_GetValidacionEmpleadoModificar;
END

GO
CREATE PROCEDURE sp_GetValidacionEmpleadoModificar(
	@Opc					char(1),
	@No_Empleado		  int = null,
	@CURP                 varchar(18) = null,
	@RFC                  varchar(13) = null,
	@NSS                  varchar(11) = null,
	@No_Cuenta_Banco      varchar(24)= null,
	@Email                nvarchar(30) = null
)
AS
BEGIN	

			IF  @Opc = 'C' --//////////////////////////////////////
			
			BEGIN
				SELECT  
				CURP  												
				FROM Empleado 
				WHERE CURP = @CURP AND No_Empleado <> @No_Empleado
			END

			
			IF  @Opc = 'R' --//////////////////////////////////////
			
			BEGIN
				SELECT  
				RFC  												
				FROM Empleado 
				WHERE RFC = @RFC AND No_Empleado <> @No_Empleado
			END
			
			IF  @Opc = 'N' --//////////////////////////////////////
			
			BEGIN
				SELECT  
				NSS  												
				FROM Empleado 
				WHERE NSS = @NSS AND No_Empleado <> @No_Empleado
			END
			
			IF  @Opc = 'B' --//////////////////////////////////////
			
			BEGIN
				SELECT  
				No_Cuenta_Banco  												
				FROM Empleado 
				WHERE No_Cuenta_Banco = @No_Cuenta_Banco AND No_Empleado <> @No_Empleado
			END

			IF  @Opc = 'E' --//////////////////////////////////////
			
			BEGIN
				SELECT  
				Email  												
				FROM Empleado 
				WHERE Email = @Email AND No_Empleado <> @No_Empleado
			END
END

------------------------------------------------------------------------------------------------------

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_GetValidacionGerentes' AND type = 'P') 
BEGIN
    DROP PROCEDURE sp_GetValidacionGerentes;
END

GO
CREATE PROCEDURE sp_GetValidacionGerentes(
	@Opc					char(1),
	@No_Empleado		  int = null
)
AS
BEGIN	


			IF  @Opc = 'G' --//////////////////////////////////////
			
			BEGIN
				UPDATE Empleado
				SET Gerente_General = 0												
				FROM Empleado 
				WHERE No_Empleado = @No_Empleado
				AND Activo = 1
			END

			IF  @Opc = 'E' --//////////////////////////////////////
			
			BEGIN
				UPDATE Empleado
				SET Gerente_Empresa = 0												
				FROM Empleado 
				WHERE No_Empleado = @No_Empleado
				AND Activo = 1
			END

			IF  @Opc = 'D' --//////////////////////////////////////
			
			BEGIN
				UPDATE Empleado
				SET Gerente_Depto = 0												
				FROM Empleado 
				WHERE No_Empleado = @No_Empleado
				AND Activo = 1
			END

			
END

------------------------------------------------------------------------------------------------------

GO
IF EXISTS(SELECT 1 FROM sysobjects WHERE name = 'sp_GetGerentes' AND type = 'P') 
BEGIN
    DROP PROCEDURE sp_GetGerentes;
END

GO
CREATE PROCEDURE sp_GetGerentes(
	@Opc					char(1),
	@No_Empleado		  int = null,
	@ID_RFC				  varchar(13) = null,
	@No_Departamento      int = null
) 
AS
BEGIN	

			IF  @Opc = 'G' --//////////////////////////////////////
			
			BEGIN
				SELECT  
				No_Empleado,
				CONCAT(Nombre, ' ', Ape_Paterno , ' ', Ape_Materno)	'Nombre'											
				FROM Empleado 
				WHERE Gerente_General = 1
				AND Activo = 1
			END

			IF  @Opc = 'E' --//////////////////////////////////////
			
			BEGIN
				SELECT  
				No_Empleado,
				CONCAT(Nombre, ' ', Ape_Paterno , ' ', Ape_Materno)	'Nombre'											
				FROM Empleado 
				WHERE Gerente_Empresa = 1 AND ID_RFC = @ID_RFC
				AND Activo = 1
			END

			IF  @Opc = 'D' --//////////////////////////////////////
			
			BEGIN
				SELECT  
				No_Empleado,
				CONCAT(Nombre, ' ', Ape_Paterno , ' ', Ape_Materno)	'Nombre'											
				FROM Empleado 
				WHERE Gerente_Depto = 1 AND No_Departamento = @No_Departamento
				AND Activo = 1
			END

			

END